package com.eventspace.exception;

public class BusinessLogicFailureException extends EventspaceException {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -4269713953677521998L;

    /**
     * Instantiates a new lead m business logic failure exception.
     *
     * @param exceptionMsgVal the exception msg val
     * @param e               the e
     */
    public BusinessLogicFailureException(final String exceptionMsgVal, final Throwable e) {
        super(exceptionMsgVal, e);
    }

    /**
     * Instantiates a new lead m business logic failure exception.
     *
     * @param exceptionMsgVal the exception msg val
     */
    public BusinessLogicFailureException(final String exceptionMsgVal) {
        super(exceptionMsgVal);
    }

}
