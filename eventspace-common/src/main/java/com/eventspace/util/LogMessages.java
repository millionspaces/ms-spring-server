package com.eventspace.util;

public interface LogMessages {

    String MC_ALL_STAT_DATE= "MOBILE controller getAllStaticData v1";

    String MC_SYNC_DATA="MOBILE controller syncDatas v1";

    String MC_PAGINATED_ADVANCE_SEARACH="MOBILE controller paginatedAdvanceSearch page : %s";

    String MC_SPACE_GET="MOBILE controller getSpace";

    String MC_PROMO_CHECK="MOBILE controller checkPromo promo : %s , space : %s ";

    String MC_SPACE_GET_CALENDAR="MOBILE controller getSpaceCalendarDetails space : %s";

    String MC_USER_UPDATE_MOBILE="MOBILE controller updateUserMobile user : %s";

    String BC_BOOK_SPACE="BOOKING Controller bookSpace space : %s , user : %s";

    String BC_BOOK_SPACE_WRONG_PROMO="BOOKING Controller bookSpace promo code not valid";

    String BC_BOOK_SPACE_TIME_UNAVAILABLE="BOOKING Controller bookSpace space unavailable";

    String BC_BOOK_SPACE_BOOKING_FAILED="BOOKING Controller bookSpace booking failed";

    String BC_BOOK_SPACE_BOOKING_SUCCESS="BOOKING Controller bookSpace booking : %s success";

    String BC_REVIEW_ADD="BOOKING Controller addReview orderID : %s";

    String BC_REMARK_ADD="BOOKING Controller addRemark orderID : %s";

    String BC_BOOKING_ACTION="BOOKING Controller bookingActions orderID : %s , event : %s , user : %s";

    String BC_CHECK_BOOKING_CALCEL_AFTER_PAID="BOOKING Controller isCanceledAfterPaid orderID : %s";

    String BC_RESERVATION_STATUS_GET_ALL="BOOKING Controller getReservationStatuses";

    String BC_NOTIFICATION_MARK_AS_SEEN="BOOKING Controller markAsSeenNotification orderID : %s , reservation status : %s";

    String BC_BOOKING_MANUAL_GET="BOOKING Controller getManualBooks ID : %s ";

    String BC_BOOKING_MANUAL_ADD="BOOKING Controller addManualBooks space : %s";

    String BC_BOOKING_MANUAL_UPDATE="BOOKING Controller updateManualBooks ID : %s ";

    String BC_BOOKING_MANUAL_DELETE="BOOKING Controller updateManualBooks ID : %s ";

    String BC_BOOKING_CALCULATE_CHARGE="BOOKING Controller findBookingCharge space : %s ";

    String BC_BOOKING_CALCULATE_REFUND="BOOKING Controller findRefund orderID : %s ";

    String BC_BOOKING_GET_PAY_LATER_DETAILS="BOOKING Controller payLaterDetails orderID : %s ";

    String BS_REVIEW_ADD="BOOKING service addReview orderID : %s , user : %s ";

    String BS_REVIEW_SUCCESS="BOOKING service addReview success orderID : %s , user : %s ";

    String BS_REVIEW_FAILED="BOOKING service addReview failed orderID : %s , user : %s ";

    String BS_REVIEW_BY_EMAIL_ADD="BOOKING service addReview orderID : %s  ";

    String BS_REVIEW_BY_EMAIL_SUCCESS="BOOKING service addReview success orderID : %s ";

    String BS_REVIEW_BY_EMAIL_FAILED="BOOKING service addReview failed orderID : %s ";

    String BS_BOOK_SPACE="BOOKING service bookSpace  space : %s , user : %s";

    String BS_BOOKING_CHARGE_CALCULATE="BOOKING service bookSpace  FE charge : %s , BE charge : %s";

    String BS_BOOKING_REDUCE_DISCOUNT="BOOKING service bookSpace  discount : %s ";

    String BS_BOOKING_ADD_ADVANCE="BOOKING service bookSpace  advance : %s ";

    String BS_BOOKING_ADD_PROMO="BOOKING service bookSpace  promo : %s ";

    String BS_BOOKING_ADMIN_PROMO="BOOKING service bookSpace using admin promo PAY_AT_COURT orderID : %s";

    String BS_BOOKING_NON_BLOCK_PROMO="BOOKING service bookSpace promo not block time slot orderID : %s";

    String BS_BOOKING_ADD_EXTRA_AMENITY="BOOKING service bookSpaceWithExtraAmenity  orderID : %s ";

    String BS_BOOKING_ACTION="BOOKING service bookingActions  orderID : %s ";

    String BS_BOOKING_ACTION_DISCARD="BOOKING service bookingActions discard  orderID : %s ";

    String BS_BOOKING_ACTION_EXPIRE="BOOKING service bookingActions expire  orderID : %s ";

    String BS_BOOKING_ACTION_PAY_IPG="BOOKING service bookingActions IPG pay  orderID : %s ";

    String BS_BOOKING_ACTION_PAY_IPG_SUCCESS="BOOKING service bookingActions IPG pay success orderID : %s ";

    String BS_BOOKING_ACTION_PAY_IPG_ADVANCE_SUCCESS="BOOKING service bookingActions IPG pay advance success orderID : %s ";

    String BS_BOOKING_ACTION_PAY_IPG_FAILED="BOOKING service bookingActions IPG pay failed orderID : %s ";

    String BS_BOOKING_ACTION_PAY_MANUAL="BOOKING service bookingActions MANUAL pay orderID : %s ";

    String BS_BOOKING_ACTION_MANUAL_PENDING="BOOKING service bookingActions MANUAL pending orderID : %s ";

    String BS_BOOKING_ACTION_MANUAL_PENDING_ADVANCE="BOOKING service bookingActions MANUAL pending advance orderID : %s ";

    String BS_BOOKING_ACTION_PAY_BANK="BOOKING service bookingActions BANK pay orderID : %s ";

    String BS_BOOKING_ACTION_PAY_PROMOTION="BOOKING service bookingActions PROMOTION pay orderID : %s ";

    String BS_BOOKING_ACTION_CORPORATE_PROMOTION="BOOKING service bookingActions Corporate PROMO orderID : %s ";

    String BS_BOOKING_ACTION_PAY_SPACE_PROMOTION="BOOKING service bookingActions SPACE PROMOTION pay orderID : %s ";

    String BS_BOOKING_ACTION_PAY_LATER="BOOKING service bookingActions pay later orderID : %s ";

    String BS_BOOKING_ACTION_MANUAL_PAID="BOOKING service bookingActions manual paid orderID : %s ";

    String BS_BOOKING_ACTION_MANUAL_ADVANCE_PAID="BOOKING service bookingActions manual advance paid orderID : %s ";

    String BS_BOOKING_ACTION_MANUAL_UNDO_PAY="BOOKING service bookingActions manual undo payment orderID : %s ";

    String BS_BOOKING_ACTION_CANCEL="BOOKING service bookingActions cancel orderID : %s , FE charge : %s , BE charge : %s";

    String BS_STATE_MACHINE_GET="BOOKING service getStateMachine ";

    String BS_STATE_CHANGED="state change {} to {}";

    String BS_IS_CANCEL_AFTER_PAID="BOOKING service isCancelledAfterPaid orderID : %s ";

    String BS_RESERVATION_GET_ALL="BOOKING service getAllreservationStatus";

    String BS_MARK_AS_SEEN_NOTIFICATION="BOOKING service markAsSeenNotification orderID : %s , reservation status : %s";

    String BS_MANUAL_BOOK_ADD="BOOKING service addHostManualBookingDetails add manual booking space : %s";

    String BS_MANUAL_BOOK_UPDATE="BOOKING service editHostManualBookingDetails update manual booking id : %s";

    String BS_MANUAL_BOOK_GET="BOOKING service addHostManualBookingDetails get manual booking id: %s";

    String BS_MANUAL_BOOK_DELETE="BOOKING service addHostManualBookingDetails delete manual booking id: %s";

    String BS_BOOKING_EXPIRE="BOOKING service expireBooking expire booking orderID: %s";

    String BS_BOOKING_EXPIRED="BOOKING service expireBooking expired booking orderID: %s";

    String BS_PAYMENT_CALLBACK="BOOKING service paymentCallback payment callback orderID: %s";

    String BS_PAYMENT_CALLBACK_URL="BOOKING service paymentCallback payment callback URL : %s";



    String PS_FRIMI_RESPONSE="PAYMENT service completeFrimi FRIMI response : %s";

    String PS_UPAY_RESPONSE="PAYMENT service completeUpay UPAY response : %s";

    String PS_PAYCORP_RESPONSE="PAYMENT service completePaycorp PAYCORP response : %s";

    String PS_FRIMI_BOOKING="PAYMENT service completeFrimi FRIMI orderID : %s";

    String PS_UPAY_BOOKING="PAYMENT service completeUpay UPAY orderID : %s";

    String PS_PAYCORP_BOOKING="PAYMENT service completePaycorp PAYCORP orderID : %s";

    String PS_FRIMI_REDIRECT_URL="PAYMENT service completeFrimi FRIMI url : %s";

    String PS_UPAY_REDIRECT_URL="PAYMENT service completeUpay UPAY url : %s";

    String PS_PAYCORP_REDIRECT_URL="PAYMENT service completePaycorp PAYCORP url : %s";

}
