/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.util;

/**
 * The Class StringUtils.
 */
public class StringUtils {

    /**
     * The rgx.
     */
    private static final String RGX = "[\\r\\n]|&#xd;";

    /**
     * Instantiates a new string utils.
     */
    private StringUtils() {

    }

    /**
     * Checks if is empty.
     *
     * @param str the str
     * @return true, if is empty
     */
    public static boolean isEmpty(final String str) {
        return str == null|| str.trim().isEmpty() || str.trim().length() == 0;
    }

    /**
     * Removes the carriage return and line feed.
     *
     * @param input the input
     * @return the string
     */
    public static String removeCarriageReturnAndLineFeed(final String input) {
        if (isEmpty(input)) {
            return input;
        }
        return input.replaceAll(RGX, "");
    }

}
