/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.util;

/**
 * The Interface Message.
 */
public interface Messages {

    /**
     * The Error Message 0001.
     */
    String ERROR_MESSAGE_0001 = "Error Message [%s]";
    String ERROR_MESSAGE_SYNC_01 = "Requested database name [%s] is invalid or not implemented yet";
    String ERROR_MESSAGE_SYNC_02 = "Exception occurred when syncing the database :[%s]";
}

