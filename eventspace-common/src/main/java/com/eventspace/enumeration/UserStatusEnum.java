/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.enumeration;

import java.util.HashMap;

public enum UserStatusEnum {

    ACTIVE(1), NOT_ACTIVE(0);

    private static final HashMap<Integer, UserStatusEnum> typeValueMap = new HashMap<>();

    static {
        for (UserStatusEnum type : UserStatusEnum.values()) {
            typeValueMap.put(type.value, type);
        }
    }

    private int value;

    UserStatusEnum(final int value) {
        this.value = value;
    }

    public static UserStatusEnum getInstanceFromValue(final int codeValue) {
        return typeValueMap.get(codeValue);
    }

    public int value() {
        return value;
    }
}
