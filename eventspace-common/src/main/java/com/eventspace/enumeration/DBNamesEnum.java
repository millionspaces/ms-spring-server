package com.eventspace.enumeration;

import java.util.HashMap;

public enum DBNamesEnum {

    ASSERT("asset"),
    ASSERT_DEFECT("asset_defect"),;

    private static final HashMap<String, DBNamesEnum> typeValueMap = new HashMap<>();

    static {
        for (DBNamesEnum type : DBNamesEnum.values()) {
            typeValueMap.put(type.value, type);
        }
    }

    private String value;

    DBNamesEnum(final String value) {
        this.value = value;
    }

    public static DBNamesEnum getInstanceFromValue(final String database) {
        return typeValueMap.get(database);
    }

    public String value() {
        return value;
    }
}
