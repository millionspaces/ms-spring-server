package com.eventspace.enumeration;

import java.util.HashMap;

public enum ChargeTypeEnum {
    HOUR_BASE(1),BLOCK_BASE(2);

    private static final HashMap<Integer, ChargeTypeEnum> typeValueMap = new HashMap<>();

    static {
        for (ChargeTypeEnum type : ChargeTypeEnum.values()) {
            typeValueMap.put(type.value, type);
        }
    }

    private int value;

    ChargeTypeEnum(final int value) {
        this.value = value;
    }

    public static ChargeTypeEnum getInstanceFromValue(final int codeValue) {
        return typeValueMap.get(codeValue);
    }

    public int value() {
        return value;
    }
}
