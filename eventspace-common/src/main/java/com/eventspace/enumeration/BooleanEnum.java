package com.eventspace.enumeration;

import java.util.HashMap;

public enum BooleanEnum {
    FALSE(0),TRUE(1);

    private static final HashMap<Integer, BooleanEnum> typeValueMap = new HashMap<>();

    static {
        for (BooleanEnum type : BooleanEnum.values()) {
            typeValueMap.put(type.value, type);
        }
    }

    private int value;

    BooleanEnum(final int value) {
        this.value = value;
    }

    public static BooleanEnum getInstanceFromValue(final int codeValue) {
        return typeValueMap.get(codeValue);
    }

    public int value() {
        return value;
    }
}
