package com.eventspace.enumeration;

import java.util.HashMap;

/**
 * Created by Aux-052 on 1/6/2017.
 */
public enum StatesEnum {
    INITIATED(1), PENDING_PAYMENT(2), PAYMENT_DONE(3), CANCELLED(4), CONFIRMED(5), EXPIRED(6),DISCARDED(7),ADVANCE_PAYMENT_PENDING(8),ADVANCE_PAYMENT_DONE(9);

    private static final HashMap<Integer, StatesEnum> typeValueMap = new HashMap<>();

    static {
        for (StatesEnum type : StatesEnum.values()) {
            typeValueMap.put(type.value, type);
        }
    }

    private int value;

    StatesEnum(final int value) {
        this.value = value;
    }

    public static StatesEnum getInstanceFromValue(final int codeValue) {
        return typeValueMap.get(codeValue);
    }

    public int value() {
        return value;
    }
}
