package com.eventspace.enumeration;

import java.util.HashMap;

public enum BlockChargeTypeEnum {
    SPACE_ONLY_CHARGE(1),PER_QUEST_BASED_CHARGE(2);

    private static final HashMap<Integer, BlockChargeTypeEnum> typeValueMap = new HashMap<>();

    static {
        for (BlockChargeTypeEnum type : BlockChargeTypeEnum.values()) {
            typeValueMap.put(type.value, type);
        }
    }

    private int value;

    BlockChargeTypeEnum(final int value) {
        this.value = value;
    }

    public static BlockChargeTypeEnum getInstanceFromValue(final int codeValue) {
        return typeValueMap.get(codeValue);
    }

    public int value() {
        return value;
    }
}
