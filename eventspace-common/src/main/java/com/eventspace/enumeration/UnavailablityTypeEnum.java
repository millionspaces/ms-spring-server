package com.eventspace.enumeration;

import java.util.HashMap;

public enum UnavailablityTypeEnum {
    GUEST_BOOKING(0),MANUAL_BOOKING(1),CHILD_BLOCK_FOR_BOOKING(3);

    private static final HashMap<Integer, UnavailablityTypeEnum> typeValueMap = new HashMap<>();

    static {
        for (UnavailablityTypeEnum type : UnavailablityTypeEnum.values()) {
            typeValueMap.put(type.value, type);
        }
    }

    private int value;

    UnavailablityTypeEnum(final int value) {
        this.value = value;
    }

    public static UnavailablityTypeEnum getInstanceFromValue(final int codeValue) {
        return typeValueMap.get(codeValue);
    }

    public int value() {
        return value;
    }
}
