package com.eventspace.enumeration;

import java.util.HashMap;

public enum PaySubEventEnum {
    BANK_PAY(1),PAY(2),UNDO_PAY(3),PROMOTION(4),PAY_LATER(5),SPACE_PROMOTION(6);

    private static final HashMap<Integer, PaySubEventEnum> typeValueMap = new HashMap<>();

    static {
        for (PaySubEventEnum type : PaySubEventEnum.values()) {
            typeValueMap.put(type.value, type);
        }
    }

    private int value;

    PaySubEventEnum(final int value) {
        this.value = value;
    }

    public static PaySubEventEnum getInstanceFromValue(final int codeValue) {
        return typeValueMap.get(codeValue);
    }

    public int value() {
        return value;
    }
}
