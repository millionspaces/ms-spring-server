package com.eventspace.enumeration;

import java.util.HashMap;

/**
 * Created by Auxenta on 6/30/2017.
 */
public enum AvailabilityMethods {
    HOUR_BASE(1), BLOCK_BASE(2);

    private static final HashMap<Integer, AvailabilityMethods> typeValueMap = new HashMap<>();

    static {
        for (AvailabilityMethods type : AvailabilityMethods.values()) {
            typeValueMap.put(type.value, type);
        }
    }

    private int value;

    AvailabilityMethods(final int value) {
        this.value = value;
    }

    public static AvailabilityMethods getInstanceFromValue(final int codeValue) {
        return typeValueMap.get(codeValue);
    }

    public int value() {
        return value;
    }
}
