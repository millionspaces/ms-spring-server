package com.eventspace.enumeration;

import java.util.HashMap;

public enum AmenityUnitEnum {

    PER_HOUR(1),PER_PERSON(2),PER_UNIT(3);

    private static final HashMap<Integer, AmenityUnitEnum> typeValueMap = new HashMap<>();

    static {
        for (AmenityUnitEnum type : AmenityUnitEnum.values()) {
            typeValueMap.put(type.value, type);
        }
    }

    private int value;

    AmenityUnitEnum(final int value) {
        this.value = value;
    }

    public static AmenityUnitEnum getInstanceFromValue(final int codeValue) {
        return typeValueMap.get(codeValue);
    }

    public int value() {
        return value;
    }
}
