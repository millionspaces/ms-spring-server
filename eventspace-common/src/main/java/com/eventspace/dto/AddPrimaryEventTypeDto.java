package com.eventspace.dto;

import java.io.Serializable;

public class AddPrimaryEventTypeDto implements Serializable {
    private static final long serialVersionUID = 7797455140996929742L;

    private Integer spaceId;
    private Integer eventTypeId;

    public Integer getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(Integer spaceId) {
        this.spaceId = spaceId;
    }

    public Integer getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(Integer eventTypeId) {
        this.eventTypeId = eventTypeId;
    }
}
