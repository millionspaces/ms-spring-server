package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HostChangeDto  implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    private Integer space;
    private UserDetailsDto user;
    private Date calendarEnd;

    public Integer getSpace() {
        return space;
    }

    public void setSpace(Integer space) {
        this.space = space;
    }

    public UserDetailsDto getUser() {
        return user;
    }

    public void setUser(UserDetailsDto user) {
        this.user = user;
    }

    public Date getCalendarEnd() {
        return calendarEnd;
    }

    public void setCalendarEnd(Date calendarEnd) {
        this.calendarEnd = calendarEnd;
    }
}
