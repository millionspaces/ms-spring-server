package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by Auxenta on 6/6/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LocationSearchDto implements Serializable {

    private static final long serialVersionUID = 7797455140996929742L;

    private String latitude;

    private String longitude;

    private Integer radius;

    private String address;

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LocationSearchDto)) return false;

        LocationSearchDto that = (LocationSearchDto) o;

        if (!getLatitude().equals(that.getLatitude())) return false;
        if (!getLongitude().equals(that.getLongitude())) return false;
        if (getRadius() != null ? !getRadius().equals(that.getRadius()) : that.getRadius() != null) return false;
        return getAddress() != null ? getAddress().equals(that.getAddress()) : that.getAddress() == null;
    }

    @Override
    public int hashCode() {
        int result = getLatitude() != null ? getLatitude().hashCode() : 0;
        result = 31 * result + (getLongitude() != null ? getLongitude().hashCode() : 0);
        result = 31 * result + (getRadius() != null ? getRadius().hashCode() : 0);
        result = 31 * result + (getAddress() != null ? getAddress().hashCode() : 0);
        return result;
    }
}
