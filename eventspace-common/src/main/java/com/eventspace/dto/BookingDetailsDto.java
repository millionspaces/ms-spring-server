package com.eventspace.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Created by Aux-054 on 12/15/2016.
 */

public class BookingDetailsDto implements Serializable {

    private Integer id;

    private String orderId;

    private boolean isPayLater = false;

    private transient Map<String, Object> user;

    private Integer eventType;

    private Integer guestCount;

    private String bookedDate;

    private Double refund;

    private transient List<MenuFileDto> menu;

    private ReservationStatusDto reservationStatus;

    private String cancelledDate;

    private Integer actionTaker;

    private Set<CommonDto> dates;

    private String bookingCharge;

    private Boolean isOwned;

    private Boolean isManual;

    private Boolean isBlock;


    private Double discount;

    private Double advance;

    private transient List<Map> extraAmenityList ;

    private transient Map<String, Object> space;

    private EventTypeDto eventTypedetail;

    private CommonDto seatingArrangement;

    private Boolean isReviewed;

    private ReviewDetailsDto review;

    private String remarks;

    private String note;

    public List<Map> getExtraAmenityList() {
        return extraAmenityList;
    }

    public void setExtraAmenityList(List<Map> extraAmenityList) {
        this.extraAmenityList = extraAmenityList;
    }

    public Boolean getIsReviewed() {
        return isReviewed;
    }

    public void setIsReviewed(Boolean isReviewed) {
        this.isReviewed = isReviewed;
    }

    public Integer getEventType() {
        return eventType;
    }

    public void setEventType(Integer eventType) {
        this.eventType = eventType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Map<String, Object> getSpace() {
        return space;
    }

    public void setSpace(Map<String, Object> space) {
        this.space = space;
    }

    public Map<String, Object> getUser() {
        return user;
    }

    public void setUser(Map<String, Object> user) {
        this.user = user;
    }

    public ReservationStatusDto getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(ReservationStatusDto reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public Set<CommonDto> getDates() {
        return dates;
    }

    public void setDates(Set<CommonDto> dates) {
        this.dates = dates;
    }

    public Boolean getIsOwned() {
        return isOwned;
    }

    public void setIsOwned(Boolean isOwned) {
        this.isOwned = isOwned;
    }

    public EventTypeDto getEventTypedetail() {
        return eventTypedetail;
    }

    public void setEventTypedetail(EventTypeDto eventTypedetail) {
        this.eventTypedetail = eventTypedetail;
    }

    public String getBookingCharge() {
        return bookingCharge;
    }

    public void setBookingCharge(String bookingCharge) {
        this.bookingCharge = bookingCharge;
    }

    public Integer getActionTaker() {
        return actionTaker;
    }

    public void setActionTaker(Integer actionTaker) {
        this.actionTaker = actionTaker;
    }

    public String getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(String cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public CommonDto getSeatingArrangement() {
        return seatingArrangement;
    }

    public void setSeatingArrangement(CommonDto seatingArrangement) {
        this.seatingArrangement = seatingArrangement;
    }

    public Integer getGuestCount() {
        return guestCount;
    }

    public void setGuestCount(Integer guestCount) {
        this.guestCount = guestCount;
    }

    public String getBookedDate() {
        return bookedDate;
    }

    public void setBookedDate(String bookedDate) {
        this.bookedDate = bookedDate;
    }

    public List<MenuFileDto> getMenu() {
        return menu;
    }

    public void setMenu(List<MenuFileDto> menu) {
        this.menu = menu;
    }

    public ReviewDetailsDto getReview() {
        return review;
    }

    public void setReview(ReviewDetailsDto review) {
        this.review = review;
    }

    public Boolean getIsManual() {
        return isManual;
    }

    public void setISManual(Boolean isManual) {
        this.isManual = isManual;
    }

    public Boolean getIsBlock() {
        return isBlock;
    }

    public void setIsBlock(Boolean isBlock) {
        this.isBlock = isBlock;
    }

    public Double getRefund() {
        return refund;
    }

    public void setRefund(Double refund) {
        this.refund = refund;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isPayLater() {
        return isPayLater;
    }

    public void setIsPayLater(boolean isPayLater) {
        this.isPayLater = isPayLater;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getAdvance() {
        return advance;
    }

    public void setAdvance(Double advance) {
        this.advance = advance;
    }
}
