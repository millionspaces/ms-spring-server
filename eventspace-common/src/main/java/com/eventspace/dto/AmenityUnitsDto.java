package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.Set;

/**
 * Event space
 * Created by Aux-054 on 12/16/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AmenityUnitsDto implements Serializable {
    /**
     * The id.
     */
    private Integer id;

    /**
     * The name.
     */
    private String name;

    private Set<AmenityDto> amenityDtoSet;


    public Set<AmenityDto> getAmenityDtoSet() {
        return amenityDtoSet;
    }

    public void setAmenityDtoSet(Set<AmenityDto> amenityDtoSet) {
        this.amenityDtoSet = amenityDtoSet;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
