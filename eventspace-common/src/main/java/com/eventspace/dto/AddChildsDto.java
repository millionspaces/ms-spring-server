package com.eventspace.dto;

import java.io.Serializable;

public class AddChildsDto implements Serializable {

    private static final long serialVersionUID = 7797455140996929742L;

    private Integer originSpace;

    private Integer[] childs;

    public Integer getOriginSpace() {
        return originSpace;
    }

    public void setOriginSpace(Integer originSpace) {
        this.originSpace = originSpace;
    }

    public Integer[] getChilds() {
        return childs;
    }

    public void setChilds(Integer[] childs) {
        this.childs = childs;
    }
}
