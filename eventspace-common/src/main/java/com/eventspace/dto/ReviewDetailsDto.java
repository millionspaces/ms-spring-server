package com.eventspace.dto;

import java.io.Serializable;

/**
 * Created by Auxenta on 3/14/2017.
 */
public class ReviewDetailsDto implements Serializable {

    /**
     * The title.
     */
    private String title;

    /**
     * The description.
     */
    private String description;

    private String msExperience;

    /**
     * The rate.
     */
    private String rate;

    /**
     * The userImg.
     */
    private String userImg;

    /**
     * The createdAt.
     */
    private String createdAt;

    private String userName;

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the rate.
     *
     * @return the rate
     */
    public String getRate() {
        return rate;
    }

    /**
     * Sets the rate.
     *
     * @param rate the new rate
     */
    public void setRate(String rate) {
        this.rate = rate;
    }

    /**
     * Gets the userImg.
     *
     * @return the userImg
     */
    public String getUserImg() {
        return userImg;
    }

    /**
     * Sets the userImg.
     *
     * @param userImg the new userImg
     */
    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }


    /**
     * Gets the createdAt.
     *
     * @return the createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * Sets the createdAt.
     *
     * @param createdAt the new createdAt
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMsExperience() {
        return msExperience;
    }

    public void setMsExperience(String msExperience) {
        this.msExperience = msExperience;
    }
}
