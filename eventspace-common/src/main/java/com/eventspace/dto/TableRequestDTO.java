/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * The Class TableRequestDTO.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class TableRequestDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 8467034165188220330L;

    /**
     * The table.
     */
    private String table;

    /**
     * The date.
     */
    private Long date;

    /**
     * The rows.
     */
    private String rows;

    /**
     * Gets the serialversionuid.
     *
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * Gets the table.
     *
     * @return the table
     */
    public String getTable() {
        return table;
    }

    /**
     * Sets the table.
     *
     * @param table the table
     */
    public void setTable(String table) {
        this.table = table;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public Long getDate() {
        return date;
    }

    /**
     * Sets the date.
     *
     * @param date the date
     */
    public void setDate(Long date) {
        this.date = date;
    }

    /**
     * @return the rows
     */
    public String getRows() {
        return rows;
    }

    /**
     * @param rows the rows to set
     */
    public void setRows(String rows) {
        this.rows = rows;
    }
}
