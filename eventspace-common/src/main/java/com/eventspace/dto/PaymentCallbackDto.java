package com.eventspace.dto;

import java.io.Serializable;

/**
 * Created by Aux-054 on 12/15/2016.
 */

public class PaymentCallbackDto implements Serializable {


    private String OrderID;

    private Integer ResponseCode;

    private Integer ReasonCode;

    private String ReasonCodeDesc;

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public Integer getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(Integer responseCode) {
        ResponseCode = responseCode;
    }

    public Integer getReasonCode() {
        return ReasonCode;
    }

    public void setReasonCode(Integer reasonCode) {
        ReasonCode = reasonCode;
    }

    public String getReasonCodeDesc() {
        return ReasonCodeDesc;
    }

    public void setReasonCodeDesc(String reasonCodeDesc) {
        ReasonCodeDesc = reasonCodeDesc;
    }
}
