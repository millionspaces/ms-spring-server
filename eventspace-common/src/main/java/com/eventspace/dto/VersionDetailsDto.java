package com.eventspace.dto;

public class VersionDetailsDto {

    private static final long serialVersionUID = 7797455140996929742L;

    private Integer id;

    private String version;

    private Integer updateRequired;

    private String releaseNote;

    private String createdDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getUpdateRequired() {
        return updateRequired;
    }

    public void setUpdateRequired(Integer updateRequired) {
        this.updateRequired = updateRequired;
    }

    public String getReleaseNote() {
        return releaseNote;
    }

    public void setReleaseNote(String releaseNote) {
        this.releaseNote = releaseNote;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
