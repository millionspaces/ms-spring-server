package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

/**
 * The Class SpaceUnavailabilityDto.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SpaceUnavailabilityDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    /**
     * The id.
     */
    private Integer id;

    private Integer bookingId;

    /**
     * The space.
     */
    private Integer space;

    /**
     * The fromDate.
     */
    private String fromDate;

    /**
     * The toDate.
     */

    private String eventTitle;

    private String toDate;

    private Integer isBlocked;

    private String guestContactNumber;

    private String guestName;

    private String dateBookingMade;

    private String guestEmail;

    private String noOfGuests;

    private String extrasRequested;

    private String seatingArrangementId;

    private String seatingArrangementName;

    private String seatingArrangementIcon;

    private String cost;

    private String note;

    private Integer eventTypeId;

    private String eventTypeName;

    private Integer isManual;

    private String remark;

    private CommonDto reservationStatus;

    private CancellationPolicyDto cancellationPolicy;

    private List<AmenityDto> amenities;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSpace() {
        return space;
    }

    public void setSpace(Integer space) {
        this.space = space;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public Integer getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(Integer isBlocked) {
        this.isBlocked = isBlocked;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getGuestContactNumber() {
        return guestContactNumber;
    }

    public void setGuestContactNumber(String guestContactNumber) {
        this.guestContactNumber = guestContactNumber;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getDateBookingMade() {
        return dateBookingMade;
    }

    public void setDateBookingMade(String dateBookingMade) {
        this.dateBookingMade = dateBookingMade;
    }

    public String getGuestEmail() {
        return guestEmail;
    }

    public void setGuestEmail(String guestEmail) {
        this.guestEmail = guestEmail;
    }

    public String getNoOfGuests() {
        return noOfGuests;
    }

    public void setNoOfGuests(String noOfGuests) {
        this.noOfGuests = noOfGuests;
    }

    public String getExtrasRequested() {
        return extrasRequested;
    }

    public void setExtrasRequested(String extrasRequested) {
        this.extrasRequested = extrasRequested;
    }

    public String getSeatingArrangementId() {
        return seatingArrangementId;
    }

    public void setSeatingArrangementId(String seatingArrangementId) {
        this.seatingArrangementId = seatingArrangementId;
    }

    public String getSeatingArrangementName() {
        return seatingArrangementName;
    }

    public void setSeatingArrangementName(String seatingArrangementName) {
        this.seatingArrangementName = seatingArrangementName;
    }

    public String getSeatingArrangementIcon() {
        return seatingArrangementIcon;
    }

    public void setSeatingArrangementIcon(String seatingArrangementIcon) {
        this.seatingArrangementIcon = seatingArrangementIcon;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public Integer getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(Integer eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public String getEventTypeName() {
        return eventTypeName;
    }

    public void setEventTypeName(String eventTypeName) {
        this.eventTypeName = eventTypeName;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public Integer getIsManual() {
        return isManual;
    }

    public void setIsManual(Integer isManual) {
        this.isManual = isManual;
    }

    public CancellationPolicyDto getCancellationPolicy() {
        return cancellationPolicy;
    }

    public void setCancellationPolicy(CancellationPolicyDto cancellationPolicy) {
        this.cancellationPolicy = cancellationPolicy;
    }

    public List<AmenityDto> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<AmenityDto> amenities) {
        this.amenities = amenities;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public CommonDto getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(CommonDto reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }
}
