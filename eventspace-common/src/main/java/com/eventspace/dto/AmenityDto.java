package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * Event space
 * Created by Aux-054 on 12/16/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AmenityDto implements Serializable {
    /**
     * The id.
     */
    private Integer id;

    /**
     * The name.
     */
    private String name;

    /**
     * The icon.
     */
    private String icon;

    private String mobileIcon;

    /**
     * The date updated.
     */
    private String timeStamp;


    private Integer amenityUnit;

    /**
     * The amenity units dto.
     */
    private AmenityUnitsDto amenityUnitsDto;


    /**
     * Gets the amenity units dto.
     *
     * @return the amenity units dto
     */
    public AmenityUnitsDto getAmenityUnitsDto() {
        return amenityUnitsDto;
    }

    /**
     * Sets the amenity units dto.
     *
     * @param amenityUnitsDto the new amenity units dto
     */
    public void setAmenityUnitsDto(AmenityUnitsDto amenityUnitsDto) {
        this.amenityUnitsDto = amenityUnitsDto;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the amenity unit.
     *
     * @return the amenity unit
     */
    public Integer getAmenityUnit() {
        return amenityUnit;
    }

    /**
     * Sets the amenity unit.
     *
     * @param amenityUnit the new amenity unit
     */
    public void setAmenityUnit(Integer amenityUnit) {
        this.amenityUnit = amenityUnit;
    }

    /**
     * Gets the icon.
     *
     * @return the icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * Sets the icon.
     *
     * @param icon the new icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * Gets the timeStamp.
     *
     * @return the timeStamp
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     * Sets the timeStamp.
     *
     * @param timeStamp the new timeStamp
     */
    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getMobileIcon() {
        return mobileIcon;
    }

    public void setMobileIcon(String mobileIcon) {
        this.mobileIcon = mobileIcon;
    }


}
