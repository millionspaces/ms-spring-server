package com.eventspace.dto;

import com.eventspace.util.Constants;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class BatchBookingDto {

    LocalDate from;
    LocalDate to;
    BookingDto booking;
    Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public LocalDate getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = convertDate(from);
    }

    public LocalDate getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = convertDate(to);
    }

    public BookingDto getBooking() {
        return booking;
    }

    public void setBooking(BookingDto booking) {
        this.booking = booking;
    }

    private LocalDate convertDate(String date) {
        DateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
        try {
            return Instant.ofEpochMilli(df.parse(date).getTime())
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }
}
