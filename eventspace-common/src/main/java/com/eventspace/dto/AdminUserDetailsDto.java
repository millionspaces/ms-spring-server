package com.eventspace.dto;

import java.io.Serializable;

/**
 * Created by Auxenta on 4/28/2017.
 */
public class AdminUserDetailsDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    private Integer id;
    private String name;
    private String phone;
    private String companyPhone;
    private String email;
    private Integer bookingCount;
    private BookingDetailsDto booking;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public BookingDetailsDto getBooking() {
        return booking;
    }

    public void setBooking(BookingDetailsDto booking) {
        this.booking = booking;
    }

    public Integer getBookingCount() {
        return bookingCount;
    }

    public void setBookingCount(Integer bookingCount) {
        this.bookingCount = bookingCount;
    }
}
