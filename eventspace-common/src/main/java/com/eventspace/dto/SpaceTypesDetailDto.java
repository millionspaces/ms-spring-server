/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;


/**
 * The Class SpaceTypesDetailDto.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SpaceTypesDetailDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    /**
     * The space id.
     */
    private Integer spaceId;

    /**
     * The event type id.
     */
    private Integer eventTypeId;

    /**
     * Gets the space id.
     *
     * @return the space id
     */
    public Integer getSpaceId() {
        return spaceId;
    }

    /**
     * Sets the space id.
     *
     * @param spaceId the new space id
     */
    public void setSpaceId(Integer spaceId) {
        this.spaceId = spaceId;
    }

    /**
     * Gets the event type id.
     *
     * @return the event type id
     */
    public Integer getEventTypeId() {
        return eventTypeId;
    }

    /**
     * Sets the event type id.
     *
     * @param eventTypeId the new event type id
     */
    public void setEventTypeId(Integer eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

}
