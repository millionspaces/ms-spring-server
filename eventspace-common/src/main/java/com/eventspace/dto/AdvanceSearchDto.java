package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Auxenta on 6/6/2017.
 */


@JsonIgnoreProperties(ignoreUnknown = true)
public class AdvanceSearchDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    private String participation;

    private String budget;

    private String organizationName;

    private Integer[] spaceType;

    private Integer[] amenities;

    private Integer[] rules;

    private Integer[] seatingArrangements;

    private Integer[] events;

    private LocationSearchDto location;

    private String available;

    private Integer limit;

    private String sortBy;

    private Integer isSocialMedia;

    private List<Integer> orderBy;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getParticipation() {
        return participation;
    }

    public void setParticipation(String participation) {
        this.participation = participation;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public Integer[] getAmenities() {
        return amenities;
    }

    public void setAmenities(Integer[] amenities) {
        this.amenities = amenities;
    }

    public Integer[] getEvents() {
        return events;
    }

    public void setEvents(Integer[] events) {
        this.events = events;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public LocationSearchDto getLocation() {
        return location;
    }

    public void setLocation(LocationSearchDto location) {
        this.location = location;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public Integer[] getRules() {
        return rules;
    }

    public void setRules(Integer[] rules) {
        this.rules = rules;
    }

    public Integer[] getSeatingArrangements() {
        return seatingArrangements;
    }

    public void setSeatingArrangements(Integer[] seatingArrangements) {
        this.seatingArrangements = seatingArrangements;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Integer[] getSpaceType() {
        return spaceType;
    }

    public void setSpaceType(Integer[] spaceType) {
        this.spaceType = spaceType;
    }

    public Integer getIsSocialMedia() {
        return isSocialMedia;
    }

    public void setIsSocialMedia(Integer isSocialMedia) {
        this.isSocialMedia = isSocialMedia;
    }

    public List<Integer> getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(List<Integer> orderBy) {
        this.orderBy = orderBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdvanceSearchDto)) return false;

        AdvanceSearchDto that = (AdvanceSearchDto) o;

        if (getParticipation() != null ? !getParticipation().equals(that.getParticipation()) : that.getParticipation() != null)
            return false;
        if (getBudget() != null ? !getBudget().equals(that.getBudget()) : that.getBudget() != null) return false;
        if (getOrganizationName() != null ? !getOrganizationName().equals(that.getOrganizationName()) : that.getOrganizationName() != null)
            return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(getSpaceType(), that.getSpaceType())) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(getAmenities(), that.getAmenities())) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(getRules(), that.getRules())) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(getSeatingArrangements(), that.getSeatingArrangements())) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(getEvents(), that.getEvents())) return false;
        if (getLocation() != null ? !getLocation().equals(that.getLocation()) : that.getLocation() != null)
            return false;
        if (getAvailable() != null ? !getAvailable().equals(that.getAvailable()) : that.getAvailable() != null)
            return false;
        if (getLimit() != null ? !getLimit().equals(that.getLimit()) : that.getLimit() != null) return false;
        if (getSortBy() != null ? !getSortBy().equals(that.getSortBy()) : that.getSortBy() != null) return false;
        if (getOrderBy() != null ? !getOrderBy().equals(that.getOrderBy()) : that.getOrderBy() != null) return false;
        return getIsSocialMedia() != null ? getIsSocialMedia().equals(that.getIsSocialMedia()) : that.getIsSocialMedia() == null;
    }

    @Override
    public int hashCode() {
        int result = getParticipation() != null ? getParticipation().hashCode() : 0;
        result = 31 * result + (getBudget() != null ? getBudget().hashCode() : 0);
        result = 31 * result + (getOrganizationName() != null ? getOrganizationName().hashCode() : 0);
        result = 31 * result + Arrays.hashCode(getSpaceType());
        result = 31 * result + Arrays.hashCode(getAmenities());
        result = 31 * result + Arrays.hashCode(getRules());
        result = 31 * result + Arrays.hashCode(getSeatingArrangements());
        result = 31 * result + Arrays.hashCode(getEvents());
        result = 31 * result + (getLocation() != null ? getLocation().hashCode() : 0);
        result = 31 * result + (getAvailable() != null ? getAvailable().hashCode() : 0);
        result = 31 * result + (getLimit() != null ? getLimit().hashCode() : 0);
        result = 31 * result + (getSortBy() != null ? getSortBy().hashCode() : 0);
        result = 31 * result + (getOrderBy() != null ? getOrderBy().hashCode() : 0);
        result = 31 * result + (getIsSocialMedia() != null ? getIsSocialMedia().hashCode() : 0);
        return result;
    }
}
