package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by Auxenta on 6/13/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GoogleGeoCodeResponseDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    private String status;

    private transient Result[] results;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Result[] getResults() {
        return results;
    }

    public void setResults(Result[] results) {
        this.results = results;
    }

    public String getSearchAddress() {
        String result = "";
        if (status.equals("OK") && results != null && results.length != 0) {
            result = results[0].getAddress();
        }
        return result;
    }
}

class Result {
    private String formatted_address;
    private String[] types;
    private AddressComponent[] address_components;

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public String[] getTypes() {
        return types;
    }

    public void setTypes(String[] types) {
        this.types = types;
    }

    public AddressComponent[] getAddress_components() {
        return address_components;
    }

    public void setAddress_components(AddressComponent[] address_components) {
        this.address_components = address_components;
    }

    public String getAddress() {
        String country = "";
        String administrativeLevel1 = "";
        String administrativeLevel2 = "";
        String route = "";
        for (AddressComponent address : address_components) {
            if (Arrays.asList(address.getTypes()).contains("route")) {
                route = address.getLong_name();
            }
            if (Arrays.asList(address.getTypes()).contains("administrative_area_level_2")) {
                administrativeLevel2 = address.getLong_name();
            }
            if (Arrays.asList(address.getTypes()).contains("administrative_area_level_1")) {
                administrativeLevel1 = address.getLong_name();
            }
            if (Arrays.asList(address.getTypes()).contains("country")) {
                country = address.getLong_name();
            }
        }
        return route + "," + administrativeLevel2 + "," + administrativeLevel1 + "," + country;
    }
}


class AddressComponent {
    private String long_name;
    private String short_name;
    private String[] types;

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String[] getTypes() {
        return types;
    }

    public void setTypes(String[] types) {
        this.types = types;
    }
}

