package com.eventspace.dto;

import java.io.Serializable;

public class LinkSpaceDto implements Serializable {

    private static final long serialVersionUID = 7797455140996929742L;

    private Integer originSpace;

    private Integer linkSpace;

    public Integer getOriginSpace() {
        return originSpace;
    }

    public void setOriginSpace(Integer originSpace) {
        this.originSpace = originSpace;
    }

    public Integer getLinkSpace() {
        return linkSpace;
    }

    public void setLinkSpace(Integer linkSpace) {
        this.linkSpace = linkSpace;
    }
}
