/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SpaceSearchDto implements Serializable {

    private static final long serialVersionUID = 7797455140996929742L;

    private String latitude;

    private String longitude;

    private Integer radius;

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

}
