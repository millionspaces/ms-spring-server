package com.eventspace.dto;

import java.io.Serializable;

public class AdminPaidBookingDetails implements Serializable {

    private static final long serialVersionUID = 7797455140996929742L;

    private Integer id;
    private String orderId;
    private String spaceName;
    private String hostName;
    private String guestName;
    private String guestMobile;
    private String guestEmail;
    private Integer commissionPercentage;
    private Double millionspacesCharge;
    private Double hostCharge;
    private Double total;
    private Integer paymentVerified;
    private String pdf;
    private String cancellationPolicy;
    private String organizationName;
    private Long bookingMade;
    private Long eventDate;
    private Long paymentDueDate;
    private Long cancellationCutOffDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getBookingMade() {
        return bookingMade;
    }

    public void setBookingMade(Long bookingMade) {
        this.bookingMade = bookingMade;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public Integer getCommissionPercentage() {
        return commissionPercentage;
    }

    public void setCommissionPercentage(Integer commissionPercentage) {
        this.commissionPercentage = commissionPercentage;
    }

    public Double getMillionspacesCharge() {
        return millionspacesCharge;
    }

    public void setMillionspacesCharge(Double millionspacesCharge) {
        this.millionspacesCharge = millionspacesCharge;
    }

    public Double getHostCharge() {
        return hostCharge;
    }

    public void setHostCharge(Double hostCharge) {
        this.hostCharge = hostCharge;
    }

    public Integer getPaymentVerified() {
        return paymentVerified;
    }

    public void setPaymentVerified(Integer paymentVerified) {
        this.paymentVerified = paymentVerified;
    }

    public String getCancellationPolicy() {
        return cancellationPolicy;
    }

    public void setCancellationPolicy(String cancellationPolicy) {
        this.cancellationPolicy = cancellationPolicy;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Long getEventDate() {
        return eventDate;
    }

    public void setEventDate(Long eventDate) {
        this.eventDate = eventDate;
    }

    public Long getCancellationCutOffDate() {
        return cancellationCutOffDate;
    }

    public void setCancellationCutOffDate(Long cancellationCutOffDate) {
        this.cancellationCutOffDate = cancellationCutOffDate;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public Long getPaymentDueDate() {
        return paymentDueDate;
    }

    public void setPaymentDueDate(Long paymentDueDate) {
        this.paymentDueDate = paymentDueDate;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getGuestMobile() {
        return guestMobile;
    }

    public void setGuestMobile(String guestMobile) {
        this.guestMobile = guestMobile;
    }

    public String getGuestEmail() {
        return guestEmail;
    }

    public void setGuestEmail(String guestEmail) {
        this.guestEmail = guestEmail;
    }

    public  AdminBookingFinanceDto build(){
        AdminBookingFinanceDto financeDto=new AdminBookingFinanceDto();
        financeDto.setId(id);
        financeDto.setOrderId(orderId);
        financeDto.setEventDate(eventDate);
        financeDto.setSpaceName(spaceName);
        financeDto.setOrganizationName(organizationName);
        financeDto.setCommissionPercentage(commissionPercentage);
        financeDto.setHostCharge(hostCharge);
        financeDto.setPaymentVerified(paymentVerified);
        financeDto.setPdf(pdf);
        financeDto.setGuestName(guestName);
        financeDto.setBookingMade(bookingMade);
        financeDto.setCancellationPolicy(cancellationPolicy);
        return financeDto;
    }
}
