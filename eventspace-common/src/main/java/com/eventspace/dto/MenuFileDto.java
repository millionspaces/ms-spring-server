package com.eventspace.dto;

import java.io.Serializable;

public class MenuFileDto implements Serializable {
    private static final long serialVersionUID = 7797455140996929742L;

    private Integer id;

    private String url;

    private String menuId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }
}
