/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The Class SpaceDetailDto.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SpaceDetailDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    /**
     * The id.
     */
    private Integer id;

    /**
     * The name.
     */
    private String name;
    /**
     * The addressLine1.
     */
    private String addressLine1;

    /**
     * The user addressLine2.
     */
    private String addressLine2;

    /**
     * The description.
     */
    private String description;

    /**
     * The size.
     */
    private Integer size;

    /**
     * The participane count.
     */
    private Integer participantCount;

    /**
     * The rate per hour.
     */
    private Double ratePerHour;

    private Double discount;

    private Double preStaringFrom;
    /**
     * The user.
     */
    private Integer user;

    /**
     * The cancellation policy.
     */
    private Integer cancellationPolicy;

    /**
     * The longitude.
     */
    private String longitude;

    /**
     * The latitude.
     */
    private String latitude;

    /**
     * The thumbnailImage.
     */
    private String thumbnailImage;

    private Integer securityDeposit;

    /**
     * The rating.
     */
    private Double rating;

    private String calendarStart;

    private String calendarEnd;

    private String calendarEventSize;

    private Integer bufferTime;

    private Integer noticePeriod;

    private CommonDto measurementUnit;

    private String menuFileName;


    private String contactPersonName;

    private String mobileNumber;

    private String companyName;

    private String companyPhone;

    private String contactPersonName2;

    private String mobileNumber2;

    private String email2;

    private Integer commissionPercentage;

    private String accountHolderName;

    private String accountNumber;

    private String bank;

    private String bankBranch;

    private String hostLogo;





    private Integer parent;

    private Integer linkSpace;

    private List<AdminSpaceDetailsDto> childs;

    /**
     * The created at.
     */
    private Date createdAt;

    /**
     * The updated at.
     */
    private Date updatedAt;

    private String availabilityMethod;

    private Integer blockChargeType;

    private Integer minParticipantCount;

    private Integer advanceOnlyEnable;

    private Set<AvailablityDetailsDto> availability;

    /**
     * The event type.
     */
    private Integer[] eventType;

    /**
     * The amenity id list.
     */
    private Integer[] amenity;

    private Integer[] rules;

    private Set<SeatingArrangementDto> seatingArrangements;

    /**
     * The extra amenity.
     */
    private Set<ExtraAmenityDto> extraAmenity;

    private Set<MenuFileDto> menuFiles;

    private Set<CommonDto> spaceType;

    private Integer reimbursableOption;


    /**
     * The images.
     */
    private Set<String> images;


    /**
     * The spaceOwnerDto.
     */
    private SpaceOwnerDto spaceOwnerDto;

    private List<AdminSpaceDetailsDto> childSpaces;

    private List<AdminSpaceDetailsDto> linkedSpaces;

    private List<ImageDetailsDto> imageDetails;
    /**
     * The booking.
     */
    private transient List<Map> futureBookingDates;

    private transient List<Map> bookingDates;

    /**
     * The review Details.
     */
    private transient List<ReviewDetailsDto> reviewDetails;

    /**
     * The similar spaces.
     */
    private Set<SpaceDetailDto> similarSpaces;

    private Integer approved;


    /**
     * Gets the images.
     *
     * @return the images
     */
    public Set<String> getImages() {
        return images;
    }

    /**
     * Sets the images.
     *
     * @param images the new images
     */
    public void setImages(Set<String> images) {
        this.images = images;
    }


    /**
     * Gets the event type.
     *
     * @return the event type
     */
    public Integer[] getEventType() {
        return eventType;
    }

    /**
     * Sets the event type.
     *
     * @param eventType the new event type
     */
    public void setEventType(Integer[] eventType) {
        this.eventType = eventType;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Gets the address line 1.
     *
     * @return the address line 1
     */
    public String getAddressLine1() {
        return addressLine1;
    }

    /**
     * Sets the address line 1.
     *
     * @param addressLine1 the new address line 1
     */
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    /**
     * Gets the address line 2.
     *
     * @return the address line 2
     */
    public String getAddressLine2() {
        return addressLine2;
    }

    /**
     * Sets the address line 2.
     *
     * @param addressLine2 the new address line 2
     */
    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the size.
     *
     * @return the size
     */
    public Integer getSize() {
        return size;
    }

    /**
     * Sets the size.
     *
     * @param size the new size
     */
    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * Gets the participant count.
     *
     * @return the participant count
     */
    public Integer getParticipantCount() {
        return participantCount;
    }

    /**
     * Sets the participant count.
     *
     * @param participantCount the new participant count
     */
    public void setParticipantCount(Integer participantCount) {
        this.participantCount = participantCount;
    }

    /**
     * Gets the rate per hour.
     *
     * @return the rate per hour
     */
    public Double getRatePerHour() {
        return ratePerHour;
    }

    /**
     * Sets the rate per hour.
     *
     * @param ratePerHour the new rate per hour
     */
    public void setRatePerHour(Double ratePerHour) {
        this.ratePerHour = ratePerHour;
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public Integer getUser() {
        return user;
    }

    /**
     * Sets the user.
     *
     * @param user the new user
     */
    public void setUser(Integer user) {
        this.user = user;
    }

    /**
     * Gets the cancellation policy.
     *
     * @return the cancellation policy
     */
    public Integer getCancellationPolicy() {
        return cancellationPolicy;
    }

    /**
     * Sets the cancellation policy.
     *
     * @param cancellationPolicy the new cancellation policy
     */
    public void setCancellationPolicy(Integer cancellationPolicy) {
        this.cancellationPolicy = cancellationPolicy;
    }

    /**
     * Gets the amenity.
     *
     * @return the amenity
     */
    public Integer[] getAmenity() {
        return amenity;
    }

    /**
     * Sets the amenity.
     *
     * @param amenity the new amenity
     */
    public void setAmenity(Integer[] amenity) {
        this.amenity = amenity;
    }

    /**
     * Gets the extra amenity.
     *
     * @return the extra amenity
     */
    public Set<ExtraAmenityDto> getExtraAmenity() {
        return extraAmenity;
    }

    /**
     * Sets the extra amenity.
     *
     * @param extraAmenity the new extra amenity
     */
    public void setExtraAmenity(Set<ExtraAmenityDto> extraAmenity) {
        this.extraAmenity = extraAmenity;
    }

    /**
     * Gets the rating.
     *
     * @return the rating
     */
    public Double getRating() {
        return rating;
    }

    /**
     * Sets the rating.
     *
     * @param rating the new rating
     */
    public void setRating(Double rating) {
        this.rating = rating;
    }

    /**
     * Gets the longitude.
     *
     * @return the longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * Sets the longitude.
     *
     * @param longitude the new longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * Gets the latitude.
     *
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * Sets the latitude.
     *
     * @param latitude the new latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public SpaceOwnerDto getSpaceOwnerDto() {
        return spaceOwnerDto;
    }

    public void setSpaceOwnerDto(SpaceOwnerDto spaceOwnerDto) {
        this.spaceOwnerDto = spaceOwnerDto;
    }

    public List<ReviewDetailsDto> getReviewDetails() {
        return reviewDetails;
    }

    public void setReviewDetails(List<ReviewDetailsDto> reviewDetails) {
        this.reviewDetails = reviewDetails;
    }

    public String getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    public Set<SpaceDetailDto> getSimilarSpaces() {
        return similarSpaces;
    }

    public void setSimilarSpaces(Set<SpaceDetailDto> similarSpaces) {
        this.similarSpaces = similarSpaces;
    }

    public List<Map> getFutureBookingDates() {
        return futureBookingDates;
    }

    public void setFutureBookingDates(List<Map> futureBookingDates) {
        this.futureBookingDates = futureBookingDates;
    }

    public Integer getSecurityDeposit() {
        return securityDeposit;
    }

    public void setSecurityDeposit(Integer securityDeposit) {
        this.securityDeposit = securityDeposit;
    }

    public Integer[] getRules() {
        return rules;
    }

    public void setRules(Integer[] rules) {
        this.rules = rules;
    }

    public Set<SeatingArrangementDto> getSeatingArrangements() {
        return seatingArrangements;
    }

    public void setSeatingArrangements(Set<SeatingArrangementDto> seatingArrangements) {
        this.seatingArrangements = seatingArrangements;
    }

    public String getCalendarStart() {
        return calendarStart;
    }

    public void setCalendarStart(String calendarStart) {
        this.calendarStart = calendarStart;
    }

    public String getCalendarEnd() {
        return calendarEnd;
    }

    public void setCalendarEnd(String calendarEnd) {
        this.calendarEnd = calendarEnd;
    }

    public String getCalendarEventSize() {
        return calendarEventSize;
    }

    public void setCalendarEventSize(String calendarEventSize) {
        this.calendarEventSize = calendarEventSize;
    }


    public Set<AvailablityDetailsDto> getAvailability() {
        return availability;
    }

    public void setAvailability(Set<AvailablityDetailsDto> availability) {
        this.availability = availability;
    }

    public String getAvailabilityMethod() {
        return availabilityMethod;
    }

    public void setAvailabilityMethod(String availabilityMethod) {
        this.availabilityMethod = availabilityMethod;
    }

    public Integer getBufferTime() {
        return bufferTime;
    }

    public void setBufferTime(Integer bufferTime) {
        this.bufferTime = bufferTime;
    }

    public Integer getNoticePeriod() {
        return noticePeriod;
    }

    public void setNoticePeriod(Integer noticePeriod) {
        this.noticePeriod = noticePeriod;
    }

    public CommonDto getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(CommonDto measurementUnit) {
        this.measurementUnit = measurementUnit;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getMenuFileName() {
        return menuFileName;
    }

    public void setMenuFileName(String menuFileName) {
        this.menuFileName = menuFileName;
    }

    public Set<MenuFileDto> getMenuFiles() {
        return menuFiles;
    }

    public void setMenuFiles(Set<MenuFileDto> menuFiles) {
        this.menuFiles = menuFiles;
    }

    public Integer getReimbursableOption() {
        return reimbursableOption;
    }

    public void setReimbursableOption(Integer reimbursableOption) {
        this.reimbursableOption = reimbursableOption;
    }

    public Integer getMinParticipantCount() {
        return minParticipantCount;
    }

    public void setMinParticipantCount(Integer minParticipantCount) {
        this.minParticipantCount = minParticipantCount;
    }

    public Integer getParent() {
        return parent;
    }

    public void setParent(Integer parent) {
        this.parent = parent;
    }

    public List<AdminSpaceDetailsDto> getChilds() {
        return childs;
    }

    public void setChilds(List<AdminSpaceDetailsDto> childs) {
        this.childs = childs;
    }

    public Integer getBlockChargeType() {
        return blockChargeType;
    }

    public void setBlockChargeType(Integer blockChargeType) {
        this.blockChargeType = blockChargeType;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Integer getCommissionPercentage() {
        return commissionPercentage;
    }

    public void setCommissionPercentage(Integer commissionPercentage) {
        this.commissionPercentage = commissionPercentage;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getHostLogo() {
        return hostLogo;
    }

    public void setHostLogo(String hostLogo) {
        this.hostLogo = hostLogo;
    }

    public Integer getLinkSpace() {
        return linkSpace;
    }

    public void setLinkSpace(Integer linkSpace) {
        this.linkSpace = linkSpace;
    }

    public List<AdminSpaceDetailsDto> getChildSpaces() {
        return childSpaces;
    }

    public void setChildSpaces(List<AdminSpaceDetailsDto> childSpaces) {
        this.childSpaces = childSpaces;
    }

    public List<AdminSpaceDetailsDto> getLinkedSpaces() {
        return linkedSpaces;
    }

    public void setLinkedSpaces(List<AdminSpaceDetailsDto> linkedSpaces) {
        this.linkedSpaces = linkedSpaces;
    }

    public List<Map> getBookingDates() {
        return bookingDates;
    }

    public void setBookingDates(List<Map> bookingDates) {
        this.bookingDates = bookingDates;
    }

    public Set<CommonDto> getSpaceType() {
        return spaceType;
    }

    public void setSpaceType(Set<CommonDto> spaceType) {
        this.spaceType = spaceType;
    }

    public String getContactPersonName2() {
        return contactPersonName2;
    }

    public void setContactPersonName2(String contactPersonName2) {
        this.contactPersonName2 = contactPersonName2;
    }

    public String getMobileNumber2() {
        return mobileNumber2;
    }

    public void setMobileNumber2(String mobileNumber2) {
        this.mobileNumber2 = mobileNumber2;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public List<ImageDetailsDto> getImageDetails() {
        return imageDetails;
    }

    public void setImageDetails(List<ImageDetailsDto> imageDetails) {
        this.imageDetails = imageDetails;
    }

    public Integer getApproved() {
        return approved;
    }

    public void setApproved(Integer approved) {
        this.approved = approved;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getPreStaringFrom() {
        return preStaringFrom;
    }

    public void setPreStaringFrom(Double preStaringFrom) {
        this.preStaringFrom = preStaringFrom;
    }

    public Integer getAdvanceOnlyEnable() {
        return advanceOnlyEnable;
    }

    public void setAdvanceOnlyEnable(Integer advanceOnlyEnable) {
        this.advanceOnlyEnable = advanceOnlyEnable;
    }
}
