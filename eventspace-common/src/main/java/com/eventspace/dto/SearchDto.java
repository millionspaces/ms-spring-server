package com.eventspace.dto;

import java.io.Serializable;

/**
 * Created by Auxenta on 5/17/2017.
 */
public class SearchDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    private Integer[] events;

    private String location;

    public Integer[] getEvents() {
        return events;
    }

    public void setEvents(Integer[] events) {
        this.events = events;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
