package com.eventspace.dto;

import java.io.Serializable;

public class ChargeDetailsDto implements Serializable {

    private Integer slotCharge;
    private Integer extraAmenityCharge;
    private Integer discount;
    private Integer promotionDiscount;
    private Integer payableAdvance;
    private Integer advance;
    private Integer bookingCharge;
    private Integer charge;
    private Boolean advanceOnlyEnable=false;

    public Integer getSlotCharge() {
        return slotCharge;
    }

    public void setSlotCharge(Integer slotCharge) {
        this.slotCharge = slotCharge;
    }

    public Integer getExtraAmenityCharge() {
        return extraAmenityCharge;
    }

    public void setExtraAmenityCharge(Integer extraAmenityCharge) {
        this.extraAmenityCharge = extraAmenityCharge;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getPromotionDiscount() {
        return promotionDiscount;
    }

    public void setPromotionDiscount(Integer promotionDiscount) {
        this.promotionDiscount = promotionDiscount;
    }

    public Integer getAdvance() {
        return advance;
    }

    public void setAdvance(Integer advance) {
        this.advance = advance;
    }

    public Integer getBookingCharge() {
        return bookingCharge;
    }

    public void setBookingCharge(Integer bookingCharge) {
        this.bookingCharge = bookingCharge;
    }

    public Boolean getAdvanceOnlyEnable() {
        return advanceOnlyEnable;
    }

    public void setAdvanceOnlyEnable(Boolean advanceOnlyEnable) {
        this.advanceOnlyEnable = advanceOnlyEnable;
    }

    public Integer getPayableAdvance() {
        return payableAdvance;
    }

    public void setPayableAdvance(Integer payableAdvance) {
        this.payableAdvance = payableAdvance;
    }

    public Integer getCharge() {
        return charge;
    }

    public void setCharge(Integer charge) {
        this.charge = charge;
    }
}
