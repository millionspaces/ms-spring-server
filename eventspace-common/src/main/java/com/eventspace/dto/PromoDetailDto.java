package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PromoDetailDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    private Integer id;
    private Set<Integer> spaces;
    private Integer toAllSpace;
    private String promoCode;
    private Integer discount;
    private Integer isFlatDiscount;
    private Long startDate;
    private Long endDate;
    private Long createDate;
    private Integer totalCount;
    private Integer userCount;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }

    public Set<Integer> getSpaces() {
        return spaces;
    }

    public void setSpaces(Set<Integer> spaces) {
        this.spaces = spaces;
    }

    public Integer getToAllSpace() {
        return toAllSpace;
    }

    public void setToAllSpace(Integer toAllSpace) {
        this.toAllSpace = toAllSpace;
    }

    public Integer getIsFlatDiscount() {
        return isFlatDiscount;
    }

    public void setIsFlatDiscount(Integer isFlatDiscount) {
        this.isFlatDiscount = isFlatDiscount;
    }

    public Long getCreateDate() {
        return createDate;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }
}
