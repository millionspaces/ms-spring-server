package com.eventspace.dto;

import java.io.Serializable;

public class RemarkDto implements Serializable {

    private static final long serialVersionUID = 7797455140996929742L;

    private Integer bookingId;
    private Boolean isManual;
    private String remark;

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Boolean getIsManual() {
        return isManual;
    }

    public void setIsManual(Boolean isManual) {
        this.isManual = isManual;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
