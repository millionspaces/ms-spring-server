package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Auxenta on 5/2/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MobileSyncResponseDatadto implements Serializable {
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    private List<AmenityDto> amenity;

    private List<EventTypeDto> eventType;

    private List<CancellationPolicyDto> cancellationPolicy;

    private List<CommonDto> rules;

    private List<CommonDto> seatingArrangements;


    public List<AmenityDto> getAmenity() {
        return amenity;
    }

    public void setAmenity(List<AmenityDto> amenity) {
        this.amenity = amenity;
    }

    public List<EventTypeDto> getEventType() {
        return eventType;
    }

    public void setEventType(List<EventTypeDto> eventType) {
        this.eventType = eventType;
    }

    public List<CancellationPolicyDto> getCancellationPolicy() {
        return cancellationPolicy;
    }

    public void setCancellationPolicy(List<CancellationPolicyDto> cancellationPolicy) {
        this.cancellationPolicy = cancellationPolicy;
    }

    public List<CommonDto> getRules() {
        return rules;
    }

    public void setRules(List<CommonDto> rules) {
        this.rules = rules;
    }

    public List<CommonDto> getSeatingArrangements() {
        return seatingArrangements;
    }

    public void setSeatingArrangements(List<CommonDto> seatingArrangements) {
        this.seatingArrangements = seatingArrangements;
    }
}
