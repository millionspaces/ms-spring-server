package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentSummaryDto implements Serializable {

    private static final long serialVersionUID = 7797455140996929742L;

    private Integer bookingId;
    private String referenceId;
    private String space;
    private String organization;
    private String address;
    private Long date;
    private Set<CommonDto> reservationTime;
    private Integer total;
    private Long dateDiff;

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getSpace() {
        return space;
    }

    public void setSpace(String space) {
        this.space = space;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Set<CommonDto> getReservationTime() {
        return reservationTime;
    }

    public void setReservationTime(Set<CommonDto> reservationTime) {
        this.reservationTime = reservationTime;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getDateDiff() {
        return dateDiff;
    }

    public void setDateDiff(Long dateDiff) {
        this.dateDiff = dateDiff;
    }
}
