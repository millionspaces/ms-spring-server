package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentVerifyDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    private Integer bookingId;
    private String pdf;
    private Integer verify;
    private String proofFromGuest;
    private String proofToHost;

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public Integer getVerify() {
        return verify;
    }

    public void setVerify(Integer verify) {
        this.verify = verify;
    }

    public String getProofFromGuest() {
        return proofFromGuest;
    }

    public void setProofFromGuest(String proofFromGuest) {
        this.proofFromGuest = proofFromGuest;
    }

    public String getProofToHost() {
        return proofToHost;
    }

    public void setProofToHost(String proofToHost) {
        this.proofToHost = proofToHost;
    }
}
