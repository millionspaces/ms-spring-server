package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpayResponseDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    private String service_code;

    private String upay_txn_ref_no;

    private String merchant_txn_ref_no;

    private String txn_status_code;

    private String txn_status_message;

    private String txn_completed_on;

    private String signature;

    public String getService_code() {
        return service_code;
    }

    public void setService_code(String service_code) {
        this.service_code = service_code;
    }

    public String getUpay_txn_ref_no() {
        return upay_txn_ref_no;
    }

    public void setUpay_txn_ref_no(String upay_txn_ref_no) {
        this.upay_txn_ref_no = upay_txn_ref_no;
    }

    public String getMerchant_txn_ref_no() {
        return merchant_txn_ref_no;
    }

    public void setMerchant_txn_ref_no(String merchant_txn_ref_no) {
        this.merchant_txn_ref_no = merchant_txn_ref_no;
    }

    public String getTxn_status_code() {
        return txn_status_code;
    }

    public void setTxn_status_code(String txn_status_code) {
        this.txn_status_code = txn_status_code;
    }

    public String getTxn_status_message() {
        return txn_status_message;
    }

    public void setTxn_status_message(String txn_status_message) {
        this.txn_status_message = txn_status_message;
    }

    public String getTxn_completed_on() {
        return txn_completed_on;
    }

    public void setTxn_completed_on(String txn_completed_on) {
        this.txn_completed_on = txn_completed_on;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "UpayResponseDto{" +
                "service_code='" + service_code + '\'' +
                ", upay_txn_ref_no='" + upay_txn_ref_no + '\'' +
                ", merchant_txn_ref_no='" + merchant_txn_ref_no + '\'' +
                ", txn_status_code='" + txn_status_code + '\'' +
                ", txn_status_message='" + txn_status_message + '\'' +
                ", txn_completed_on='" + txn_completed_on + '\'' +
                ", signature='" + signature + '\'' +
                '}';
    }
}
