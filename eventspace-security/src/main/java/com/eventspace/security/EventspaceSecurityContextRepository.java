/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security;

import com.eventspace.security.domain.EventspaceAuthenticationToken;
import com.eventspace.security.service.SecurityService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.SecurityContextRepository;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The Class RestSecurityContextRepository. For loading and storing the security
 * context
 *
 * @author Auxenta Inc.
 *
 */
public class EventspaceSecurityContextRepository implements SecurityContextRepository {

	/** The security service. */
	private SecurityService securityService;

	/** The security cookie name. */
	@Value("${cookie.name}")
	private String cookieName;

	@Value("${cookie.cookiePrifix}")
	private String cookiePrifix;

	/** The Constant APPENDER. */
	private static final String APPENDER = "APP";

	/**
	 * Load the context of the request & response. The HttpRequestResponseHolder
	 * is simply a container for the incoming request and response objects
	 * 
	 * @param requestResponseHolder
	 *            the request response holder
	 * @return the security context
	 */
	public SecurityContext loadContext(HttpRequestResponseHolder requestResponseHolder) {

		HttpServletRequest request = requestResponseHolder.getRequest();
		HttpServletResponse response = requestResponseHolder.getResponse();

		request.getSession().invalidate();
		EventspaceAuthenticationToken token_old = null;

		EventspaceSecurityContext restSecurityContext_old = (EventspaceSecurityContext) securityService
				.retrieveCookieValue(cookiePrifix + APPENDER, request);

		EventspaceSecurityContext restSecurityContext = (EventspaceSecurityContext) securityService
				.retrieveCookieValue(cookieName, request);

		if (restSecurityContext_old != null && restSecurityContext_old.getAuthentication() != null) {
			token_old = (EventspaceAuthenticationToken) restSecurityContext_old.getAuthentication();
		}

		if (token_old != null) {
			if (restSecurityContext == null || restSecurityContext.getAuthentication() == null) {
				restSecurityContext = securityService.createRestSecurityContext(token_old, request, response);
			}
			securityService.expireSecurityContextCookie(cookiePrifix + APPENDER, request, response);
		}

		if (restSecurityContext != null && restSecurityContext.getAuthentication() != null) {
			return restSecurityContext;
		}

		return SecurityContextHolder.createEmptyContext();
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean containsContext(HttpServletRequest request) {

		Cookie securityContextCookie = securityService.retrieveCookie(cookieName, request);

		if (securityContextCookie == null) {
			securityContextCookie = securityService.retrieveCookie(cookiePrifix + APPENDER, request);
		}

		return securityContextCookie != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.web.context.SecurityContextRepository#
	 * saveContext(org.springframework.security.core.context.SecurityContext,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	public void saveContext(SecurityContext arg0, HttpServletRequest arg1, HttpServletResponse arg2) {

	}

	/**
	 * Sets the security service.
	 * 
	 * @param tribezSecurityService
	 *            the new security service
	 */
	public void setSecurityService(SecurityService tribezSecurityService) {
		this.securityService = tribezSecurityService;
	}

}
