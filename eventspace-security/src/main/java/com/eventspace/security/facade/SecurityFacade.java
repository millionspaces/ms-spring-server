/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security.facade;

import com.eventspace.dto.UserContext;

import javax.servlet.http.HttpServletRequest;

/**
 * The Interface SecurityFacade.
 */
public interface SecurityFacade {

	/**
	 * Gets the user context.
	 * 
	 * @param request
	 *            the request
	 * @return the user context
	 * @throws Exception
	 *             the exception
	 */
	UserContext getUserContext(HttpServletRequest request) throws Exception;
	
	Boolean isAuthenticatedRequest(HttpServletRequest request);

}
