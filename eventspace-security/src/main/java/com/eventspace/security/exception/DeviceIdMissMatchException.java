/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security.exception;

import org.springframework.security.authentication.BadCredentialsException;

/**
 * The Class DeviceIdMissMatchException.
 */
public class DeviceIdMissMatchException extends BadCredentialsException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2533313878000699610L;

	/**
	 * Instantiates a new device id miss match exception.
	 * 
	 * @param exceptionMsgVal
	 *            the exception msg val
	 * @param e
	 *            the e
	 */
	public DeviceIdMissMatchException(final String exceptionMsgVal, final Throwable e) {
		super(exceptionMsgVal, e);
	}

	/**
	 * Instantiates a new device id miss match exception.
	 * 
	 * @param exceptionMsgVal
	 *            the exception msg val
	 */
	public DeviceIdMissMatchException(final String exceptionMsgVal) {
		super(exceptionMsgVal);
	}

}
