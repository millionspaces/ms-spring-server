/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security.exception;

import org.springframework.security.authentication.BadCredentialsException;

/**
 * The Class UserNotActivatedException.
 */
public class UserNotActivatedException extends BadCredentialsException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2533313878000699610L;

	/**
	 * Instantiates a new user not activated exception.
	 * 
	 * @param exceptionMsgVal
	 *            the exception msg val
	 * @param e
	 *            the e
	 */
	public UserNotActivatedException(final String exceptionMsgVal, final Throwable e) {
		super(exceptionMsgVal, e);
	}

	/**
	 * Instantiates a new user not activated exception.
	 * 
	 * @param exceptionMsgVal
	 *            the exception msg val
	 */
	public UserNotActivatedException(final String exceptionMsgVal) {
		super(exceptionMsgVal);
	}

}
