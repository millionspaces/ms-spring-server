/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security.provider;

import com.eventspace.domain.User;
import com.eventspace.dto.FbGoogleJsonDto;
import com.eventspace.dto.UserDetailsDto;
import com.eventspace.enumeration.BooleanEnum;
import com.eventspace.enumeration.LoginMethodEnum;
import com.eventspace.security.domain.EventspaceAuthenticationToken;
import com.eventspace.security.exception.DeviceIdMissMatchException;
import com.eventspace.security.exception.DeviceNotActivatedException;
import com.eventspace.security.exception.GoogleAuthenticationFailureException;
import com.eventspace.security.exception.UserNotActivatedException;
import com.eventspace.security.service.FacebookData;
import com.eventspace.security.service.ObtainGoogleDetails;
import com.eventspace.service.UserManagementService;
import com.eventspace.util.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The Class EventspaceAuthenticationProvider.
 */
public class EventspaceAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private UserManagementService userManagementService;

	/** The authority list. */
	private static List<GrantedAuthority> GUEST_AUTHORITY_LIST = new ArrayList<>();

	private static List<GrantedAuthority> USER_AUTHORITY_LIST = new ArrayList<>();

	private static List<GrantedAuthority> ADMIN_AUTHORITY_LIST = new ArrayList<>();

	private static List<GrantedAuthority> FINANCE_AUTHORITY_LIST = new ArrayList<>();

	/** The logger. */
	private static Logger logger = Logger.getLogger(EventspaceAuthenticationProvider.class);

	static {
		GrantedAuthority authorityGuest =new GrantedAuthority() {

			private static final long serialVersionUID = 1L;

			@Override
			public String getAuthority() {
				return "ROLE_GUEST";
			}
		};

		GrantedAuthority authorityUser =new GrantedAuthority() {

			private static final long serialVersionUID = 1L;

			@Override
			public String getAuthority() {
				return "ROLE_USER";
			}
		};

		GrantedAuthority authorityAdmin=new GrantedAuthority() {

			private static final long serialVersionUID = 1L;

			@Override
			public String getAuthority() {
				return "ROLE_ADMIN";
			}
		};

		GrantedAuthority authorityFinance=new GrantedAuthority() {

			private static final long serialVersionUID = 1L;

			@Override
			public String getAuthority() {
				return "ROLE_FINANCE";
			}
		};

		GUEST_AUTHORITY_LIST.add(authorityGuest);
		USER_AUTHORITY_LIST.add(authorityGuest);
		USER_AUTHORITY_LIST.add(authorityUser);
		FINANCE_AUTHORITY_LIST.add(authorityGuest);
		FINANCE_AUTHORITY_LIST.add(authorityUser);
		FINANCE_AUTHORITY_LIST.add(authorityFinance);
		ADMIN_AUTHORITY_LIST.add(authorityGuest);
		ADMIN_AUTHORITY_LIST.add(authorityUser);
		ADMIN_AUTHORITY_LIST.add(authorityFinance);
		ADMIN_AUTHORITY_LIST.add(authorityAdmin);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.authentication.AuthenticationProvider#
	 * authenticate(org.springframework.security.core.Authentication)
	 */
	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
		return validate(authentication);
	}

	/**
	 * Validate.
	 *
	 * @param authentication
	 *            the authentication
	 * @return the eventspace authentication token
	 */
	private EventspaceAuthenticationToken validate(final Authentication authentication) {

		EventspaceAuthenticationToken authenticationToken = (EventspaceAuthenticationToken) authentication;
		return validateByUserCredentials(authenticationToken);

	}

	/**
	 * Validate by user credentials.
	 *
	 * @param authentication
	 *            the authentication
	 * @return the eventspace authentication token
	 */
	private EventspaceAuthenticationToken validateByUserCredentials(final Authentication authentication) {
		EventspaceAuthenticationToken authenticationToken = (EventspaceAuthenticationToken) authentication;

		try {

			UserDetailsDto userDto = null;
			LoginMethodEnum loginMethod = LoginMethodEnum.DEFAULT;

			String principal = (String) authenticationToken.getPrincipal();
			String password = (String) authenticationToken.getCredentials();
			String googleToken = (String) authenticationToken.getGoogleToken();
			String facebookToken = (String) authenticationToken.getFacebookToken();

			if (StringUtils.isEmpty(principal) && StringUtils.isEmpty(password)) {
				if (!StringUtils.isEmpty(googleToken) && StringUtils.isEmpty(facebookToken))
					loginMethod = LoginMethodEnum.GOOGLE;
				else
					loginMethod = LoginMethodEnum.FACEBOOK;
			}

			switch (loginMethod) {
			case DEFAULT:
				logger.info("Default User ---> get called");

				userDto = userManagementService.getUserByEmail(principal);

				if (!Optional.ofNullable(userDto).isPresent() && !Optional.ofNullable(password).isPresent()){
					userDto=userManagementService.createGuest(principal);
				}
				else if(Optional.ofNullable(password).isPresent() && !userDto.getPassword().equals(password))
					throw new BadCredentialsException("Bad username or password");
				/*if(userDto.getActive().equals(BooleanEnum.FALSE.value()))
					throw new BadCredentialsException("verify the email first");*/

				break;
			case GOOGLE:
				
				logger.info("Google User ---> get called");

				FbGoogleJsonDto dto = ObtainGoogleDetails.getUserDetails(googleToken);
				principal = dto.getEmail();
				userDto = userManagementService.getUserByEmail(dto.getEmail());
				if (userDto == null)
					userDto = userManagementService.saveGoogleFBData(dto);
				break;
			case FACEBOOK:
				
				logger.info("Facebook User ---> get called");

				FbGoogleJsonDto fbdto = FacebookData.getUserDetails(facebookToken);
				principal = fbdto.getEmail();
				userDto = userManagementService.getUserByEmail(fbdto.getEmail());
				if (userDto == null)
					userDto = userManagementService.saveGoogleFBData(fbdto);
				break;
			}

			if (userDto == null)
				throw new BadCredentialsException("Authentication Failed");


			EventspaceAuthenticationToken newToken;
			User user=userManagementService.getUserByEmailPri(principal);

			if (!loginMethod.equals(LoginMethodEnum.DEFAULT) ||
					(loginMethod.equals(LoginMethodEnum.DEFAULT) && Optional.ofNullable(password).isPresent())
							&& !user.getPassword().equals("")) {
				if (user.getRole().equals("ADMIN")) {
					newToken = new EventspaceAuthenticationToken(principal, password,
							ADMIN_AUTHORITY_LIST);
				} else if (user.getRole().equals("FUSER")) {
					newToken = new EventspaceAuthenticationToken(principal, password,
							FINANCE_AUTHORITY_LIST);
				} else {
					newToken = new EventspaceAuthenticationToken(principal, password,
							USER_AUTHORITY_LIST);
				}
			}else{
				newToken = new EventspaceAuthenticationToken(principal, password,
						GUEST_AUTHORITY_LIST);
				user.setRole("GUEST");
			}

			newToken.setUserId(userDto.getId());
			newToken.setEmail(userDto.getEmail());

			return newToken;

		} catch (DeviceIdMissMatchException e) {
			throw new DeviceIdMissMatchException(e.getMessage());
		} catch (UserNotActivatedException e) {
			throw new UserNotActivatedException(e.getMessage());
		} catch (DeviceNotActivatedException e) {
			throw new DeviceNotActivatedException(e.getMessage());
		} catch (GoogleAuthenticationFailureException e) {
			throw new GoogleAuthenticationFailureException(e.getMessage());
		} catch (BadCredentialsException e) {
			throw new BadCredentialsException(e.getMessage());
		} catch (Exception e) {
			logger.error("Error validating with user credentials", e);
			throw new BadCredentialsException("Authentication Failed");
		}
	}

	/**
	 * Supports.
	 *
	 * @param authentication
	 *            the authentication
	 * @return true, if supports
	 */
	@Override
	public boolean supports(final Class<? extends Object> authentication) {

		return true;
	}

}
