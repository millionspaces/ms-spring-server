/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security.handler;

import com.eventspace.security.service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * The Class EventspaceSimpleUrlLogoutSuccessHandler.
 */
public class EventspaceSimpleUrlLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {

	/** The security service. */
	private SecurityService securityService;

	/** The cookie name. */
	private @Value("${cookie.name}")
	String cookieName;

	/** The Constant LOGGER. */
	//@Autowired
	//private SecurityFacade securityFacade;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(EventspaceSimpleUrlLogoutSuccessHandler.class);


	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.web.authentication.logout.
	 * SimpleUrlLogoutSuccessHandler
	 * #onLogoutSuccess(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse,
	 * org.springframework.security.core.Authentication)
	 */
	@Override
	public void onLogoutSuccess(final HttpServletRequest request, final HttpServletResponse response,
			final Authentication authentication) throws IOException, ServletException {
		try {
			//UserContext user = securityFacade.getUserContext(request);
			//LOGGER.info("Logout request procceding for " + user.getUsername());
		} catch (Exception e) {
			LOGGER.error("Error occurred while geting logged user : ", e);
		}
		securityService.removeSecurityContextCookie(cookieName, request, response);

		super.handle(request, response, authentication);

	}

	/**
	 * Sets the security service.
	 *
	 * @param securityService
	 *            the new security service
	 */
	public void setSecurityService(final SecurityService securityService) {
		this.securityService = securityService;
	}

}
