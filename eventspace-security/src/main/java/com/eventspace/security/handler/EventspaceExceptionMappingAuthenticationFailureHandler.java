/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security.handler;

import com.eventspace.security.exception.DeviceIdMissMatchException;
import com.eventspace.security.exception.DeviceNotActivatedException;
import com.eventspace.security.exception.GoogleAuthenticationFailureException;
import com.eventspace.security.exception.UserNotActivatedException;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * The Class EventspaceExceptionMappingAuthenticationFailureHandler.
 */
public class EventspaceExceptionMappingAuthenticationFailureHandler extends
		ExceptionMappingAuthenticationFailureHandler {

	/** The Constant STATUS_MESSAGE. */
	private static final String STATUS_MESSAGE = "localcoins-message";

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.web.authentication.
	 * ExceptionMappingAuthenticationFailureHandler
	 * #onAuthenticationFailure(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse,
	 * org.springframework.security.core.AuthenticationException)
	 */
	@Override
	public void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response,
			final AuthenticationException exception) throws IOException, ServletException {

		if (exception instanceof DeviceIdMissMatchException) {
			String loggedDevice = request.getParameter("device");
			// String email = request.getParameter("j_username");
			String email = exception.getMessage();
			setDefaultFailureUrl("/api/deviceIdNotMatching?device=" + loggedDevice + "&email=" + email);
		} else if (exception instanceof UserNotActivatedException) {
			setDefaultFailureUrl("/api/userNotActivated?email=" + exception.getMessage());
		} else if (exception instanceof DeviceNotActivatedException) {
			response.setHeader(STATUS_MESSAGE, exception.getMessage());
		} else if (exception instanceof GoogleAuthenticationFailureException) {
			response.setHeader(STATUS_MESSAGE, exception.getMessage());
		} else if (exception instanceof BadCredentialsException) {
			setDefaultFailureUrl("/api/userCredentialNotMatching");
		} else if (exception instanceof AuthenticationServiceException) {
			response.setStatus(HttpStatus.METHOD_NOT_ALLOWED.value());
			response.setHeader("Allow", "POST");
		}

		super.onAuthenticationFailure(request, response, exception);
	}

}
