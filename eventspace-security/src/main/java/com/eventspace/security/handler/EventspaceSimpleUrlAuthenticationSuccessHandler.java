/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security.handler;

import com.eventspace.security.domain.EventspaceAuthenticationToken;
import com.eventspace.security.service.SecurityService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * The Class EventspaceSimpleUrlAuthenticationSuccessHandler.
 */
public class EventspaceSimpleUrlAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    /**
     * The security service.
     */
    private SecurityService securityService;

    /**
     * The app host prefix.
     */
    @Value("${app.host.prefix}")
    private String appHostPrefix;

    /**
     * The app protocol.
     */
    @Value("${app.host.protocol}")
    private String appProtocol;

    @Value("${million.spaces.api.url}")
    private String apiUrl;

    /**
     * The login url.
     */
    @Value("${app.host.api.login.success.url}")
    private String loginUrl;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.security.web.authentication.
     * SimpleUrlAuthenticationSuccessHandler
     * #onAuthenticationSuccess(javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse,
     * org.springframework.security.core.Authentication)
     */
    @Override
    public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response,
                                        final Authentication authentication) throws IOException, ServletException {

        EventspaceAuthenticationToken authToken = (EventspaceAuthenticationToken) authentication;

        securityService.createRestSecurityContext(authToken, request, response);

        super.setDefaultTargetUrl("/" + loginUrl);
        //super.setDefaultTargetUrl(appProtocol + appHostPrefix + loginUrl);
        super.onAuthenticationSuccess(request, response, authentication);

    }

    /**
     * Sets the security service.
     *
     * @param securityService the new security service
     */
    public void setSecurityService(final SecurityService securityService) {
        this.securityService = securityService;
    }

}
