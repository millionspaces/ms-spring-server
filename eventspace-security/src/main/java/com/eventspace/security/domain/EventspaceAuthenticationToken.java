/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security.domain;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

/**
 * The Class LocalCoinsAuthenticationToken.
 */
public class EventspaceAuthenticationToken extends UsernamePasswordAuthenticationToken {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5838655938418033064L;

	/** The user id. */
	private Integer userId;
	
	private String googleToken;
	
	private String facebookToken;
	
	private String email;

	/**
	 * Instantiates a new local coins authentication token.
	 * 
	 * @param principal
	 *            the principal
	 * @param credentials
	 *            the credentials
	 */
	public EventspaceAuthenticationToken(final Object principal, final Object credentials, final String googleToken, final String facebookToken) {
		super(principal, credentials);
		this.googleToken = googleToken;
		this.facebookToken = facebookToken;
	}

	/**
	 * Instantiates a new local coins authentication token.
	 * 
	 * @param username
	 *            the username
	 * @param password
	 *            the password
	 * @param grantedAuthorities
	 *            the granted authorities
	 */
	public EventspaceAuthenticationToken(final String username, final String password,
			final List<GrantedAuthority> grantedAuthorities) {
		super(username, password, grantedAuthorities);
	}

	/**
	 * Gets the user id.
	 * 
	 * @return the user id
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 * 
	 * @param userIdVal
	 *            the new user id
	 */
	public void setUserId(Integer userIdVal) {
		this.userId = userIdVal;
	}


	public String getGoogleToken() {
		return googleToken;
	}
	
	public String getFacebookToken() {
		return facebookToken;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
