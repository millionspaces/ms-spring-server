/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security.service;

import com.eventspace.security.EventspaceSecurityContext;
import com.eventspace.security.domain.EventspaceAuthenticationToken;
import com.eventspace.security.service.cookie.CookieManagementService;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SecurityServiceImpl implements SecurityService {
	
	/** The cookie service. */
    private CookieManagementService cookieManagementService;
    
    /** The security cookie name. */
    @Value("${cookie.name}")
    private String securityCookieName;
    
    /** The max age. */
    @Value("${cookie.max.age}")
    private Integer maxAge;

	
	/**
     * {@inheritDoc}
     */
    public Object retrieveCookieValue(String key, HttpServletRequest request) {
        return this.cookieManagementService.retrieveCookieObject(key, request);
    }
    
    /**
     * {@inheritDoc}
     */
    public Cookie retrieveCookie(String key, HttpServletRequest request) {
        if (key == null || request == null) {
            return null;
        }
        return getCookie(key, request);
    }
    
    /**
     * {@inheritDoc}
     */
    public void removeSecurityContextCookie(String cookieName, HttpServletRequest request, HttpServletResponse response) {

        this.cookieManagementService.removeCookie(cookieName, request, response);

    }
    
    /**
     * {@inheritDoc}
     */
    public void expireSecurityContextCookie(String cookieName, HttpServletRequest request, HttpServletResponse response) {

        this.cookieManagementService.expireCookie(cookieName, request, response);

    }
    
    /**
     * {@inheritDoc}
     */
    public EventspaceSecurityContext createRestSecurityContext(EventspaceAuthenticationToken token, HttpServletRequest request,
                                                               HttpServletResponse response) {

    	EventspaceSecurityContext securityContext = new EventspaceSecurityContext();
    	securityContext.setAuthentication(token);

        addOrUpdateCookie(securityContext, securityCookieName, maxAge.intValue(), request, response);
        
        return securityContext;

    }
    
    /**
     * Adds the or update cookie.
     * 
     * @param object the object
     * @param cookieName the cookie name
     * @param maxAgeVal the max age val
     * @param request the request
     * @param response the response
     */
    private void addOrUpdateCookie(Object object, String cookieName, int maxAgeVal, HttpServletRequest request,
            HttpServletResponse response) {

        cookieManagementService.addOrUpdateCookie(object, cookieName, maxAgeVal, request, response);

    }
    
    /**
     * Gets cookie.
     *
     * @param name the name
     * @param request the request
     * @return Cookie
     */
    private Cookie getCookie(String name, HttpServletRequest request) {
    	name = name.trim();
        if (request.getCookies() != null && request.getCookies().length > 0) {
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equalsIgnoreCase(name))
                    return cookie;
            }
        }
        return null;
    }
    
    /**
     * Gets the cookie service.
     * 
     * @return the cookie service
     */
    public CookieManagementService getCookieManagementService() {
        return cookieManagementService;
    }

    /**
     * Sets the cookie service.
     *
     * @param cookieService the new cookie management service
     */
    public void setCookieManagementService(CookieManagementService cookieService) {
        this.cookieManagementService = cookieService;
    }
}
