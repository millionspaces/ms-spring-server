package com.eventspace.security.service;

import com.eventspace.dto.FbGoogleJsonDto;
import com.eventspace.exception.EventspaceException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

public class ObtainGoogleDetails {
	
	private static final Log logger = LogFactory.getLog(ObtainGoogleDetails.class);

	private static RestTemplate restTemplate = new RestTemplate();

	/**
	 * Gets the user details.
	 * 
	 * @param accessToken
	 *            the access token
	 * @return the user details
	 */
	public static FbGoogleJsonDto getUserDetails(final String accessToken) {
		
		logger.info("ObtainGoogleDetails class getUserDetails method ---> get called");

		FbGoogleJsonDto retUserDto = null;
		logger.info("Google accessToken :" + accessToken);


		// Prepare acceptable media type
		List<MediaType> acceptableMediaTypes = new ArrayList<>();
		acceptableMediaTypes.add(MediaType.APPLICATION_JSON);

		// Prepare header
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(acceptableMediaTypes);
		headers.set("Authorization", "Bearer " + accessToken);
		HttpEntity<FbGoogleJsonDto> entity = new HttpEntity<>(headers);


		// Send the request as GET
		try {
			String url = "https://www.googleapis.com/userinfo/v2/me";
			ResponseEntity<FbGoogleJsonDto> result = restTemplate.exchange(url, HttpMethod.GET, entity,
					FbGoogleJsonDto.class);
			retUserDto = result.getBody();

		} catch (Exception e) {
            logger.error("getUserDetails exception----->{}",e);

			throw new EventspaceException("google login  failed");

		}

		return retUserDto;

	}

}
