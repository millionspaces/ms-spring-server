/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security.service;

import com.eventspace.security.EventspaceSecurityContext;
import com.eventspace.security.domain.EventspaceAuthenticationToken;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface SecurityService {
	
	/**
     * Retrieve cookie value.
     * 
     * @param key the key
     * @param request the request
     * @return the object
     */
    Object retrieveCookieValue(String key, HttpServletRequest request);
    
    /**
     * Retrieve cookie.
     * 
     * @param key the key
     * @param request the request
     * @return the cookie
     */
    Cookie retrieveCookie(String key, HttpServletRequest request);
    
    /**
     * Removes the security context cookie by cookie name.
     * 
     * @param cookieName the cookie name
     * @param request the request
     * @param response the response
     */
    void removeSecurityContextCookie(String cookieName, HttpServletRequest request, HttpServletResponse response);
    
    /**
     * Creates the security context.
     * 
     * @param token the token
     * @param request the request
     * @param response the response
     * @return TribezSecurityContext
     */
    EventspaceSecurityContext createRestSecurityContext(EventspaceAuthenticationToken token, HttpServletRequest request,
            HttpServletResponse response);
    
    /**
     * Expires the cookie by cookie name.
     * 
     * @param cookieName the cookie name
     * @param request the request
     * @param response the response
     */
    void expireSecurityContextCookie(String cookieName, HttpServletRequest request, HttpServletResponse response);

}
