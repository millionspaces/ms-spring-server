/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security.service.cookie;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public interface CookieManagementService {

	/**
	 * Retrieve cookie object.
	 * 
	 * @param key
	 *            
	 * @param request
	 *           
	 * @return object
	 */
	Object retrieveCookieObject(String key, HttpServletRequest request);
	
	 /**
 	 * Retrieve the cookie after decrypting.
 	 *
 	 * @param key the key
 	 * @param request the request
 	 * @return the decrypted string of the cookie
 	 */
    String retrieveCookieValue(String key, HttpServletRequest request);
    
    /**
     * Add if cookie doesn't exist or update otherwise with the given values.
     *
     * @param object the object
     * @param cookieName the cookie name
     * @param maxAge the max age
     * @param request the request
     * @param response the response
     */
    void addOrUpdateCookie(Object object, String cookieName, int maxAge, HttpServletRequest request,
            HttpServletResponse response);
    
    /**
     * Add or update cookie.
     *
     * @param cookieName the cookie name
     * @param maxAge the max age
     * @param content the content
     * @param path the path
     * @param request the request
     * @param response the response
     */
    void addOrUpdateCookieContent(String cookieName, int maxAge, String content, String path, HttpServletRequest request,
            HttpServletResponse response);
    
    /**
     * Removes the cookie.
     *
     * @param cookieName the cookie name
     * @param request the request
     * @param response the response
     */
    void removeCookie(String cookieName, HttpServletRequest request, HttpServletResponse response);
    
    /**
     * Expires the cookie.
     *
     * @param cookieName the cookie name
     * @param request the request
     * @param response the response
     */
    void expireCookie(String cookieName, HttpServletRequest request, HttpServletResponse response);

}
