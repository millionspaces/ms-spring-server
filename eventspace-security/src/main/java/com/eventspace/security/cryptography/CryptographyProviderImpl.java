package com.eventspace.security.cryptography;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The Class CryptographyProviderImpl.
 */
public class CryptographyProviderImpl implements CryptographyProvider {

	@Autowired
	private StandardPBEStringEncryptor encryptorAndDecryptor;

	public final String encryptContent(final String rawContent) {
		return encryptorAndDecryptor.encrypt(rawContent);
	}

	@Override
	public final String decryptContent(final String encryptedContent) {
		return encryptorAndDecryptor.decrypt(encryptedContent);
	}
}
