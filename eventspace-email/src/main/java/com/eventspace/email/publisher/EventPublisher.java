package com.eventspace.email.publisher;

import com.eventspace.email.dto.EmailMetaData;

public interface EventPublisher {

	void proceedEmailEvent(EmailMetaData emailMetaData);

}
