package com.eventspace.email.consumer.Impl;

import com.eventspace.email.dto.EmailMetaData;
import com.eventspace.email.sender.EmailSender;
import com.eventspace.exception.EventspaceException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

@Component
public class EmailConsumerImpl implements MessageListener {

	/** The Constant logger. */
	private static final Log logger = LogFactory.getLog(EmailConsumerImpl.class);

	@Autowired
	private EmailSender emailSender;

	@Override
	public void onMessage(final Message message) {

		logger.info("onMessage -> Get Called");

		try {
			if (message instanceof ObjectMessage) {

				EmailMetaData emailMetaData = (EmailMetaData) ((ObjectMessage) message).getObject();

				logger.info(
						String.format("onMessage -> Casted the message Successfully... [%s]", emailMetaData));
				if(emailMetaData.isWithAttachment()){
					emailSender.sendEmailWithAttachment(emailMetaData, emailMetaData.getFileName(),emailMetaData.getOutput());
				}else{
					emailSender.sendEmail(emailMetaData);
				}
				

			} else {
				logger.error("onMessage -> Received message of unknown type");
				throw new EventspaceException("Message type isn't compatible");
			}
		} catch (Exception ex) {
			logger.error("onMessage -> Exception : ", ex);
		}
	}

}
