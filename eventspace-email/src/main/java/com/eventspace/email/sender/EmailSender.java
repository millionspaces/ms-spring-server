/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.email.sender;

import com.eventspace.email.dto.EmailMetaData;
import com.eventspace.email.publisher.EventPublisher;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;
import org.springframework.ui.velocity.VelocityEngineUtils;

import javax.activation.DataSource;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.util.Arrays;
import java.util.List;

@Component
public class EmailSender {

	/** The Constant logger. */
	private static final Log logger = LogFactory.getLog(EmailSender.class);

	/** The Constant ENCODE_TYPE. */
	protected static final String ENCODE_TYPE = "UTF-8";

	/** The mail sender. */
	@Autowired
	private JavaMailSender mailSender;

	/** The velocity engine. */
	@Autowired
	private VelocityEngine velocityEngine;

	/** The sender email. */
	@Value("${sender.email}")
	private String senderEmail;

	@Value("${admin.email}")
	private String adminEmail;

	/** Sender name. */
	@Value("${sender.name}")
	private String senderName;

	/** The email session. */
	@Autowired
	private Session emailSession;

	@Autowired
	EventPublisher eventPublisher;


	/**
	 * Send email.
	 *
	 * @param metaData
	 *            the meta data
	 */
	public void sendEmail(final EmailMetaData metaData) {
		try {

			logger.info(String.format("sendEmail -> get called with [%s]", metaData));

			MimeMessagePreparator preparator = new MimeMessagePreparator() {

				@Override
				public void prepare(MimeMessage mimeMessage) throws Exception {

					MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);

					List<String> emailArrayList = Arrays.asList(metaData.getToEmailAddresses().split("\\|"));
					InternetAddress[] addressList = new InternetAddress[emailArrayList.size()];

					int i = 0;
					for (String address : emailArrayList)
						addressList[i++] = new InternetAddress(address);

					message.setFrom(new InternetAddress(senderEmail, senderName));
					message.setTo(addressList);
					if (metaData.getBccRequire()!=null && metaData.getBccRequire())
						message.setBcc(adminEmail);
					message.setSubject(metaData.getSubject());

					String body = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
							metaData.getVmFile(), ENCODE_TYPE, metaData.getData());
					message.setText(body, true);


				}
			};

			mailSender.send(preparator);

		} catch (Exception ex) {
			logger.error("Exception occurred." + ex);
		}
	}
	
	public void sendEmailWithAttachment(final EmailMetaData metaData, final String fileName, final byte[] outputStreem) {
		try {

			logger.info(String.format("sendEmail -> get called with [%s]", metaData));

			MimeMessagePreparator preparator = new MimeMessagePreparator() {

				@Override
				public void prepare(MimeMessage mimeMessage) throws Exception {

					MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);

					List<String> emailArrayList = Arrays.asList(metaData.getToEmailAddresses().split("\\|"));
					InternetAddress[] addressList = new InternetAddress[emailArrayList.size()];

					int i = 0;
					for (String address : emailArrayList)
						addressList[i++] = new InternetAddress(address);

					message.setTo(addressList);

					message.setFrom(new InternetAddress(senderEmail, senderName));
					message.setSubject(metaData.getSubject());

					String body = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
							metaData.getVmFile(), ENCODE_TYPE, metaData.getData());
					message.setText(body, true);
					
					DataSource aAttachment = new  ByteArrayDataSource(outputStreem,"application/pdf");
					message.addAttachment(fileName, aAttachment);

				}
			};

			mailSender.send(preparator);

		} catch (Exception ex) {
			logger.error("Exception occurred." + ex);
		}
	}
}
