package com.eventspace.ws.mobile.controller;

import com.eventspace.dto.*;
import com.eventspace.security.facade.SecurityFacade;
import com.eventspace.service.BookingService;
import com.eventspace.service.CommonService;
import com.eventspace.service.SpaceService;
import com.eventspace.service.UserManagementService;
import com.eventspace.util.LogMessages;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/mobile")
public class MobileController {

    private static final Log LOGGER = LogFactory.getLog(MobileController.class);

    @Autowired
    private CommonService commonService;

    @Autowired
    private SpaceService spaceService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private SecurityFacade securityFacade;

    @Autowired
    private UserManagementService userService;


    @GetMapping(value = "/batch/v1")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> getAllStaticData() {
        LOGGER.info(String.format(LogMessages.MC_ALL_STAT_DATE));
        return commonService.getAllStaticData();
    }

    @PostMapping(value = "/batch/sync/v1")
    @ResponseStatus(HttpStatus.OK)
    public MobileSyncResponseDatadto syncDatas(@RequestBody List<MobileSyncRequestDatadto> mobileSyncRequestDatadtos) {
        LOGGER.info(String.format(LogMessages.MC_SYNC_DATA));
        return commonService.syncDataForMobile(mobileSyncRequestDatadtos);
    }

    @PostMapping("/space/asearch/page/{page}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> paginatedAdvanceSearch(@RequestBody AdvanceSearchDto advanceSearchDto, @PathVariable Integer page) {
        LOGGER.info(String.format(LogMessages.MC_PAGINATED_ADVANCE_SEARACH, page));
        Map<String, Object> result = spaceService.advanceSearch(advanceSearchDto, page);
        return result;
    }

    @GetMapping("/space/{id}")
    @ResponseStatus(HttpStatus.OK)
    public SpaceDetailDto getSpace(@PathVariable Integer id, @RequestParam(required = false) Integer[] eventId) {
        LOGGER.info(String.format(LogMessages.MC_SPACE_GET));
        SpaceDetailDto result = spaceService.getSpace(id, eventId, false);
        return result;
    }

    @PostMapping("/space/promo/v1")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> checkPromo(@RequestBody PromoCheckDto promoCheckDto, HttpServletRequest request) {
        LOGGER.info(String.format(LogMessages.MC_PROMO_CHECK, promoCheckDto.getPromoCode(), promoCheckDto.getSpace()));
        UserContext user = null;
        try {
            user = securityFacade.getUserContext(request);
        } catch (Exception e) {
            LOGGER.info("user didn't login");
        } finally {
            return bookingService.checkPromoCode(promoCheckDto.getSpace(), promoCheckDto.getPromoCode(), user);
        }
    }

    @GetMapping("/calendarDetails/space/{id}")
    @ResponseStatus(HttpStatus.OK)
    public SpaceDetailDto getSpaceCalendarDetails(@PathVariable Integer id) {
        LOGGER.info(String.format(LogMessages.MC_SPACE_GET_CALENDAR, id));
        return spaceService.spaceCalendarDetails(id);
    }

    @PutMapping("/user/mobile/v1")
    @ResponseStatus(HttpStatus.OK)
    public ResponseDto updateUserMobile(@RequestBody UserDetailsDto userDetailsDto, HttpServletRequest request)
            throws Exception {
        ResponseDto responseDto = new ResponseDto();
        UserContext user = securityFacade.getUserContext(request);
        LOGGER.info(String.format(LogMessages.MC_USER_UPDATE_MOBILE, user.getEmail()));
        if (!user.getRole().equals("ROLE_GUEST"))
            userService.updateUserMobileNumber(user.getEmail(), userDetailsDto.getMobileNumber());
        else
            userService.updateGuestMobileNumber(user.getEmail(), userDetailsDto.getMobileNumber());

        responseDto.setStatus(200);
        responseDto.setMessage("success");
        return responseDto;
    }
}
