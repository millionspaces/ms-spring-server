package com.eventspace.ws.controller;

import com.eventspace.dto.SpaceDetailDto;
import com.eventspace.service.SpaceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(value = "/api/similarSpaces")
public class SimilarSpacesController {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(SimilarSpacesController.class);
    /**
     * The space service.
     */
    @Autowired
    private SpaceService spaceService;

    @GetMapping("/space/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Set<SpaceDetailDto> getSimilarSpaces(@PathVariable Integer id, @RequestParam(required = false) Integer[] eventId) {
        LOGGER.info("getSimilarSpaces ----->\t"+id);
        return spaceService.findSimilarSpaces(id,eventId,false);
    }

    @GetMapping("/space/v2/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Set<SpaceDetailDto> getSimilarSpaces2(@PathVariable Integer id, @RequestParam(required = false) Integer[] eventId) {
        LOGGER.info("getSimilarSpaces2 ----->\t"+id);
        return spaceService.findSimilarSpaces(id,eventId,true);
    }
}
