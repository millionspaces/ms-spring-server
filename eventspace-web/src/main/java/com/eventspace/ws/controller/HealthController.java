package com.eventspace.ws.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HealthController {

    private static final Log LOGGER = LogFactory.getLog(HealthController.class);

    @GetMapping(value = "/")
    @ResponseStatus(HttpStatus.OK)
    public String healthCheck(){
        LOGGER.info("healthCheck method in CommonController-----> get call");
        return "OK";
    }

}
