/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.ws.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * The Class EmailController.
 */
@RestController
@RequestMapping(value = "/email")
public class EmailController {

    /**
     * The Constant logger.
     */
    private static final Log LOGGER = LogFactory.getLog(EmailController.class);

    /**
     * Send mail.
     *
     * @return the String
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public String sendMails() {
        LOGGER.info("sendMails method in EmailController-----> get call");
        return "Success";

    }

}
