/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.ws.controller;

import com.eventspace.dto.*;
import com.eventspace.enumeration.UserCreateEnum;
import com.eventspace.security.facade.SecurityFacade;
import com.eventspace.service.SpaceService;
import com.eventspace.service.UserManagementService;
import com.eventspace.util.LogMessages;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * The Class UserController.
 */
@RestController
@RequestMapping(value = "/api/user")
public class UserController {

    /**
     * The Constant logger.
     */
    private static final Log logger = LogFactory.getLog(UserController.class);


    /**
     * The user manage service.
     */
    @Autowired
    private UserManagementService userManageService;

    /**
     * The security facade.
     */
    @Autowired
    private SecurityFacade securityFacade;

    /**
     * The space service.
     */
    @Autowired
    private SpaceService spaceService;

    /**
     * Creates the.
     *
     * @param userDetailsDto the user details dto
     * @param request        the request
     * @param response       the response
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public final String create(@RequestBody UserDetailsDto userDetailsDto, HttpServletRequest request,
                               HttpServletResponse response) throws IOException {
        logger.info("create method -----> get call");
        UserCreateEnum userCreateEnum = userManageService.create(userDetailsDto,request.getHeader("Origin"));
        if (userCreateEnum == UserCreateEnum.EXISTING) {
            response.sendRedirect(request.getContextPath() + "/api/user/already_exist");
            return null;
        } else {
            UserDetailsDto userDetailDto = userManageService.getUserByEmail(userDetailsDto.getEmail());
            return "User creation success userId: " + userDetailDto.getId();
        }
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public final Map<String,Object> createUser(@RequestBody UserDetailsDto userDetailsDto, HttpServletRequest request,
                                               HttpServletResponse response) throws IOException {
        Map<String,Object> responseMap=new HashMap<>();
        UserCreateEnum userCreateEnum = userManageService.create(userDetailsDto,request.getHeader("Origin"));
        if (userCreateEnum == UserCreateEnum.EXISTING) {
            responseMap.put("message","User already exists");
            responseMap.put("status",200);
        } else {
            UserDetailsDto userDetailDto = userManageService.getUserByEmail(userDetailsDto.getEmail());
            responseMap.put("message","User creation success");
            responseMap.put("userId",userDetailDto.getId());
            responseMap.put("status",200);
        }
        return responseMap;
    }
    /**
     * Reset password.
     *
     * @param key        the key
     * @return the string
     */
    @PostMapping(value = "/verify")
    public Boolean resetPassword(@RequestBody CommonDto key) {
        return userManageService.verifyEmailAddress(key.getKey());
    }

    /**
     * Update.
     *
     * @param userDetailsDto the user details dto
     * @param request        the request
     * @return the string
     * @throws Exception the exception
     */
    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public final String update(@RequestBody UserDetailsDto userDetailsDto, HttpServletRequest request)
            throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        if (!user.getRole().equals("ROLE_GUEST"))
            userManageService.updateUser(user.getEmail(), userDetailsDto);
        else
            userManageService.updateGuestMobileNumber(user.getEmail(),userDetailsDto.getMobileNumber());
        logger.info("update method -----> get call");
        return "User update success";
    }

    /**
     * Gets the.
     *
     * @param request the request
     * @return the string
     * @throws Exception the exception
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public final UserDetailsDto get(HttpServletRequest request) throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        UserDetailsDto userDetailsDto=userManageService.getUserByEmail(user.getEmail());
        if (user.getRole().equals("ROLE_GUEST")){
            UserDetailsDto guestDetailsDto=new UserDetailsDto();
            guestDetailsDto.setId(userDetailsDto.getId());
            guestDetailsDto.setEmail(userDetailsDto.getEmail());
            guestDetailsDto.setRole("GUEST");
            userDetailsDto=guestDetailsDto;
        }
        return userDetailsDto;
    }

    /**
     * User already exist.
     *
     * @return the string
     */
    @RequestMapping(value = "/already_exist", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseStatus(HttpStatus.OK)
    public String userAlreadyExist() {
        return "User already exists";
    }

    /**
     * Check input emailt.
     *
     * @return the boolean
     */
    @PostMapping("/emailcheck")
    @ResponseStatus(HttpStatus.OK)
    public boolean checkInputEmail(@RequestParam String email) {
        logger.info("checkInputEmail method -----> get call");
        return userManageService.checkInputMail(email);
    }

    /**
     * Get the spaces.
     *
     * @return the list<space>
     * @throws Exception the exception
     */
    @GetMapping("/spaces")
    @ResponseStatus(HttpStatus.OK)
    public List<SpaceDetailDto> getUserSpaces(HttpServletRequest request) throws Exception {
        logger.info("getUserSpaces method in SpaceController -----> get call");
        UserContext user = securityFacade.getUserContext(request);
        return spaceService.getUserSpaces(user.getUserId());
    }

    @PutMapping("/mobile/v1")
    @ResponseStatus(HttpStatus.OK)
    public ResponseDto updateUserMobile(@RequestBody UserDetailsDto userDetailsDto, HttpServletRequest request)
            throws Exception {
        ResponseDto responseDto = new ResponseDto();
        UserContext user = securityFacade.getUserContext(request);
        logger.info(String.format(LogMessages.MC_USER_UPDATE_MOBILE, user.getEmail()));
        if (!user.getRole().equals("ROLE_GUEST"))
            userManageService.updateUserMobileNumber(user.getEmail(), userDetailsDto.getMobileNumber());
        else
            userManageService.updateGuestMobileNumber(user.getEmail(), userDetailsDto.getMobileNumber());

        responseDto.setStatus(200);
        responseDto.setMessage("success");
        return responseDto;
    }
}
