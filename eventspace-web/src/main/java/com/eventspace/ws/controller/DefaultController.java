package com.eventspace.ws.controller;

import com.eventspace.dto.SpaceDetailDto;
import com.eventspace.service.SpaceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api")
public class DefaultController {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(DefaultController.class);
    /**
     * The space service.
     */
    @Autowired
    private SpaceService spaceService;

    @Value("${cache.enable}")
    public  Boolean enableCache;


    @GetMapping("/old/space/{id}")
    @ResponseStatus(HttpStatus.OK)
    public SpaceDetailDto getSpace(@PathVariable Integer id, @RequestParam(required = false) Integer[] eventId) {
        Long start=System.currentTimeMillis();
        LOGGER.info("getSpace ----->\t"+id);
        SpaceDetailDto result = spaceService.getSpace(id,eventId,false);
        result.setSimilarSpaces(spaceService.findSimilarSpaces(id, eventId,false));
        result.setFutureBookingDates(spaceService.getUpcomingPaidBookingsDuration(id));
        Long end=System.currentTimeMillis();
        LOGGER.info("GET A SPACE DURATIONMS"+(end-start));
        return result;
    }
}
