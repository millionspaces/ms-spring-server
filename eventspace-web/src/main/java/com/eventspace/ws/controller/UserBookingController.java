/* *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================*/

package com.eventspace.ws.controller;

import com.eventspace.dto.BookingCommonDto;
import com.eventspace.dto.BookingDetailsDto;
import com.eventspace.dto.UserContext;
import com.eventspace.dto.UserLogDetails;
import com.eventspace.security.facade.SecurityFacade;
import com.eventspace.service.BookingService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * The Class UserBookingController.
 */
@RestController
@RequestMapping(value = "/api/book/spaces")
public class UserBookingController {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(UserBookingController.class);

    /**
     * The booking service.
     */
    @Autowired
    private BookingService bookingService;

    /**
     * The security facade.
     */
    @Autowired
    private SecurityFacade securityFacade;

    /**
     * Get user bookings.
     *
     * @param request the request
     * @return the list<booking details dto>
     * @throws Exception the exception
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<BookingDetailsDto> getBookings(HttpServletRequest request) throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        LOGGER.info("getUserBookings method in UserBookingController-----> get call");
        return bookingService.getBookings(user.getUserId());
    }

    /**
     * Get Booking.
     *
     * @param id      the id
     * @param request the request
     * @return the booking details dto
     * @throws Exception the exception
     */
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BookingDetailsDto getBooking(@PathVariable Integer id, HttpServletRequest request) throws Exception {
        LOGGER.info("getBooking  method in UserBookingController -----> get call");
        UserContext user = securityFacade.getUserContext(request);
        return bookingService.getBooking(id, user.getUserId());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public BookingDetailsDto getABookings(HttpServletRequest request, @RequestBody BookingCommonDto bookingCommonDto) throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        LOGGER.info("getUserBookings method in UserBookingController-----> get call");
        return bookingService.getABooking(bookingCommonDto,user.getUserId());
    }


    @GetMapping(value = "/space/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<BookingDetailsDto> getSpaceBookings(HttpServletRequest request,@PathVariable Integer id) throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        LOGGER.info("getUserBookings method in UserBookingController-----> get call");
        return bookingService.getSpaceBooking(id,user.getUserId());
    }

    @GetMapping(value = "/guest")
    @ResponseStatus(HttpStatus.OK)
    public List<BookingDetailsDto> getUserBookings(HttpServletRequest request) throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        LOGGER.info("getUserBookings method in UserBookingController-----> get call");
        return bookingService.getUserBooking(user.getUserId());
    }

    @GetMapping(value = "/host")
    @ResponseStatus(HttpStatus.OK)
    public List<BookingDetailsDto> getHostBookings(HttpServletRequest request) throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        LOGGER.info("getUserBookings method in UserBookingController-----> get call");
        return bookingService.getHostBooking(user.getUserId());
    }


    /**
     * Get cost.
     *
     * @param id the id
     * @return the double
     */
    @GetMapping(value = "/cost/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Double getCost(@PathVariable Integer id) {
        LOGGER.info("getCost method in UserBookingController-----> get call");
        return bookingService.getHiringCharge(id);
    }



    @GetMapping(value = "/logs")
    @ResponseStatus(HttpStatus.OK)
    public List<UserLogDetails> getLogs(HttpServletRequest request) throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        return bookingService.getUserLogs(user.getUserId());
    }

}
