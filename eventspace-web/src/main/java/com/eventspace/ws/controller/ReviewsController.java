package com.eventspace.ws.controller;

import com.eventspace.dto.ReviewDetailsDto;
import com.eventspace.dto.ReviewDto;
import com.eventspace.service.BookingService;
import com.eventspace.service.SpaceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/reviews")
public class ReviewsController {

    /**
     * The Constant logger.
     */
    private static final Log LOGGER = LogFactory
            .getLog(BookingController.class);

    @Autowired
    private SpaceService spaceService;

    @Autowired
    private BookingService bookingService;

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<ReviewDetailsDto> getASpaceReview(@PathVariable Integer id) {
        LOGGER.info("getASpaceReview method -----> get call");
        return spaceService.getSpaceReviewdetails(id);
    }

    @PostMapping(value = "/")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> addReview(@RequestParam String key, @RequestBody ReviewDto reviewDto) {
        LOGGER.info("addReview method -----> get call");
        return bookingService.addReviewByEmail(key,reviewDto);
    }
}
