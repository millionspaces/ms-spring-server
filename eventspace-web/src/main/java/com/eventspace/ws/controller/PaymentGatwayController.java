package com.eventspace.ws.controller;

import com.eventspace.dto.PaymentCallbackDto;
import com.eventspace.service.BookingService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Auxenta on 6/19/2017.
 */
@RestController
@RequestMapping(value = "/api/payment")
public class PaymentGatwayController {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(SpaceController.class);

    /**
     * The booking service.
     */
    @Autowired
    private BookingService bookingService;

    /**
     * The login url.
     */
    @Value("${app.payment.redirect.url}")
    private String paymentRedirectUrl;

    /**
     * PAY.

     * @return the map<string, object>
     * @throws Exception the exception
     */
    @PostMapping(value = "/callback")
    @ResponseStatus(HttpStatus.OK)
    public void pay(@ModelAttribute PaymentCallbackDto paymentCallbackDto, HttpServletResponse httpServletResponse ) throws IOException {
        LOGGER.info("IPG - PAY method  -----> get call");

       String url= bookingService.paymentCallback(paymentCallbackDto,false);
        httpServletResponse.sendRedirect(url);
    }


    /**
     * PAY.

     * @return the map<string, object>
     * @throws Exception the exception
     */
    @PostMapping(value = "/mobileCallback")
    @ResponseStatus(HttpStatus.OK)
    public void mobilePay(@ModelAttribute PaymentCallbackDto paymentCallbackDto, HttpServletResponse httpServletResponse ) throws IOException {
        LOGGER.info("IPG - mobilePay method  -----> get call");
        LOGGER.info("IPG - booking  ----->"+paymentCallbackDto.getOrderID());

        String url= bookingService.paymentCallback(paymentCallbackDto,true);
        httpServletResponse.sendRedirect(url);
    }


}
