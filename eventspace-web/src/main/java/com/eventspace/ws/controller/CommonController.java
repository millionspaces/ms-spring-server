/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.ws.controller;


import au.com.gateway.client.GatewayClient;
import au.com.gateway.client.component.Redirect;
import au.com.gateway.client.component.TransactionAmount;
import au.com.gateway.client.config.ClientConfig;
import au.com.gateway.client.enums.TransactionType;
import au.com.gateway.client.ex.GatewayClientException;
import au.com.gateway.client.payment.PaymentCompleteRequest;
import au.com.gateway.client.payment.PaymentCompleteResponse;
import au.com.gateway.client.payment.PaymentInitRequest;
import au.com.gateway.client.payment.PaymentInitResponse;
import com.eventspace.dto.*;
import com.eventspace.security.facade.SecurityFacade;
import com.eventspace.service.CommonService;
import com.eventspace.service.EmailService;
import com.eventspace.service.SpaceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.*;

/**
 * The Class CommonController.
 */
@RestController
@RequestMapping(value = "/api/common")
public class CommonController {


    /**
     * The Constant logger.
     */
    private static final Log LOGGER = LogFactory.getLog(CommonController.class);
    /**
     * The common service.
     */
    @Autowired
    private CommonService commonService;

    @Autowired
    private SpaceService spaceService;

    @Autowired
    private SecurityFacade securityFacade;

    @Autowired
    private EmailService emailService;

    /**
     * Gets  amenities.
     *
     * @return the list<amenity>
     */
    @GetMapping(value = "/amenities")
    @ResponseStatus(HttpStatus.OK)
    //@RequestMapping(value = "/amenities", method = RequestMethod.GET)
    public List<AmenityDto> getAmenities(){
        LOGGER.info("getAmenities method in CommonController-----> get call");
        return commonService.getAllAmenities();
    }

    @GetMapping(value = "/amenities2")
    @ResponseStatus(HttpStatus.OK)
    //@ApiVersion(2)
    //@RequestMapping(value = "/amenities2", method = RequestMethod.GET)
    public List<EventTypeDto> getAmenities2(){
        LOGGER.info("getAmenities method in CommonController-----> get call");
        return commonService.getEventTypes();
    }

    /**
     * Gets the latest amenities.
     *
     * @param date the date
     * @return the list<latest amenities>
     */
    @GetMapping(value = "/amenities/{date}")
    @ResponseStatus(HttpStatus.OK)
    public List<AmenityDto> getLatestAmenities(@PathVariable Long date) {
        LOGGER.info("getLatestAmenities method in CommonController-----> get call");
        Date lastUpdateDate = new Date(date);
        return commonService.getLatestAmenities(lastUpdateDate);
    }

    /**
     * Gets the event types.
     *
     * @return the list<event types>
     */
    @GetMapping(value = "/eventTypes")
    @ResponseStatus(HttpStatus.OK)
    public List<EventTypeDto> getEventTypes() {
        LOGGER.info("getEventTypes method in CommonController-----> get call");
        return commonService.getEventTypes();

    }

    /**
     * Gets the amenity units.
     *
     * @return the list<amenity units>
     */
    @GetMapping(value = "/amenityUnits")
    @ResponseStatus(HttpStatus.OK)
    public List<AmenityUnitsDto> getAmenityUnits() {
        LOGGER.info("ListAmenityUnits method in CommonController-----> get call");
        return commonService.getAmenityUnits();
    }

    /**
     * Gets the cancellation policies.
     *
     * @return the list<cancellation policies>
     */
    @GetMapping(value = "/cancellationPolicies")
    @ResponseStatus(HttpStatus.OK)
    public List<CancellationPolicyDto> getCancellationPolicies() {
        LOGGER.info("getCancellationPolicies method in CommonController-----> get call");
        return commonService.getCancellationPolicies();
    }

    /**
     * Gets the cancellation policies.
     *
     * @return the list<cancellation policies>
     */
    @GetMapping(value = "/rules")
    @ResponseStatus(HttpStatus.OK)
    public List<CommonDto> getRules(){
        LOGGER.info("getRules method in CommonController-----> get call");
        return commonService.getAllRules();
    }

    /**
     * Gets the cancellation policies.
     *
     * @return the list<cancellation policies>
     */
    @GetMapping(value = "/seatingArrangement")
    @ResponseStatus(HttpStatus.OK)
    public List<CommonDto> getSeatingArrangement() {
        LOGGER.info("getSeatingArrangement method in CommonController-----> get call");
        return commonService.getAllSeatingArrangement();
    }

    /**
     * Gets the cancellation policies.
     *
     * @return the list<cancellation policies>
     */
    @GetMapping(value = "/measurementUnit")
    @ResponseStatus(HttpStatus.OK)
    public List<CommonDto> getMeasurementUnits(){
        LOGGER.info("getMeasurementUnits method in CommonController-----> get call");
        return commonService.getMeasurementUnits();
    }

    @GetMapping(value = "/chargeTypes")
    @ResponseStatus(HttpStatus.OK)
    public List<CommonDto> getChargeTypes(){
        LOGGER.info("getChargeTypes method in CommonController-----> get call");
        return commonService.getAllChargeType();
    }


    @GetMapping(value = "/blockChargeTypes")
    @ResponseStatus(HttpStatus.OK)
    public List<CommonDto> getBlockChargeTypes(){
        LOGGER.info("getBlockChargeTypes method in CommonController-----> get call");
        return commonService.getAllBlockChargeTypes();
    }

    @GetMapping(value = "/spaceTypes")
    @ResponseStatus(HttpStatus.OK)
    public List<CommonDto> getSpaceTypes(){
        LOGGER.info("getSpaceTypes method in CommonController-----> get call");
        return commonService.getAllSpaceTypes();
    }

    @GetMapping(value = "/system")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> getSystemInfo(){
        LOGGER.info("getSystemInfo method in CommonController-----> get call");
        return commonService.getSystemInfo();
    }

    @GetMapping(value = "/android")
    @ResponseStatus(HttpStatus.OK)
    public VersionDetailsDto getAndroidVersionInfo(){
        LOGGER.info("getAndroidVersionInfo method in CommonController-----> get call");
        return commonService.getAndroidVersion();
    }

    @GetMapping(value = "/ios")
    @ResponseStatus(HttpStatus.OK)
    public VersionDetailsDto getIosVersionInfo(){
        LOGGER.info("getAndroidVersionInfo method in CommonController-----> get call");
        return commonService.getIosVersion();
    }

    @GetMapping(value = "/all")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> getAllStaticData(){
        LOGGER.info("getAllStaticData method in CommonController-----> get call");
        return commonService.getAllStaticData();
    }


    //@GetMapping(value="/testing")
    //@ResponseStatus(HttpStatus.OK)
    //public String testing(){
        //emailService.sendTest();
        //emailService.sendBookingActionMails(69, EventsEnum.PAY.value());
        //emailService.sendBookingActionMails(69, EventsEnum.CANCEL.value());
      //  emailService.sendBookingActionMails(69, EventsEnum.CONFIRM.value());
        //emailService.sendBookingActionMails(69, EventsEnum.EXPIRE.value());
        //emailService.sendBookingActionMails(69, EventsEnum.WAIT_FOR_PAY.value());
        //return "OK";
    //}

    @GetMapping(value="/testing")
    @ResponseStatus(HttpStatus.OK)
    public List<SpaceDetailDto> testing(HttpServletRequest request) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        List<MediaType> acceptableMediaTypes = new ArrayList<>();
        acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(acceptableMediaTypes);
        HttpEntity entity = new HttpEntity(headers);
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        String url = "https://cpsolutions.dialog.lk/index.php/cbs/sms/send?destination=0728202699&q=15010606236846&message="+URLEncoder.encode("Thanks&Regards","UTF-8");
        HttpEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity,String.class);
        LOGGER.info("sms gateway response "+response.getBody());


        //for (SpaceDetailDto space:spaces){
          //  spaceService.createSpace(space,user);
        //}
        //spaceService.createSpace(spaceDetailDto,user);

        //return spaces;
        return null;
    }

    @GetMapping("/spaces")
    @ResponseStatus(HttpStatus.OK)
    public List<AdminSpaceDetailsDto> getAllSpaces()  {
        return spaceService.getAllSpaces();
    }


    /*@PostMapping("/ipg")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> procedIpg(@RequestBody CommonDto booking) throws Exception {

    }

    @RequestMapping(value = "/ipg/response", method = RequestMethod.POST,consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void procedIpgResponse(HttpServletRequest request, HttpServletResponse response) throws Exception {
            }*/


    @GetMapping(value="/reports")
    @ResponseStatus(HttpStatus.OK)
    public List<Map<String,Object>> generateReport(@RequestParam String procedureName){
        return commonService.procedureListResult(procedureName);
    }

    @GetMapping(value="/ipgdata")
    @ResponseStatus(HttpStatus.OK)
    public List<Map<String,Object>> getIpgData(){
        return commonService.tableResults("ipg_details");
    }

    @GetMapping(value="/ipgdata/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> getIpgData(@PathVariable Integer id){
        return commonService.tableUniqueResults("ipg_details",id);
    }

    @GetMapping(value="/procedure/{procedureName}")
    @ResponseStatus(HttpStatus.OK)
    public List<Map<String,Object>> callCommonProcedure(@PathVariable String procedureName){
        return commonService.commonProcedureListResult(procedureName);
    }
}
