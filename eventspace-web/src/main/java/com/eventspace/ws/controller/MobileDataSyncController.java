package com.eventspace.ws.controller;

import com.eventspace.dto.MobileSyncRequestDatadto;
import com.eventspace.dto.MobileSyncResponseDatadto;
import com.eventspace.service.CommonService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/sync")
public class MobileDataSyncController {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(MobileDataSyncController.class);

    /**
     * The common service.
     */
    @Autowired
    private CommonService commonService;

    /**
     * Sync datas.
     *
     * @param mobileSyncRequestDatadtos the mobile sync request data dto
     * @return the mobile sync response data dto
     */
    @PostMapping(value = "/batch")
    @ResponseStatus(HttpStatus.OK)
    public MobileSyncResponseDatadto syncDatas(@RequestBody List<MobileSyncRequestDatadto> mobileSyncRequestDatadtos)  {
        LOGGER.info("syncDatas method in MobileDataSyncController-----> get call");
        return commonService.syncDataForMobile(mobileSyncRequestDatadtos);
    }
}
