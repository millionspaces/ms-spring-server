package com.eventspace.ws.cache;

import net.spy.memcached.MemcachedClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;
import java.net.InetSocketAddress;

@Configuration
@PropertySource("classpath:application.properties")
public class MemcachedFactory {

    private static final Logger LOGGER = Logger.getLogger(MemcachedFactory.class);

    @Value("${cache.prefix.name}")
    private String NAMESPACE;

    @Value("${cache.server.name}")
    private String HOSTNAME;

    @Value("${cache.is.enable}")
    private boolean isCacheEnabled;

    private static MemcachedFactory instance = null;
    private static MemcachedClient m = null;

    public MemcachedFactory() throws IOException {
        //getInstance();
    }

    @Bean
    public static synchronized MemcachedFactory getInstance() throws IOException {
        if (instance == null) {
            LOGGER.info("Creating a new cache instance");
            instance = new MemcachedFactory();
        }
        return instance;
    }

    public void set(String key, int ttl, final Object o) throws IOException {
        LOGGER.info(String.format("Save to cache with key [%s}", key));
        getCache().set(NAMESPACE + key, ttl, o);
    }

    public Object get(String key) throws IOException {
        Object o = getCache().get(NAMESPACE + key);
        if (o == null) {
            LOGGER.info("Cache MISS for KEY: " + key);
        } else {
            LOGGER.info("Cache HIT for KEY: " + key);
        }
        return o;
    }

    public Object delete(String key) throws IOException {
        return getCache().delete(NAMESPACE + key);
    }


    @Bean
    public MemcachedClient getCache() throws IOException {
        if (m == null) {
            LOGGER.info(String.format("Creating the cache for the endpoint [%s]", HOSTNAME));
            m = new MemcachedClient(new InetSocketAddress(HOSTNAME, 11211));
        }
        return m;
        // new MemcachedClient(new InetSocketAddress("127.0.0.1", 11211));
        // new MemcachedClient(new BinaryConnectionFactory(), AddrUtil.getAddresses("127.0.0.1:11211"));
    }

    public boolean isCacheEnabled() {
        return isCacheEnabled;
    }

}
