package com.eventspace.ws.cache;

import javax.servlet.ServletInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ResettableServletInputStream extends ServletInputStream {

    private InputStream stream;

    @Override
    public int read() throws IOException {
        return stream.read();
    }

    public void setStream(InputStream stream) {
        this.stream = stream;
    }
}
