package com.eventspace.ws.admin.controller;

import com.eventspace.dto.AdminUserDetailsDto;
import com.eventspace.dto.UserStatsDto;
import com.eventspace.service.UserManagementService;
import com.eventspace.ws.controller.BookingController;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The Class AdminUserController.
 */
@RestController
@RequestMapping(value = "/api/admin")
public class AdminUserController {

    /**
     * The Constant logger.
     */
    private static final Log LOGGER = LogFactory
            .getLog(BookingController.class);

    /**
     * The user manage service.
     */
    @Autowired
    private UserManagementService userManageService;

    /**
     * Get all hosts.
     *
     * @param page the page
     * @return the list< hosts >
     */
    @GetMapping(value = "/host/{page}")
    @ResponseStatus(HttpStatus.OK)
    public List<AdminUserDetailsDto> getAllHosts(@PathVariable Integer page){
        LOGGER.info("getAllHosts method in AdminUserController -----> get call");
        return userManageService.getHosts(page);
    }

    /**
     * Get all guests.
     *
     * @param page the page
     * @return the list< guests>
     */
    @GetMapping(value = "/guest/{page}")
    @ResponseStatus(HttpStatus.OK)
    public List<AdminUserDetailsDto> getAllGuests(@PathVariable Integer page){
        LOGGER.info("getAllGuests method in AdminUserController -----> get call");
        return userManageService.getGuests(page);
    }

    /**
     * Update a user.
     *
     * @param adminUserDetailsDto the admin user details dto
     * @return the String
     */
    @PutMapping("/user")
    @ResponseStatus(HttpStatus.OK)
    public String updateUser(@RequestBody AdminUserDetailsDto adminUserDetailsDto){
        LOGGER.info("updateUser method in AdminUserController -----> get call");
        return userManageService.adminUpdateUserDetails(adminUserDetailsDto);
    }


    @GetMapping("/user/details")
    @ResponseStatus(HttpStatus.OK)
    public List<UserStatsDto> getStats(@RequestParam String startDate,@RequestParam String endDate){
        return userManageService.getUserSignUpStats(startDate,endDate);
    }


    @PostMapping("/user/verify")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> verifyUser(@RequestBody AdminUserDetailsDto adminUserDetailsDto){
        LOGGER.info("verifyUser method in AdminUserController -----> get call");
        return userManageService.verifyUser(adminUserDetailsDto);
    }
}
