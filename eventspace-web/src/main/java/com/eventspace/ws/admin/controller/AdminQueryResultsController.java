package com.eventspace.ws.admin.controller;

import com.eventspace.service.CommonService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/admin/query")
public class AdminQueryResultsController {

    private static final Log LOGGER = LogFactory.getLog(AdminQueryResultsController.class);

    @Autowired
    private CommonService commonService;

    @GetMapping(value="/listProcedure")
    @ResponseStatus(HttpStatus.OK)
    public List<Map<String, Object>> procedureListOfResult(@RequestParam String procedureName){
        return commonService.procedureListResult(procedureName);
    }

    @GetMapping(value="/uniqueProcedure")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> procedureUniqueResult(@RequestParam String procedureName){
        return commonService.procedureUniqueResult(procedureName);
    }

    @GetMapping(value="/tableResult")
    @ResponseStatus(HttpStatus.OK)
    public List<Map<String, Object>> tableResult(@RequestParam String tableName){
        return commonService.tableResults(tableName);
    }

    @GetMapping(value="/listQuery")
    @ResponseStatus(HttpStatus.OK)
    public List<Map<String, Object>> queryListOfResult(@RequestParam String sqlQuery){
        return commonService.queryListResult(sqlQuery);
    }

    @GetMapping(value="/uniqueQuery")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> queryuniqueOfResult(@RequestParam String sqlQuery){
        return commonService.queryUniqueResult(sqlQuery);
    }
}
