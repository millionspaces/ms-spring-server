package com.eventspace.ws.admin.controller;

import com.eventspace.dto.*;
import com.eventspace.enumeration.StatesEnum;
import com.eventspace.security.facade.SecurityFacade;
import com.eventspace.service.BookingService;
import com.eventspace.service.UserManagementService;
import com.eventspace.ws.controller.BookingController;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/admin")
public class AdminBookingController {

    /**
     * The Constant logger.
     */
    private static final Log LOGGER = LogFactory
            .getLog(BookingController.class);

    /**
     * The booking service.
     */
    @Autowired
    private BookingService bookingService;

    /**
     * The user manage service.
     */
    @Autowired
    private UserManagementService userManageService;

    /**
     * The security facade.
     */
    @Autowired
    private SecurityFacade securityFacade;

    /**
     * Get all bookings.
     *
     * @param page the page
     * @return the list< bookings>
     */
    @GetMapping("/booking/{page}")
    @ResponseStatus(HttpStatus.OK)
    public List<AdminBookingDetailsDto>  getAllBookings(@PathVariable Integer page){
        LOGGER.info("getAllBookings method in AdminSpaceController -----> get call");
        return bookingService.getAllBookings(page);
    }

    @GetMapping("/booking/detail/{bookingId}")
    @ResponseStatus(HttpStatus.OK)
    public AdminBookingDetailsDto  getABookings(@PathVariable Integer bookingId){
        LOGGER.info("getABookings method in AdminSpaceController -----> get call");
        return bookingService.generateAdminBookingDto(bookingId);
    }

    @GetMapping("/booking/new/{page}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object>  getAllBookings2(@PathVariable Integer page){
        LOGGER.info("getAllBookings method in AdminSpaceController -----> get call");
        Map<String,Object>  result=new HashMap<>();
        result.put("bookings",bookingService.getAllBookings(page));
        result.put("count",bookingService.getAllBookingCount());
        return result;
    }

    //for finance window
    @GetMapping("/booking/paid/{page}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object>  getAllPaidBookings(@PathVariable Integer page){
        LOGGER.info("getAllPaidBookings method  -----> get call");
        Map<String,Object>  result=new HashMap<>();
        Long count=bookingService.getBookingsCount(StatesEnum.PAYMENT_DONE.value())+bookingService.getBookingsCount(StatesEnum.CONFIRMED.value())+bookingService.getBookingsCount(StatesEnum.ADVANCE_PAYMENT_DONE.value());
        result.put("bookings",bookingService.getAdminPaidBookings(page));
        result.put("count",count);
        result.put("pending",count-bookingService.getVerifiedBookingCount());
        return result;
    }

    @PostMapping("/booking/pay")
    public Map<String,Object> markAsPaymentDone(@RequestBody PaymentVerifyDto paymentVerifyDto){
        LOGGER.info("markAsPaymentDone method  -----> get call");
        return bookingService.markAsPaymentDone(paymentVerifyDto.getBookingId());
    }

    @PostMapping("/booking/payment")
    public Map<String,Object> markAsPaymentReceive(@RequestBody PaymentVerifyDto paymentVerifyDto){
        LOGGER.info("markAsPaymentReceive method  -----> get call");
        Map<String,Object>  result=new HashMap<>();
        result.put("responce",bookingService.markAsPaymentReceive(paymentVerifyDto));
        result.put("id",paymentVerifyDto.getBookingId());
        return result;
    }

    @PostMapping("/booking/payment/sent")
    public Map<String,Object> verifyPaymentSentToHost(@RequestBody PaymentVerifyDto paymentVerifyDto){
        LOGGER.info("verifyPaymentSentToHost method  -----> get call");
        Map<String,Object>  result=new HashMap<>();
        result.put("response",bookingService.verifyPaymentSendToHost(paymentVerifyDto));
        result.put("id",paymentVerifyDto.getBookingId());
        return result;
    }

    @GetMapping("/booking/payment/{bookingId}")
    public AdminPaidBookingDetails getAPaidBooking(@PathVariable Integer bookingId){
        LOGGER.info("getAPaidBooking method  -----> get call");
        return bookingService.setAdminPaidBookingDetails(bookingId);
    }

    @GetMapping("/tentative/{page}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object>  getAllTentitiveBookings(@PathVariable Integer page){
        LOGGER.info("getAllTentitiveBookings method in AdminSpaceController -----> get call");
        Map<String,Object>  result=new HashMap<>();
        result.put("bookings",bookingService.getAllTentitiveBookings(page));
        result.put("count",bookingService.getBookingsCount(StatesEnum.PENDING_PAYMENT.value())+bookingService.getBookingsCount(StatesEnum.ADVANCE_PAYMENT_PENDING.value()));
        return result;
    }

    @GetMapping("/tentative/booking/{bookingId}")
    @ResponseStatus(HttpStatus.OK)
    public  AdminBookingDetailsDto getATentitiveBooking(@PathVariable Integer bookingId){
        LOGGER.info("getATentitiveBooking method in AdminSpaceController -----> get call");
        return bookingService.getAAdminTentitiveBooking(bookingId);
    }

    @PostMapping("/addPromo")
    @ResponseStatus(HttpStatus.OK)
    public  Boolean addPromo(@RequestBody PromoDetailDto promoDetailDto, HttpServletRequest request)
            throws Exception {
        LOGGER.info("addPromo method -----> get call");
        UserContext user = securityFacade.getUserContext(request);
        return userManageService.createPromotion(promoDetailDto,user.getUserId(),request.isUserInRole("ROLE_ADMIN"));
    }


    @GetMapping("/user/{userId}/page/{page}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object>  getUserBookings(@PathVariable Integer userId,@PathVariable Integer page) {
        LOGGER.info("getUserBookings method in -----> get call");
        return bookingService.getAUserBookingWithPagination(userId,page);
    }

    @GetMapping("/booking/finance/{page}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object>  getAllFinanceBookings(@PathVariable Integer page){
        LOGGER.info("getAllFinanceBookings method  -----> get call");
        Map<String,Object>  result=new HashMap<>();
        Long count=bookingService.getBookingsCount(StatesEnum.PAYMENT_DONE.value())+bookingService.getBookingsCount(StatesEnum.CONFIRMED.value())+bookingService.getBookingsCount(StatesEnum.ADVANCE_PAYMENT_DONE.value());
        result.put("bookings",bookingService.getFinanceDetails(page));
        result.put("count",count);
        result.put("pending",count-bookingService.getVerifiedBookingCount());
        return result;
    }
}
