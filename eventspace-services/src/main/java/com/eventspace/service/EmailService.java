package com.eventspace.service;

import com.eventspace.dto.SpaceContactPersonDto;

import java.util.List;

/**
 * Created by Auxenta on 5/9/2017.
 */
public interface EmailService {
    void sendMail(String receiver);

    void setDataOfMailVerificationEmail(String userName, String key);
    void setDataOfUserWelcomeMail(String receiverName);
    void setDataOfUpadetCalenderEndDate(String receiverName,Integer spaceId);
    void setDataOfSpaceDeactivated(String receiverName,Integer spaceId);
    void setDataOfPasswordResetMail(String userName,String host,String key);
    void sendBookingActionMails(Integer bookingId, Integer event);
    void sendSms(String phoneNumber,String message);
    void sendWelcomeSms(String phoneNumber, String userName);
    void sendViewReviewEmail(Integer bookingId);

    //void sendBookConfirmHost();
    void sendTest();

    void sendSpaceListedSms(String hostMobileNumber,String spaceName);
    void sendSpaceApprovedSms(String hostMobileNumber,String spaceName);
    void setDataOfSpacePublish(String hostName,String spaceName);
    void sendMailToPromotedSpaceBooking(Integer bookingId);
    List<SpaceContactPersonDto> getASpaceContactPersons(Integer spaceId);

}
