package com.eventspace.service;

import com.eventspace.domain.User;
import com.eventspace.dto.*;
import com.eventspace.enumeration.UserCreateEnum;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * The Interface UserManagementService.
 */
public interface UserManagementService {

	/**
	 * Gets the user by email.
	 *
	 * @param principal
	 *            the principal
	 * @return the user by email
	 */
	UserDetailsDto getUserByEmail(String principal);

	UserDetailsDto saveGoogleFBData(FbGoogleJsonDto dto);

	UserCreateEnum create(UserDetailsDto userDetailsDto,String host);

	UserDetailsDto createGuest(String email);
	
	void updateUser(String email, UserDetailsDto usrDetailsDto);
	
	User getUserByEmailPri(String principal);

	boolean checkInputMail(String email);

	List<AdminUserDetailsDto> getHosts(Integer page);

	List<AdminUserDetailsDto> getGuests(Integer page);

	String adminUpdateUserDetails(AdminUserDetailsDto adminUserDetailsDto);

	Map<String,Object> verifyUser(AdminUserDetailsDto adminUserDetailsDto);

	Boolean verifyEmailAddress(String encrypted);

	Boolean createPromotion(PromoDetailDto promoDetailDto,Integer userId,Boolean isAdmin);

	void updateGuestMobileNumber(String email,String mobileNumber);

	void updateUserMobileNumber(String email,String mobileNumber);

	List<UserStatsDto> getUserSignUpStats(String start,String end);
}
