/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.service;

import com.eventspace.dto.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface PaymentService {

	
	PaymentDetailsDto payments(String encryptedString);
	String encrypt(String data);
	String decryption(String encryptedString);
	PaymentSummaryDto getPaymentSummary(Integer bookingId);
	Map<String,Object> initiatedPaycorp(CommonDto booking);
	void completePaycorp(HttpServletRequest request, HttpServletResponse response);
	void completeUpay(UpayResponseDto upayResponseDto,HttpServletResponse response);
	void completeFrimi(FrimiResponseDto frimiResponseDto, HttpServletResponse response);

}
