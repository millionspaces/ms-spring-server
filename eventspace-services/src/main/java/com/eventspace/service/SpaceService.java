/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.service;

import com.eventspace.domain.BookingSlots;
import com.eventspace.domain.Space;
import com.eventspace.dto.*;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The Interface SpaceService.
 */
public interface SpaceService {

    /**
     * Creates the space.
     *
     * @param spaceDetailDto the space detail dto
     */

    Map<String, Object> createSpace(SpaceDetailDto spaceDetailDto, UserContext user);

    Map<String, Object> updateSpace(int id, SpaceDetailDto spaceDetailDto, Integer user, boolean isAdmin);

    List<AdminSpaceDetailsDto> getAllSpaces();

    SpaceDetailDto getSpace(Integer id,Integer[] eventId,boolean isAdmin);

    Boolean deleteSpace(int id);

    //List<SpaceDetailDto> getFilteredSpaces(FilterDto filterDto);

   // List<SpaceDetailDto> searchByCoordinates(SpaceSearchDto spaceSearchDto);

    Map<String,Object> isSpaceAvailable(BookingDto bookingDto);

    boolean isSpaceAvailableWIthInSelectedTimeDuration(BookingDto bookingDto);

    boolean isSpaceHasAvalablity(Integer spaceId,BookingSlots bookingSlots);

    boolean isNotBlockTime(BookingDto bookingDto);

    Map<String,Object>  getPaginatedSpaces(int page);

    List<AdminSpaceDetailsDto> getAdminSpaces(int page);

    int adminApproveSpace(int spaceId);

    //List<SpaceDetailDto> searchSpaces(SearchDto searchDto);

    List<SpaceDetailDto> getUserSpaces(Integer userId);

    Map<String,Object> advanceSearch(AdvanceSearchDto advanceSearchDto,Integer page);

    Integer enableAutoPublish(Boolean enable);

    List<SpaceDetailDto> getFeaturedSpaces();

    List<FeaturedSpacesDto> getAdminFeaturedSpaces();

    void unFeaturedSpace(Integer spaceId);

    void saveFeaturedSpaces(FeaturedSpacesDto featuredSpacesDto);

    void addMiss(SpaceDetailDto spaceDetailDto);

    void disapproveCalenderEndSpaces();

    Integer getLinkedSpace(Integer origin);

    List<Map> getUpcomingPaidBookingsDuration(Integer spaceId);

    SpaceDetailDto hostCalendarDetails(Integer spaceId);

    void setLinkedSpaces(Integer originSpace,Integer linkSpace);

    void addChilds(AddChildsDto addChildsDto);

     Set<SpaceDetailDto> findSimilarSpaces(Integer spaceId, Integer[] eventId,boolean isWeb);

     SpaceDetailDto spaceCalendarDetails(Integer spaceId);

      void enableScheduler();

      void disableScheduler();

    Long getAllSpacesCount();

    ResponseDto draftSpace(SpaceDetailDto spaceDetailDto, Integer userId);

    ResponseDto updateDraftSpace(SpaceDetailDto spaceDetailDto, Integer userId, Integer id);

    ResponseDto removeDraftSpace(Integer id, Integer userId);

    List<DraftSpaceDto> getDraftSpaces(Integer userId);

    Map<String,Object> saveImageDetails(List<ImageDetailsDto> imageDetails);

    ImageDetailsDto getImageDetails(String url);

    PrioritySpacesDto getPrioritySpaces();

    Map<String,Object> savePrioritySpaces(List<PrioritySpaceDto> prioritySpaces);

    Map<String,Object> updatePrioritySpaces(List<PrioritySpaceDto> prioritySpaces);

    Map<String,Object> changeHostOfSpace(HostChangeDto hostChangeDto);

    Map<String,Object> changeExpireDateOfSpace(HostChangeDto hostChangeDto);

    SpaceOfTheWeekDto getSpaceOfTheWeek();

    List<SpaceContactPersonDto> getOtherSpaceContactPersons(Integer spaceId);

    List<ReviewDetailsDto> getSpaceReviewdetails(Integer spaceId);

    Map<Integer,Object> batchAdvanceSearch(List<AdvanceSearchDto> advanceSearchDtos);

    Map<String,Object> addPrimaryEventType(AddPrimaryEventTypeDto addPrimaryEventTypeDto);

    HostChangeDto spaceHostDetails(Integer spaceId);

    List<AdminSpaceDetailsDto> getAdminFilterSpaces(int page, Integer isActive);

    List<AdminSpaceDetailsDto> getSearchSpaceData();

    Long getAdminFilterSpacesCount(Integer isActive);

    Double findDiscountOfSpace(Space space);

    List<ReviewDetailsDto> getMSReviews();

    BookingSpaceDto getBookingSpace(Integer spaceId);
}
