package com.eventspace.service;

/**
 * Created by Auxenta on 3/24/2017.
 */
public interface SchedulerService {

    void performDailyScheduler();
    void startDiscardBookings();
    void expireBooking(int bookingId);
    void discardBooking(int bookingId);
}
