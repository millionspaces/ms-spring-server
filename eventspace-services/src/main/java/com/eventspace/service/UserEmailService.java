package com.eventspace.service;

public interface UserEmailService {
	
	void sendMail(String template, String toAddress, String Subject, String receiverName);

}
