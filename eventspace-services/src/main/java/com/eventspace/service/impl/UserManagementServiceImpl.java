/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.service.impl;

import com.eventspace.dao.*;
import com.eventspace.domain.*;
import com.eventspace.dto.*;
import com.eventspace.enumeration.BooleanEnum;
import com.eventspace.enumeration.UserCreateEnum;
import com.eventspace.service.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The Class MobileDataSyncServiceImpl.
 */
@Service
@Transactional
public class UserManagementServiceImpl implements UserManagementService {

    private static final Log logger = LogFactory.getLog(UserManagementServiceImpl.class);

    /**
     * The user dao.
     */
    @Autowired
    private UserDao userDao;

    @Autowired
    private BookingDao bookingDao;

    @Autowired
    private UserAdditionalDetailsDao userAdditionalDetailsDao;

    @Autowired
    private UserEmailService userEmailService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private CryptographyService cryptographyService;

    @Autowired
    private SpaceDao spaceDao;

    @Autowired
    private BookingService bookingService;

    @Value("${welcome.email.template}")
    private String welcomeEmailTemplate;

    @Value("${welcome.email.subject}")
    private String welcomeSubject;

    /**
     * The rest api.
     */
    @Value("${email.verify.url}")
    private String emailVerifyUrl;

    @Autowired
    private CommonService commonService;

    @Autowired
    private PromoDetailsDao promoDetailsDao;


    /*
     * (non-Javadoc)
     *
     * @see
     * com.eventspace.service.UserManagementService#getUserByEmailPri(java.lang.
     * String)
     */
    @Override
    @Transactional
    public User getUserByEmailPri(String principal) {
        logger.info("getUserByEmailPri method ---> get called");
        return userDao.readUserByName(principal);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.eventspace.service.UserManagementService#getUserByEmail(java.lang.
     * String)
     */
    @SuppressWarnings("null")
    @Override
    @Transactional
    public UserDetailsDto getUserByEmail(String principal) {
        logger.info("getUserByEmail method ---> get called");

        User user = userDao.readUserByName(principal);
        if (user != null) {
            return user.build();
        } else {
            return null;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.eventspace.service.UserManagementService#saveGoogleFBData(com.
     * eventspace.dto.FbGoogleJsonDto)
     */
    @Override
    @Transactional
    public UserDetailsDto saveGoogleFBData(FbGoogleJsonDto dto) {
        logger.info("saveGoogleFBData method ---> get called");

        UserDetailsDto uDdto = new UserDetailsDto();
        uDdto.setEmail(dto.getEmail());
        uDdto.setName(dto.getName());
        uDdto.setPassword("");
        if (dto.getPicture() == null)
            uDdto.setImageUrl("https://graph.facebook.com/" + dto.getId() + "/picture");
        else
            uDdto.setImageUrl(dto.getPicture());
        create(uDdto);
        return getUserByEmail(dto.getEmail());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.eventspace.service.UserManagementService#create(com.eventspace.dto.
     * UserDetailsDto)
     */
    @Override
    @Transactional
    public UserCreateEnum create(UserDetailsDto userDetailsDto,String host) {

        logger.info("create method ---> get called");

        UserCreateEnum userCreateEnum = create(userDetailsDto);
        if (userCreateEnum.equals(UserCreateEnum.CREATED)) {
            emailService.setDataOfMailVerificationEmail(userDetailsDto.getName(), generateEmailVerificationUrl(userDetailsDto.getEmail()));
            emailService.sendMail(userDetailsDto.getEmail());
        }
        return userCreateEnum;
    }


    private UserCreateEnum create(UserDetailsDto userDetailsDto){
        UserCreateEnum userCreateEnum = UserCreateEnum.EXISTING;

        if (getUserByEmailPri(userDetailsDto.getEmail()) == null) {
            User user = new User();
            user.setEmail(userDetailsDto.getEmail());
            user = User.build(userDetailsDto, user);
            user.setPassword(userDetailsDto.getPassword());
            user.setIsTrustedUser(false);
            user.setActive(0);
            user.setRole("USER");
            userDao.create(user);

            UserAdditionalDetails userAdditionalDetails = new UserAdditionalDetails();
            userAdditionalDetails.setUser(user.getId());
            setUserAdditionalDetails(userAdditionalDetails, userDetailsDto);
            user.setUserAdditionalDetails(userAdditionalDetails);
            userDao.update(user);


            userCreateEnum = UserCreateEnum.CREATED;
        }

        return userCreateEnum;

    }

    @Override
    public UserDetailsDto createGuest(String email) {
        User user = new User();
        user.setName("");
        user.setEmail(email);
        user.setIsTrustedUser(false);
        user.setActive(0);
        user.setRole("GUEST");
        user.setPassword("");
        user.setCreatedAt(new Date());
        user.setUpdatedAt(new Date());
        userDao.create(user);
        return user.build();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.eventspace.service.UserManagementService#updateUser(java.lang.String,
     * com.eventspace.dto.UserDetailsDto)
     */
    @Override
    @Transactional
    public void updateUser(String email, UserDetailsDto userDetailsDto) {

        logger.info("updateUser method ---> get called");

        User user = getUserByEmailPri(email);
        user = User.build(userDetailsDto, user);
        UserAdditionalDetails userAdditionalDetails = new UserAdditionalDetails();
        userAdditionalDetails.setUser(user.getId());
        if (userAdditionalDetailsDao.read(user.getId()) != null) {
            userAdditionalDetails = userAdditionalDetailsDao.read(user.getId());
        }
        setUserAdditionalDetails(userAdditionalDetails, userDetailsDto);
        user.setUserAdditionalDetails(userAdditionalDetails);
        userDao.update(user);
    }

    @Override
    public void updateGuestMobileNumber(String email, String mobileNumber) {
        User user = getUserByEmailPri(email);
        if(!Optional.ofNullable(user.getPhone()).isPresent() || (Optional.ofNullable(user.getPhone()).isPresent() && user.getPhone().length()>0)){
            user.setPhone(mobileNumber);
            userDao.update(user);
        }
    }

    @Override
    public void updateUserMobileNumber(String email, String mobileNumber) {
        User user = getUserByEmailPri(email);
            user.setPhone(mobileNumber);
            userDao.update(user);
    }

    @Override
    @Transactional
    public boolean checkInputMail(String email) {
        logger.info("checkInputMail method ---> get called");
        return userDao.checkEmail(email) == BooleanEnum.TRUE.value();
    }

    @Override
    @Transactional
    public List<AdminUserDetailsDto> getHosts(Integer page) {
        logger.info("getHosts method ---> get called");
        List<AdminUserDetailsDto> hosts = new ArrayList<>();
        List<User> users = userDao.getHosts(page);
        for (User user : users) {
            AdminUserDetailsDto host = user.buildUserDetailsForAdmin();
            hosts.add(host);
        }
        return hosts;
    }

    @Override
    @Transactional
    public List<AdminUserDetailsDto> getGuests(Integer page) {
        logger.info("getGuests method ---> get called");
        List<AdminUserDetailsDto> guests = new ArrayList<>();
        List<User> users = userDao.getGuests(page);
        for (User user : users) {
            AdminUserDetailsDto guest = user.buildUserDetailsForAdmin();
            Booking booking=bookingDao.getAUserLatestBooking(user.getId());
            if (booking!=null){
                BookingDetailsDto bookingDetailsDto=bookingService.generateSimpleBookingDetails(booking.getId());
                guest.setBooking(bookingDetailsDto);

            }
            guest.setBookingCount(bookingDao.getAUserBookingCount(user.getId()).intValue());
            guests.add(guest);
        }
        return guests;
    }

    @Override
    @Transactional
    public String adminUpdateUserDetails(AdminUserDetailsDto adminUserDetailsDto) {
        User user = userDao.read(adminUserDetailsDto.getId());

        user.setName(adminUserDetailsDto.getName());
        user.setPhone(adminUserDetailsDto.getPhone());
        user.setCompanyPhone(adminUserDetailsDto.getCompanyPhone());
        user.setEmail(adminUserDetailsDto.getEmail());
        userDao.update(user);

        return "successfully updated";
    }

    @Override
    @Transactional
    public Boolean verifyEmailAddress(String encrypted) {

            String decrypted=cryptographyService.decrypt(encrypted.replace(" ","+"));
            if (decrypted!=null && decrypted.contains("date:"))
            {
            String[] datas = (decrypted).split("date:");
            Integer userId = Integer.parseInt(datas[0].trim());

            logger.info(userId+"-key"+datas[0].trim());

            User user = userDao.read(userId);
            if (user.getActive().equals(0))
            {
            user.setActive(BooleanEnum.TRUE.value());
            //if (TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis() - Long.parseLong(datas[1].trim())) < 1) {
            userDao.update(user);
            emailService.setDataOfUserWelcomeMail(user.getName());
            emailService.sendMail(user.getEmail());
            return true;
            }
            }
                return false;

    }

    /**
     * nenerate rest url.
     *
     * @param mail the mail
     * @return url string
     */

    private String generateEmailVerificationUrl(String mail) {
        User user = userDao.readUserByName(mail);
        Long date = commonService.getCurrentTime();

        String key=cryptographyService.encrypt(user.getId() + "date:" + date.toString());
        logger.info(user.getId()+" -key "+key);
        return key;
    }

    private void setUserAdditionalDetails(UserAdditionalDetails userAdditionalDetails, UserDetailsDto userDto) {
        if (userDto.getAddress() != null)
            userAdditionalDetails.setAddress(userDto.getAddress());

        if(userDto.getCommissionPercentage() !=null)
            userAdditionalDetails.setCommissionPercentage(userDto.getCommissionPercentage());
            else
            userAdditionalDetails.setCommissionPercentage(20);

        if (userDto.getContactPersonName() != null)
            userAdditionalDetails.setLastName(userDto.getContactPersonName());

        if (userDto.getJobTitle() != null)
            userAdditionalDetails.setJob(userDto.getJobTitle());

        if (userDto.getCompanyName() != null)
            userAdditionalDetails.setCompanyName(userDto.getCompanyName());

        if (userDto.getAbout() != null)
            userAdditionalDetails.setAbout(userDto.getAbout());

        if (userDto.getAccountHolderName() != null)
            userAdditionalDetails.setAccountHolderName(userDto.getAccountHolderName());

        if (userDto.getAccountNumber() != null)
            userAdditionalDetails.setAccountNumber(userDto.getAccountNumber());

        if (userDto.getBank() != null)
            userAdditionalDetails.setBank(userDto.getBank());

        if (userDto.getBankBranch() != null)
            userAdditionalDetails.setBankBranch(userDto.getBankBranch());

    }

    @Override
    public Boolean createPromotion(PromoDetailDto promoDetailDto,Integer userId,Boolean isAdmin) {
        if (isAdmin) {
            if (promoDetailsDao.getPromoByCode(promoDetailDto.getPromoCode()) != null)
                return false;
            else {
                PromoDetails promoDetail=PromoDetails.build(promoDetailDto);
                if (promoDetailDto.getSpaces()!=null)
                    setPromoForSpaces(promoDetail,promoDetailDto.getSpaces());
                promoDetailsDao.create(promoDetail);
                return true;
            }
        }
        else
            return false;
    }

    private void setPromoForSpaces(PromoDetails promoDetails,Set<Integer> spacesId){
        Set<Space> spaces=new HashSet<>();
        for (Integer spaceId:spacesId){
            Space space=spaceDao.read(spaceId);
            if (space!=null)
                spaces.add(space);
        }
        promoDetails.setSpaces(spaces);
        promoDetails.setToAllSpaces(BooleanEnum.FALSE.value());
    }

    @Override
    public Map<String, Object> verifyUser(AdminUserDetailsDto adminUserDetailsDto) {
        Map<String,Object> response=new HashMap<>();
        User user=userDao.readUserByName(adminUserDetailsDto.getEmail());
        user.setActive(BooleanEnum.TRUE.value());
        userDao.update(user);

        response.put("response","success");
        response.put("status",200);
        return response;
    }

    @Override
    public List<UserStatsDto> getUserSignUpStats(String start,String end) {
        List<UserStatsDto> response=new ArrayList<>();
        List<Object[]> usersList=userDao.getUserSignUpStats(start,end);


        for(int i=0;i<usersList.size();i++){
            Date date=(Date)usersList.get(i)[0];
            BigInteger userCount=(BigInteger) usersList.get(i)[3];
            BigInteger guestCount=(BigInteger) usersList.get(i)[2];
            BigInteger totalCount=(BigInteger) usersList.get(i)[1];


            UserStatsDto userStatsDto=new UserStatsDto();
            userStatsDto.setDate(date);
            userStatsDto.setTotal(totalCount.intValue());
            userStatsDto.setGuestCount(guestCount.intValue());
            userStatsDto.setUserCount(userCount.intValue());
            response.add(userStatsDto);
        }

        return response;
    }
}