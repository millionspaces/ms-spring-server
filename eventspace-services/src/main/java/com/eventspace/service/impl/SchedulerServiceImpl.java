package com.eventspace.service.impl;

import com.eventspace.dao.*;
import com.eventspace.domain.Booking;
import com.eventspace.domain.BookingExpireDetails;
import com.eventspace.domain.BookingHistory;
import com.eventspace.domain.BookingPayLater;
import com.eventspace.dto.BookingActionDto;
import com.eventspace.enumeration.EventsEnum;
import com.eventspace.enumeration.StatesEnum;
import com.eventspace.service.*;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.awt.print.Book;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


/**
 * Created by Auxenta on 3/24/2017.
 */

/**
 * The Class SchedulerServiceImpl.
 */
@Service
@Transactional
public class SchedulerServiceImpl implements SchedulerService {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SchedulerServiceImpl.class);

    /**
     * The booking dao.
     */
    @Autowired
    private BookingDao bookingDao;

    @Autowired
    private SpaceDao spaceDao;

    /**
     * The booking EXPIRE details dao.
     */
    @Autowired
    private BookingExpireDetailsDao bookingExpireDetailsDao;

    @Autowired
    private BookingHistoryDao bookingHistoryDao;

    /**
     * The email service.
     */
    @Autowired
    private EmailService emailService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private SpaceService spaceService;

    @Autowired
    private CommonService commonService;

    ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);

    String cron = "";

    private Worker worker = new Worker();

    @Value("${daily.scheduler.enable}")
    private Boolean dailySchedulerEnable;

    private Boolean schedulerStated=false;

    @Autowired
    private ScheduleStartDao scheduleStartDao;
    /**
     * perform daily scheduler
     */
    @Scheduled(cron = "0 0 0 * * *")
    public void performDailyScheduler() {
        //ScheduleStart scheduleStart=scheduleStartDao.read(1);
        if (dailySchedulerEnable) {
            try {
                LOGGER.info("daily scheduler -----> get call {}" , schedulerStated);
                schedulerStated = true;
                //spaceService.disableScheduler();
                //expire the pending payment bookings
                List<BookingExpireDetails> bookingExpireDetails = bookingExpireDetailsDao.getUpcomingExpireBookings();
                for (BookingExpireDetails bookingExpireDetail : bookingExpireDetails) {
                    expireBooking(bookingExpireDetail.getBooking());
                }
                //disapprove the calender end spaces
                spaceService.disapproveCalenderEndSpaces();

                //change reservation status to confirm
                bookingService.confirmBookings();
            } catch (Exception ex) {
                LOGGER.error("performDailyScheduler -> Exception : ", ex);
            }

            /*ScheduledFuture scheduledFuture =
                    scheduledExecutorService.schedule(
                            new Runnable() {
                                @Transactional
                                public void run() {
                                    spaceService.enableScheduler();
                                }
                            }
                            ,60000,
                            TimeUnit.MILLISECONDS);*/

        }
    }

    @Scheduled(fixedRate = 10*60*1000)
    public void startDiscardBookings(){
        if(dailySchedulerEnable){
            LOGGER.info("every 10 min scheduler started");
            List<Booking> bookings = bookingDao.missedDiscardBookings();
            for (Booking booking : bookings) {
                discardBooking(booking.getId());
            }
        }
    }


    @Transactional
    public void expireBooking(int bookingId) {
        BookingExpireDetails bookingExpireDetails = bookingExpireDetailsDao.getByBookingId(bookingId);
        Date expireDate=bookingExpireDetails.getExpireDate();
        Booking booking=bookingDao.read(bookingId);
        BookingPayLater bookingPayLater=booking.getBookingPayLater();

        if (bookingPayLater!=null && !bookingPayLater.getExpire()) {
            expireDate = bookingPayLater.getExpireDate();
        }

        Long time = (expireDate.getTime() - commonService.getCurrentTime());
        Long dailySchedulerTimeDiff=new DateTime(commonService.getCurrentTime()).toDateMidnight().getMillis()+(24 * 60 * 60 * 1000)-(commonService.getCurrentTime()-(10*1000));
        if (time < 0)
            time = 0L;

        if (time<dailySchedulerTimeDiff) {
            LOGGER.info("booking :{} will be EXPIRE in {} ms>>>>>>>>>>>>>>>", bookingId, time);

            scheduledExecutorService.schedule(
                    new ExpireBooking(bookingId, worker)
                    , time,
                    TimeUnit.MILLISECONDS);
        }
    }

    @Transactional
    public void discardBooking(int bookingId) {
        //LOGGER.info("discardBooking scheduler start for bookingId:----->{}", bookingId);
        BookingHistory bookingHistory=bookingHistoryDao.getBookingHistory(bookingId, StatesEnum.INITIATED.value());
        Long time = (bookingHistory.getCreatedAt().getTime()+(10 * 60 * 1000) - commonService.getCurrentTime());
        if (time < 0)
            time = 0L;
        //Long time = 5 * 60 * 1000L;
        LOGGER.info("booking :{} will be DISCARD in {} ms>>>>>>>>>>>>>>>", bookingId, time);
        scheduledExecutorService.schedule(
                new DeclineBooking(bookingId, worker)
                , time,
                TimeUnit.MILLISECONDS);
    }

    private class ExpireBooking implements Runnable {
        private int bookingId;

        private Worker worker;

        public ExpireBooking(int bookingId, Worker worker) {
            this.bookingId = bookingId;
            this.worker = worker;
        }

        @Transactional
        public void run() {
            worker.expire(bookingId);
        }
    }

    private class DeclineBooking implements Runnable {
        private int bookingId;

        private Worker worker;

        public DeclineBooking(int bookingId, Worker worker) {
            this.bookingId = bookingId;
            this.worker = worker;
        }

        @Transactional
        public void run() {
            worker.decline(bookingId);
        }
    }

    private class Worker {

       // private Object lock = new Object();

        @Transactional
        private synchronized void expire(int bookingId) {
            //synchronized (lock) {
                LOGGER.info("inside expire scheduler>>>>>>>>>>> {}" , bookingId);
                BookingActionDto bookingActionDto = new BookingActionDto();
                bookingActionDto.setBooking_id(bookingId);
                bookingActionDto.setEvent(EventsEnum.EXPIRE.value());
                try {
                    bookingService.bookingActions(bookingActionDto, true);
                } catch (Exception e) {
                    LOGGER.error("expireBooking exception----->{}", e);
                }
            }
        //}

        @Transactional
        private synchronized void decline(int bookingId) {
            LOGGER.info("inside decline scheduler>>>>>>>>>>> {}" , bookingId);
            BookingActionDto bookingActionDto = new BookingActionDto();
            bookingActionDto.setBooking_id(bookingId);
            bookingActionDto.setEvent(EventsEnum.DISCARD.value());
            try {
                bookingService.bookingActions(bookingActionDto, true);
            } catch (Exception e) {
                LOGGER.error("discardBooking exception----->{}", e);
            }
        }
    }

    @PostConstruct
    private void beanCreated(){
        LOGGER.info("service scheduler has been created");
    }
}
