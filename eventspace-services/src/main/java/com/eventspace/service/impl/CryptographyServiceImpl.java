package com.eventspace.service.impl;

import com.eventspace.service.CommonService;
import com.eventspace.service.CryptographyService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.transaction.Transactional;
import javax.xml.bind.DatatypeConverter;

/**
 * Created by Auxenta on 6/7/2017.
 */
@Service
public class CryptographyServiceImpl implements CryptographyService {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(CryptographyServiceImpl.class);

    @Autowired
    private CommonService commonService;

    private static final  String KEY = "t2s67p1a345o7rzm";
    private static final  String IV = "abcdefghabcdefgd";

    @Override
    @Transactional
    public String encrypt(String originalString) {
        try {


            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            int blockSize = cipher.getBlockSize();

            byte[] dataBytes = originalString.getBytes();
            int plaintextLength = dataBytes.length;
            if (plaintextLength % blockSize != 0) {
                plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
            }

            byte[] plaintext = new byte[plaintextLength];
            System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);

            SecretKeySpec keyspec = new SecretKeySpec(KEY.getBytes(), "AES");
            IvParameterSpec ivspec = new IvParameterSpec(IV.getBytes());

            cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
            byte[] encrypted = cipher.doFinal(plaintext);

            return DatatypeConverter.printBase64Binary(encrypted);

        } catch (Exception e) {
            LOGGER.error("encrypt exception----->{}",e);
            return null;
        }
    }

    @Override
    @Transactional
    public String decrypt(String encryptedString) {
        try {
            byte[] encrypted1 = DatatypeConverter.parseBase64Binary(encryptedString);

            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            SecretKeySpec keyspec = new SecretKeySpec(KEY.getBytes(), "AES");
            IvParameterSpec ivspec = new IvParameterSpec(IV.getBytes());

            cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

            byte[] original = cipher.doFinal(encrypted1);
            LOGGER.info("The original String is : " + new String(original));
            return new String(original);
        } catch (Exception e) {
            LOGGER.error("decrypt exception----->{}",e);
            return null;
        }
    }


    public String encryption(Integer key){
        Long date = commonService.getCurrentTime();
        return encrypt(key + "date:" + date.toString());
    }

    public Integer decryption(String encrypted){
        String[] datas = (decrypt(encrypted.replace(" ", "+"))).split("date:");
        return Integer.parseInt(datas[0].trim());
    }
}
