package com.eventspace.service.impl;

import com.eventspace.dao.*;
import com.eventspace.domain.*;
import com.eventspace.dto.*;
import com.eventspace.service.BookingService;
import com.eventspace.service.CommonService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

/**
 * Created by Aux-054 on 12/15/2016.
 */

/**
 * The Class CommonServiceImpl.
 */
@Service
@Transactional
public class CommonServiceImpl implements CommonService {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(CommonServiceImpl.class);

    @Value("${million.spaces.time.zone}")
    private String timeZone;

    /**
     * The amenity dao.
     */
    @Autowired
    private AmenityDao amenityDao;

    /**
     * The amenity units dao.
     */
    @Autowired
    private AmenityUnitsDao amenityUnitDao;

    /**
     * The event type dao.
     */
    @Autowired
    private EventTypeDao eventTypeDao;

    /**
     * The cancellation policy dao.
     */
    @Autowired
    private CancellationPolicyDao cancellationPolicyDao;

    @Autowired
    private RulesDao rulesDao;

    @Autowired
    private  SeatingArrangementDao seatingArrangementDao;

    @Autowired
    private  MeasurementUnitDao measurementUnitDao;

    @Autowired
    private ChargeTypeDao chargeTypeDao;

    @Autowired
    private BlockChargeTypeDao blockChargeTypeDao;

    @Autowired
    private AndroidReleaseDao androidReleaseDao;

    @Autowired
    private IosReleaseDao iosReleaseDao;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private  SpaceTypeDao spaceTypeDao;

    @Value("${cache.enable}")
    public  Boolean enableCache;

    @Value("${million.spaces.url}")
    private String webUrl;

    @Value("${million.spaces.api.url}")
    private String apiUrl;

    @Value("${cache.server}")
    private String cacheServer;

    @Value("${jdbc.url}")
    private String dbUrl;

    @Value("${scheduler.enable}")
    private Boolean enableScheduler;

    @Autowired
    private ReportsDao reportsDao;
    /**
     * get all amenities.
     *
     * @return the list of amenities
     */
    @Transactional
    @Override
        public List<AmenityDto> getAllAmenities() {
            LOGGER.info("getAllAmenities method -----> get call");
            List<AmenityDto> amenityDtoList = new ArrayList<>();
            List<Amenity> amenities = amenityDao.getAllAmenities();
            if (amenities != null) {
                for (Amenity amenity : amenities) {
                    amenityDtoList.add(amenity.build());
                }
            }
            return amenityDtoList;
    }

    /**
     * get all event types.
     *
     * @return the list of event types
     */
    @Transactional
    @Override
    public List<EventTypeDto> getEventTypes() {
            LOGGER.info("getEventType method -----> get call");
            List<EventTypeDto> eventTypeDtoLists = new ArrayList<>();
            List<EventType> eventTypes = eventTypeDao.getEventTypes();
            if (eventTypes != null) {
                for (EventType eventType : eventTypes) {
                    eventTypeDtoLists.add(eventType.build());
                }
                return eventTypeDtoLists;
            }
            return Collections.emptyList();
    }


    /**
     * get all amenity units.
     *
     * @return the list of amenity units
     */
    @Transactional
    @Override
    public List<AmenityUnitsDto> getAmenityUnits() {
            LOGGER.info("getAmenityUnits method -----> get call");
            List<AmenityUnitsDto> amenityUnitsDtoList = new ArrayList<>();
            List<AmenityUnit> amenityUnits = amenityUnitDao.getAllAmenityUnits();

            if (amenityUnits != null) {
                for (AmenityUnit amenityUnit : amenityUnits) {
                    amenityUnitsDtoList.add(amenityUnit.build());
                }
                return amenityUnitsDtoList;
            }
            return Collections.emptyList();
    }

    /**
     * get all cancellation policy.
     *
     * @return the list of cancellation policy
     */
    @Transactional
    @Override
    public List<CancellationPolicyDto> getCancellationPolicies() {
            LOGGER.info("getACancellationPolicies method -----> get call");
            List<CancellationPolicyDto> cancellationPolicyDtoList = new ArrayList<>();
            List<CancellationPolicy> cancellationPolicies = cancellationPolicyDao.getCancellationPolicies();

            if (cancellationPolicies != null) {
                for (CancellationPolicy cancellationPolicy : cancellationPolicies) {
                    cancellationPolicyDtoList.add(cancellationPolicy.build());
                }
                return cancellationPolicyDtoList;
            }
            return Collections.emptyList();
    }

    @Override
    public List<CommonDto> getAllRules() {
            LOGGER.info("getAllRules method -----> get call");
            List<CommonDto> rulesDtoList = new ArrayList<>();
            List<Rules> rules = rulesDao.getAllRules();

            if (rules != null) {
                for (Rules rule : rules) {
                    rulesDtoList.add(rule.build());
                }
                return rulesDtoList;
            }
            return Collections.emptyList();
    }

    @Override
    public List<CommonDto> getAllSeatingArrangement() {
            LOGGER.info("getAllSeatingArrangement method -----> get call");
            List<CommonDto> seatingArrangementDtoList = new ArrayList<>();
            List<SeatingArrangement> seatingArrangements = seatingArrangementDao.getAllSeatingArrangement();

            if (seatingArrangements != null) {
                for (SeatingArrangement seatingArrangement : seatingArrangements) {
                    seatingArrangementDtoList.add(seatingArrangement.build());
                }
                return seatingArrangementDtoList;
            }
            return Collections.emptyList();
    }


    /**
     * get latest amenities.
     *
     * @param lastUpdateDate the last update date
     * @return the list of latest amenities
     */
    @Transactional
    @Override
    public List<AmenityDto> getLatestAmenities(Date lastUpdateDate) {
            LOGGER.info("getLatestAmenities method -----> get call");
            List<AmenityDto> amenityDtoList = new ArrayList<>();
            List<Amenity> amenities = amenityDao.getLatestAmenities(lastUpdateDate);
            if (amenities != null) {
                for (Amenity amenity : amenities) {
                    amenityDtoList.add(amenity.build());
                }
                return amenityDtoList;
            }
            return Collections.emptyList();
    }


    @Override
    public List<CommonDto> getMeasurementUnits() {
        List<CommonDto> measurementUnitsDtoList = new ArrayList<>();
        List<MeasurementUnit> measurementUnits = measurementUnitDao.getMeasurementUnits();

        if (measurementUnits != null) {
            for (MeasurementUnit measurementUnit : measurementUnits) {
                measurementUnitsDtoList.add(measurementUnit.build());
            }
            return measurementUnitsDtoList;
        }
        else
        return Collections.emptyList();
    }

    @Override
    public List<CommonDto> getAllSpaceTypes() {
        List<CommonDto> spaceTypesDtoList=new ArrayList<>();
        List<SpaceType> spaceTypes= spaceTypeDao.getAllSpaceTypes();

        if (spaceTypes != null) {
            for (SpaceType spaceType : spaceTypes) {
                spaceTypesDtoList.add(spaceType.build());
            }
            return spaceTypesDtoList;
        }
        else
            return Collections.emptyList();
    }

    /**
     * sync data for mobile.
     *
     * @param mobileSyncRequestDatadtos the mobile sync request data dtos
     * @return mobile sync response data dto
     */
    @Transactional
    @Override
    public MobileSyncResponseDatadto syncDataForMobile(List<MobileSyncRequestDatadto> mobileSyncRequestDatadtos) {
            LOGGER.info("syncDataForMobile method -----> get call");
            MobileSyncResponseDatadto mobileSyncResponseDatadto = new MobileSyncResponseDatadto();
            List<AmenityDto> amenityDtoList = new ArrayList<>();
            List<EventTypeDto> eventTypeDtoList = new ArrayList<>();
            List<CancellationPolicyDto> cancellationPolicyDtoList = new ArrayList<>();
            List<CommonDto> rulesDtoList = new ArrayList<>();
            List<CommonDto> seatingArrangementDtoList = new ArrayList<>();


            for (MobileSyncRequestDatadto data : mobileSyncRequestDatadtos) {
                if (data.getTable().equals("amenity")) {
                    List<Amenity> amenities = amenityDao.getLatestAmenities(new Date(data.getTimeStamp()));
                    if (amenities != null) {
                        for (Amenity amenity : amenities) {
                            amenityDtoList.add(amenity.build());
                        }
                    }
                }
                if (data.getTable().equals("eventType")) {
                    List<EventType> eventTypes = eventTypeDao.getLatestEventTypes(new Date(data.getTimeStamp()));
                    if (eventTypes != null) {
                        for (EventType eventType : eventTypes) {
                            eventTypeDtoList.add(eventType.build());
                        }
                    }
                }
                if (data.getTable().equals("cancellationPolicy")) {
                    List<CancellationPolicy> cancellationPolicies = cancellationPolicyDao.getLatestCancellationPolicy(new Date(data.getTimeStamp()));
                    if (cancellationPolicies != null) {
                        for (CancellationPolicy cancellationPoliciy : cancellationPolicies) {
                            cancellationPolicyDtoList.add(cancellationPoliciy.build());
                        }
                    }
                }

                if (data.getTable().equals("rules")) {
                    List<Rules> rules = rulesDao.getLatestRules(new Date(data.getTimeStamp()));
                    if (rules != null) {
                        for (Rules rule : rules) {
                            rulesDtoList.add(rule.build());
                        }
                    }
                }

                if (data.getTable().equals("seatingArrangement")) {
                    List<SeatingArrangement> seatingArrangements = seatingArrangementDao.getLatestSeatingArrangement(new Date(data.getTimeStamp()));
                    if (seatingArrangements != null) {
                        for (SeatingArrangement seatingArrangement : seatingArrangements) {
                            seatingArrangementDtoList.add(seatingArrangement.build());
                        }
                    }
                }

            }

            mobileSyncResponseDatadto.setAmenity(amenityDtoList);
            mobileSyncResponseDatadto.setEventType(eventTypeDtoList);
            mobileSyncResponseDatadto.setCancellationPolicy(cancellationPolicyDtoList);
            mobileSyncResponseDatadto.setRules(rulesDtoList);
            mobileSyncResponseDatadto.setSeatingArrangements(seatingArrangementDtoList);

            return mobileSyncResponseDatadto;
    }


    @Override
    public String getRequestServerName(HttpServletRequest request) {
            //return (server+":"+request.getServerPort());

            return request.getHeader("Origin");
    }

    @Override
    public List<CommonDto> getAllChargeType() {
        List<ChargeType> chargeTypes=chargeTypeDao.getAllChargeTypes();
        List<CommonDto> chargeTypeDtoList = new ArrayList<>();

        for (ChargeType chargeType:chargeTypes)
                chargeTypeDtoList.add(chargeType.build());


        return chargeTypeDtoList;
    }

    @Override
    public List<CommonDto> getAllBlockChargeTypes() {
        List<BlockChargeType> chargeTypes=blockChargeTypeDao.getAllBlockChargeTypes();
        List<CommonDto> chargeTypeDtoList = new ArrayList<>();

        for (BlockChargeType chargeType:chargeTypes)
            chargeTypeDtoList.add(chargeType.build());
        return chargeTypeDtoList;
    }

    @Override
    public Long getCurrentTime() {
        //Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(timeZone));
        LOGGER.info("current time : "+System.currentTimeMillis());
        return System.currentTimeMillis();
    }

    @Override
    public Map<String, Object> getSystemInfo() {
        Map<String, Object> system = new HashMap<>();
        try {
            InetAddress ip=InetAddress.getLocalHost();
            system.put("ip",ip.getHostAddress());
        } catch (UnknownHostException e) {
           LOGGER.info("ERROR : ",e);
        }
        system.put("enableCache", enableCache);
        system.put("webUrl", webUrl);
        system.put("apiUrl", apiUrl);
        system.put("dbUrl", dbUrl);
        system.put("enableScheduler",enableScheduler);
        if (enableCache)
            system.put("cacheServer", cacheServer);
        return system;
    }


    @Override
    public VersionDetailsDto getAndroidVersion() {
        return androidReleaseDao.getLatestRelease().build();
    }

    @Override
    public VersionDetailsDto getIosVersion() {
        return iosReleaseDao.getLatestRelease().build();
    }

    @Override
    public Long convertEpoch(Date date) {
        return date.getTime();
    }

    @Override
    public Map<String, Object> getAllStaticData() {
        Map<String, Object> result = new HashMap<>();
        result.put("amenities",getAllAmenities());
        result.put("eventTypes",getEventTypes());
        result.put("rules",getAllRules());
        result.put("seatingArrangements",getAllSeatingArrangement());
        result.put("amenityUnits",getAmenityUnits());
        result.put("cancellationPolicies",getCancellationPolicies());
        result.put("measurementUnits",getMeasurementUnits());
        result.put("chargeTypes",getAllChargeType());
        result.put("blockChargeTypes",getAllBlockChargeTypes());
        result.put("spaceTypes",getAllSpaceTypes());
        result.put("reservationStatus",bookingService.getAllreservationStatus());

        return result;
    }

    @Override
    public List<Map<String,Object>> procedureListResult(String procedureName) {
        return reportsDao.procedureListResult(procedureName);
    }

    @Override
    public Map<String, Object> procedureUniqueResult(String procedureName) {
        return reportsDao.procedureUniqueResult(procedureName);
    }

    @Override
    public List<Map<String, Object>> tableResults(String tableName) {
        return reportsDao.queryListResults(String.format("select * from %s",tableName));
    }

    @Override
    public Map<String, Object> tableUniqueResults(String tableName, Integer id) {
         return reportsDao.queryUniqueResults(String.format("select * from %s where id=%s",tableName,id));
    }

    @Override
    public List<Map<String, Object>> queryListResult(String sqlQuery) {
        return reportsDao.queryListResults(sqlQuery);
    }

    @Override
    public Map<String, Object> queryUniqueResult(String sqlQuery) {
        return reportsDao.queryUniqueResults(sqlQuery);
    }

    @Override
    public List<Map<String, Object>> commonProcedureListResult(String procedureName) {
        List<Map<String, Object>> allowedProcedures=queryListResult(String.format("select * from common_procedure where name='%s' ",procedureName));
        if (!allowedProcedures.isEmpty()) {
            return procedureListResult(procedureName);
        }
        return null;
    }
}
