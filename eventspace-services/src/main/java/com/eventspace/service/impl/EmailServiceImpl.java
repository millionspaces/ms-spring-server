package com.eventspace.service.impl;


import com.eventspace.dao.*;
import com.eventspace.domain.*;
import com.eventspace.dto.SpaceContactPersonDto;
import com.eventspace.email.dto.EmailMetaData;
import com.eventspace.email.publisher.EventPublisher;
import com.eventspace.enumeration.StatesEnum;
import com.eventspace.eventaction.dto.EmailCriteriaDto;
import com.eventspace.eventaction.dto.MailCriteriaComponents;
import com.eventspace.service.BookingService;
import com.eventspace.service.CryptographyService;
import com.eventspace.service.EmailService;
import com.eventspace.sms.dto.MessageMetaData;
import com.eventspace.sms.dto.SmsContentDto;
import com.eventspace.sms.publisher.MessageEventPublisher;
import com.eventspace.util.Constants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Auxenta on 5/9/2017.
 */

/**
 * The Class EmailServiceImpl.
 */
@Service
@Transactional
public class EmailServiceImpl implements EmailService {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(EmailServiceImpl.class);

    /**
     * The event publisher.
     */
    @Autowired
    private EventPublisher eventPublisher;


    @Autowired
    private MessageEventPublisher messageEventPublisher;



    /**
     * The booking dao.
     */
    @Autowired
    private BookingDao bookingDao;

    /**
     * The review dao.
     */
    @Autowired
    private ReviewsDao reviewsDao;

    /**
     * The space dao.
     */
    @Autowired
    private SpaceDao spaceDao;

    /**
     * The amenity dao.
     */
    @Autowired
    private AmenityDao amenityDao;

    @Autowired
    private PromoDetailsDao promoDetailsDao;

    /**
     * The user dao.
     */
    @Autowired
    private UserDao userDao;

    @Autowired
    private BookingExpireDetailsDao bookingExpireDetailsDao;

    /**
     * The booking service.
     */
    @Autowired
    private BookingService bookingService;

    @Autowired
    private AmenityUnitsDao amenityUnitsDao;

    @Autowired
    private SpacePromotionDao spacePromotionDao;

    @Autowired
    private BookingHistoryDao bookingHistoryDao;

    @Autowired
    private CryptographyService cryptographyService;

    /**
     * The mail criteria components.
     */
    @Autowired
    private MailCriteriaComponents mailCriteriaComponents;


    /** The million spaces url. */
    @Value("${million.spaces.url}")
    private String millionSpacesUrl;


    /** The million spaces facebook url. */
    @Value("${million.spaces.faceboook}")
    private String millionSpacesFacebookUrl;

    /** The million spaces twitter url. */
    @Value("${million.spaces.twitter}")
    private String millionSpacesTwitterUrl;

    /** The million spaces instagram url.. */
    @Value("${million.spaces.instagram}")
    private String millionSpacesInstagramUrl;

    /** The million spaces instagram url.. */
    @Value("${million.spaces.pinterest}")
    private String millionSpacesPinterestUrl;

    /** The million spaces instagram url.. */
    @Value("${million.spaces.telegram}")
    private String millionSpacesTelegramUrl;

    /** The million spaces instagram url.. */
    @Value("${million.spaces.youtube}")
    private String millionSpacesYoutubeUrl;

    @Value("${million.spaces.linkdin}")
    private String millionSpacesLinkdinUrl;

    @Value("${million.spaces.enail.link}")
    private String millionSpacesEmailLink;

    /** The password reset url. */
    @Value("${password.reset.url}")
    private String passwordResetUrl;


    /** The booking approve url. */
    @Value("${upcoming.booking.url}")
    private String bookingDetailsUrl;

    @Value("${guest.view.upcoming.booking.url}")
    private String guestBookingDetailsUrl;

    /** The booking approve url. */
    @Value("${similar.spaces.url}")
    private String similarSpacesUrl;

    /** The booking approve url. */
    @Value("${view.booking.url}")
    private String viewBookingUrl;

    /** The booking approve url. */
    @Value("${review.write.url}")
    private String reviewWriteUrl;

    @Value("${review.view.url}")
    private String viewReviewUrl;

    @Value("${view.user.profile}")
    private String viewUserProfile;

    /**
     * The rest api.
     */
    @Value("${email.verify.url}")
    private String emailVerifyUrl;


    /** The million spaces url. */
    @Value("${sms.booked.confirm.host}")
    private String smsConfirmHost;

    /** The million spaces url. */
    @Value("${sms.booked.confirm.guest}")
    private String smsConfirmGuest;

    /** The million spaces url. */
    @Value("${sms.booked.confirm.host1}")
    private String smsConfirmHost1;

    /** The million spaces url. */
    @Value("${sms.booked.confirm.guest1}")
    private String smsConfirmGuest1;

    /** The million spaces url. */
    @Value("${sms.canceled.host}")
    private String smsCanceledHost;

    /** The million spaces url. */
    @Value("${sms.canceled.guest}")
    private String smsCanceledGuest;

    /** The million spaces url. */
    @Value("${sms.canceled.host1}")
    private String smsCanceledHost1;

    /** The million spaces url. */
    @Value("${sms.canceled.guest1}")
    private String smsCanceledGuest1;

    /** The million spaces url. */
    @Value("${sms.expire.guest}")
    private String smsExpiredGuest;

    @Value("${sms.tentative.book.guest}")
    private String smsTentativeBookGuest;

    @Value("${sms.tentative.book.host}")
    private String smsTentativeBookHost;

    @Value("${sms.tentative.payment.received}")
    private String smsTentativePaymentReceived;

    @Value("${sms.tentative.book.guest1}")
    private String smsTentativeBookGuest1;

    @Value("${sms.tentative.book.host1}")
    private String smsTentativeBookHost1;

    @Value("${sms.tentative.payment.received1}")
    private String smsTentativePaymentReceived1;

    @Value("${space.listing.approved}")
    private String smsSpaceApproved;

    @Value("${space.listed}")
    private String smsSpaceListed;

    /** The million spaces url. */
    @Value("${sms.welcome}")
    private String welcome;


    @Value("${promotion.success.guest}")
    private String smsPromotionSuccessGuest;

    @Value("${promotion.failed.guest}")
    private String smsPromotionFailGuest;

    @Value("${promotion.success.host}")
    private String smsPromotionSuccessHost;

    @Value("${promotion.failed.host}")
    private String smsPromotionFailHost;

    @Value("${sms.paylater.book.guest}")
    private String payLaterGuest;

    @Value("${sms.paylater.book.host}")
    private String payLaterHost;

    @Value("${sms.payatcourt.book.guest}")
    private String payAtCourtGuest;

    @Value("${sms.payatcourt.book.host}")
    private String payAtCourtHost;

    @Value("${is.email.bcc.require}")
    private Boolean isEmailBccRequire;

    @Value("${sms.enable}")
    private Boolean isSmsEnable;

    @Value("${admin.name}")
    private String adminName;

    @Value("${admin.mobile}")
    private String adminMobile;

    private SmsContentDto smsContentDto;

    /**
     * The template.
     */
    private String template;

    /**
     * The subject.
     */
    private String subject;

    /**
     * The date.
     */
    private Map<String, Object> data;

    /**
     * The host email.
     */
    private String hostEmail;

    private String message;

    private List<SpaceContactPersonDto> hosts;

    /**
     * The gueste mail.
     */
    private String guestEmail;

    private Boolean isBccRequire=false;

    private boolean isManual=false;

    /**
     * The date.
     */
    DateFormat date = new SimpleDateFormat(Constants.PATTERN_8);

    /**
     * The time.
     */
    DateFormat time = new SimpleDateFormat(Constants.PATTERN_9);

    DateFormat smsTimeFormat = new SimpleDateFormat(Constants.PATTERN_11);

    private String googleMapUrl="https://www.google.com/maps/?q=";

    private static final int PAY=1;
    private static final int UNDO_PAY=2;
    private static final int CANCEL=3;
    private static final int CONFIRM=4;
    private static final int EXPIRE=5;
    private static final int DISCARD=6;
    private static final int WAIT_FOR_PAY=7;
    private static final int ADVANCE_PENDING_PAYMENT=8;
    private static final int ADVANCED_PAY=9;
    private static final int PAY_AT_COURT=10;
    private static final int PAY_LATER=11;

    /**
     * send mail.
     *
     * @param receiver the receiver
     */
    @Override
    @Transactional
    public void sendMail(String receiver) {
        if(receiver!=null && !receiver.equals("")) {

            LOGGER.info("sendMail method  ---> get called : email send to " + receiver);
            data.put("millionSpacesTwitterUrl", millionSpacesTwitterUrl);
            data.put("millionSpacesFacebookUrl", millionSpacesFacebookUrl);
            data.put("millionSpacesInstagramUrl", millionSpacesInstagramUrl);
            data.put("millionSpacesPinterestUrl", millionSpacesPinterestUrl);
            data.put("millionSpacesTelegramUrl", millionSpacesTelegramUrl);
            data.put("millionSpacesYoutubeUrl", millionSpacesYoutubeUrl);
            data.put("millionSpacesLinkdinUrl", millionSpacesLinkdinUrl);
            data.put("millionSpacesEmailLink", millionSpacesEmailLink);
            data.put("millionSpacesUrl", millionSpacesUrl);
            eventPublisher.proceedEmailEvent(createEmailMetaData(template, receiver, subject, data));
        }
    }


    private void sendMailToSetOfUser(List<SpaceContactPersonDto> hosts){
        for(SpaceContactPersonDto host:hosts)
            sendMail(host.getEmail());
    }

    private void sendSmsToSetOfUser(List<SpaceContactPersonDto> hosts,String message){

        if (isEmailBccRequire) {
            SpaceContactPersonDto admin=new SpaceContactPersonDto();
            admin.setName(adminName);
            admin.setMobile(adminMobile);
            this.hosts.add(admin);
        }

        for(SpaceContactPersonDto host:hosts)
            sendSms(host.getMobile(),message);
    };
    /**
     * send mail.
     *
     * @param bookingId the booking id
     * @param event     the event
     */
    @Override
    @Transactional
    public void sendBookingActionMails(Integer bookingId, Integer event) {
            LOGGER.info("sendBookingActionMails method in EmailService ---> get called");
            switch (event) {
                case PAY:
                    setDataOfBookingConfirmHost(bookingId);
                    sendMailToSetOfUser(hosts);
                    // sendMail(hostEmail);
                    setDataOfBookingConfirmGuest(bookingId);
                     sendMail(guestEmail);
                    sendConfirmGuestSms();
                    sendConfirmHostSms();
                    break;
                case UNDO_PAY:
                    break;
                case CANCEL:
                    setDataOfBookingCancelHost(bookingId);
                    sendMailToSetOfUser(hosts);
                    //sendMail(hostEmail);
                    setDataOfBookingCancelGuest(bookingId);
                    sendMail(guestEmail);
                    sendCanceledHostSms();
                    sendCanceledGuestSms();
                    break;
                case CONFIRM:
                    setDataOfRequestReview(bookingId);
                    sendMail(guestEmail);
                    break;
                case EXPIRE:
                    setDataOfBookingExpireGuest(bookingId);
                    sendMail(guestEmail);
                    setDataOfBookingExpireHost(bookingId);
                    sendMailToSetOfUser(hosts);
                    //sendMail(hostEmail);
                    sendExpiredGuestSms();
                    break;
                case DISCARD:
                    break;
                case WAIT_FOR_PAY:
                    setDataOfManualBookingGuest(bookingId);
                    sendMail(guestEmail);
                    setDataOfManualBookingHost(bookingId);
                    sendMailToSetOfUser(hosts);
                    //sendMail(hostEmail);
                    sendTentativeBookGuestSms();
                    sendTentativeBookHostSms();
                    break;
                case PAY_LATER:
                    setDataOfPayLaterBookingGuest(bookingId);
                    sendMail(guestEmail);
                    //sendPayLaterBookGuestSms();
                case ADVANCED_PAY:
                    setDataOfAdvancedBookingConfirmHost(bookingId);
                    sendMailToSetOfUser(hosts);
                    // sendMail(hostEmail);
                    setDataOfAdvancedBookingConfirmGuest(bookingId);
                    sendMail(guestEmail);
                    sendPayLaterBookGuestSms();
                    sendPayLaterBookHostSms();
                    break;
                case ADVANCE_PENDING_PAYMENT:
                    setDataOfAdvancePendingPaymentBookingHost(bookingId);
                    sendMailToSetOfUser(hosts);
                    setDataOfAdvancePendingPaymentBookingGuest(bookingId);
                    sendMail(guestEmail);
                    sendAdvanceTentativeBookGuestSms();
                    sendAdvanceTentativeBookHostSms();
                    break;
                case PAY_AT_COURT:
                    setDataOfPayAtCourtBookingConfirmHost(bookingId);
                    sendMailToSetOfUser(hosts);
                    // sendMail(hostEmail);
                    setDataOfPayAtCourtBookingConfirmGuest(bookingId);
                    sendMail(guestEmail);
                    sendPayAtCourtGuestSms();
                    sendPayAtCourtHostSms();
                    break;
                default:
                    break;
            }
    }

    /**
     * set data of email verification mail.
     *
     * @param userName  the user name
     * @param key the reset path
     */

    @Override
    @Transactional
    public void setDataOfMailVerificationEmail(String userName, String key) {
        Map<String, Object> verificationData = new HashMap<>();
        verificationData.put("receiver", userName);
        String url=millionSpacesUrl+emailVerifyUrl+ "?key="+key;
        verificationData.put("emailVerificationUrl",url);

        template = mailCriteriaComponents.buildMailCriteria().getUserEmailVerificationTemplateFIle();
        subject = mailCriteriaComponents.buildMailCriteria().getUserEmailVerificationEmailSubject();
        data = verificationData;
    }

    /**
     * set data of user creation mail.
     *
     * @param receiverName the receiver name
     */

    @Override
    @Transactional
    public void setDataOfUserWelcomeMail(String receiverName) {
        Map<String, Object> welcomeData = new HashMap<>();
        welcomeData.put("receiverName", receiverName);
        welcomeData.put("millionSpacesUrl",millionSpacesUrl);

        template = mailCriteriaComponents.buildMailCriteria().getUserWelcomeTemplateFile();
        subject = mailCriteriaComponents.buildMailCriteria().getUserWelcomeEmailSubject();
        data = welcomeData;
    }

    @Override
    public void sendViewReviewEmail(Integer bookingId) {
        setDataOfViewReview(bookingId);
        sendMailToSetOfUser(hosts);
        //sendMail(hostEmail);
    }

    /**
     * set data of password reset mail.
     *
     * @param userName  the user name
     * @param key the reset path
     */

    @Override
    @Transactional
    public void setDataOfPasswordResetMail(String userName,String host, String key) {
        Map<String, Object> passwordResetData = new HashMap<>();
        passwordResetData.put("receiver", userName);
        String url=millionSpacesUrl+passwordResetUrl+ "?key="+key;
        passwordResetData.put("passwordResetUrl",url);

        template = mailCriteriaComponents.getUserPasswordResetTemplate();
        subject = mailCriteriaComponents.getUserPasswordResetSubject();
        data = passwordResetData;
    }


    @Override
    public void setDataOfUpadetCalenderEndDate(String receiverName,Integer spaceId) {
        Map<String, Object> sampleData = new HashMap<>();
        template = mailCriteriaComponents.buildMailCriteria().getUpdateCalenderEndDateTemplate();
        subject = mailCriteriaComponents.buildMailCriteria().getUpdateCalenderEndDateSubject();

        Space space=spaceDao.read(spaceId);
        User user=userDao.read(space.getUser());
        sampleData.put("SpaceName", space.getName());
        sampleData.put("HostName", user.getName());
        if(space.getAddressLine2()!=null){
            sampleData.put("OrganizationName", space.getAddressLine2());}
        else{
            sampleData.put("OrganizationName", "N/A");
        }

        sampleData.put("Address", space.getAddressLine1());
        if (space.getSpaceAdditionalDetails()!=null) {
            sampleData.put("ContactPersonName", Optional.ofNullable(space.getSpaceAdditionalDetails().getContactPerson()).orElse("N/A"));
            sampleData.put("ContactNumber", Optional.ofNullable(space.getSpaceAdditionalDetails().getCompanyPhone()).orElse("N/A"));
        }else{
            sampleData.put("ContactPersonName","N/A");
            sampleData.put("ContactNumber", "N/A");
        }
        sampleData.put("HostEmail",user.getEmail());
        sampleData.put("ValidTillDate", date.format(space.getCalendarEnd()));
        data=sampleData;

        isBccRequire=isEmailBccRequire;
    }


    @Override
    public void setDataOfSpaceDeactivated(String receiverName, Integer spaceId) {
        setDataOfUpadetCalenderEndDate(receiverName,spaceId);
        template = mailCriteriaComponents.buildMailCriteria().getSpaceDeactivatedTemplate();
        subject=mailCriteriaComponents.buildMailCriteria().getSpaceDeactivatedSubject();
    }


    private void setDataOfManualBookingGuest(Integer bookingId) {
        data = setBookingDetails(bookingId);
        template = mailCriteriaComponents.buildMailCriteria().getManualBookingGuestTemplateFile();
        subject = "Complete your Payment for the Booking: "+ data.get("BookingId");
        isBccRequire=isEmailBccRequire;

    }

    private void setDataOfAdvancePendingPaymentBookingGuest(Integer bookingId) {
        data = setBookingDetails(bookingId);
        template = mailCriteriaComponents.buildMailCriteria().getAdvancedBookingPendingGuestTemplateFile();
        subject = "Complete your Advance Payment for the Booking: "+ data.get("BookingId");
        isBccRequire=isEmailBccRequire;

    }

    private void setDataOfPayLaterBookingGuest(Integer bookingId) {
        data = setBookingDetails(bookingId);
        template = mailCriteriaComponents.buildMailCriteria().getBookNowPayLaterGuestEmailTemplate();
        subject = "Complete your Payment for the Booking: "+ data.get("BookingId");
        isBccRequire=isEmailBccRequire;

    }

    private void setDataOfManualBookingHost(Integer bookingId) {
        data = setBookingDetails(bookingId);
        template = mailCriteriaComponents.buildMailCriteria().getManualBookingHostTemplateFile();
        subject = mailCriteriaComponents.buildMailCriteria().getManualBookingHostEmailSubject();
        isBccRequire=isEmailBccRequire;

    }

    private void setDataOfAdvancePendingPaymentBookingHost(Integer bookingId) {
        data = setBookingDetails(bookingId);
        template = mailCriteriaComponents.buildMailCriteria().getAdvancedBookingPendingHostTemplateFile();
        subject ="Tentative Reservation";
        isBccRequire=isEmailBccRequire;

    }


    /**
     * set data of booking paid host.
     *
     * @param bookingId the booking id
     */
    private void setDataOfBookingConfirmHost(Integer bookingId) {
        data = setBookingDetails(bookingId);
        template = mailCriteriaComponents.buildMailCriteria().getBookingConfirmHostTemplateFile();
        isBccRequire=isEmailBccRequire;
        subject = "Congratulations! Your Space " + data.get("SpaceName") + " has been Reserved!";

    }

    private void setDataOfAdvancedBookingConfirmHost(Integer bookingId) {
        data = setBookingDetails(bookingId);
        template = mailCriteriaComponents.buildMailCriteria().getAdvancedBookingConfirmHostTemplateFile();
        isBccRequire=isEmailBccRequire;
        subject = "Congratulations! Your Space " + data.get("SpaceName") + " has been Reserved!";

    }

    private void setDataOfPayAtCourtBookingConfirmHost(Integer bookingId) {
        data = setBookingDetails(bookingId);
        template = mailCriteriaComponents.buildMailCriteria().getBookingPayAtCourtHostEmailTemplate();
        isBccRequire=isEmailBccRequire;
        subject = "Congratulations! Your Space " + data.get("SpaceName") + " has been Reserved!";

    }
    /**
     * set data of booking paid guest.
     *
     * @param bookingId the booking id
     */
    private void setDataOfBookingConfirmGuest(Integer bookingId) {
        Booking booking=bookingDao.read(bookingId);

        data = setBookingDetails(bookingId);
        template = mailCriteriaComponents.buildMailCriteria().getBookingConfirmGuestTemplateFile();
        Optional.ofNullable(booking.getPromoDetails()).ifPresent(bookingHasPromo -> {
            PromoDetails promoDetails=promoDetailsDao.read(booking.getPromoDetails().getPromoDetails());
            Optional.ofNullable(promoDetails.getCorporatePromo()).ifPresent(corporatePromo -> {
                template=mailCriteriaComponents.buildMailCriteria().getCorporateBookingConfirmGuestEmailTemplate();
            });
        });

        subject = "Congratulations! The Space " + data.get("SpaceName") + " is Yours!";
    }

    private void setDataOfAdvancedBookingConfirmGuest(Integer bookingId) {
        data = setBookingDetails(bookingId);
        template = mailCriteriaComponents.buildMailCriteria().getAdvancedBookingConfirmGuestTemplateFile();
        subject = "Congratulations! The Space " + data.get("SpaceName") + " is Yours!";
    }

    private void setDataOfPayAtCourtBookingConfirmGuest(Integer bookingId) {
        data = setBookingDetails(bookingId);
        template = mailCriteriaComponents.buildMailCriteria().getBookingPayAtCourtGuestEmailTemplate();
        subject = "Congratulations! The Space " + data.get("SpaceName") + " is Yours!";
    }


    /**
     * set data of booking CANCEL host.
     *
     * @param bookingId the booking id
     */
    private void setDataOfBookingCancelHost(Integer bookingId) {
        data = setBookingDetails(bookingId);
        data.put("viewBookingUrl",millionSpacesUrl);
            template = mailCriteriaComponents.buildMailCriteria().getBookingCancelHostTemplateFile();

        subject = data.get("BookingId") + " Reservation Cancellation";
        isBccRequire=isEmailBccRequire;
    }


    /**
     * set data of booking CANCEL guest.
     *
     * @param bookingId the booking id
     */
    private void setDataOfBookingCancelGuest(Integer bookingId) {
        data = setBookingDetails(bookingId);
        data.put("viewBookingUrl",millionSpacesUrl);
            template = mailCriteriaComponents.buildMailCriteria().getBookingCancelGuestTemplateFile();

        subject = "Your Reservation "+data.get("BookingId")+" has been Cancelled";
    }

    private void setDataOfBookingExpireGuest(Integer bookingId) {
        data = setBookingDetails(bookingId);
            template = mailCriteriaComponents.buildMailCriteria().
                    getBookingExpireGuestTemplateFile();

        subject = mailCriteriaComponents.buildMailCriteria().getBookingExpireGuestEmailSubject();
        isBccRequire=isEmailBccRequire;
    }

    private void setDataOfBookingExpireHost(Integer bookingId) {
        data = setBookingDetails(bookingId);
        template = mailCriteriaComponents.buildMailCriteria().
                getBookingExpireHostTemplateFile();

        subject = mailCriteriaComponents.buildMailCriteria().getBookingExpireHostEmailSubject();
        isBccRequire=isEmailBccRequire;
    }

    /**
     * set data of  reque reviewst.
     *
     * @param bookingId the booking id
     */
    private void setDataOfRequestReview(Integer bookingId) {
        if (reviewsDao.getReviewByBookingId(bookingId)==null) {
            template = mailCriteriaComponents.buildMailCriteria().getBookingRequestReviewEmailTemplateFile();
            data = setBookingDetails(bookingId);
            subject = "Write a Review for " + data.get("SpaceName");
            isBccRequire=isEmailBccRequire;
        }
    }

    private void setDataOfViewReview(Integer bookingId) {
        template = mailCriteriaComponents.buildMailCriteria().getViewReviewEmailTemplateFile();
        data = setBookingDetails(bookingId);
        subject = "Review Received for " + data.get("SpaceName");
        isBccRequire=isEmailBccRequire;
    }


    public void setDataOfSpacePublish(String hostName,String spaceName) {
        template = mailCriteriaComponents.buildMailCriteria().getSpacePublishEmailTemplate();
        Map<String, Object> spaceData = new HashMap<>();
        spaceData.put("SpaceName", spaceName);
        spaceData.put("HostName", hostName);
        String url=millionSpacesUrl+viewUserProfile;
        spaceData.put("userProfileUrl", url);
        subject = mailCriteriaComponents.buildMailCriteria().getSpacePublishEmailSubject();
        data=spaceData;
    }

    /**
     * create email meta data.
     *
     * @param template the template
     * @param receiver the receiver
     * @param subject  the subject
     * @param data     the data
     * @return email meta data
     */
    private EmailMetaData createEmailMetaData(String template, String receiver, String subject, Map<String, Object> data) {

        EmailMetaData emailMData = new EmailMetaData();
        emailMData.setToEmailAddresses(receiver);
        emailMData.setVmFile(template);
        emailMData.setSubject(subject);
        emailMData.setData(data);
        emailMData.setBccRequire(isBccRequire);
        isBccRequire=false;
        return emailMData;
    }

    /**
     * set booking details.
     *
     * @param bookingId the booking id
     * @return map of email needed details
     */
    private Map<String, Object> setBookingDetails(Integer bookingId) {
        Map<String, Object> data = new HashMap<>();

        Booking booking = bookingDao.read(bookingId);
        Space space = spaceDao.read(booking.getSpace());
        User host = userDao.read(space.getUser());
        User guest = userDao.read(booking.getUser());

        String smsDate="";
        String smsTime="";
        smsContentDto=new SmsContentDto();

        this.hosts=getASpaceContactPersons(space.getId());

        this.hostEmail = host.getEmail();
        this.guestEmail = guest.getEmail();
       // if(date.format(booking.getFromDate()).equals(date.format(booking.getToDate())) )

        data.put("HostName", host.getName());
        data.put("BookingId", booking.getOrderId());
        data.put("GuestName", guest.getName());
        data.put("GuestEmail", guest.getEmail());
        data.put("SpaceName", space.getName());
        data.put("Address", space.getAddressLine1());
        data.put("AddressLink",googleMapUrl+space.getLatitude()+","+space.getLongitude());
        if(space.getAddressLine2()!=null){
            data.put("OrganizationName", space.getAddressLine2());}
        else{
            data.put("OrganizationName", "N/A");
        }
        //data.put("Date", date.format(booking.getFromDate()).toString());
        //data.put("StartDate", date.format(booking.getFromDate()).toString());
        //data.put("EndDate", date.format(booking.getToDate()).toString());
        //data.put("StartTime", time.format(booking.getFromDate()).toString());
        //data.put("EndTime", time.format(booking.getToDate()).toString());
        Map<String, String> times = new HashMap<>();
        for (BookingSlots bookingSlot:booking.getBookingSlots()){
            times.put(time.format(bookingSlot.getFromDate()),time.format(bookingSlot.getToDate()));
            data.put("Date", date.format(bookingSlot.getFromDate()));
            data.put("StartTime",time.format(bookingSlot.getFromDate()));
            data.put("EndTime", time.format(bookingSlot.getToDate()));
           smsDate=date.format(bookingSlot.getFromDate());
           smsTime=smsTime+smsTimeFormat.format(bookingSlot.getFromDate())+"-"+smsTimeFormat.format(bookingSlot.getToDate())+", ";
        }
        LOGGER.info("times:>>>"+times.toString());
        data.put("Times",times);

        Double cost= booking.getBookingCharge();
        if (cost != null)
            data.put("HiringCharge", String.format("%.2f",cost));
        else
            data.put("HiringCharge", "N/A");

        if(Optional.ofNullable(booking.getAdvance()).isPresent()) {
            data.put("AdvancedCharge", String.format("%.2f",booking.getAdvance()));
            data.put("PendingCharge",String.format("%.2f",cost-booking.getAdvance()));
        }


        Map<String, Integer> ExtrasCount = new HashMap<>();
        Map<String, String> ExtrasUnit = new HashMap<>();
        Set<SpaceExtraAmenity> spaceExtraAmenities = spaceDao.read(booking.getSpace()).getExtraAmenities();
        if (booking.getBookingSpaceExtraAmenities() != null && !booking.getBookingSpaceExtraAmenities().isEmpty()) {
            Set<BookingSpaceExtraAmenity> bookingWithExtraAmenitySet = booking.getBookingSpaceExtraAmenities();
            for (BookingSpaceExtraAmenity bookingWithExtraAmenity : bookingWithExtraAmenitySet) {
                String amenityName = bookingWithExtraAmenity.getPk1().getAmenity().getName();
                Integer amenityAmount = bookingWithExtraAmenity.getNumber();

                for (SpaceExtraAmenity extra : spaceExtraAmenities) {
                    if (extra.getPk().getAmenity().getId().equals(bookingWithExtraAmenity.getPk1().getAmenity().getId())) {
                        String amenityUnit= (amenityUnitsDao.read(extra.getAmenityUnit()).getName().replace("Per",""))+"(s)";
                        //String amenityUnit=(bookingWithExtraAmenity.getPk1().getAmenity().getAmenityUnit().getName().replace("Per",""))+"(s)";
                        ExtrasUnit.put(amenityName, amenityUnit);
                    }
                }

                //String amenityUnit=(bookingWithExtraAmenity.getPk1().getAmenity().getAmenityUnit().getName().replace("Per",""))+"(s)";
                ExtrasCount.put(amenityName, amenityAmount);
            }
            data.put("Na","");
        }else {
            data.put("Na","N/A");
        }
        data.put("ExtraCount", ExtrasCount);
        data.put("newLine","\n");
        data.put("ExtraUnit", ExtrasUnit);
        data.put("viewBookingUrl",millionSpacesUrl+String.format(viewBookingUrl,space.getId(),booking.getId()));
        data.put("similarSpacesUrl",millionSpacesUrl+similarSpacesUrl);
        data.put("writeReviewUrl",millionSpacesUrl+reviewWriteUrl);
        data.put("viewReviewUrl",millionSpacesUrl+String.format(viewReviewUrl,space.getId(),booking.getId()));
        data.put("bookingDetailsUrl",millionSpacesUrl+bookingDetailsUrl);
        data.put("guestBookingView",millionSpacesUrl+String.format(guestBookingDetailsUrl,booking.getId()));


        BookingHistory bookingHistory=bookingHistoryDao.getBookingHistory(bookingId, StatesEnum.PENDING_PAYMENT.value());
        if (bookingHistory==null)
            data.put("PaymentMethod","VISA/Master card");
        else
            data.put("PaymentMethod","Bank payment");

        this.smsContentDto=new SmsContentDto();
        smsContentDto.setReferenceId(booking.getOrderId());
        smsContentDto.setSpaceName(space.getName());
        smsContentDto.setGuestName(guest.getName());
        smsContentDto.setHostName(host.getName());
        smsContentDto.setCharge(String.format("%.2f",cost));
        if (Optional.ofNullable(booking.getAdvance()).isPresent() && booking.getAdvance()>0) {
            smsContentDto.setAdvance(String.format("%.2f", booking.getAdvance()));
            smsContentDto.setBalance(String.format("%.2f", cost - booking.getAdvance()));
        }
        if (space.getSpaceAdditionalDetails()!=null) {
            smsContentDto.setOrganizationName(space.getSpaceAdditionalDetails().getCompanyName());
            smsContentDto.setHostMobile(space.getSpaceAdditionalDetails().getMobilePhone());
        }
        smsContentDto.setDate(smsDate);
        smsContentDto.setTime(smsTime.substring(0,smsTime.length()-2));
        smsContentDto.setGuestMobile(guest.getPhone());
        //smsContentDto.setDate();

        if (bookingExpireDetailsDao.getByBookingId(bookingId)!=null)
            isManual=true;
        else
            isManual=false;
        return data;
    }

    @Override
    public void sendSms(String phoneNumber,String message){
     /*   MessageMetaData metaData=new MessageMetaData();
        metaData.setPhoneNumber(phoneNumber);
        metaData.setMessage(message);
        messageEventPublisher.proceedSmsEvent(metaData);*/
        //AuditMetaData auditMetaData=new AuditMetaData();
        //auditMetaData.setPhoneNumber(phoneNumber);
        //auditMetaData.setMessage(message);
        //auditEventPublisher.proceedAuditEvent(auditMetaData);
        if(isSmsEnable && phoneNumber!=null && !phoneNumber.equals("")) {

            String sms = message;
            if (sms.length() > 160)
                sms = sms.substring(0, 160);

            MessageMetaData messageMetaData = new MessageMetaData();
            messageMetaData.setMessage(sms);
            messageMetaData.setPhoneNumber(phoneNumber);
            messageEventPublisher.proceedSampleEvent(messageMetaData);

            if (message.length() > 160)
                sendSms(phoneNumber, message.substring(160));
        }
    }


    @Override
    public void sendWelcomeSms(String phoneNumber, String userName) {
        message=String.format(welcome,userName);
        sendSms(phoneNumber, message);
    }

    public void sendTest(){
        Integer bookingId=69;
       // setBookingDetails(bookingId);
        //String mobile="0778768065";
        //String mobile="0772150589";
        //String mobile="0776475748";

        setDataOfBookingConfirmHost(bookingId);
        sendMailToSetOfUser(hosts);
        setDataOfBookingConfirmGuest(bookingId);
        sendMail(guestEmail);
        sendConfirmGuestSms();
        sendConfirmHostSms();

        setDataOfBookingCancelHost(bookingId);
        sendMailToSetOfUser(hosts);
        setDataOfBookingCancelGuest(bookingId);
        sendMail(guestEmail);
        sendCanceledHostSms();
        sendCanceledGuestSms();
        setDataOfRequestReview(bookingId);
        sendMail(guestEmail);

        setDataOfBookingExpireGuest(bookingId);
        sendMail(guestEmail);
        setDataOfBookingExpireHost(bookingId);
        sendMailToSetOfUser(hosts);
        sendExpiredGuestSms();

        setDataOfManualBookingGuest(bookingId);
        sendMail(guestEmail);
        setDataOfManualBookingHost(bookingId);
        sendMailToSetOfUser(hosts);
        sendTentativeBookGuestSms();
        sendTentativeBookHostSms();


    }

    private void sendConfirmHostSms(){
        if (smsContentDto.getOrganizationName()!=null) {
        message=String.format(smsConfirmHost,smsContentDto.getReferenceId(),smsContentDto.getSpaceName(),
                smsContentDto.getOrganizationName(),smsContentDto.getGuestName(),smsContentDto.getDate(),smsContentDto.getTime());

        LOGGER.info(message);
        }else{
            message=String.format(smsConfirmHost1,smsContentDto.getReferenceId(),smsContentDto.getSpaceName(),
                    smsContentDto.getGuestName(),smsContentDto.getDate(),smsContentDto.getTime());
            LOGGER.info(message);
        }

        sendSmsToSetOfUser(hosts,message);
        //sendSms(smsContentDto.getHostMobile(),message);
    }

    private void sendConfirmGuestSms(){
        if (isManual) {
            if (smsContentDto.getOrganizationName()!=null) {
                message = String.format(smsTentativePaymentReceived, smsContentDto.getReferenceId(), smsContentDto.getSpaceName(),
                        smsContentDto.getOrganizationName(), smsContentDto.getDate(), smsContentDto.getTime());
                LOGGER.info(message);
                sendSms(smsContentDto.getGuestMobile(), message);
            }else{
                message = String.format(smsTentativePaymentReceived1, smsContentDto.getReferenceId(), smsContentDto.getSpaceName(),
                        smsContentDto.getDate(), smsContentDto.getTime());
                LOGGER.info(message);
                sendSms(smsContentDto.getGuestMobile(), message);
            }
        }else {
            if (smsContentDto.getOrganizationName()!=null) {
                message = String.format(smsConfirmGuest, smsContentDto.getReferenceId(), smsContentDto.getSpaceName(),
                        smsContentDto.getOrganizationName(),smsContentDto.getHostName(),smsContentDto.getDate(), smsContentDto.getTime());
                LOGGER.info(message);
                sendSms(smsContentDto.getGuestMobile(), message);
            }else{
                message = String.format(smsConfirmGuest1, smsContentDto.getReferenceId(), smsContentDto.getSpaceName(),
                        smsContentDto.getHostName(), smsContentDto.getDate(), smsContentDto.getTime());
                LOGGER.info(message);
                sendSms(smsContentDto.getGuestMobile(), message);
            }
            }
        }

    private void sendTentativeBookHostSms(){
        if (smsContentDto.getOrganizationName()!=null) {
            message = String.format(smsTentativeBookHost, smsContentDto.getReferenceId(), smsContentDto.getSpaceName(),
                    smsContentDto.getOrganizationName(), smsContentDto.getGuestName(), smsContentDto.getDate(), smsContentDto.getTime());
            LOGGER.info(message);
            sendSmsToSetOfUser(hosts,message);
            //sendSms(smsContentDto.getHostMobile(), message);
        }else{
            message = String.format(smsTentativeBookHost1, smsContentDto.getReferenceId(), smsContentDto.getSpaceName(),
                     smsContentDto.getGuestName(), smsContentDto.getDate(), smsContentDto.getTime());
            LOGGER.info(message);
            sendSmsToSetOfUser(hosts,message);
            //sendSms(smsContentDto.getHostMobile(), message);
        }

    }

    private void sendAdvanceTentativeBookHostSms(){
            message = String.format(smsTentativeBookHost, smsContentDto.getReferenceId(), smsContentDto.getSpaceName(),
                    smsContentDto.getOrganizationName(), smsContentDto.getGuestName(), smsContentDto.getDate(), smsContentDto.getTime());
            LOGGER.info(message);
            sendSmsToSetOfUser(hosts,message);
    }

    private void sendTentativeBookGuestSms(){
        if (smsContentDto.getOrganizationName()!=null) {
            message=String.format(smsTentativeBookGuest,smsContentDto.getReferenceId(),smsContentDto.getSpaceName(),
                    smsContentDto.getOrganizationName(),smsContentDto.getDate(),smsContentDto.getCharge());
            LOGGER.info(message);
            sendSms(smsContentDto.getGuestMobile(),message);
        }else{
            message=String.format(smsTentativeBookGuest1,smsContentDto.getReferenceId(),smsContentDto.getSpaceName(),
                    smsContentDto.getDate(),smsContentDto.getCharge());
            LOGGER.info(message);
            sendSms(smsContentDto.getGuestMobile(),message);
        }
    }

    private void sendAdvanceTentativeBookGuestSms(){
            message=String.format(smsTentativeBookGuest,smsContentDto.getReferenceId(),smsContentDto.getSpaceName(),
                    smsContentDto.getOrganizationName(),smsContentDto.getDate(),smsContentDto.getAdvance());
            LOGGER.info(message);
            sendSms(smsContentDto.getGuestMobile(),message);
    }

    private void sendPayLaterBookGuestSms(){
            message=String.format(payLaterGuest,smsContentDto.getReferenceId(),Optional.ofNullable(smsContentDto.getOrganizationName()).orElse(smsContentDto.getSpaceName()),
                    smsContentDto.getDate(),smsContentDto.getTime(),smsContentDto.getCharge()
                    ,smsContentDto.getAdvance(),smsContentDto.getBalance());
            LOGGER.info(message);
            sendSms(smsContentDto.getGuestMobile(),message);
    }

    private void sendPayLaterBookHostSms(){
            message=String.format(payLaterHost,smsContentDto.getReferenceId(),Optional.ofNullable(smsContentDto.getOrganizationName()).orElse(smsContentDto.getSpaceName()),
                    smsContentDto.getGuestName(),smsContentDto.getDate(),smsContentDto.getTime(),smsContentDto.getCharge()
                    ,smsContentDto.getAdvance(),smsContentDto.getBalance());
            LOGGER.info(message);
        sendSmsToSetOfUser(hosts,message);

    }

    private void sendPayAtCourtGuestSms(){
        message=String.format(payAtCourtGuest,smsContentDto.getReferenceId(),Optional.ofNullable(smsContentDto.getOrganizationName()).orElse(smsContentDto.getSpaceName()),
                smsContentDto.getDate(),smsContentDto.getTime(),smsContentDto.getCharge());
        LOGGER.info(message);
        sendSms(smsContentDto.getGuestMobile(),message);
    }

    private void sendPayAtCourtHostSms(){
        message=String.format(payAtCourtHost,smsContentDto.getReferenceId(),Optional.ofNullable(smsContentDto.getOrganizationName()).orElse(smsContentDto.getSpaceName()),
                smsContentDto.getGuestName(),smsContentDto.getDate(),smsContentDto.getTime(),smsContentDto.getCharge());
        LOGGER.info(message);
        sendSmsToSetOfUser(hosts,message);

    }

    private void sendCanceledHostSms(){
        if (smsContentDto.getOrganizationName()!=null) {
            message = String.format(smsCanceledHost, smsContentDto.getReferenceId(), smsContentDto.getSpaceName(),
                    smsContentDto.getOrganizationName(), smsContentDto.getDate(), smsContentDto.getTime());
            LOGGER.info(message);
            sendSmsToSetOfUser(hosts,message);
            //sendSms(smsContentDto.getHostMobile(), message);
        }else{
            message = String.format(smsCanceledHost1, smsContentDto.getReferenceId(), smsContentDto.getSpaceName(),
                     smsContentDto.getDate(), smsContentDto.getTime());
            LOGGER.info(message);
            sendSmsToSetOfUser(hosts,message);
            //sendSms(smsContentDto.getHostMobile(), message);
        }
    }

    private void sendCanceledGuestSms(){
        if (smsContentDto.getOrganizationName()!=null) {
            message = String.format(smsCanceledGuest, smsContentDto.getReferenceId(), smsContentDto.getSpaceName(),
                    smsContentDto.getOrganizationName(), smsContentDto.getDate(), smsContentDto.getTime());
            LOGGER.info(message);
            sendSms(smsContentDto.getGuestMobile(), message);
        }else{
            message = String.format(smsCanceledGuest1, smsContentDto.getReferenceId(), smsContentDto.getSpaceName(),
                     smsContentDto.getDate(), smsContentDto.getTime());
            LOGGER.info(message);
            sendSms(smsContentDto.getGuestMobile(), message);
        }
    }

    private void sendExpiredGuestSms(){
        message=String.format(smsExpiredGuest,smsContentDto.getReferenceId());
        LOGGER.info(message);
        sendSms(smsContentDto.getGuestMobile(),message);
    }

    @Override
    public void sendSpaceListedSms(String hostMobileNumber,String spaceName) {
        message=String.format(smsSpaceListed,spaceName);
        LOGGER.info(message);
        sendSms(hostMobileNumber,message);
    }

    @Override
    public void sendSpaceApprovedSms(String hostMobileNumber,String spaceName) {
        message=String.format(smsSpaceApproved,spaceName);
        LOGGER.info(message);
        sendSms(hostMobileNumber,message);
    }

    @Override
    public void sendMailToPromotedSpaceBooking(Integer bookingId) {
        data = setBookingDetails(bookingId);
        isBccRequire=isEmailBccRequire;
        EmailCriteriaDto emailCriteriaDto = mailCriteriaComponents.buildMailCriteria();

        Booking booking=bookingDao.read(bookingId);
        BookingSlots bookingSlot=booking.getBookingSlots().stream().findFirst().get();
        SpacePromotion spacePromotion=spacePromotionDao.getSpacePromotion(booking.getSpace(),bookingSlot.getFromDate(),bookingSlot.getToDate());

        if (spacePromotion!=null) {
            data.put("msCode", spacePromotion.getPromotionCode());
            template = emailCriteriaDto.getBookingPromotionGuestEmailTemplate();
            subject = emailCriteriaDto.getBookingPromotionGuestEmailSubject();
            sendMail(guestEmail);

            template = emailCriteriaDto.getBookingPromotionHostEmailTemplate();
            subject = booking.getOrderId()+" "+ emailCriteriaDto.getBookingPromotionHostEmailSubject();
            sendMailToSetOfUser(hosts);

            sendPromotionGotSms();
        }else{
            template = emailCriteriaDto.getBookingNoPromotionGuestEmailTemplate();
            subject = emailCriteriaDto.getBookingNoPromotionGuestEmailSubject();
            sendMail(guestEmail);

            template = emailCriteriaDto.getBookingNoPromotionHostEmailTemplate();
            subject =booking.getOrderId()+" "+ emailCriteriaDto.getBookingNoPromotionHostEmailSubject();
            sendMailToSetOfUser(hosts);

            sendPromotionMissedSms();
        }

    }

    private void sendPromotionGotSms(){
        sendConfirmHostSms();

        message = String.format(smsPromotionSuccessGuest)+" -"+ " Team MillionSpaces ";
        LOGGER.info(message);
        sendSms(smsContentDto.getGuestMobile(),message);
    }

    private void sendPromotionMissedSms(){
        //message =String.format(smsPromotionFailHost);
        //LOGGER.info(message);
        //sendSmsToSetOfUser(hosts,message);
        sendConfirmHostSms();

        message =String.format(smsPromotionFailGuest)+" -"+" Team MillionSpaces ";
        LOGGER.info(message);
        sendSms(smsContentDto.getGuestMobile(),message);
    }

    @Override
    public  List<SpaceContactPersonDto> getASpaceContactPersons(Integer spaceId) {
        Space space=spaceDao.read(spaceId);
        User user=userDao.read(space.getUser());
        Set<SpaceContactPerson> spaceContactPersons=space.getSpaceContactPersons();
        List<SpaceContactPersonDto> hosts=new ArrayList<>();

        if (spaceContactPersons!=null && !spaceContactPersons.isEmpty()) {
            for (SpaceContactPerson spaceContactPerson : spaceContactPersons) {
                hosts.add(spaceContactPerson.build());
            }
        }
        SpaceContactPersonDto host=new SpaceContactPersonDto();
        host.setEmail(user.getEmail());
        if (space.getSpaceAdditionalDetails()!=null) {
            host.setName(space.getSpaceAdditionalDetails().getContactPerson());
            host.setMobile(space.getSpaceAdditionalDetails().getMobilePhone());
        }
        hosts.add(host);

        return hosts;
    }

}
