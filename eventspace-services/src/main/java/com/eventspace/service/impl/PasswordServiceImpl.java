package com.eventspace.service.impl;

import com.eventspace.dao.UserDao;
import com.eventspace.domain.User;
import com.eventspace.dto.PassWordResetDto;
import com.eventspace.dto.UserPasswordResetDto;
import com.eventspace.email.publisher.EventPublisher;
import com.eventspace.enumeration.BooleanEnum;
import com.eventspace.eventaction.dto.MailCriteriaComponents;
import com.eventspace.service.CommonService;
import com.eventspace.service.CryptographyService;
import com.eventspace.service.EmailService;
import com.eventspace.service.PasswordService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by Auxenta on 3/1/2017.
 */

/**
 * The Class PasswordServiceImpl.
 */
@Service
@Transactional
public class PasswordServiceImpl implements PasswordService {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(PasswordServiceImpl.class);
    /**
     * The user dao.
     */
    @Autowired
    private UserDao userDao;

    /**
     * The event publisher.
     */
    @Autowired
    private EventPublisher eventPublisher;

    /**
     * The mail criteria components.
     */
    @Autowired
    private MailCriteriaComponents mailCriteriaComponents;

    /**
     * The email service.
     */
    @Autowired
    private EmailService emailService;

    /**
     * The encryptor and decryptor.
     */
    @Autowired
    private CryptographyService cryptographyService;

    /**
     * The rest api.
     */
    @Value("${password.reset.url}")
    private String passwordResetUrl;

    @Autowired
    private CommonService commonService;

    /**
     * forget password.
     *
     * @param userMail the user mail
     * @return boolean
     */
    @Override
    @Transactional
    public Boolean forgetPassword(String userMail, String host) {
        LOGGER.error("forgetPassword in PasswordServiceImpl---> get called");
        if (userDao.checkEmail(userMail) == BooleanEnum.TRUE.value()) {
            User user = userDao.readUserByName(userMail);
            emailService.setDataOfPasswordResetMail(user.getName(), host, generatePasswordResetKey(user.getEmail()));
            emailService.sendMail(user.getEmail());
            return true;
        } else {
            return false;
        }
    }

    /**
     * reset password.
     *
     * @param encrypted        the encrypted
     * @param passWordResetDto the password reset dto
     * @return string
     */
    @Override
    @Transactional
    public Boolean resetPassword(String encrypted, PassWordResetDto passWordResetDto) {
        LOGGER.error("resetPassword in PasswordServiceImpl---> get called");
        String[] datas = (cryptographyService.decrypt(encrypted.replace(" ", "+"))).split("date:");
        Integer userId = Integer.parseInt(datas[0].trim());

        User user = userDao.read(userId);
        if (user != null) {
            user.setPassword(passWordResetDto.getNewPassWord());
            //if (TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis() - Long.parseLong(datas[1].trim())) < 1) {
            userDao.update(user);
            return true;
        } else
            return false;
        // } else
        //   return "link has been expire";

    }

    /**
     * nenerate rest url.
     *
     * @param mail the mail
     * @return url string
     */
    @Transactional
    private String generatePasswordResetKey(String mail) {
        User user = userDao.readUserByName(mail);
        Long date = commonService.getCurrentTime();

        return cryptographyService.encrypt(user.getId() + "date:" + date.toString());
    }

    @Override
    public Boolean resetUserPassword(Integer userId, UserPasswordResetDto userPasswordResetDto) {
        User user = userDao.read(userId);
        if (user.getPassword().equals(userPasswordResetDto.getOldPassWord())) {
            user.setPassword(userPasswordResetDto.getNewPassWord());
            userDao.update(user);
            return true;
        } else
            return false;
    }
}
