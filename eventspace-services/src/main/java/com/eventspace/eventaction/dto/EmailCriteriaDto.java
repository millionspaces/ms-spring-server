package com.eventspace.eventaction.dto;

import com.eventspace.dao.BookingSpaceExtraAmenityDao;
import com.eventspace.domain.BookingSpaceExtraAmenity;

import java.io.Serializable;

/**
 * Event space
 * Created by Aux-054 on 1/9/2017.
 */
public class EmailCriteriaDto implements Serializable {

    private String userEmailVerificationEmailSubject;
    private String userWelcomeEmailSubject;
    private String userPasswordResetSubject;
    private String bookingPaymentHostEmailSubject;
    private String bookingPaymentGuestEmailSubject;
    private String bookingCancelGuestEmailSubject;
    private String bookingCancelHostEmailSubject;
    private String bookingExpireGuestEmailSubject;
    private String bookingExpireHostEmailSubject;
    private String bookingRequestReviewEmailSubject;
    private String viewReviewEmailSubject;
    private String manualBookingGuestEmailSubject;
    private String manualBookingHostEmailSubject;
    private String updateCalenderEndDateSubject;
    private String spaceDeactivatedSubject;
    private String spacePublishEmailSubject;
    private String bookingPromotionHostEmailSubject;
    private String bookingNoPromotionHostEmailSubject;
    private String bookingPromotionGuestEmailSubject;
    private String bookingNoPromotionGuestEmailSubject;
    private String bookNowPayLaterGuestEmailSubject;
    private String bookNowPayLaterReminderGuestEmailSubject;
    private String bookingPayAtCourtHostEmailSubject;
    private String bookingPayAtCourtGuestEmailSubject;



    private String userEmailVerificationTemplateFIle;
    private String userWelcomeTemplateFile;
    private String userPasswordResetTemplateFile;
    private String bookingConfirmHostTemplateFile;
    private String bookingConfirmGuestTemplateFile;
    private String advancedBookingConfirmHostTemplateFile;
    private String advancedBookingConfirmGuestTemplateFile;
    private String advancedBookingPendingHostTemplateFile;
    private String advancedBookingPendingGuestTemplateFile;
    private String bookingCancelHostTemplateFile;
    private String bookingCancelGuestTemplateFile;
    private String bookingExpireGuestTemplateFile;
    private String bookingExpireHostTemplateFile;
    private String bookingRequestReviewEmailTemplateFile;
    private String viewReviewEmailTemplateFile;
    private String manualBookingGuestTemplateFile;
    private String manualBookingHostTemplateFile;
    private String updateCalenderEndDateTemplate;
    private String spaceDeactivatedTemplate;
    private String spacePublishEmailTemplate;
    private String bookingPromotionHostEmailTemplate;
    private String bookingNoPromotionHostEmailTemplate;
    private String bookingPromotionGuestEmailTemplate;
    private String bookingNoPromotionGuestEmailTemplate;
    private String bookNowPayLaterGuestEmailTemplate;
    private String bookNowPayLaterReminderGuestEmailTemplate;
    private String corporateBookingConfirmGuestEmailTemplate;
    private String bookingPayAtCourtGuestEmailTemplate;
    private String bookingPayAtCourtHostEmailTemplate;

    public String getUserEmailVerificationTemplateFIle() {
        return userEmailVerificationTemplateFIle;
    }

    public void setUserEmailVerificationTemplateFIle(String userEmailVerificationTemplateFIle) {
        this.userEmailVerificationTemplateFIle = userEmailVerificationTemplateFIle;
    }

    public String getUserEmailVerificationEmailSubject() {
        return userEmailVerificationEmailSubject;
    }

    public void setUserEmailVerificationEmailSubject(String userEmailVerificationEmailSubject) {
        this.userEmailVerificationEmailSubject = userEmailVerificationEmailSubject;
    }


    private transient BookingSpaceExtraAmenityDao bookingSpaceExtraAmenityDao;

    private BookingSpaceExtraAmenity bookingSpaceExtraAmenity;


    public String getBookingConfirmHostTemplateFile() {
        return bookingConfirmHostTemplateFile;
    }

    public void setBookingConfirmHostTemplateFile(String bookingConfirmHostTemplateFile) {
        this.bookingConfirmHostTemplateFile = bookingConfirmHostTemplateFile;
    }

    public String getBookingPaymentHostEmailSubject() {
        return bookingPaymentHostEmailSubject;
    }

    public void setBookingPaymentHostEmailSubject(String bookingPaymentHostEmailSubject) {
        this.bookingPaymentHostEmailSubject = bookingPaymentHostEmailSubject;
    }

    public String getBookingConfirmGuestTemplateFile() {
        return bookingConfirmGuestTemplateFile;
    }

    public void setBookingConfirmGuestTemplateFile(String bookingConfirmGuestTemplateFile) {
        this.bookingConfirmGuestTemplateFile = bookingConfirmGuestTemplateFile;
    }

    public String getBookingPaymentGuestEmailSubject() {
        return bookingPaymentGuestEmailSubject;
    }

    public void setBookingPaymentGuestEmailSubject(String bookingPaymentGuestEmailSubject) {
        this.bookingPaymentGuestEmailSubject = bookingPaymentGuestEmailSubject;
    }

    public String getBookingCancelHostTemplateFile() {
        return bookingCancelHostTemplateFile;
    }

    public void setBookingCancelHostTemplateFile(String bookingCancelHostTemplateFile) {
        this.bookingCancelHostTemplateFile = bookingCancelHostTemplateFile;
    }

    public String getBookingCancelHostEmailSubject() {
        return bookingCancelHostEmailSubject;
    }

    public void setBookingCancelHostEmailSubject(String bookingCancelHostEmailSubject) {
        this.bookingCancelHostEmailSubject = bookingCancelHostEmailSubject;
    }

    public String getBookingCancelGuestTemplateFile() {
        return bookingCancelGuestTemplateFile;
    }

    public void setBookingCancelGuestTemplateFile(String bookingCancelGuestTemplateFile) {
        this.bookingCancelGuestTemplateFile = bookingCancelGuestTemplateFile;
    }

    public String getBookingCancelGuestEmailSubject() {
        return bookingCancelGuestEmailSubject;
    }

    public void setBookingCancelGuestEmailSubject(String bookingCancelGuestEmailSubject) {
        this.bookingCancelGuestEmailSubject = bookingCancelGuestEmailSubject;
    }


    public BookingSpaceExtraAmenityDao getBookingSpaceExtraAmenityDao() {
        return bookingSpaceExtraAmenityDao;
    }

    public void setBookingSpaceExtraAmenityDao(BookingSpaceExtraAmenityDao bookingSpaceExtraAmenityDao) {
        this.bookingSpaceExtraAmenityDao = bookingSpaceExtraAmenityDao;
    }

    public BookingSpaceExtraAmenity getBookingSpaceExtraAmenity() {
        return bookingSpaceExtraAmenity;
    }

    public void setBookingSpaceExtraAmenity(BookingSpaceExtraAmenity bookingSpaceExtraAmenity) {
        this.bookingSpaceExtraAmenity = bookingSpaceExtraAmenity;
    }

    public String getUserWelcomeEmailSubject() {
        return userWelcomeEmailSubject;
    }

    public void setUserWelcomeEmailSubject(String userWelcomeEmailSubject) {
        this.userWelcomeEmailSubject = userWelcomeEmailSubject;
    }

    public String getUserWelcomeTemplateFile() {
        return userWelcomeTemplateFile;
    }

    public void setUserWelcomeTemplateFile(String userWelcomeTemplateFile) {
        this.userWelcomeTemplateFile = userWelcomeTemplateFile;
    }

    public String getBookingRequestReviewEmailSubject() {
        return bookingRequestReviewEmailSubject;
    }

    public void setBookingRequestReviewEmailSubject(String bookingRequestReviewEmailSubject) {
        this.bookingRequestReviewEmailSubject = bookingRequestReviewEmailSubject;
    }

    public String getBookingRequestReviewEmailTemplateFile() {
        return bookingRequestReviewEmailTemplateFile;
    }

    public void setBookingRequestReviewEmailTemplateFile(String bookingRequestReviewEmailTemplateFile) {
        this.bookingRequestReviewEmailTemplateFile = bookingRequestReviewEmailTemplateFile;
    }


    public String getUserPasswordResetSubject() {
        return userPasswordResetSubject;
    }

    public void setUserPasswordResetSubject(String userPasswordResetSubject) {
        this.userPasswordResetSubject = userPasswordResetSubject;
    }

    public String getUserPasswordResetTemplateFile() {
        return userPasswordResetTemplateFile;
    }

    public void setUserPasswordResetTemplateFile(String userPasswordResetTemplateFile) {
        this.userPasswordResetTemplateFile = userPasswordResetTemplateFile;
    }

    public String getManualBookingGuestEmailSubject() {
        return manualBookingGuestEmailSubject;
    }

    public void setManualBookingGuestEmailSubject(String manualBookingGuestEmailSubject) {
        this.manualBookingGuestEmailSubject = manualBookingGuestEmailSubject;
    }

    public String getManualBookingGuestTemplateFile() {
        return manualBookingGuestTemplateFile;
    }

    public void setManualBookingGuestTemplateFile(String manualBookingGuestTemplateFile) {
        this.manualBookingGuestTemplateFile = manualBookingGuestTemplateFile;
    }

    public String getUpdateCalenderEndDateSubject() {
        return updateCalenderEndDateSubject;
    }

    public void setUpdateCalenderEndDateSubject(String updateCalenderEndDateSubject) {
        this.updateCalenderEndDateSubject = updateCalenderEndDateSubject;
    }

    public String getUpdateCalenderEndDateTemplate() {
        return updateCalenderEndDateTemplate;
    }

    public void setUpdateCalenderEndDateTemplate(String updateCalenderEndDateTemplate) {
        this.updateCalenderEndDateTemplate = updateCalenderEndDateTemplate;
    }

    public String getSpaceDeactivatedTemplate() {
        return spaceDeactivatedTemplate;
    }

    public void setSpaceDeactivatedTemplate(String spaceDeactivatedTemplate) {
        this.spaceDeactivatedTemplate = spaceDeactivatedTemplate;
    }

    public String getSpaceDeactivatedSubject() {
        return spaceDeactivatedSubject;
    }

    public void setSpaceDeactivatedSubject(String spaceDeactivatedSubject) {
        this.spaceDeactivatedSubject = spaceDeactivatedSubject;
    }

    public String getBookingExpireGuestEmailSubject() {
        return bookingExpireGuestEmailSubject;
    }

    public void setBookingExpireGuestEmailSubject(String bookingExpireGuestEmailSubject) {
        this.bookingExpireGuestEmailSubject = bookingExpireGuestEmailSubject;
    }

    public String getBookingExpireHostEmailSubject() {
        return bookingExpireHostEmailSubject;
    }

    public void setBookingExpireHostEmailSubject(String bookingExpireHostEmailSubject) {
        this.bookingExpireHostEmailSubject = bookingExpireHostEmailSubject;
    }

    public String getViewReviewEmailSubject() {
        return viewReviewEmailSubject;
    }

    public void setViewReviewEmailSubject(String viewReviewEmailSubject) {
        this.viewReviewEmailSubject = viewReviewEmailSubject;
    }

    public String getManualBookingHostEmailSubject() {
        return manualBookingHostEmailSubject;
    }

    public void setManualBookingHostEmailSubject(String manualBookingHostEmailSubject) {
        this.manualBookingHostEmailSubject = manualBookingHostEmailSubject;
    }

    public String getSpacePublishEmailSubject() {
        return spacePublishEmailSubject;
    }

    public void setSpacePublishEmailSubject(String spacePublishEmailSubject) {
        this.spacePublishEmailSubject = spacePublishEmailSubject;
    }

    public String getBookingExpireGuestTemplateFile() {
        return bookingExpireGuestTemplateFile;
    }

    public void setBookingExpireGuestTemplateFile(String bookingExpireGuestTemplateFile) {
        this.bookingExpireGuestTemplateFile = bookingExpireGuestTemplateFile;
    }

    public String getBookingExpireHostTemplateFile() {
        return bookingExpireHostTemplateFile;
    }

    public void setBookingExpireHostTemplateFile(String bookingExpireHostTemplateFile) {
        this.bookingExpireHostTemplateFile = bookingExpireHostTemplateFile;
    }

    public String getViewReviewEmailTemplateFile() {
        return viewReviewEmailTemplateFile;
    }

    public void setViewReviewEmailTemplateFile(String viewReviewEmailTemplateFile) {
        this.viewReviewEmailTemplateFile = viewReviewEmailTemplateFile;
    }

    public String getManualBookingHostTemplateFile() {
        return manualBookingHostTemplateFile;
    }

    public void setManualBookingHostTemplateFile(String manualBookingHostTemplateFile) {
        this.manualBookingHostTemplateFile = manualBookingHostTemplateFile;
    }

    public String getSpacePublishEmailTemplate() {
        return spacePublishEmailTemplate;
    }

    public void setSpacePublishEmailTemplate(String spacePublishEmailTemplate) {
        this.spacePublishEmailTemplate = spacePublishEmailTemplate;
    }

    public String getBookingPromotionHostEmailSubject() {
        return bookingPromotionHostEmailSubject;
    }

    public void setBookingPromotionHostEmailSubject(String bookingPromotionHostEmailSubject) {
        this.bookingPromotionHostEmailSubject = bookingPromotionHostEmailSubject;
    }

    public String getBookingPromotionGuestEmailSubject() {
        return bookingPromotionGuestEmailSubject;
    }

    public void setBookingPromotionGuestEmailSubject(String bookingPromotionGuestEmailSubject) {
        this.bookingPromotionGuestEmailSubject = bookingPromotionGuestEmailSubject;
    }

    public String getBookingPromotionHostEmailTemplate() {
        return bookingPromotionHostEmailTemplate;
    }

    public void setBookingPromotionHostEmailTemplate(String bookingPromotionHostEmailTemplate) {
        this.bookingPromotionHostEmailTemplate = bookingPromotionHostEmailTemplate;
    }

    public String getBookingPromotionGuestEmailTemplate() {
        return bookingPromotionGuestEmailTemplate;
    }

    public void setBookingPromotionGuestEmailTemplate(String bookingPromotionGuestEmailTemplate) {
        this.bookingPromotionGuestEmailTemplate = bookingPromotionGuestEmailTemplate;
    }

    public String getBookingNoPromotionGuestEmailSubject() {
        return bookingNoPromotionGuestEmailSubject;
    }

    public void setBookingNoPromotionGuestEmailSubject(String bookingNoPromotionGuestEmailSubject) {
        this.bookingNoPromotionGuestEmailSubject = bookingNoPromotionGuestEmailSubject;
    }

    public String getBookingNoPromotionGuestEmailTemplate() {
        return bookingNoPromotionGuestEmailTemplate;
    }

    public void setBookingNoPromotionGuestEmailTemplate(String bookingNoPromotionGuestEmailTemplate) {
        this.bookingNoPromotionGuestEmailTemplate = bookingNoPromotionGuestEmailTemplate;
    }

    public String getBookingNoPromotionHostEmailSubject() {
        return bookingNoPromotionHostEmailSubject;
    }

    public void setBookingNoPromotionHostEmailSubject(String bookingNoPromotionHostEmailSubject) {
        this.bookingNoPromotionHostEmailSubject = bookingNoPromotionHostEmailSubject;
    }

    public String getBookingNoPromotionHostEmailTemplate() {
        return bookingNoPromotionHostEmailTemplate;
    }

    public void setBookingNoPromotionHostEmailTemplate(String bookingNoPromotionHostEmailTemplate) {
        this.bookingNoPromotionHostEmailTemplate = bookingNoPromotionHostEmailTemplate;
    }

    public String getBookNowPayLaterGuestEmailSubject() {
        return bookNowPayLaterGuestEmailSubject;
    }

    public void setBookNowPayLaterGuestEmailSubject(String bookNowPayLaterGuestEmailSubject) {
        this.bookNowPayLaterGuestEmailSubject = bookNowPayLaterGuestEmailSubject;
    }

    public String getBookNowPayLaterReminderGuestEmailSubject() {
        return bookNowPayLaterReminderGuestEmailSubject;
    }

    public void setBookNowPayLaterReminderGuestEmailSubject(String bookNowPayLaterReminderGuestEmailSubject) {
        this.bookNowPayLaterReminderGuestEmailSubject = bookNowPayLaterReminderGuestEmailSubject;
    }

    public String getBookNowPayLaterGuestEmailTemplate() {
        return bookNowPayLaterGuestEmailTemplate;
    }

    public void setBookNowPayLaterGuestEmailTemplate(String bookNowPayLaterGuestEmailTemplate) {
        this.bookNowPayLaterGuestEmailTemplate = bookNowPayLaterGuestEmailTemplate;
    }

    public String getBookNowPayLaterReminderGuestEmailTemplate() {
        return bookNowPayLaterReminderGuestEmailTemplate;
    }

    public void setBookNowPayLaterReminderGuestEmailTemplate(String bookNowPayLaterReminderGuestEmailTemplate) {
        this.bookNowPayLaterReminderGuestEmailTemplate = bookNowPayLaterReminderGuestEmailTemplate;
    }

    public String getCorporateBookingConfirmGuestEmailTemplate() {
        return corporateBookingConfirmGuestEmailTemplate;
    }

    public void setCorporateBookingConfirmGuestEmailTemplate(String corporateBookingConfirmGuestEmailTemplate) {
        this.corporateBookingConfirmGuestEmailTemplate = corporateBookingConfirmGuestEmailTemplate;
    }

    public String getAdvancedBookingConfirmHostTemplateFile() {
        return advancedBookingConfirmHostTemplateFile;
    }

    public void setAdvancedBookingConfirmHostTemplateFile(String advancedBookingConfirmHostTemplateFile) {
        this.advancedBookingConfirmHostTemplateFile = advancedBookingConfirmHostTemplateFile;
    }

    public String getAdvancedBookingConfirmGuestTemplateFile() {
        return advancedBookingConfirmGuestTemplateFile;
    }

    public void setAdvancedBookingConfirmGuestTemplateFile(String advancedBookingConfirmGuestTemplateFile) {
        this.advancedBookingConfirmGuestTemplateFile = advancedBookingConfirmGuestTemplateFile;
    }

    public String getBookingPayAtCourtHostEmailSubject() {
        return bookingPayAtCourtHostEmailSubject;
    }

    public void setBookingPayAtCourtHostEmailSubject(String bookingPayAtCourtHostEmailSubject) {
        this.bookingPayAtCourtHostEmailSubject = bookingPayAtCourtHostEmailSubject;
    }

    public String getBookingPayAtCourtGuestEmailSubject() {
        return bookingPayAtCourtGuestEmailSubject;
    }

    public void setBookingPayAtCourtGuestEmailSubject(String bookingPayAtCourtGuestEmailSubject) {
        this.bookingPayAtCourtGuestEmailSubject = bookingPayAtCourtGuestEmailSubject;
    }

    public String getBookingPayAtCourtGuestEmailTemplate() {
        return bookingPayAtCourtGuestEmailTemplate;
    }

    public void setBookingPayAtCourtGuestEmailTemplate(String bookingPayAtCourtGuestEmailTemplate) {
        this.bookingPayAtCourtGuestEmailTemplate = bookingPayAtCourtGuestEmailTemplate;
    }

    public String getBookingPayAtCourtHostEmailTemplate() {
        return bookingPayAtCourtHostEmailTemplate;
    }

    public void setBookingPayAtCourtHostEmailTemplate(String bookingPayAtCourtHostEmailTemplate) {
        this.bookingPayAtCourtHostEmailTemplate = bookingPayAtCourtHostEmailTemplate;
    }

    public String getAdvancedBookingPendingHostTemplateFile() {
        return advancedBookingPendingHostTemplateFile;
    }

    public void setAdvancedBookingPendingHostTemplateFile(String advancedBookingPendingHostTemplateFile) {
        this.advancedBookingPendingHostTemplateFile = advancedBookingPendingHostTemplateFile;
    }

    public String getAdvancedBookingPendingGuestTemplateFile() {
        return advancedBookingPendingGuestTemplateFile;
    }

    public void setAdvancedBookingPendingGuestTemplateFile(String advancedBookingPendingGuestTemplateFile) {
        this.advancedBookingPendingGuestTemplateFile = advancedBookingPendingGuestTemplateFile;
    }
}
