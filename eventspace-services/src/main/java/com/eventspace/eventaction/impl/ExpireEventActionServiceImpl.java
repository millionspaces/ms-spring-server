package com.eventspace.eventaction.impl;

import com.eventspace.enumeration.EventsEnum;
import com.eventspace.enumeration.StatesEnum;
import com.eventspace.eventaction.EventActionServices;
import com.eventspace.eventaction.dto.BookingContextDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.statemachine.StateContext;

/**
 * Event space
 * Created by Aux-054 on 1/9/2017.
 */

public class ExpireEventActionServiceImpl extends EventActionServices {

    private static final Log logger = LogFactory.getLog(ExpireEventActionServiceImpl.class);

    public ExpireEventActionServiceImpl(BookingContextDto bookingContextDto) {
        super(bookingContextDto);
    }

    /**
     * @param stateContext
     */
    @Override
    public void execute(StateContext<StatesEnum, EventsEnum> stateContext) {
        logger.info(String.format("BOOKING action EXPIRE OrderID : %s",booking.getOrderId()));
        updateBookingDetails(bookingActionDto,StatesEnum.EXPIRED.value());
        emailService.sendBookingActionMails(booking.getId(),EventsEnum.EXPIRE.value());
        bookingService.removeBookingFromSpaceUnavailablity(booking.getId());
        bookingService. expirePayLater(booking.getId());
        logger.info("booking :"+bookingActionDto.getBooking_id() +"  has been EXPIRE");
    }
}
