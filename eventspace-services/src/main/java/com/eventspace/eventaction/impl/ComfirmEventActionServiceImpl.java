package com.eventspace.eventaction.impl;

import com.eventspace.enumeration.EventsEnum;
import com.eventspace.enumeration.StatesEnum;
import com.eventspace.eventaction.EventActionServices;
import com.eventspace.eventaction.dto.BookingContextDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.statemachine.StateContext;

/**
 * Event space
 * Created by Aux-054 on 1/9/2017.
 */

public class ComfirmEventActionServiceImpl extends EventActionServices {

    private static final Log logger = LogFactory.getLog(ComfirmEventActionServiceImpl.class);


    public ComfirmEventActionServiceImpl(BookingContextDto bookingContextDto) {
        super(bookingContextDto);
    }

    @Override
    public void execute(StateContext<StatesEnum, EventsEnum> stateContext) {
        logger.info(String.format("BOOKING action CONFIRM OrderID : %s",booking.getOrderId()));
        updateBookingDetails(bookingActionDto,StatesEnum.CONFIRMED.value());
        emailService.sendBookingActionMails(booking.getId(),EventsEnum.CONFIRM.value());

    }
}
