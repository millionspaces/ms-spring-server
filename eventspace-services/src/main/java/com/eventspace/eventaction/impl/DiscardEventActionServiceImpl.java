package com.eventspace.eventaction.impl;

import com.eventspace.enumeration.EventsEnum;
import com.eventspace.enumeration.StatesEnum;
import com.eventspace.eventaction.EventActionServices;
import com.eventspace.eventaction.dto.BookingContextDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.statemachine.StateContext;

public class DiscardEventActionServiceImpl extends EventActionServices {

    private static final Log logger = LogFactory.getLog(DiscardEventActionServiceImpl.class);


    /**
     * @param bookingContextDto
     */
    public DiscardEventActionServiceImpl(BookingContextDto bookingContextDto) {
        super(bookingContextDto);
    }

    @Override
    public void execute(StateContext<StatesEnum, EventsEnum> stateContext) {
        logger.info(String.format("BOOKING action DISCARD OrderID : %s",booking.getOrderId()));
        updateBookingDetails(bookingActionDto,StatesEnum.DISCARDED.value());
        emailService.sendBookingActionMails(booking.getId(),EventsEnum.DISCARD.value());
        bookingService.removeBookingFromSpaceUnavailablity(booking.getId());
    }
}
