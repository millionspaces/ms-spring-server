package com.eventspace.eventaction.impl;

import com.eventspace.enumeration.EventsEnum;
import com.eventspace.enumeration.StatesEnum;
import com.eventspace.eventaction.EventActionServices;
import com.eventspace.eventaction.dto.BookingContextDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.statemachine.StateContext;

public class PendingAdvanceActionServiceImpl extends EventActionServices {

    private static final Log logger = LogFactory.getLog(PendingAdvanceActionServiceImpl.class);
    /**
     * @param bookingContextDto
     */
    public PendingAdvanceActionServiceImpl(BookingContextDto bookingContextDto) {
        super(bookingContextDto);
    }

    /**
     * @param stateContext
     */
    @Override
    public void execute(StateContext<StatesEnum, EventsEnum> stateContext) {
        logger.info(String.format("BOOKING action ADVANCE PENDING OrderID : %s",booking.getOrderId()));
        updateBookingDetails(bookingActionDto,StatesEnum.ADVANCE_PAYMENT_PENDING.value());
        emailService.sendBookingActionMails(booking.getId(),EventsEnum.WAIT_FOR_PAY_ADVANCE.value());

    }
}
