/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.sms.dto;


import java.io.Serializable;

/**
 * The Class MessageMetaData.
 */
public class MessageMetaData implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5476366337258358234L;


	private String phoneNumber;

	/** The subject. */
	private String message;


	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
