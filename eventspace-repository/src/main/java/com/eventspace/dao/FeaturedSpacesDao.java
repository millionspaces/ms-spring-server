package com.eventspace.dao;

import com.eventspace.domain.FeaturedSpaces;

import java.util.List;

/**
 * Created by Auxenta on 8/24/2017.
 */
public interface FeaturedSpacesDao extends BaseDao<FeaturedSpaces> {

    List<FeaturedSpaces> getAll();
}
