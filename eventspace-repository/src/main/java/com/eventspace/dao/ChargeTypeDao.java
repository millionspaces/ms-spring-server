package com.eventspace.dao;

import com.eventspace.domain.ChargeType;

import java.util.List;

public interface ChargeTypeDao extends BaseDao<ChargeType> {

    List<ChargeType> getAllChargeTypes();
}
