package com.eventspace.dao;

import com.eventspace.domain.SpaceAmenity;

import java.util.List;

/**
 * Created by Aux-052 on 12/14/2016.
 */
public interface SpaceAmenityDao extends BaseDao<SpaceAmenity> {
    List<SpaceAmenity> getAllSpaceAmenities();
}
