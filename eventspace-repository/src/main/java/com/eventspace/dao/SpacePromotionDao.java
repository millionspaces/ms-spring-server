package com.eventspace.dao;

import com.eventspace.domain.SpacePromotion;

import java.util.Date;

public interface SpacePromotionDao extends BaseDao<SpacePromotion>{

    SpacePromotion getSpacePromotion(Integer spaceId,Date startDate, Date endDate);
}
