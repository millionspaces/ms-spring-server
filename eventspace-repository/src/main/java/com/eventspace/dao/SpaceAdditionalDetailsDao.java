package com.eventspace.dao;

import com.eventspace.domain.SpaceAdditionalDetails;

public interface SpaceAdditionalDetailsDao extends BaseDao<SpaceAdditionalDetails> {
}
