package com.eventspace.dao;

import com.eventspace.domain.DiscountDetails;

public interface DiscountDetailsDao extends BaseDao<DiscountDetails> {

    DiscountDetails getCurrentDiscounts();
}
