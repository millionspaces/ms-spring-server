package com.eventspace.dao;

import com.eventspace.domain.SearchAddress;

/**
 * Created by Auxenta on 6/12/2017.
 */
public interface SearchAddressDao extends BaseDao<SearchAddress> {
}
