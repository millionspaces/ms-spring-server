package com.eventspace.dao;

import com.eventspace.domain.PaymentHistory;

/**
 * Created by Auxenta on 6/2/2017.
 */
public interface PaymentHistoryDao extends BaseDao<PaymentHistory> {

    Integer getWrongCardUsesForABooking(Integer bookingId);

    PaymentHistory getPaymentHistory(Integer bookingId);
}
