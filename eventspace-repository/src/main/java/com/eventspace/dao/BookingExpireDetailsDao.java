package com.eventspace.dao;


import com.eventspace.domain.BookingExpireDetails;

import java.util.List;

/**
 * Created by Auxenta on 6/5/2017.
 */
public interface BookingExpireDetailsDao extends BaseDao<BookingExpireDetails> {

    BookingExpireDetails getByBookingId(int bookingId);

    List<BookingExpireDetails> getUpcomingExpireBookings();

}
