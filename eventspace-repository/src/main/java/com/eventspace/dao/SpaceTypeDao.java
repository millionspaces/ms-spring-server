package com.eventspace.dao;

import com.eventspace.domain.SpaceType;

import java.util.List;

public interface SpaceTypeDao  extends BaseDao<SpaceType>  {

    List<SpaceType> getAllSpaceTypes();
}
