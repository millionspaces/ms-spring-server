package com.eventspace.dao;

import com.eventspace.domain.Rules;

import java.util.Date;
import java.util.List;

/**
 * Created by Auxenta on 6/16/2017.
 */
public interface RulesDao extends BaseDao<Rules> {

    List<Rules> getAllRules();

    List<Rules> getLatestRules(Date lastUpdateDate);
}
