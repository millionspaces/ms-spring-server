package com.eventspace.dao;

import com.eventspace.domain.AutoPublish;

/**
 * Created by Auxenta on 6/16/2017.
 */
public interface AutoPublishDao extends BaseDao<AutoPublish> {

}
