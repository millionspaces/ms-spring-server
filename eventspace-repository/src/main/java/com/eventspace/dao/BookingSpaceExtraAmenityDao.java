package com.eventspace.dao;


import com.eventspace.domain.BookingSpaceExtraAmenity;

import java.util.List;

/**
 * Created by Aux-052 on 12/16/2016.
 */
public interface BookingSpaceExtraAmenityDao extends BaseDao<BookingSpaceExtraAmenity> {
    BookingSpaceExtraAmenity readByPrimaryKey(Integer spaceId, Integer amenityId, Integer bookingId);

    List<BookingSpaceExtraAmenity> getByBookingId(Integer boookingId);

}
