package com.eventspace.dao;

import com.eventspace.domain.ReservationStatus;

import java.util.List;

/**
 * Created by Auxenta on 2/14/2017.
 */
public interface ReservationStatusDao extends BaseDao<ReservationStatus> {

    List<ReservationStatus> getReservationSatues();
}
