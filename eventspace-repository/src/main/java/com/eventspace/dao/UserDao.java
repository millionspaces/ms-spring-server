/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.dao;

import com.eventspace.domain.User;
import com.eventspace.dto.UserStatsDto;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * The Interface UserDao.
 */
public interface UserDao extends BaseDao<User> {

    /**
     * Read user by name.
     *
     * @param userName the user name
     * @return the user
     */
    User readUserByName(String userName);

    Integer checkEmail(String email);

    List<User> getHosts(Integer page);

    List<User> getGuests(Integer page);

    List<Object[]> getUserSignUpStats(String start,String end);


}
