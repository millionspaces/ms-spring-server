package com.eventspace.dao;

import com.eventspace.domain.BookingHistory;

import java.util.List;

/**
 * Created by Auxenta on 3/15/2017.
 */
public interface BookingHistoryDao extends BaseDao<BookingHistory> {

    BookingHistory getBookingHistory(Integer bookingId, Integer reservationStatus);

    List<BookingHistory> getHisttoryByUserId(Integer userId);
}
