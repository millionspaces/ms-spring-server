/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.dao;

import com.eventspace.domain.EventType;

import java.util.Date;
import java.util.List;

/**
 * Created by Aux-052 on 12/14/2016.
 */
public interface EventTypeDao extends BaseDao<EventType> {
    List<EventType> getEventTypes();

    List<EventType> getLatestEventTypes(Date lastUpdateDate);
}
