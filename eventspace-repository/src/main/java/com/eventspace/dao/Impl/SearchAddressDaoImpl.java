package com.eventspace.dao.Impl;

import com.eventspace.dao.SearchAddressDao;
import com.eventspace.domain.SearchAddress;
import org.springframework.stereotype.Repository;

/**
 * Created by Auxenta on 6/12/2017.
 */
@Repository
public class SearchAddressDaoImpl extends BaseDaoImpl<SearchAddress> implements SearchAddressDao {


}
