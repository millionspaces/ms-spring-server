package com.eventspace.dao.Impl;

import com.eventspace.dao.PrioritySpacesDao;
import com.eventspace.domain.PrioritySpaces;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PrioritySpacesDaoImpl extends BaseDaoImpl<PrioritySpaces> implements PrioritySpacesDao {

    @Autowired
    private SessionFactory sessionFactory;

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RulesDaoImpl.class);

    @Override
    public List<PrioritySpaces> getAllSpaces(){
        LOGGER.info("getAllRules-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (List<PrioritySpaces>) currentSession.createCriteria(PrioritySpaces.class).list();
    }
}
