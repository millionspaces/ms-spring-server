package com.eventspace.dao.Impl;

import com.eventspace.dao.SeatingArrangementDao;
import com.eventspace.domain.SeatingArrangement;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by Auxenta on 6/16/2017.
 */
@Repository
public class SeatingArrangementDaoImpl extends BaseDaoImpl<SeatingArrangement> implements SeatingArrangementDao {


    @Autowired
    private SessionFactory sessionFactory;

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SeatingArrangementDaoImpl.class);

    @Override
    public List<SeatingArrangement> getAllSeatingArrangement() {
        LOGGER.info("getAllSeatingArrangement-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (List<SeatingArrangement>) currentSession.createCriteria(SeatingArrangement.class).list();
    }

    @Override
    public List<SeatingArrangement> getLatestSeatingArrangement(Date lastUpdateDate) {
        LOGGER.info("getLatestSeatingArrangement-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (List<SeatingArrangement>) currentSession.createCriteria(SeatingArrangement.class)
                .add( Restrictions.gt("updatedDate",lastUpdateDate)).list();
    }


}
