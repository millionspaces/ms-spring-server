package com.eventspace.dao.Impl;

import com.eventspace.dao.UserAdditionalDetailsDao;
import com.eventspace.domain.UserAdditionalDetails;
import org.springframework.stereotype.Repository;

/**
 * Created by Auxenta on 7/5/2017.
 */
@Repository
public class UserAdditionalDetailsDaoImpl extends BaseDaoImpl<UserAdditionalDetails> implements UserAdditionalDetailsDao {
}
