/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.dao.Impl;

import com.eventspace.dao.SpaceEventTypeDao;
import com.eventspace.domain.SpaceEventType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Aux-052 on 12/14/2016.
 */
@Repository
public class SpaceEventTypeDaoImpl extends BaseDaoImpl<SpaceEventType> implements SpaceEventTypeDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<SpaceEventType> getAllSpaceEventTypes() {
		Session currentSession = sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<SpaceEventType> spaceEventTypeList = currentSession.createCriteria(SpaceEventType.class).list();
		return spaceEventTypeList;
	}
}
