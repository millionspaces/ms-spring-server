package com.eventspace.dao.Impl;

import com.eventspace.dao.SpaceAdditionalDetailsDao;
import com.eventspace.domain.SpaceAdditionalDetails;
import org.springframework.stereotype.Repository;

@Repository
public class SpaceAdditionalDetailsDaoImpl extends BaseDaoImpl<SpaceAdditionalDetails> implements SpaceAdditionalDetailsDao {

}
