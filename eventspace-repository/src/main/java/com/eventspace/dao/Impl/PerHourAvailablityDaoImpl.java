package com.eventspace.dao.Impl;

import com.eventspace.dao.PerHourAvailablityDao;
import com.eventspace.domain.PerHourAvailablity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Time;

/**
 * Created by Auxenta on 7/12/2017.
 */
@Repository
public class PerHourAvailablityDaoImpl extends BaseDaoImpl<PerHourAvailablity> implements PerHourAvailablityDao {

    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger LOGGER = LoggerFactory.getLogger(BlockAvailablityDaoImpl.class);

    @Override
    public PerHourAvailablity findTheAvailablity(Integer spaceId, Integer day, Time fromTime, Time toTime) {
        LOGGER.info("findTheAvailablity ----->\tspace:"+spaceId+"\tday"+day+"\tfrom"+fromTime+"\tto"+toTime);
        Session currentSession = sessionFactory.getCurrentSession();

         return (PerHourAvailablity) currentSession.createCriteria(PerHourAvailablity.class)
                .add(Restrictions.eq("space.id", spaceId))
                .add(Restrictions.eq("day", day))
                .add(Restrictions.le("from", fromTime))
                .add(Restrictions.ge("to", toTime))
                .add(Restrictions.eq("active", 1))
                .uniqueResult();
    }
}
