/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.dao.Impl;

import com.eventspace.dao.EventTypeDao;
import com.eventspace.domain.EventType;
import com.eventspace.enumeration.BooleanEnum;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by Aux-052 on 12/14/2016.
 */
@Repository
public class EventTypeDaoImpl extends BaseDaoImpl<EventType> implements EventTypeDao {

    @Autowired
    private SessionFactory sessionFactory;

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(EventTypeDaoImpl.class);

    @Override
    public List<EventType> getEventTypes() {
        Session currentSession = sessionFactory.getCurrentSession();
        @SuppressWarnings("unchecked")
        List<EventType> eventTypeList = currentSession.createCriteria(EventType.class)
                .add(Restrictions.eq("active", BooleanEnum.TRUE.value()))
                .addOrder(Order.desc("priority"))
                .list();
        return eventTypeList;
    }

    @Override
    public List<EventType> getLatestEventTypes(Date lastUpdateDate) {
        LOGGER.info("getLatestEventTypes method in EventTypeDaoImpl-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        @SuppressWarnings("unchecked")
        List<EventType> eventTypeList =currentSession.createCriteria(EventType.class)
                .add( Restrictions.gt("updatedDate",lastUpdateDate))
                .list();

        return eventTypeList;
    }
}
