package com.eventspace.dao.Impl;

import com.eventspace.dao.PaymentHistoryDao;
import com.eventspace.domain.PaymentHistory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by Auxenta on 6/2/2017.
 */
@Repository
public class PaymentHistoryDaoImpl extends BaseDaoImpl<PaymentHistory> implements PaymentHistoryDao{

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentHistoryDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Integer getWrongCardUsesForABooking(Integer bookingId) {
        LOGGER.info("getWrongCardUsesForABooking-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        List<PaymentHistory> paymentHistories=currentSession.createCriteria(PaymentHistory.class)
                .add(Restrictions.eq("bookingId",bookingId))
                .add(Restrictions.ne("response",1))
                .list();
        return paymentHistories.size();
    }

    @Override
    public PaymentHistory getPaymentHistory(Integer bookingId) {
        LOGGER.info("getPaymentHistory method -----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        Object result= currentSession.createCriteria(PaymentHistory.class)
                .add(Restrictions.eq("bookingId", bookingId))
                .uniqueResult();
        return Optional.ofNullable(result).isPresent()?(PaymentHistory)result:null;
    }
}
