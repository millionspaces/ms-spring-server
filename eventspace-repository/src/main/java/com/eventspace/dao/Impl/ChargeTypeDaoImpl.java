package com.eventspace.dao.Impl;

import com.eventspace.dao.ChargeTypeDao;
import com.eventspace.domain.ChargeType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ChargeTypeDaoImpl extends BaseDaoImpl<ChargeType> implements ChargeTypeDao{
    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentHistoryDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<ChargeType> getAllChargeTypes() {
        LOGGER.info("getAllBlockChargeTypes-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (List<ChargeType>) currentSession.createCriteria(ChargeType.class)
                .list();
    }
}
