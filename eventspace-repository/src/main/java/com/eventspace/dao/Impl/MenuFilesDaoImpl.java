package com.eventspace.dao.Impl;

import com.eventspace.dao.MenuFilesDao;
import com.eventspace.domain.MenuFiles;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class MenuFilesDaoImpl extends BaseDaoImpl<MenuFiles> implements MenuFilesDao {

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public MenuFiles readByUrl(String url) {
        Session currentSession = sessionFactory.getCurrentSession();
        return (MenuFiles)currentSession.createCriteria(MenuFiles.class)
                .add(Restrictions.eq("url", url))
                .uniqueResult();
    }
}
