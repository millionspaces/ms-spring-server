package com.eventspace.dao.Impl;

import com.eventspace.dao.CorporatePromoDao;
import com.eventspace.domain.CorporatePromo;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

@Repository
public class CorporatePromoDaoImpl extends BaseDaoImpl<CorporatePromo> implements CorporatePromoDao {
}
