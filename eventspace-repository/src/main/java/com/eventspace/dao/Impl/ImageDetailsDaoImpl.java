package com.eventspace.dao.Impl;

import com.eventspace.dao.ImageDetailsDao;
import com.eventspace.domain.ImageDetails;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ImageDetailsDaoImpl extends BaseDaoImpl<ImageDetails> implements ImageDetailsDao {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ImageDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public ImageDetails getByUrl(String url) {
        Session currentSession = sessionFactory.getCurrentSession();
        return (ImageDetails) currentSession.createCriteria(ImageDetails.class).add(Restrictions.eq("url",url)).uniqueResult();

    }
}
