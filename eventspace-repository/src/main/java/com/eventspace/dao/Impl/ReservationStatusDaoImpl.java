package com.eventspace.dao.Impl;

import com.eventspace.dao.ReservationStatusDao;
import com.eventspace.domain.ReservationStatus;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Auxenta on 2/14/2017.
 */
@Repository
public class ReservationStatusDaoImpl extends BaseDaoImpl<ReservationStatus> implements ReservationStatusDao{

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReservationStatusDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<ReservationStatus> getReservationSatues() {
        LOGGER.info("getReservationSatues method in ReservationStatusDaoImpl-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (List<ReservationStatus>) currentSession.createCriteria(ReservationStatus.class).list();
    }
}
