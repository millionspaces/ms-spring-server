package com.eventspace.dao.Impl;

import com.eventspace.dao.SpaceDao;
import com.eventspace.domain.BookingSlots;
import com.eventspace.domain.Space;
import com.eventspace.domain.SpaceUnavailability;
import com.eventspace.dto.AdvanceSearchDto;
import com.eventspace.enumeration.BooleanEnum;
import com.eventspace.util.Constants;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.eventspace.domain.Space.*;

/**
 * The Class SpaceDaoImpl.
 */
@Repository
public class SpaceDaoImpl extends BaseDaoImpl<Space> implements SpaceDao {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(SpaceDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    private Integer PAGINATION_MAX_COUNT = 18;

    private Integer ADMIN_PAGINATION_COUNT=10;

    @Override
    public List<Space> getAllSpaces(BooleanEnum isActive) {
        LOGGER.info("getAllSpaces---->\t");
        Session currentSession = sessionFactory.getCurrentSession();

        return (List<Space>) currentSession.createCriteria(Space.class)
                .add(Restrictions.eq("approved", isActive.value()))
                .list();
    }

    @Override
    public List<Space> filteredSpaces(String[] participation, String[] ratePerHour, Integer[] amenities,
                                      Integer[] event_types, Integer limit) {
        LOGGER.info("filteredSpaces----->\t");
        Session currentSession = sessionFactory.getCurrentSession();

        if (amenities.length == 0) {
            amenities = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        }
        if (event_types.length == 0) {
            event_types = new Integer[]{1, 2, 3, 4, 5, 6, 7};
        }

        String amenitiesList = Arrays.toString(amenities).replace("[", "").replace("]", "").replace(" ", "").trim();
        String eventList = Arrays.toString(event_types).replace("[", "").replace("]", "").replace(" ", "").trim();
        Query query = currentSession.getNamedQuery(FILTER_SPACE_SP)
                .setParameter("param1", participation[0])
                .setParameter("param2", participation[1])
                .setParameter("rate1", ratePerHour[0])
                .setParameter("rate2", ratePerHour[1])
                .setParameter("eventList", eventList)
                .setParameter("amenityList", amenitiesList)
                .setParameter("limitFor", limit);

        return (List<Space>) query.list();
    }

    @Override
    public List<Space> getCoordinateSpaces(String latitude, String longitude, Integer radius) {
        LOGGER.info("getCoordinateSpaces---->\t");

        Session currentSession = sessionFactory.getCurrentSession();
        Query query = currentSession.getNamedQuery(SPACE_BY_COORDINATES)
                .setParameter("latitudeVal", latitude)
                .setParameter("longitudeVal", longitude)
                .setParameter("radius", radius);

        return (List<Space>) query.list();
    }

    @Override
    public Boolean deleteSpace(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        Boolean returnVal = false;
        Query query = session.getNamedQuery(DELETE_SPACE_SP)
                .setParameter("p_spaceId", id);
        query.executeUpdate();
        returnVal = (Boolean) query.uniqueResult();
        return returnVal;
    }

    @Override
    public boolean isAvalableForBook(Integer spaceId, BookingSlots solt, Integer bufferTime, Integer noticePeriod,Long currentTime) {
        LOGGER.info("isAvalableForBook----->\t{}",spaceId);
        boolean isAvilable = true;
        Session session = sessionFactory.getCurrentSession();
        if (bufferTime==null)
            bufferTime=0;
        if (noticePeriod==null)
            noticePeriod=0;
        Date startDate=solt.getFromDate();
        Date endDate=solt.getToDate();

        if (startDate.getTime()-currentTime >=noticePeriod*60*60*1000){
            startDate.setTime(startDate.getTime()-((bufferTime*60)-1)*(60*1000));
            endDate.setTime(endDate.getTime()+((bufferTime*60)-1)*(60*1000));
            List<SpaceUnavailability> bookings = session.createCriteria(SpaceUnavailability.class)
                    .add(Restrictions.eq("space", spaceId))
                    .add(Restrictions.eq("isBlocked", BooleanEnum.TRUE.value()))
                    .add(Restrictions.or(
                            Restrictions.and(Restrictions.le(Constants.FROM_DATE, startDate), Restrictions.ge(Constants.TO_DATE, endDate)),
                            Restrictions.and(Restrictions.ge(Constants.FROM_DATE, startDate), Restrictions.le(Constants.TO_DATE, endDate)),
                            Restrictions.between(Constants.FROM_DATE, startDate, endDate),
                            Restrictions.between(Constants.TO_DATE, startDate, endDate)
                    ))
                    .list();


            if (bookings.size() != 0)
                isAvilable = false;
        }else
            isAvilable=false;
        return isAvilable;
    }

    @Override
    public boolean isBlockTime(Integer spaceId,BookingSlots solt) {
        LOGGER.info("isBlockTime----->\t{}",spaceId);
        boolean isAvilable = true;
        Date startDate=solt.getFromDate();
        Date endDate=solt.getToDate();
        startDate.setTime(startDate.getTime()+(1*60*1000));
        endDate.setTime(endDate.getTime()-(1*60*1000));
        Session session = sessionFactory.getCurrentSession();
        List<SpaceUnavailability> bookings = session.createCriteria(SpaceUnavailability.class)
                .add(Restrictions.eq("space", spaceId))
                .add(Restrictions.eq("isBlocked", BooleanEnum.TRUE.value()))
                .add(Restrictions.or(
                        Restrictions.and(Restrictions.le(Constants.FROM_DATE, startDate), Restrictions.ge(Constants.TO_DATE, endDate)),
                        Restrictions.and(Restrictions.ge(Constants.FROM_DATE, startDate), Restrictions.le(Constants.TO_DATE, endDate)),
                        Restrictions.between(Constants.FROM_DATE, startDate, endDate),
                        Restrictions.between(Constants.TO_DATE, startDate, endDate)
                ))
                .list();

        if (bookings.size() != 0)
            isAvilable = false;
        return isAvilable;
    }

    @Override
    public List<Space> getPaginatedSpaces(int page) {
        LOGGER.info("getPaginatedSpaces----->\t");
        Session currentSession = sessionFactory.getCurrentSession();
        return (List<Space>) currentSession.createCriteria(Space.class)
                .add(Restrictions.eq("approved", BooleanEnum.TRUE.value()))
                .setFirstResult(page * PAGINATION_MAX_COUNT)
                .setMaxResults(PAGINATION_MAX_COUNT)
                .list();

    }


    @Override
    public List<Space> getAdminSpaces(int page) {
        LOGGER.info("getAdminSpaces ----->\t");
        Session currentSession = sessionFactory.getCurrentSession();
        return (List<Space>) currentSession.createCriteria(Space.class)
                .setFirstResult(page * ADMIN_PAGINATION_COUNT)
                .setMaxResults(ADMIN_PAGINATION_COUNT)
                .addOrder(Order.desc("id"))
                .list();

    }

    @Override
    public List<Space> getAdminFilterSpaces(int page,Integer isActive) {
        LOGGER.info("getAdminSpaces ----->\t");
        Session currentSession = sessionFactory.getCurrentSession();
        return (List<Space>) currentSession.createCriteria(Space.class)
                .add(Restrictions.eq("approved", isActive))
                .setFirstResult(page * ADMIN_PAGINATION_COUNT)
                .setMaxResults(ADMIN_PAGINATION_COUNT)
                .addOrder(Order.desc("id"))
                .list();

    }

    @Override
    public List<Space> getSearchSpaces(Integer[] events, String location) {
        LOGGER.info("getSearchSpaces----->\t");

        String eventList = Arrays.toString(events).replace("[", "").replace("]", "").replace(" ", "").trim();

        Session currentSession = sessionFactory.getCurrentSession();
        String query = "select s.* from space s ,space_has_event_type e where s.id=e.space and  FIND_IN_SET (e.event_type,:events)";

        if (location != null) {
            String locations = location;
            List<String> locationLists = Arrays.asList(location.split(","));
            for (String x : locationLists) {
                String y = "|" + x;
                locations += y;
            }
            query = query + " and s.address_line_1  rlike '" + location + "'";
        }
        query = query + " group by s.id";

        return (List<Space>) currentSession
                .createSQLQuery(query)
                .addEntity(Space.class)
                .setParameter("events", eventList)
                .list();
    }

    @Override
    public List<Space> getUserSpaces(Integer userId) {
        LOGGER.info("getUserSpaces----->\t");
        Session currentSession = sessionFactory.getCurrentSession();

        return (List<Space>) currentSession.createCriteria(Space.class)
                .add(Restrictions.eq("user", userId))
                .list();
    }

    @Override
    public Map<String, Object> advancedSearch(AdvanceSearchDto advanceSearchDto, boolean isCount, Integer page) {
        LOGGER.info("advancedSearch ----->\t{}",page);
        Session currentSession = sessionFactory.getCurrentSession();
        Map<String, Object> result = new HashMap<>();

        String selectQuery = "";
        if (isCount)
            selectQuery = "select count(distinct s.id) from  space s";
        else
            selectQuery = " select s.* from  space s";
        String whereQuery = " where s.approved=1";

        String query;
        if (advanceSearchDto.getAvailable() != null) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(Constants.PATTERN_5);
                String date = advanceSearchDto.getAvailable().replace("T", " ").replace("Z", "");
                String fromTime="'"+date.substring(date.indexOf(' ')+1)+"'";
                Date dateObj=sdf.parse(date);
                String toTime=String.format("'%s:00:00'",dateObj.getHours()+2);
                Integer day=dateObj.getDay();
                Integer weekday=8;
                if (day==0)
                    day=7;
                if (day>5)
                    weekday=day;


                selectQuery = selectQuery +
                        " left join per_hour_availablity ha on s.id=ha.space and s.charge_type=1 and ha.day=1 and TIME(ha.from_time)<=TIME("+fromTime+") and TIME(ha.to_time)>=TIME("+toTime+")  and ha.active=1 " +
                        " left join block_availability ba on s.id=ba.space and s.charge_type=2 and ba.day=8 and TIME(ba.from_time)>=TIME("+fromTime+") and TIME(ba.to_time)<=TIME("+toTime+")  and ba.active=1 "
                        +"left outer join space_unavailablity su on s.id=su.space "
                        +"and DATE(su.from_date)=DATE('"+ date+"') and "
                        +"( "
                        +" (s.charge_type=2 and "
                        +"    ( (TIME(su.from_date)<=TIME(ba.from_time) and TIME(su.to_date)>TIME(ba.from_time)) or "
                        +"      (TIME(su.from_date)<TIME(ba.to_time) and TIME(su.to_date)>=TIME(ba.to_time))"
                        +"     ) and su.is_blocked=1"
                        +"  ) "
                        + " or "
                        + " (s.charge_type=1 and "
                        +"      ((TIME(su.from_date)<=TIME("+fromTime+") and TIME(su.to_date)>TIME("+fromTime+") ) or "
                        + "       (TIME(su.from_date)<TIME("+toTime+") and TIME(su.to_date)>=TIME("+toTime+") ) "
                        + "      ) and su.is_blocked=1"
                        +"   ) "
                        +")";



                        whereQuery = whereQuery +" and DATE(s.calendar_end) > DATE('"+date+"') and "+
                             " (ba.id is not null or ha.id is not null) and su.id is null ";

            } catch (ParseException e1) {
                LOGGER.error("advancedSearch exception----->{}",e1);
            }

        }
        if (advanceSearchDto.getParticipation() != null) {
            String[] participations = advanceSearchDto.getParticipation().split("-");
            whereQuery = whereQuery + " and s.participant_count >= " + participations[0] + " and  s.participant_count <= " + participations[1];
        }
        if (advanceSearchDto.getBudget() != null) {

            String[] ratePerHours = advanceSearchDto.getBudget().split("-");
            whereQuery = whereQuery + " and rate_per_hour >= " + ratePerHours[0] + " and  rate_per_hour <= " + ratePerHours[1];
        }
        if (advanceSearchDto.getOrganizationName() != null && !advanceSearchDto.getOrganizationName().equals("")) {
            String organizationName = advanceSearchDto.getOrganizationName().replace("'","");
            whereQuery = whereQuery + " and  (address_line_2 like (concat('%','"+organizationName+"','%')) or s.name like (concat('%','"+organizationName+"','%')) ) ";
        }
        if (advanceSearchDto.getAmenities() != null && advanceSearchDto.getAmenities().length != 0) {
            String amenityList = Arrays.toString(advanceSearchDto.getAmenities()).replace("[", "(").replace("]", ")").replace(" ", "").trim();
            selectQuery = selectQuery + ",(SELECT * FROM space_has_amenity where amenity in "+amenityList+" group by space having count(distinct amenity)>="+advanceSearchDto.getAmenities().length+" ) a";
            selectQuery = selectQuery + ",(SELECT * FROM space_has_extra_amenity where amenity in "+amenityList+" group by space having count(distinct amenity)>="+advanceSearchDto.getAmenities().length+" ) ea";
            whereQuery = whereQuery + " and (s.id=ea.space or s.id=a.space ) ";
        }
        if (advanceSearchDto.getSpaceType() != null && advanceSearchDto.getSpaceType().length != 0) {
            String spaceTypeList = Arrays.toString(advanceSearchDto.getSpaceType()).replace("[", "(").replace("]", ")").replace(" ", "").trim();
            selectQuery = selectQuery + ",space_has_space_type st";
            whereQuery = whereQuery + " and s.id=st.space and  st.space_type in " + spaceTypeList;

        }
        if (advanceSearchDto.getEvents() != null && advanceSearchDto.getEvents().length != 0) {
            String eventsList = Arrays.toString(advanceSearchDto.getEvents()).replace("[", "(").replace("]", ")").replace(" ", "").trim();
            selectQuery = selectQuery + ",space_has_event_type e";
            whereQuery = whereQuery + " and s.id=e.space and  e.event_type in" + eventsList;
        }
        if (advanceSearchDto.getRules() != null && advanceSearchDto.getRules().length != 0) {
            String rulesList = Arrays.toString(advanceSearchDto.getRules()).replace("[", "(").replace("]", ")").replace(" ", "").trim();
            whereQuery = whereQuery + "  and  s.id not in(select r.space from space_has_rule r where r.rule in" + rulesList+"   group by space having count(distinct r.rule)>="+advanceSearchDto.getRules().length+" )";
        }
        if (advanceSearchDto.getSeatingArrangements() != null && advanceSearchDto.getSeatingArrangements().length != 0) {
            String seatingArrangementsList = Arrays.toString(advanceSearchDto.getSeatingArrangements()).replace("[", "(").replace("]", ")").replace(" ", "").trim();
            selectQuery = selectQuery + ",space_has_seating_arrangement sam";
            whereQuery = whereQuery + " and s.id=sam.space and  sam.seating_arrangement in" + seatingArrangementsList;
        }
        if (advanceSearchDto.getLocation() != null && advanceSearchDto.getLocation().getAddress()!=null &&
                advanceSearchDto.getLocation().getLatitude()!=null && advanceSearchDto.getLocation().getLongitude()!=null) {
            String latitudeVal = advanceSearchDto.getLocation().getLatitude();
            String longitudeVal = advanceSearchDto.getLocation().getLongitude();
            String address = "address";
            if (advanceSearchDto.getLocation().getAddress() != null)
                address = advanceSearchDto.getLocation().getAddress();
            String locations = address;
            String dis = " ( 6371 * acos( cos( radians(" + latitudeVal + ") ) * cos( radians( latitude ) ) * \n" +
                    "    cos( radians( longitude ) - radians(" + longitudeVal + ") ) + sin( radians(" + latitudeVal + ") ) * \n" +
                    "    sin( radians( latitude ) ) ) )";


            List<String> locationLists = Arrays.asList(address.split(","));
            for (String x : locationLists) {
                String y = "|" + x;
                locations += y;
            }
            selectQuery = selectQuery + ",search_address sa";
            whereQuery = whereQuery + " and  s.id=sa.space and (" + dis + " < 8 or sa.address  rlike '" + locations + "') ";

            if(!isCount && (advanceSearchDto.getSortBy()==null || advanceSearchDto.getSortBy().equals("distance"))){
                    whereQuery=whereQuery+"group by s.id order by "+dis;
            }
        }
        else if(advanceSearchDto.getSortBy() ==null && !isCount && advanceSearchDto.getOrderBy()!=null){
            StringBuilder orderQuery=new StringBuilder("order by ");

            for(int i=0;i<advanceSearchDto.getOrderBy().size();i++) {
                orderQuery.append("case when s.id in ( ").append(advanceSearchDto.getOrderBy().get(i)).append(" ) then ").append(i).append(" end desc ");
                if (i!=(advanceSearchDto.getOrderBy().size()-1))
                    orderQuery.append(" , ");
            }
            whereQuery=whereQuery+" group by s.id "+orderQuery;

        }
        else if (advanceSearchDto.getSortBy() ==null && !isCount) {
            whereQuery = whereQuery + " group by s.id order by";
            if (advanceSearchDto.getEvents() != null && advanceSearchDto.getEvents().length != 0) {
                String eventsList = Arrays.toString(advanceSearchDto.getEvents()).replace("[", "(").replace("]", ")").replace(" ", "").trim();
                whereQuery=whereQuery+" case when s.primary_event_type in "+eventsList+" then 1 else 0 end  desc,";
            }
            whereQuery = whereQuery + " case when s.id in(select space from priority_spaces order by id) then  " +
                    "(select ps.id from priority_spaces ps where ps.space=s.id) else (s.id+(select count(* ) from priority_spaces)) end ";

        }
            if (!isCount) {
                if (advanceSearchDto.getSortBy()!=null && advanceSearchDto.getSortBy().equals("priceLowestFirst"))
                    whereQuery=whereQuery+" group by s.id order by s.rate_per_hour";
                if (advanceSearchDto.getSortBy()!=null && advanceSearchDto.getSortBy().equals("priceHighestFirst"))
                    whereQuery=whereQuery+" group by s.id order by s.rate_per_hour desc";
               /* if (advanceSearchDto.getSortBy()!=null && advanceSearchDto.getSortBy().equals("feedback1")){
                    selectQuery = selectQuery + " left outer join booking b on s.id=b.space left outer join reviews r on b.id=r.booking_id ";
                    whereQuery=whereQuery+" order by round(avg(SUBSTRING_INDEX(r.rate,'|',1)),0)";
                }
                if (advanceSearchDto.getSortBy()!=null && advanceSearchDto.getSortBy().equals("feedback2")){
                    selectQuery = selectQuery + " left outer join booking b on s.id=b.space left outer join reviews r on b.id=r.booking_id ";
                    whereQuery=whereQuery+" order by round(avg(SUBSTRING_INDEX(r.rate,'|',1)),0) desc";
                }*/
            }
        query = selectQuery + whereQuery;

        if (isCount) {
            int count = ((BigInteger)currentSession.createSQLQuery(query).uniqueResult()).intValue();
            result.put("count",count);
        } else {
            List<Space> spaceList;
            if (page == null) {
                spaceList = currentSession
                        .createSQLQuery(query)
                        .addEntity(Space.class)
                        .list();
            } else {
                spaceList = currentSession
                        .createSQLQuery(query)
                        .addEntity(Space.class)
                        .setFirstResult(page * Optional.ofNullable(advanceSearchDto.getLimit()).orElse(PAGINATION_MAX_COUNT))
                        .setMaxResults(Optional.ofNullable(advanceSearchDto.getLimit()).orElse(PAGINATION_MAX_COUNT))
                        .list();
            }
            result.put("spaces", spaceList);
        }
        return result;
    }

    @Override
    public List<Space> getFeaturedSpaces() {
        LOGGER.info("getFeaturedSpaces----->\t");
        Session currentSession = sessionFactory.getCurrentSession();
        String query="select * from space s,featured_spaces fs where s.id=fs.space and fs.end_date>now() and fs.featured=1";
        return currentSession.createSQLQuery(query).addEntity(Space.class).list();
    }

    @Override
    public List<Space> getChilds(Integer spaceId) {
        Session currentSession = sessionFactory.getCurrentSession();

        return (List<Space>) currentSession.createCriteria(Space.class)
                .add(Restrictions.eq("parent", spaceId))
                .list();
    }

    @Override
    public List<Space> getLinkedSpaces(Integer spaceId) {
        Session currentSession = sessionFactory.getCurrentSession();

        return (List<Space>) currentSession.createCriteria(Space.class)
                .add(Restrictions.or(
                        Restrictions.eq("linkSpace", spaceId),Restrictions.eq("id", spaceId)
                ))
                .list();
    }

    @Override
    public List<Space> getCalenderEndingSpaces(Date date) {

        Session session = sessionFactory.getCurrentSession();
        DateFormat df = new SimpleDateFormat(Constants.PATTERN_2);
        LOGGER.info("getCalenderEndingSpaces----->\tfor:{}\t",df.format(date));
        String sql = "Select s FROM Space s WHERE s.approved=1 and s.calendarEnd ='"+ df.format(date) +"'";
        return (List<Space>) session.createQuery(String.format(sql)).list();
    }

    @Override
    public List<Space> getCalenderEndSpaces(Date date) {
        Session session = sessionFactory.getCurrentSession();

        DateFormat df = new SimpleDateFormat(Constants.PATTERN_2);
        LOGGER.info("getCalenderEndSpaces----->\tfor:{}\t",df.format(date));

        String sql = "Select s FROM Space s WHERE s.approved=1 and s.calendarEnd <='"+ df.format(date) +"'";
        return (List<Space>) session.createQuery(String.format(sql)).list();
    }

    @Override
    public Long getAllSpacesCount() {
            LOGGER.info("getAllSpacesCount----->\t");
            Session currentSession = sessionFactory.getCurrentSession();
            return (Long)currentSession.createQuery("select count(*) from Space").uniqueResult();
    }

    @Override
    public Long getAdminFilterSpacesCount(Integer isActive) {
        Session currentSession = sessionFactory.getCurrentSession();
          return (Long)currentSession.createQuery("select count(*) from Space where approved="+isActive).uniqueResult();
    }
}
