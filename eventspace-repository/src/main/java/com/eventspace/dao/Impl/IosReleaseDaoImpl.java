package com.eventspace.dao.Impl;

import com.eventspace.dao.IosReleaseDao;
import com.eventspace.domain.IosRelease;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class IosReleaseDaoImpl extends BaseDaoImpl<IosRelease> implements IosReleaseDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public IosRelease getLatestRelease() {
        Session session = sessionFactory.getCurrentSession();
        return (IosRelease)session.createCriteria(IosRelease.class).addOrder(Order.desc("id")).setMaxResults(1).uniqueResult();
    }
}
