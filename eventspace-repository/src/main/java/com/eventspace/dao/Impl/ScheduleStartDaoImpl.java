package com.eventspace.dao.Impl;

import com.eventspace.dao.ScheduleStartDao;
import com.eventspace.domain.ScheduleStart;
import org.springframework.stereotype.Repository;

@Repository
public class ScheduleStartDaoImpl extends BaseDaoImpl<ScheduleStart> implements ScheduleStartDao {
}
