package com.eventspace.dao.Impl;

import com.eventspace.dao.BookingSpaceExtraAmenityDao;
import com.eventspace.domain.BookingSpaceExtraAmenity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Aux-052 on 12/16/2016.
 */
@Repository
public class BookingSpaceExtraAmenityDaoImpl extends BaseDaoImpl<BookingSpaceExtraAmenity> implements BookingSpaceExtraAmenityDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingSpaceExtraAmenityDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public BookingSpaceExtraAmenity readByPrimaryKey(Integer spaceId, Integer amenityId,Integer bookingId) {
        LOGGER.info("readByPrimaryKey for spaceId:{}  amenityId:{}",spaceId,amenityId);
        Session currentSession = sessionFactory.getCurrentSession();
        String hql ="select b from booking_has_extra_amenity b where b.space="+ spaceId +"and b.amenity="+ amenityId+" and b.booking="+bookingId;
        return (BookingSpaceExtraAmenity) currentSession.createQuery(hql).list();
    }

    @Override
    public List<BookingSpaceExtraAmenity> getByBookingId(Integer bookingId) {
        LOGGER.info("getByBookingId for bookingId:{}  ",bookingId);
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(BookingSpaceExtraAmenity.class);
        criteria.add(Restrictions.eq("bookingId", bookingId));
        return (List<BookingSpaceExtraAmenity>) criteria.list();

    }
}
