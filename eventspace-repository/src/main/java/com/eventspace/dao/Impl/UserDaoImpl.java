/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.dao.Impl;

import com.eventspace.dao.UserDao;
import com.eventspace.domain.User;
import com.eventspace.dto.UserStatsDto;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The Class UserDaoImpl.
 */
@Repository
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

	/** The session factory. */
	@Autowired
	private SessionFactory sessionFactory;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.eventspace.dao.UserDao#readUserByName(java.lang.String)
	 */
	@Override
	public User readUserByName(String userName) {
		LOGGER.info("readUserByName in  UserDaoImpl.userName is {}" , userName);
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("email", userName));

		return (User) criteria.uniqueResult();

	}

	@Override
	public Integer checkEmail(String email) {
		LOGGER.info("checkEmail in  method called in UserDaoImpl");
		Session session = sessionFactory.getCurrentSession();
		String query = "select count(*) as num FROM user where email='%s'";
		BigInteger obj =  (BigInteger) session.createSQLQuery(String.format(query, email)).uniqueResult(); 
		return obj.intValue();
	}

	@Override
	public List<User> getHosts(Integer page){
		LOGGER.info("getHosts in  method called in UserDaoImpl");
		Session currentSession = sessionFactory.getCurrentSession();
		int maxResult=10;

        return (List<User>) currentSession.createSQLQuery("select u.* from user as u  join space as s on u.id=s.user group by u.id")
                .addEntity(User.class)
                .setFirstResult(page*maxResult)
                .setMaxResults(maxResult)
                .list();
	}

	@Override
	public List<User> getGuests(Integer page){
		LOGGER.info("getGuests in  method called in UserDaoImpl");
		Session currentSession = sessionFactory.getCurrentSession();
		int maxResult=10;

        return (List<User>) currentSession.createCriteria(User.class)
                .setFirstResult(page*maxResult)
                .setMaxResults(maxResult)
                .list();
	}

	@Override
	public List<Object[]> getUserSignUpStats(String  start,String end) {
		Session currentSession = sessionFactory.getCurrentSession();
		String query = "select DATE(created_at), COUNT(*) AS TOTAL, COUNT(IF(user_role='GUEST',1,null)) AS GUEST, COUNT(IF(user_role!='GUEST',1,null)) AS USER " +
				"FROM user where DATE(created_at)>='%s' and DATE(created_at)<='%s' group by DATE(created_at)";
		List<Object[]> result  =currentSession.createSQLQuery(String.format(query,start,end)).list();
		return result;
	}

}
