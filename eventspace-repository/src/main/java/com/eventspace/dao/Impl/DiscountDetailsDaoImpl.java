package com.eventspace.dao.Impl;

import com.eventspace.dao.DiscountDetailsDao;
import com.eventspace.domain.DiscountDetails;
import com.eventspace.enumeration.BooleanEnum;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class DiscountDetailsDaoImpl extends BaseDaoImpl<DiscountDetails> implements DiscountDetailsDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(DiscountDetailsDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public DiscountDetails getCurrentDiscounts() {
        Session currentSession = sessionFactory.getCurrentSession();
        return (DiscountDetails) currentSession.createCriteria(DiscountDetails.class)
                .add(Restrictions.eq("active",BooleanEnum.TRUE.value()))
                .add(Restrictions.lt("startDate",new Date()))
                .add(Restrictions.gt("endDate",new Date()))
                .uniqueResult();

    }
}
