package com.eventspace.dao.Impl;

import com.eventspace.dao.SpaceExtraAmenityDao;
import com.eventspace.domain.SpaceExtraAmenity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Aux-052 on 12/16/2016.
 */
@Repository
public class SpaceExtraAmenityDaoImpl extends BaseDaoImpl<SpaceExtraAmenity> implements SpaceExtraAmenityDao {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SpaceExtraAmenityDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<SpaceExtraAmenity> getAllSpaceExtraAmenities() {
        LOGGER.info("getAllSpaceExtraAmenities method in SpaceExtraAmenityDaoImpl-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        @SuppressWarnings("unchecked")
        List<SpaceExtraAmenity> spaceExtraAmenityList =currentSession.createCriteria(SpaceExtraAmenity.class).list();
        return spaceExtraAmenityList;
    }

    public  void removeExtraAmenity(Integer spaceId, Integer amenityId){
        LOGGER.info("removeExtraAmenity in SpaceExtraAmenityDaoImpl spaceId:{}  amenityId:{}",spaceId,amenityId);
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.createSQLQuery(String.format("delete  from space_has_extra_amenity where space=%s and amenity=%s",spaceId,amenityId)).executeUpdate();
    }

    @Override
    public SpaceExtraAmenity findSpaceExtraAmenity(Integer spaceId, Integer amenityId) {
        LOGGER.info("findSpaceExtraAmenity spaceId:{}  amenityId:{}",spaceId,amenityId);
        Session currentSession = sessionFactory.getCurrentSession();
        return (SpaceExtraAmenity) currentSession.createCriteria(SpaceExtraAmenity.class)
                .add(Restrictions.eq("space.id", spaceId))
                .add(Restrictions.eq("extraAmenity.id", amenityId)).uniqueResult();
    }
}
