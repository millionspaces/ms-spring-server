package com.eventspace.dao.Impl;

import com.eventspace.dao.BookingHistoryDao;
import com.eventspace.domain.BookingHistory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Auxenta on 3/15/2017.
 */
@Repository
public class BookingHistoryDaoImpl extends BaseDaoImpl<BookingHistory> implements BookingHistoryDao {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(BookingHistoryDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public BookingHistory getBookingHistory(Integer bookingId, Integer reservationStatus) {
        LOGGER.info("getBookingHistory method -----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (BookingHistory) currentSession.createCriteria(BookingHistory.class)
                .add(Restrictions.eq("bookingId", bookingId))
                .add(Restrictions.eq("reservationStatus", reservationStatus))
                .uniqueResult();

    }

    @Override
    public List<BookingHistory> getHisttoryByUserId(Integer userId) {
        Session currentSession = sessionFactory.getCurrentSession();
        String query = "Select bh.* FROM Booking b , Space s,booking_history bh WHERE s.id=b.space and b.id=bh.booking and (b.user=:id or s.user=:id )  order by bh.created_at";
        return (List<BookingHistory>) currentSession
                .createSQLQuery(query)
                .addEntity(BookingHistory.class)
                .setParameter("id", userId)
                .list();
    }

}
