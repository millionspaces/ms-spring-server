package com.eventspace.dao.Impl;

import com.eventspace.dao.AmenityUnitsDao;
import com.eventspace.domain.AmenityUnit;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Aux-052 on 12/14/2016.
 */
@Repository
public class AmenityUnitsDaoImpl extends BaseDaoImpl<AmenityUnit> implements AmenityUnitsDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<AmenityUnit> getAllAmenityUnits() {
        Session currentSession = sessionFactory.getCurrentSession();
        @SuppressWarnings("unchecked")
        List<AmenityUnit> amenityUnitsList =currentSession.createCriteria(AmenityUnit.class).list();
        return amenityUnitsList;
    }
}
