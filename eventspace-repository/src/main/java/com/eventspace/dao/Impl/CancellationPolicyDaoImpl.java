package com.eventspace.dao.Impl;

import com.eventspace.dao.CancellationPolicyDao;
import com.eventspace.domain.CancellationPolicy;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by Aux-052 on 12/14/2016.
 */
@Repository
public class CancellationPolicyDaoImpl extends BaseDaoImpl<CancellationPolicy> implements CancellationPolicyDao {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CancellationPolicyDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<CancellationPolicy> getCancellationPolicies() {
        LOGGER.info("getCancellationPolicies method in CancellationPolicyDaoImpl-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        @SuppressWarnings("unchecked")
        List<CancellationPolicy> cancellationPolicyList = currentSession.createCriteria(CancellationPolicy.class).list();

        return cancellationPolicyList;
    }

    @Override
    public List<CancellationPolicy> getLatestCancellationPolicy(Date lastUpdateDate) {
        LOGGER.info("getLatestCancellationPolicy method in CancellationPolicyDaoImpl-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        @SuppressWarnings("unchecked")
        List<CancellationPolicy> cancellationPolicieList =currentSession.createCriteria(CancellationPolicy.class)
                .add( Restrictions.gt("updatedDate",lastUpdateDate))
                .list();

        return cancellationPolicieList;
    }
}
