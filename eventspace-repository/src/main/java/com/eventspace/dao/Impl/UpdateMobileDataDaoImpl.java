/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.dao.Impl;

import com.eventspace.dao.UpdateMobileDataDao;
import com.eventspace.domain.BaseDomain;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * The Class UpdateMobileDataDaoImpl.
 */
@Repository
public class UpdateMobileDataDaoImpl implements UpdateMobileDataDao {

	/** The Constant logger. */
	private static final Log LOGGER = LogFactory.getLog(UpdateMobileDataDaoImpl.class);

	/** The session factory. */
	@Autowired
	private SessionFactory sessionFactory;

	/* (non-Javadoc)
	 * @see com.eventspace.dao.UpdateMobileDataDao#getUpdatedTableDetails(java.lang.Object, java.lang.Long)
	 */
	@Override
	public <T> List<T> getUpdatedTableDetails(T type, Long timestamp) {
		return getUpdatedTableList(type, timestamp, null);
	}

	/* (non-Javadoc)
	 * @see com.eventspace.dao.UpdateMobileDataDao#getUpdatedTableDetails(java.lang.Object, java.lang.Long, java.lang.Integer)
	 */
	@Override
	public <T> List<T> getUpdatedTableDetails(T type, Long timestamp, Integer enterpriseId) {
		return getUpdatedTableList(type, timestamp, enterpriseId);
	}

	/**
	 * Gets the updated table list.
	 *
	 * @param <T>
	 *            the generic type
	 * @param type
	 *            the type
	 * @param timestamp
	 *            the timestamp
	 * @param enterpriseId
	 *            the enterprise id
	 * @return the updated table list
	 */
	@SuppressWarnings("unchecked")
	private <T> List<T> getUpdatedTableList(T type, Long timestamp, Integer enterpriseId) {
		try {
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(type.getClass());

			if (enterpriseId != null)
				criteria.add(Restrictions.eq("enterprise.id", enterpriseId));

			if (timestamp != null && timestamp != 0)
				criteria.add(Restrictions.gt("lastModified", new Date(timestamp)));

			return criteria.list();
		} catch (Exception e) {
			LOGGER.error("Exception occurred." +e);
			throw e;
		}
	}


	/* (non-Javadoc)
	 * @see com.eventspace.dao.UpdateMobileDataDao#save(com.eventspace.domain.BaseDomain)
	 */
	@Override
	public void save(BaseDomain domain) {
		try {
			sessionFactory.getCurrentSession().save(domain);
		} catch (Exception e) {
			LOGGER.error("Exception occurred." +e);
			throw e;
		}
	}

}
