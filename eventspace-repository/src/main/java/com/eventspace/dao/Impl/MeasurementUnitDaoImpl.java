package com.eventspace.dao.Impl;

import com.eventspace.dao.MeasurementUnitDao;
import com.eventspace.domain.MeasurementUnit;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Auxenta on 7/7/2017.
 */
@Repository
public class MeasurementUnitDaoImpl extends BaseDaoImpl<MeasurementUnit> implements MeasurementUnitDao {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentHistoryDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public List<MeasurementUnit> getMeasurementUnits() {
        LOGGER.info("getMeasurementUnits-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (List<MeasurementUnit>) currentSession.createCriteria(MeasurementUnit.class)
                .list();
    }
}
