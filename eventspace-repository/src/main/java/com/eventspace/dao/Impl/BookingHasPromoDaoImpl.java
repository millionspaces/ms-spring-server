package com.eventspace.dao.Impl;

import com.eventspace.dao.BookingHasPromoDao;
import com.eventspace.domain.BookingHasPromo;
import org.springframework.stereotype.Repository;

@Repository
public class BookingHasPromoDaoImpl extends BaseDaoImpl<BookingHasPromo> implements BookingHasPromoDao{
}
