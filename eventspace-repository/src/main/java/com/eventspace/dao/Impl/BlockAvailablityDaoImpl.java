package com.eventspace.dao.Impl;

import com.eventspace.dao.BlockAvailablityDao;
import com.eventspace.domain.BlockAvailablity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Time;

/**
 * Created by Auxenta on 6/29/2017.
 */
@Repository
public class BlockAvailablityDaoImpl extends BaseDaoImpl<BlockAvailablity> implements BlockAvailablityDao {

    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger LOGGER = LoggerFactory.getLogger(BlockAvailablityDaoImpl.class);

    @Override
    public BlockAvailablity findTheAvailablity(Integer spaceId,Integer day, Time fromTime, Time toTime) {
        LOGGER.info("findTheAvailablity ----->\tspace:"+spaceId+"\tday"+day+"\tfrom"+fromTime+"\tto"+toTime);
        Session currentSession = sessionFactory.getCurrentSession();

            return (BlockAvailablity) currentSession.createCriteria(BlockAvailablity.class)
                    .add(Restrictions.eq("space.id", spaceId))
                    .add(Restrictions.eq("day", day))
                    .add(Restrictions.eq("from", fromTime))
                    .add(Restrictions.eq("to", toTime))
                    .add(Restrictions.eq("active", 1))
                    .setMaxResults(1)
                    .uniqueResult();
    }
}
