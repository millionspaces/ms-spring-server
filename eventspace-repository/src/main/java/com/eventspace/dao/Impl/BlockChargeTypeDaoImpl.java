package com.eventspace.dao.Impl;

import com.eventspace.dao.BlockChargeTypeDao;
import com.eventspace.domain.BlockChargeType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BlockChargeTypeDaoImpl extends BaseDaoImpl<BlockChargeType> implements BlockChargeTypeDao {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentHistoryDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<BlockChargeType> getAllBlockChargeTypes() {
        LOGGER.info("getAllBlockChargeTypes-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (List<BlockChargeType>) currentSession.createCriteria(BlockChargeType.class)
                .list();
    }
}
