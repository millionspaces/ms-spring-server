package com.eventspace.dao.Impl;

import com.eventspace.dao.PaymentVerifyDao;
import com.eventspace.domain.PaymentVerify;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PaymentVerifyDaoImpl extends BaseDaoImpl<PaymentVerify> implements PaymentVerifyDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public boolean isPaymentVerified(Integer bookingId) {
        return false;
    }

    @Override
    public Long paymentVerifiedBookingCount() {
        Session session = sessionFactory.getCurrentSession();

        return (Long)session.createCriteria(PaymentVerify.class)
                .add(Restrictions.isNotNull("hostReceivedProof"))
                .setProjection(Projections.rowCount())
                .uniqueResult();

    }
}
