package com.eventspace.dao.Impl;

import com.eventspace.dao.SpaceOfWeekDao;
import com.eventspace.domain.SpaceOfWeek;
import com.eventspace.enumeration.BooleanEnum;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SpaceOfWeekDaoImpl extends BaseDaoImpl<SpaceOfWeek> implements SpaceOfWeekDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpacePromotionDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public SpaceOfWeek getSpaceOfTheWeek() {
        LOGGER.info("getSpaceOfTheWeek-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (SpaceOfWeek) currentSession.createCriteria(SpaceOfWeek.class)
                .add(Restrictions.eq("active", BooleanEnum.TRUE.value()))
                .uniqueResult();
    }
}
