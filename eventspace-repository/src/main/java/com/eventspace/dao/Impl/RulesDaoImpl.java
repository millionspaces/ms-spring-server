package com.eventspace.dao.Impl;

import com.eventspace.dao.RulesDao;
import com.eventspace.domain.Rules;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by Auxenta on 6/16/2017.
 */
@Repository
public class RulesDaoImpl extends BaseDaoImpl<Rules> implements RulesDao {

    @Autowired
    private SessionFactory sessionFactory;

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RulesDaoImpl.class);

    @Override
    public List<Rules> getAllRules() {
        LOGGER.info("getAllRules-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (List<Rules>) currentSession.createCriteria(Rules.class).list();
    }

    @Override
    public List<Rules> getLatestRules(Date lastUpdateDate) {
        LOGGER.info("getLatestRules-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (List<Rules>) currentSession.createCriteria(Rules.class)
                .add( Restrictions.gt("updatedDate",lastUpdateDate)).list();
    }
}
