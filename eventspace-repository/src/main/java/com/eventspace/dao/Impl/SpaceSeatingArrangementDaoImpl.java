package com.eventspace.dao.Impl;

import com.eventspace.dao.SpaceSeatingArrangementDao;
import com.eventspace.domain.SpaceSeatingArrangement;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Auxenta on 6/16/2017.
 */
@Repository
public class SpaceSeatingArrangementDaoImpl extends BaseDaoImpl<SpaceSeatingArrangement> implements SpaceSeatingArrangementDao {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SpaceSeatingArrangementDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void removeSeatingArrangement(Integer spaceeId, Integer seatingArrangementId) {
        LOGGER.info("RemoveSeatingArrangement  spaceId:{} seatingArrangementId:{} " ,spaceeId,seatingArrangementId);
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.createSQLQuery(String.format("delete  from space_has_seating_arrangement where space=%s and seating_arrangement=%s",spaceeId,seatingArrangementId)).executeUpdate();

    }
}
