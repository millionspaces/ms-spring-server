package com.eventspace.dao.Impl;

import com.eventspace.dao.DraftSpaceDao;
import com.eventspace.domain.DraftSpace;
import org.springframework.stereotype.Repository;

@Repository
public class DraftSpaceDaoImpl extends BaseDaoImpl<DraftSpace> implements DraftSpaceDao {

}
