package com.eventspace.dao.Impl;

import com.eventspace.dao.SpaceUnavailabilityDao;
import com.eventspace.domain.SpaceUnavailability;
import com.eventspace.enumeration.BooleanEnum;
import com.eventspace.util.Constants;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by Auxenta on 5/22/2017.
 */
/**
 * SpaceUnavailabilityDaoImpl.
 */
@Repository
public class SpaceUnavailabilityDaoImpl extends BaseDaoImpl<SpaceUnavailability> implements SpaceUnavailabilityDao {

    @Autowired
    private SessionFactory sessionFactory;
    public List<SpaceUnavailability> getUnavaliableDurations(Integer spaceId){
        Session session = sessionFactory.getCurrentSession();

        return (List<SpaceUnavailability>) session.createCriteria(SpaceUnavailability.class)
                .add(Restrictions.eq("space",spaceId))
                .add(Restrictions.ge(Constants.TO_DATE,new Date()))
                .add(Restrictions.ge("isBlocked", BooleanEnum.TRUE.value()))
                .list();
    }

    public List<SpaceUnavailability> getAllUnavaliableDurations(Integer spaceId){
        Session session = sessionFactory.getCurrentSession();

        return (List<SpaceUnavailability>) session.createCriteria(SpaceUnavailability.class)
                .add(Restrictions.eq("space",spaceId))
                .add(Restrictions.ge("isBlocked", BooleanEnum.TRUE.value()))
                .list();
    }

    @Override
    public SpaceUnavailability findSpaceUnavailability(Integer spaceId, Date from, Date to) {
        Session session = sessionFactory.getCurrentSession();
        return (SpaceUnavailability)session.createCriteria(SpaceUnavailability.class)
                .add(Restrictions.eq("space", spaceId))
                .add(Restrictions.eq(Constants.FROM_DATE,from))
                .add(Restrictions.eq(Constants.TO_DATE, to))
                .add(Restrictions.ge("isBlocked",BooleanEnum.TRUE.value()))
                .uniqueResult();
    }

    @Override
    public List<SpaceUnavailability> getHostManualBookings(Integer userId) {
        Session session = sessionFactory.getCurrentSession();
        String query="SELECT su.* FROM space_unavailablity su,space s where s.user=:userId and s.id=su.space and su.is_blocked=1 and su.is_manual=1 ;";
        return (List<SpaceUnavailability>)session.createSQLQuery(query)
                .addEntity(SpaceUnavailability.class)
                .setParameter("userId", userId)
                .list();
    }

    @Override
    public List<SpaceUnavailability> getSpaceManualBookings(Integer spaceId) {
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(SpaceUnavailability.class)
                .add(Restrictions.eq("space", spaceId))
                .add(Restrictions.eq("isManual",BooleanEnum.TRUE.value()))
                .add(Restrictions.eq("isBlocked",BooleanEnum.TRUE.value()))
                .list();
    }
}
