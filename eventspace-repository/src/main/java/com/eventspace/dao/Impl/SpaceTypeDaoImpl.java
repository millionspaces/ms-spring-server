package com.eventspace.dao.Impl;

import com.eventspace.dao.SpaceTypeDao;
import com.eventspace.domain.SpaceType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SpaceTypeDaoImpl extends BaseDaoImpl<SpaceType> implements SpaceTypeDao {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentHistoryDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<SpaceType> getAllSpaceTypes() {
        LOGGER.info("getAllSpaceTypes-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (List<SpaceType>) currentSession.createCriteria(SpaceType.class)
                .list();
    }
}
