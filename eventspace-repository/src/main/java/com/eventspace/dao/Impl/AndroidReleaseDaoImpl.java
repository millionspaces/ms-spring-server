package com.eventspace.dao.Impl;

import com.eventspace.dao.AndroidReleaseDao;
import com.eventspace.domain.AndroidRelease;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AndroidReleaseDaoImpl extends BaseDaoImpl<AndroidRelease> implements AndroidReleaseDao{

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public AndroidRelease getLatestRelease() {
        Session session = sessionFactory.getCurrentSession();
        return (AndroidRelease)session.createCriteria(AndroidRelease.class).addOrder(Order.desc("id")).setMaxResults(1).uniqueResult();
    }
}
