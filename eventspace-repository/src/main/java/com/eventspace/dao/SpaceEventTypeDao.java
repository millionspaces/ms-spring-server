/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.dao;

import com.eventspace.domain.SpaceEventType;

import java.util.List;

/**
 * Created by Aux-052 on 12/14/2016.
 */
public interface SpaceEventTypeDao extends BaseDao<SpaceEventType> {

    List<SpaceEventType> getAllSpaceEventTypes();
}
