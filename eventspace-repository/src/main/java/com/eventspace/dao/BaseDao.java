/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.dao;


/**
 * The Interface BaseDao.
 *
 * @param <T> the generic type
 */
public interface BaseDao<T> {

    /**
     * Creates the.
     *
     * @param type the type
     * @return the long
     */
    void create(T type);

    /**
     * Merge.
     *
     * @param type the type
     * @return the long
     */
    void merge(T type);

    /**
     * Read.
     *
     * @param id the id
     * @return the t
     */
    T read(Integer id);

    /**
     * Update.
     *
     * @param type the type
     */
    void update(T type);

    /**
     * Delete.
     *
     * @param type the type
     */
    void delete(T type);

}
