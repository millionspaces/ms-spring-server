package com.eventspace.dao;

import com.eventspace.domain.MenuFiles;

public interface MenuFilesDao extends BaseDao<MenuFiles>  {

    MenuFiles readByUrl(String url);
}
