/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.dao;

import com.eventspace.domain.Booking;

import java.util.Date;
import java.util.List;

/**
 * The Interface UserDao.
 */
public interface BookingDao extends BaseDao<Booking> {


    Double getBookingCharge(Integer id);

    Boolean isCancelledAfterPaid(Integer bookingId);

    Date getCancelledDate(Integer id);

    List<Booking> getBookingsEventsHeldToday();

    List<Booking> getUpcomingPaidBookingsBySpaceId(Integer spaceId);

    Boolean isUserAllowForBooking(Integer user);

    List<Booking> missedDiscardBookings();

    List<Booking> getAllBooking(Integer page);

    List<Booking> getBookings(Integer id);

    List<Booking> getBookingsBySpaceId(Integer spaceId);

    List<Booking> getBookingByUserId(Integer userId);

    List<Booking> getBookingByHostId(Integer hostId);

    Long getAllBookingCount();

    List<Booking> getAllTentitiveBookings(Integer page);

    List<Booking> getPaidBookings(Integer page);

    Long getCount(Integer bookingStatus);

    Booking getAUserLatestBooking(Integer userId);

    Long getAUserBookingCount(Integer userId);

    List<Booking> getAUserBookingWithPagination(Integer userId,Integer page);

    Integer getNoOfBookingOfSpaceForATimSlot(Integer spaceId,Date startDate,Date endDate);

    Integer getNoOfBookingByUserUsingAPromo(Integer userId,Integer promoCodeId);

    Integer getNoOFBookingsForAPromo(Integer promoCodeId);
}
