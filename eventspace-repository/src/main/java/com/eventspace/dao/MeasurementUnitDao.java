package com.eventspace.dao;

import com.eventspace.domain.MeasurementUnit;

import java.util.List;

/**
 * Created by Auxenta on 7/7/2017.
 */
public interface MeasurementUnitDao extends BaseDao<MeasurementUnit> {

    List<MeasurementUnit> getMeasurementUnits();
}
