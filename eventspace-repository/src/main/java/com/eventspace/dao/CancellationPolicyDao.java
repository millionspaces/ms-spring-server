package com.eventspace.dao;

import com.eventspace.domain.CancellationPolicy;

import java.util.Date;
import java.util.List;

/**
 * Created by Aux-052 on 12/14/2016.
 */
public interface CancellationPolicyDao extends BaseDao<CancellationPolicy> {

    List<CancellationPolicy> getCancellationPolicies();

    List<CancellationPolicy> getLatestCancellationPolicy(Date lastUpdateDate);

}
