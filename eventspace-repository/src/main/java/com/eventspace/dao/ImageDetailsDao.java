package com.eventspace.dao;

import com.eventspace.domain.ImageDetails;

public interface ImageDetailsDao extends BaseDao<ImageDetails> {

    ImageDetails getByUrl(String url);
}
