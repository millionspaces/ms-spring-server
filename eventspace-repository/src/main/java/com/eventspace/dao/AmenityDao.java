package com.eventspace.dao;

import com.eventspace.domain.Amenity;

import java.util.Date;
import java.util.List;

/**
 * Created by Aux-052 on 12/14/2016.
 */
public interface AmenityDao extends BaseDao<Amenity> {

    List<Amenity> getAllAmenities();

    List<Amenity> getLatestAmenities(Date lastUpdateDate);

}
