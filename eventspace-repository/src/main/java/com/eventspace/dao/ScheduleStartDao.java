package com.eventspace.dao;

import com.eventspace.domain.ScheduleStart;

public interface ScheduleStartDao extends BaseDao<ScheduleStart> {
}
