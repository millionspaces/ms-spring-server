package com.eventspace.dao;

import com.eventspace.domain.SeatingArrangement;

import java.util.Date;
import java.util.List;

/**
 * Created by Auxenta on 6/16/2017.
 */
public interface SeatingArrangementDao extends BaseDao<SeatingArrangement> {

    List<SeatingArrangement> getAllSeatingArrangement();

    List<SeatingArrangement> getLatestSeatingArrangement(Date lastUpdateDate);
}
