package com.eventspace.dao;

import com.eventspace.domain.PrioritySpaces;

import java.util.List;

public interface PrioritySpacesDao extends BaseDao<PrioritySpaces>{
    List<PrioritySpaces> getAllSpaces();
}
