package com.eventspace.dao;

import com.eventspace.domain.BookingPayLater;

import java.util.Date;
import java.util.List;

public interface BookingPayLaterDao  extends BaseDao<BookingPayLater>  {

}
