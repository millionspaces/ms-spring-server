package com.eventspace.dao;

import com.eventspace.domain.DraftSpace;

public interface DraftSpaceDao extends BaseDao<DraftSpace> {
}
