package com.eventspace.dao;

import com.eventspace.domain.UserAdditionalDetails;

/**
 * Created by Auxenta on 7/5/2017.
 */
public interface UserAdditionalDetailsDao extends BaseDao<UserAdditionalDetails> {
}
