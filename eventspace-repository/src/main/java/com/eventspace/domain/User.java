/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.domain;

import com.eventspace.dto.AdminUserDetailsDto;
import com.eventspace.dto.SpaceOwnerDto;
import com.eventspace.dto.UserDetailsDto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Optional;
import java.util.Set;

/**
 * The Class User.
 */
@Entity
@Table(name = "user")
public class User implements Serializable, BaseDomain {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;


	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	/** The name. */
	@Column(name = "name", nullable= false)
	private String name;


	/** The password. */
	@Column(name = "password", nullable= false)
	private String password;
	
	/** The email. */
	@Column(name="email", nullable= false)
	private String email;
	
	/** The is trusted user. */
	@Column(name="is_trusted_owner", nullable= false)
	private Boolean isTrustedUser;
	
	/** The phone. */
	@Column(name="phone")
	private String phone;

	/** The company phone. */
	@Column(name="company_phone")
	private String companyPhone;

	@Column(name="date_of_birth")
	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;

	@Column(name="anniversary")
	@Temporal(TemporalType.DATE)
	private Date anniversary;

	/** The created at. */
	@Column(name="created_at", nullable= false)
	private Date createdAt;
	
	/** The updated at. */
	@Column(name="updated_at", nullable= false)
	private Date updatedAt;
	
	/** The image url. */
	@Column(name="image_url")
	private String imageUrl;

	/** The active. */
	@Column(name="active" )
	private Integer active;

	/** The role. */
	@Column(name="user_role")
	private String role;

	@OneToOne(cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private UserAdditionalDetails userAdditionalDetails;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<DraftSpace> draftSpaces;
	
	/**
	 * Gets the image url.
	 *
	 * @return the image url
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * Sets the image url.
	 *
	 * @param imageUrl the new image url
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * The Constructor.
	 *
	 * @param id
	 *            the id
	 */
	public User(int id) {
		this.id = id;
	}

	/**
	 * The Constructor.
	 */
	public User() {
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	
	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the checks if is trusted user.
	 *
	 * @return the checks if is trusted user
	 */
	public Boolean getIsTrustedUser() {
		return isTrustedUser;
	}

	/**
	 * Sets the checks if is trusted user.
	 *
	 * @param isTrustedUser the new checks if is trusted user
	 */
	public void setIsTrustedUser(Boolean isTrustedUser) {
		this.isTrustedUser = isTrustedUser;
	}

	/**
	 * Gets the phone.
	 *
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Sets the phone.
	 *
	 * @param phone the new phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Gets the created at.
	 *
	 * @return the created at
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * Sets the created at.
	 *
	 * @param createdAt the new created at
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * Gets the updated at.
	 *
	 * @return the updated at
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * Sets the updated at.
	 *
	 * @param updatedAt the new updated at
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}


	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public UserAdditionalDetails getUserAdditionalDetails() {
		return userAdditionalDetails;
	}

	public void setUserAdditionalDetails(UserAdditionalDetails userAdditionalDetails) {
		this.userAdditionalDetails = userAdditionalDetails;
	}

	public Set<DraftSpace> getDraftSpaces() {
		return draftSpaces;
	}

	public void setDraftSpaces(Set<DraftSpace> draftSpaces) {
		this.draftSpaces = draftSpaces;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Date getAnniversary() {
		return anniversary;
	}

	public void setAnniversary(Date anniversary) {
		this.anniversary = anniversary;
	}

	/**
	 * Builds the.
	 *
	 * @return the stsus
	 */


	@Override
	public UserDetailsDto build() {
		UserDetailsDto userDto = new UserDetailsDto();
		
		userDto.setId(id);
		userDto.setName(name);
		userDto.setEmail(email);
		userDto.setPassword(password);
		userDto.setMobileNumber(phone);
		userDto.setCompanyPhone(companyPhone);
		userDto.setIsTrustedUser(isTrustedUser);
		userDto.setImageUrl(imageUrl);
		userDto.setActive(active);
		userDto.setRole(role);
		userDto.setDateOfBirth(dateOfBirth);
		userDto.setAnniversary(anniversary);

		if (userAdditionalDetails!=null) {
			userDto.setContactPersonName(userAdditionalDetails.getLastName());
			userDto.setAddress(userAdditionalDetails.getAddress());
			userDto.setJobTitle(userAdditionalDetails.getJob());
			userDto.setCommissionPercentage(userAdditionalDetails.getCommissionPercentage());
			userDto.setCompanyName(userAdditionalDetails.getCompanyName());
			userDto.setAbout(userAdditionalDetails.getAbout());
			userDto.setAccountHolderName(userAdditionalDetails.getAccountHolderName());
			userDto.setAccountNumber(userAdditionalDetails.getAccountNumber());
			userDto.setBank(userAdditionalDetails.getBank());
			userDto.setBankBranch(userAdditionalDetails.getBankBranch());
		}
		return userDto;
	}

	public AdminUserDetailsDto buildUserDetailsForAdmin(){
		AdminUserDetailsDto userDto=new AdminUserDetailsDto();

		userDto.setId(id);
		userDto.setName(name);
		userDto.setPhone(phone);
		userDto.setCompanyPhone(companyPhone);
		userDto.setEmail(email);
		return userDto;
	}

	public SpaceOwnerDto buildSpaceOwnerDto(){
		SpaceOwnerDto spaceOwnerDto = new SpaceOwnerDto();
		spaceOwnerDto.setId(id);
		spaceOwnerDto.setName(name);
		spaceOwnerDto.setImageUrl(imageUrl);
		return  spaceOwnerDto;
	}
	public static User build(UserDetailsDto userDto,User user){
		user.setName(userDto.getName());
		user.setPhone(userDto.getMobileNumber());
		user.setCompanyPhone(userDto.getCompanyPhone());
		user.setImageUrl(userDto.getImageUrl());
		user.setDateOfBirth(Optional.ofNullable(userDto.getDateOfBirth()).orElse(null));
		user.setAnniversary(Optional.ofNullable(userDto.getAnniversary()).orElse(null));
		if (user.getCreatedAt()==null)
			user.setCreatedAt(new Date());
		user.setUpdatedAt(new Date());
		return user;
	}
}
