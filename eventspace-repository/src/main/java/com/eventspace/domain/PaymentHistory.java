package com.eventspace.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Auxenta on 6/2/2017.
 */
@Entity
@Table(name = "payment_history")
public class PaymentHistory  implements Serializable, BaseDomain{

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /** The booking. */
    @Column(name = "booking", nullable = false)
    private Integer bookingId;

    /** The response. */
    @Column(name = "response", nullable = false)
    private Integer response;

    @Column(name = "response_code")
    private String responseCode;

    @Column(name = "provider")
    private String provider;
    /**
     * The created date.
     */
    @Column(name = "created_at", nullable = false)
    private Date createdAt=new Date();



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getResponse() {
        return response;
    }

    public void setResponse(Integer response) {
        this.response = response;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    @Override
    public Object build() {
        return null;
    }

    @Override
    public String toString() {
        return "PaymentHistory{" +
                "id=" + id +
                ", bookingId=" + bookingId +
                ", response=" + response +
                ", responseCode='" + responseCode + '\'' +
                ", provider='" + provider + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }
}
