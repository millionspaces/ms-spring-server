package com.eventspace.domain;

import com.eventspace.dto.BookingWithExtraAmenityDto;
import com.eventspace.dto.ExtraAmenityDto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * Created by Aux-052 on 12/16/2016.
 */

/**
 * The Class SpaceExtraAmenity.
 */
@Entity
@Table(name = "booking_has_extra_amenity")
@AssociationOverrides({
        @AssociationOverride(name = "pk1.space",
                joinColumns = @JoinColumn(name = "space_has_extra_amenity_space")),
        @AssociationOverride(name = "pk1.amenity",
                joinColumns = @JoinColumn(name = "space_has_extra_amenity_amenity")),
        @AssociationOverride(name = "pk1.booking",
                joinColumns = @JoinColumn(name = "booking")) })
public class BookingSpaceExtraAmenity implements Serializable, BaseDomain {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "number", nullable = false)
    private Integer number;

    @Column(name = "rate", nullable = false)
    private Double rate;

    @Column(name = "booking", insertable = false,updatable = false)
    private Integer bookingId;

    @EmbeddedId
    private BookingSpaceAmenityPK pk1;

    @Transient
    private Booking booking;

    @Transient
    private Space space;

    @Transient
    private Amenity amenity;

    public void setPk1(BookingSpaceAmenityPK pk1) {
        if (pk1 != null) {
            this.space = pk1.getSpace();
            this.amenity = pk1.getAmenity();
            this.booking = pk1.getBooking();
        }
        this.pk1 = pk1;
    }

    public BookingSpaceAmenityPK getPk1() {
        if (pk1 == null)
            pk1 = new BookingSpaceAmenityPK(space,amenity,booking);
        return pk1;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public Space getSpace() {
        return space;
    }

    public void setSpace(Space space) {
        this.space = space;
    }

    public Amenity getAmenity() {
        return amenity;
    }

    public void setAmenity(Amenity amenity) {
        this.amenity = amenity;
    }

    public Integer getBookingId() {
        return booking.getId();
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = booking.getId();
    }

    @Override
    public ExtraAmenityDto build() {
        return null;
    }

    public static BookingSpaceExtraAmenity build(BookingWithExtraAmenityDto bookingWithExtraAmenityDto , Space space, Amenity amenity, Booking booking, Set<SpaceExtraAmenity> spaceExtraAmenitySet) {
        BookingSpaceExtraAmenity bookingSpaceExtraAmenity = new BookingSpaceExtraAmenity();
        bookingSpaceExtraAmenity.setPk1(new BookingSpaceAmenityPK(space,amenity,booking));
        bookingSpaceExtraAmenity.setAmenity(amenity);
        bookingSpaceExtraAmenity.setBooking(booking);
        bookingSpaceExtraAmenity.setSpace(space);
        bookingSpaceExtraAmenity.setBookingId(booking.getId());
        for(SpaceExtraAmenity spaceExtraAmenity : spaceExtraAmenitySet){
            if(Objects.equals(spaceExtraAmenity.getPk().getAmenity().getId(), bookingWithExtraAmenityDto.getAmenityId())){
                bookingSpaceExtraAmenity.setNumber(bookingWithExtraAmenityDto.getNumber());
                bookingSpaceExtraAmenity.setRate(spaceExtraAmenity.getExtraRate());
            }
        }
        return bookingSpaceExtraAmenity;
    }
}
