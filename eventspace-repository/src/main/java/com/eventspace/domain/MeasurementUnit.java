package com.eventspace.domain;

import com.eventspace.dto.CommonDto;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Auxenta on 7/7/2017.
 */
@Entity
@Table(name = "measurement_unit")
public class MeasurementUnit implements Serializable, BaseDomain {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /** The name. */
    @Column(name = "unit_type", nullable = false)
    private String unitType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    @Override
    public CommonDto build() {
        CommonDto measurementUnit=new CommonDto();
        measurementUnit.setId(id);
        measurementUnit.setName(unitType);

        return  measurementUnit;
    }
}
