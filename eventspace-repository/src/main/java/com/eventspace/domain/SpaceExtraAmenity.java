package com.eventspace.domain;

import com.eventspace.dto.ExtraAmenityDto;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Aux-052 on 12/16/2016.
 */

/**
 * The Class SpaceExtraAmenity.
 */
@Entity
@Table(name = "space_has_extra_amenity")
@AssociationOverrides({
        @AssociationOverride(name = "pk.space",
                joinColumns = @JoinColumn(name = "space")),
        @AssociationOverride(name = "pk.amenity",
                joinColumns = @JoinColumn(name = "amenity")) })
public class SpaceExtraAmenity implements Serializable, BaseDomain {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private SpaceAmenityPK pk;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "space", insertable = false, updatable = false)
    //@Transient
    //@Column(name = "space", nullable = false)
    private Space space;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "amenity", insertable = false, updatable = false)
    //@Transient
    //@Column(name = "amenity", nullable = false)
    private Amenity extraAmenity;

    @Column(name = "amenity_unit", nullable = false)
    private Integer amenityUnit;

    @Column(name = "extra_rate", nullable = false)
    private Double extraRate;

    public SpaceAmenityPK getPk() {
        if (pk == null)
            pk = new SpaceAmenityPK(space, extraAmenity);
        return pk;
    }

    public void setPk(SpaceAmenityPK pk) {
        if (pk != null) {
            this.space = pk.getSpace();
            this.extraAmenity = pk.getAmenity();
        }
        this.pk = pk;
    }

    public Space getSpace() {
        return space;
    }

    public void setSpace(Space space) {
        this.space = space;
    }

    public Amenity getExtraAmenity() {
        return extraAmenity;
    }

    public void setExtraAmenity(Amenity extraAmenity) {
        this.extraAmenity = extraAmenity;
    }

    public Integer getAmenityUnit() {
        return amenityUnit;
    }

    public void setAmenityUnit(Integer amenityUnit) {
        this.amenityUnit = amenityUnit;
    }

    public Double getExtraRate() {
        return extraRate;
    }

    public void setExtraRate(Double extraRate) {
        this.extraRate = extraRate;
    }

    /* (non-Javadoc)
   * @see com.eventspace.domain.BaseDomain#build(java.lang.Integer)
   */
    @Override
    public ExtraAmenityDto build() {
        ExtraAmenityDto extraAmenityDto=new ExtraAmenityDto();
        extraAmenityDto.setAmenityId(pk.getAmenity().getId());
        extraAmenityDto.setName(pk.getAmenity().getName());
        extraAmenityDto.setAmenityUnit(amenityUnit);
        extraAmenityDto.setExtraRate(extraRate);
        extraAmenityDto.setAmenityIcon(pk.getAmenity().getIcon());
        extraAmenityDto.setMobileIcon(pk.getAmenity().getMobileIcon());
        return extraAmenityDto;
    }

    public static SpaceExtraAmenity build(Space space,Amenity amenity,ExtraAmenityDto extraAmenityDto) {
        SpaceExtraAmenity spaceExtraAmenity=new SpaceExtraAmenity();
        spaceExtraAmenity.setSpace(space);
        spaceExtraAmenity.setExtraAmenity(amenity);
        spaceExtraAmenity.setPk(new SpaceAmenityPK(space,amenity));
        spaceExtraAmenity.setAmenityUnit(extraAmenityDto.getAmenityUnit());
        spaceExtraAmenity.setExtraRate(extraAmenityDto.getExtraRate());
        return spaceExtraAmenity;
    }
}
