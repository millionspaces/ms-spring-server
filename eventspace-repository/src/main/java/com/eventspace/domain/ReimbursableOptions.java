package com.eventspace.domain;

import com.eventspace.dto.ReimbursableOptionDto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "reimbursable_options")
public class ReimbursableOptions implements Serializable, BaseDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "desc", nullable = false)
    private String desc;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public ReimbursableOptionDto build() {
        ReimbursableOptionDto reimbursableOptionDto=new ReimbursableOptionDto();
        reimbursableOptionDto.setId(id);
        reimbursableOptionDto.setCode(code);
        reimbursableOptionDto.setDesc(desc);
        return reimbursableOptionDto;
    }
}
