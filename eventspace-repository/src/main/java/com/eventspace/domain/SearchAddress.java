package com.eventspace.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Auxenta on 6/12/2017.
 */
@Entity
@Table(name = "search_address")
public class SearchAddress implements Serializable, BaseDomain{

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @Column(name = "space", nullable = false, unique = true)
    private Integer space;

    /** The name. */
    @Column(name = "address", nullable= false)
    private String address;

    public Integer getSpace() {
        return space;
    }

    public void setSpace(Integer space) {
        this.space = space;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public Object build() {
        return null;
    }
}
