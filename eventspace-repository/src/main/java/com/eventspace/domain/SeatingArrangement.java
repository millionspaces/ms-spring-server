package com.eventspace.domain;

import com.eventspace.dto.CommonDto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Auxenta on 6/16/2017.
 */
@Entity
@Table(name = "seating_arrangement")
public class SeatingArrangement implements Serializable, BaseDomain {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /** The name. */
    @Column(name = "name", nullable = false)
    private String name;


    /** The icon. */
    @Column(name="icon")
    private String icon;

    @Column(name="mobile_icon")
    private String mobileIcon;

    /** The date updated.
     * */
    @Column(name="date_updated", nullable= false)
    private Date updatedDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getMobileIcon() {
        return mobileIcon;
    }

    public void setMobileIcon(String mobileIcon) {
        this.mobileIcon = mobileIcon;
    }

    @Override
    public CommonDto build() {
        CommonDto seatingArrangement =new CommonDto();
        seatingArrangement.setId(id);
        seatingArrangement.setName(name);
        seatingArrangement.setIcon(icon);
        seatingArrangement.setMobileIcon(mobileIcon);
        seatingArrangement.setTimeStamp(String.valueOf(updatedDate.getTime()));

        return seatingArrangement;
    }

}
