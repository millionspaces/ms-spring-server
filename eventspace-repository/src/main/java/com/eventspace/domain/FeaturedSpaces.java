package com.eventspace.domain;

import com.eventspace.dto.FeaturedSpacesDto;
import com.eventspace.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Auxenta on 8/24/2017.
 */
@Entity
@Table(name = "featured_spaces")
public class FeaturedSpaces implements Serializable, BaseDomain{

    private static final Logger LOGGER = LoggerFactory
            .getLogger(FeaturedSpaces.class);

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "space", nullable = false, unique = true)
    private Integer space;

    @Column(name="featured_type")
    private Integer type;

    @Column(name="start_date")
    private Date startDate;

    @Column(name="end_date")
    private Date endDate;

    @Column(name="featured")
    private Integer featured;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSpace() {
        return space;
    }

    public void setSpace(Integer space) {
        this.space = space;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getFeatured() {
        return featured;
    }

    public void setFeatured(Integer featured) {
        this.featured = featured;
    }

    @Override
    public FeaturedSpacesDto build() {
        FeaturedSpacesDto featuredSpacesDto=new FeaturedSpacesDto();
        featuredSpacesDto.setId(id);
        featuredSpacesDto.setSpace(space);
        featuredSpacesDto.setType(type);
        featuredSpacesDto.setFeatured(featured);
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
            if(startDate!=null)
                featuredSpacesDto.setStartDate(df.format(startDate));
            if (endDate!=null)
                featuredSpacesDto.setEndDate(df.format(endDate));
        } catch (Exception e) {
            LOGGER.error("date convert exception----->{}",e);
        }
        return featuredSpacesDto;
    }

    public static FeaturedSpaces build(FeaturedSpacesDto featuredSpacesDto){
        FeaturedSpaces featuredSpaces=new FeaturedSpaces();
        featuredSpaces.setSpace(featuredSpacesDto.getSpace());
        featuredSpaces.setType(featuredSpacesDto.getType());
        featuredSpaces.setFeatured(featuredSpacesDto.getFeatured());
        try {
            DateFormat df = new SimpleDateFormat(Constants.PATTERN_2);

            featuredSpaces.setStartDate(df.parse(featuredSpacesDto.getStartDate()));
            featuredSpaces.setEndDate(df.parse(featuredSpacesDto.getEndDate()));
        } catch (Exception e) {
            LOGGER.error("build exception----->{}",e);
        }
        return featuredSpaces;
    }
}
