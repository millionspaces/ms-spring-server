package com.eventspace.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Aux-052 on 12/20/2016.
 */
@Entity
@Table(name="image")
@AssociationOverrides({
        @AssociationOverride(name="space"
                ,joinColumns=@JoinColumn(name="space", nullable = false, updatable = false))
})
public class Image implements Serializable, BaseDomain {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;


    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /** The name. */
    @Column(name = "url", nullable = false,unique=true)
    private String url;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "space",insertable=false,updatable=false)
    private Space space;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Space getSpace() {
        return space;
    }

    public void setSpace(Space space) {
        this.space = space;
    }

    @Override
    public Object build() {
        return null;
    }

    public static Image build(Space space,String imgUrl) {
        Image image=new Image();
        image.setSpace(space);
        image.setUrl(imgUrl);
        return image;
    }
}
