/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.domain;

/**
 * The Interface BaseDomain.
 */
public interface BaseDomain {


	/**
	 * Builds the.
	 *
	 * @return the object
	 */
	Object build();
}
