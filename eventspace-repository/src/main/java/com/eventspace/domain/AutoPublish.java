package com.eventspace.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Auxenta on 6/16/2017.
 */
@Entity
@Table(name = "auto_publish")
public class AutoPublish implements Serializable, BaseDomain{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * The id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /**
     * The name.
     */
    @Column(name = "enabled", nullable = false)
    private Integer enable;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    @Override
    public Object build() {
        return null;
    }
}
