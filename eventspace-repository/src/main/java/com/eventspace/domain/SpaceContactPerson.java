package com.eventspace.domain;

import com.eventspace.dto.SpaceContactPersonDto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "space_contact_persons")
public class SpaceContactPerson implements Serializable, BaseDomain{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "space")
    private Integer space;

    @Column(name = "contact_person_name")
    private String contactPersonName;

    @Column(name = "contact_person_email")
    private String contactPersonEmail;


    @Column(name = "contact_person_mobile")
    private String contactPersonMobile;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSpace() {
        return space;
    }

    public void setSpace(Integer space) {
        this.space = space;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getContactPersonEmail() {
        return contactPersonEmail;
    }

    public void setContactPersonEmail(String contactPersonEmail) {
        this.contactPersonEmail = contactPersonEmail;
    }

    public String getContactPersonMobile() {
        return contactPersonMobile;
    }

    public void setContactPersonMobile(String contactPersonMobile) {
        this.contactPersonMobile = contactPersonMobile;
    }

    @Override
    public SpaceContactPersonDto build() {
        SpaceContactPersonDto spaceContactPersonDto=new SpaceContactPersonDto();
        spaceContactPersonDto.setId(id);
        spaceContactPersonDto.setName(contactPersonName);
        spaceContactPersonDto.setEmail(contactPersonEmail);
        spaceContactPersonDto.setMobile(contactPersonMobile);
        return spaceContactPersonDto;
    }
}
