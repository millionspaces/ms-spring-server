/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.domain;

import com.eventspace.dto.EventTypeDto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The Class EventType.
 */
@Entity
@Table(name = "event_type")
public class EventType implements Serializable, BaseDomain {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	/** The name. */
	@Column(name = "name", nullable = false)
	private String name;
	
	/** The icon. */
	@Column(name="icon")
	private String icon;

	@Column(name="mobile_icon")
	private String mobileIcon;

	@Column(name="tile_image")
	private String tileImage;

	@Column(name="active")
	private Integer active;

	@Column(name="priority")
	private Integer priority;

	/** The date created. */
	@Column(name="date_updated", nullable= false)
	private Date updatedDate;
	

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the new icon
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getTileImage() {
		return tileImage;
	}

	public void setTileImage(String tileImage) {
		this.tileImage = tileImage;
	}

	@Override
	public EventTypeDto build() {
		EventTypeDto eventTypeDto = new EventTypeDto();
		eventTypeDto.setName(name);
		eventTypeDto.setId(id);
		eventTypeDto.setIcon(icon);
		eventTypeDto.setMobileIcon(mobileIcon);
		eventTypeDto.setTileImage(tileImage);
		eventTypeDto.setTimeStamp(String.valueOf(updatedDate.getTime()));
		return eventTypeDto;
	}

}
