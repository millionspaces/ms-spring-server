package com.eventspace.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Auxenta on 6/5/2017.
 */
@Entity
@Table(name = "booking_expire_details")
public class BookingExpireDetails implements Serializable, BaseDomain  {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /** The booking. */
    @Column(name = "booking", nullable = false)
    private Integer booking;

    /**
     * The expire date.
     */
    @Column(name = "expire_date", nullable = false)
    private Date expireDate;

    /**
     * The expired.
     */
    @Column(name = "expired", nullable = false)
    private int expired;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBooking() {
        return booking;
    }

    public void setBooking(Integer booking) {
        this.booking = booking;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public int getExpired() {
        return expired;
    }

    public void setExpired(int expired) {
        this.expired = expired;
    }

    @Override
    public Object build() {
        return null;
    }
}
