package com.eventspace.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "booking_has_promotion")
public class BookingHasPromo implements Serializable, BaseDomain {

    @Id
    @Column(name = "booking", nullable = false, unique = true)
    private Integer booking;

    @Column(name = "promo_details", nullable = false, unique = true)
    private Integer promoDetails;

    public Integer getBooking() {
        return booking;
    }

    public void setBooking(Integer booking) {
        this.booking = booking;
    }

    public Integer getPromoDetails() {
        return promoDetails;
    }

    public void setPromoDetails(Integer promoDetails) {
        this.promoDetails = promoDetails;
    }

    @Override
    public Object build() {
        return null;
    }
}
