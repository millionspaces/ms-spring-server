/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.domain;

import com.eventspace.dto.*;
import com.eventspace.enumeration.AvailabilityMethods;
import com.eventspace.enumeration.BlockChargeTypeEnum;
import com.eventspace.enumeration.BooleanEnum;
import com.eventspace.enumeration.ChargeTypeEnum;
import com.eventspace.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.eventspace.domain.Space.*;



/**
 * The Class Space.
 */
@NamedNativeQueries({
        @NamedNativeQuery(
                name = DELETE_SPACE_SP,
                query = "CALL deleteSpace(:p_spaceId)"
        ),
        @NamedNativeQuery(
                name = FILTER_SPACE_SP,
                query = "CALL filterSpace(:param1,:param2,:rate1,:rate2,:eventList,:amenityList,:limitFor)",
                resultClass = Space.class
        ),
        @NamedNativeQuery(
                name = SPACE_BY_COORDINATES,
                query = "CALL spaceByCoordinates(:latitudeVal,:longitudeVal,:radius)",
                resultClass = Space.class
        )
})
@Entity
@Table(name = "space")
public class Space implements Serializable, BaseDomain {

    public static final String DELETE_SPACE_SP = "callDeleteSpaceStoreProcedure";
    public static final String FILTER_SPACE_SP = "callFilterSpaceStoredProcedure";
    public static final String SPACE_BY_COORDINATES = "callSpaceByCoordinatesSP";
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory
            .getLogger(Space.class);


    /**
     * The id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /**
     * The name.
     */
    @Column(name = "name", nullable = false)
    private String name;

    /**
     * The addressLine1.
     */
    @Column(name = "address_line_1")
    private String addressLine1;

    /**
     * The addressLine2.
     */
    @Column(name = "address_line_2")
    private String addressLine2;

    /**
     * The description.
     */
    @Column(name = "description")
    private String description;

    /**
     * The size.
     */
    @Column(name = "size", nullable = false)
    private Integer size;

    /**
     * The participant count.
     */
    @Column(name = "participant_count", nullable = false)
    private Integer participantCount;

    /**
     * The rate per hour.
     */
    @Column(name = "rate_per_hour", nullable = false)
    private Double ratePerHour;

    /**
     * The user.
     */
    @Column(name = "user", nullable = false)
    private Integer user;

    /**
     * The cancellation policy.
     */
    @Column(name = "cancellation_policy", nullable = false)
    private Integer cancellationPolicy;

    /**
     * The latitude.
     */
    @Column(name = "latitude", nullable = false)
    private String latitude;

    /**
     * The longitude.
     */
    @Column(name = "longitude", nullable = false)
    private String longitude;

    /**
     * The thumbnailImage.
     */
    @Column(name = "thumbnail_image", nullable = false)
    private String thumbnailImage;

    /**
     * The security deposite.
     */
    @Column(name = "security_deposit")
    private Integer securityDeposite;

    /**
     * The approved.
     */
    @Column(name = "approved", nullable = false)
    private int approved;
    
    @Column(name = "calendar_start")
    private Date calendarStart;
        
    @Column(name = "calendar_end")
    private Date calendarEnd;
    
    @Column(name = "calendar_event_size")
    private String calendarEventSize;

    @Column(name = "buffer_time")
    private Integer bufferTime;

    @Column(name = "notice_period")
    private Integer noticePeriod;

    //@Column(name = "measurement_unit")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "measurement_unit", insertable = false, updatable = false)
    private MeasurementUnit measurementUnitDto;

    @Column(name = "measurement_unit")
    private Integer measurementUnit;

    @Column(name = "charge_type")
    private Integer chargeType;

    @Column(name = "block_charge_type")
    private Integer blockChargeType;

    @Column(name = "min_participant_count")
    private Integer minParticipantCount;



    @Column(name = "reimbursable_options")
    private Integer reimbursableOption;

    @Column(name = "menu_file_name")
    private String menuFileName;

    /** The created at. */
    @Column(name="created_at")
    private Date createdAt;

    @Column(name="approved_date")
    private Date approvedDate;

    /** The updated at. */
    @Column(name="updated_at")
    private Date updatedAt;

    @Column(name="parent_id")
    private Integer parent;

    @Column(name="link_space")
    private Integer linkSpace;
    /**
     * The images.
     */
    @OneToMany(mappedBy = "space", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Image> images;

    @OneToMany(mappedBy = "space", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<SpacePromotion> promotions;

    @OneToMany(mappedBy = "space", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<SpaceContactPerson> spaceContactPersons;

    /**
     * The images.
     */
    @OneToMany(mappedBy = "space", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @OrderBy("day ASC,from ASC")
    private Set<BlockAvailablity> blockAvailablityBlocks;

    @OneToMany(mappedBy = "space", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @OrderBy("day ASC")
    private Set<PerHourAvailablity> perHourAvailablity;

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private SearchAddress searchAddress;

    /**
     * The amenities.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "space_has_amenity", joinColumns = {
            @JoinColumn(name = "space", nullable = false, updatable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "amenity", nullable = false, updatable = false)})
    private Set<Amenity> amenities = new HashSet<>(0);

    /**
     * The event types.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "space_has_event_type", joinColumns = {
            @JoinColumn(name = "space", nullable = false, updatable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "event_type", nullable = false, updatable = false)})
    private Set<EventType> eventTypes = new HashSet<>(0);

    /**
     * The rules.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "space_has_rule", joinColumns = {
            @JoinColumn(name = "space", nullable = false, updatable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "rule", nullable = false, updatable = false)})
    private Set<Rules> rules;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "space_has_menu_files", joinColumns = {
            @JoinColumn(name = "space", nullable = false, updatable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "menu_files", nullable = false, updatable = false)})
    private Set<MenuFiles> menuFiles;

    /**
     * The extra amenities.
     */
    @OneToMany(mappedBy = "pk.space", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    private Set<SpaceExtraAmenity> extraAmenities;

    /**
     * The extra amenities.
     */
    @OneToMany(mappedBy = "spaceSeatingArrangementPk.space", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    private Set<SpaceSeatingArrangement> spaceSeatingArrangements;

    /**
     * The space extra amenities.
     */
    @OneToMany(mappedBy = "pk1.space", fetch = FetchType.LAZY)
    private Set<BookingSpaceExtraAmenity> spaceExtraAmenities;

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private SpaceAdditionalDetails spaceAdditionalDetails;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "space_has_space_type", joinColumns = {
            @JoinColumn(name = "space", nullable = false, updatable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "space_type", nullable = false, updatable = false)})
    private Set<SpaceType> spaceTypes;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "primary_event_type")
    private EventType primaryEventType;

    /**
     * Gets the images.
     *
     * @return the images
     */
    public Set<Image> getImages() {
        return images;
    }

    /**
     * Sets the images.
     *
     * @param images the new images
     */
    public void setImages(Set<Image> images) {
        this.images = images;
    }

    /**
     * Gets the event types.
     *
     * @return the event types
     */
    public Set<EventType> getEventTypes() {
        return eventTypes;
    }

    /**
     * Sets the event types.
     *
     * @param eventTypes the new event types
     */
    public void setEventTypes(Set<EventType> eventTypes) {
        this.eventTypes = eventTypes;
    }

    /**
     * Gets the extra amenities.
     *
     * @return the extra amenities
     */
    public Set<SpaceExtraAmenity> getExtraAmenities() {
        return extraAmenities;
    }

    /**
     * Sets the extra amenities.
     *
     * @param extraAmenities the new extra amenities
     */
    public void setExtraAmenities(Set<SpaceExtraAmenity> extraAmenities) {
        this.extraAmenities = extraAmenities;
    }


    public Set<SpaceSeatingArrangement> getSpaceSeatingArrangements() {
        return spaceSeatingArrangements;
    }

    public void setSpaceSeatingArrangements(Set<SpaceSeatingArrangement> spaceSeatingArrangements) {
        this.spaceSeatingArrangements = spaceSeatingArrangements;
    }

    /**
     * The Constructor.
     *
     * @param id the id
     */
    public Space(int id) {
        this.id = id;
    }

    /**
     * The Constructor.
     */
    public Space() {
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the address line 1.
     *
     * @return the address line 1
     */
    public String getAddressLine1() {
        return addressLine1;
    }

    /**
     * Sets the address line 1.
     *
     * @param addressLine1 the new address line 1
     */
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    /**
     * Gets the address line 2.
     *
     * @return the address line 2
     */
    public String getAddressLine2() {
        return addressLine2;
    }

    /**
     * Sets the address line 2.
     *
     * @param addressLine2 the new address line 2
     */
    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the size.
     *
     * @return the size
     */
    public Integer getSize() {
        return size;
    }

    /**
     * Sets the size.
     *
     * @param size the new size
     */
    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * Gets the participant count.
     *
     * @return the participant count
     */
    public Integer getParticipant_count() {
        return participantCount;
    }

    /**
     * Sets the participant count.
     *
     * @param participant_count the new participant count
     */
    public void setParticipantCount(Integer participant_count) {
        this.participantCount = participant_count;
    }

    /**
     * Gets the rate per hour.
     *
     * @return the rate per hour
     */
    public Double getRatePerHour() {
        return ratePerHour;
    }

    /**
     * Sets the rate per hour.
     *
     * @param ratePerHour the new rate per hour
     */
    public void setRatePerHour(Double ratePerHour) {
        this.ratePerHour = ratePerHour;
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public Integer getUser() {
        return user;
    }

    /**
     * Sets the user.
     *
     * @param user the new user
     */
    public void setUser(Integer user) {
        this.user = user;
    }

    /**
     * Gets the cancellation policy.
     *
     * @return the cancellation policy
     */
    public Integer getCancellationPolicy() {
        return cancellationPolicy;
    }

    /**
     * Sets the cancellation policy.
     *
     * @param cancellationPolicy the new cancellation policy
     */
    public void setCancellationPolicy(Integer cancellationPolicy) {
        this.cancellationPolicy = cancellationPolicy;
    }


    /**
     * Gets the latitude.
     *
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * Sets the latitude.
     *
     * @param latitude the new latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * Gets the longitude.
     *
     * @return the longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * Sets the longitude.
     *
     * @param longitude the new longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * Gets the amenity.
     *
     * @return the amenity
     */
    public Set<Amenity> getAmenity() {
        return amenities;
    }

    /**
     * Sets the amenity.
     *
     * @param amenities the new amenity
     */
    public void setAmenity(Set<Amenity> amenities) {
        this.amenities = amenities;
    }

    /**
     * Gets the thumbnailImage.
     *
     * @return the thumbnailImage
     */
    public String getThumbnailImage() {
        return thumbnailImage;
    }

    /**
     * Sets the thumbnailImage.
     *
     * @param thumbnailImage the new thumbnailImage
     */
    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    public int getApproved() {
        return approved;
    }

    public void setApproved(int approved) {
        this.approved = approved;
    }

    public Date getCalendarStart() {
		return calendarStart;
	}

	public void setCalendarStart(Date calendarStart) {
		this.calendarStart = calendarStart;
	}

	public Date getCalendarEnd() {
		return calendarEnd;
	}

	public void setCalendarEnd(Date calendarEnd) {
		this.calendarEnd = calendarEnd;
	}

	
	public String getCalendarEventSize() {
		return calendarEventSize;
	}

	public void setCalendarEventSize(String calendarEventSize) {
		this.calendarEventSize = calendarEventSize;
	}

	public SearchAddress getSearchAddress() {
        return searchAddress;
    }

    public void setSearchAddress(SearchAddress searchAddress) {
        this.searchAddress = searchAddress;
    }

    public Integer getSecurityDeposite() {
        return securityDeposite;
    }

    public void setSecurityDeposite(Integer securityDeposite) {
        this.securityDeposite = securityDeposite;
    }

    public Set<Rules> getRules() {
        return rules;
    }

    public void setRules(Set<Rules> rules) {
        this.rules = rules;
    }

    public Integer getBufferTime() {
        return bufferTime;
    }

    public void setBufferTime(Integer bufferTime) {
        this.bufferTime = bufferTime;
    }

    public Set<BlockAvailablity> getBlockAvailablityBlocks() {
        return blockAvailablityBlocks;
    }

    public void setBlockAvailablityBlocks(Set<BlockAvailablity> blockAvailablityBlocks) {
        this.blockAvailablityBlocks = blockAvailablityBlocks;
    }

    public Integer getNoticePeriod() {
        return noticePeriod;
    }

    public void setNoticePeriod(Integer noticePeriod) {
        this.noticePeriod = noticePeriod;
    }

    public MeasurementUnit getMeasurementUnitDto() {
        return measurementUnitDto;
    }

    public void setMeasurementUnitDto(MeasurementUnit measurementUnitDto) {
        this.measurementUnitDto = measurementUnitDto;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Integer getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(Integer measurementUnit) {
        this.measurementUnit = measurementUnit;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Set<PerHourAvailablity> getPerHourAvailablity() {
        return perHourAvailablity;
    }

    public void setPerHourAvailablity(Set<PerHourAvailablity> perHourAvailablity) {
        this.perHourAvailablity = perHourAvailablity;
    }

    public Integer getChargeType() {
        return chargeType;
    }

    public void setChargeType(Integer chargeType) {
        this.chargeType = chargeType;
    }

    public String getMenuFileName() {
        return menuFileName;
    }

    public void setMenuFileName(String menuFileName) {
        this.menuFileName = menuFileName;
    }

    public Set<MenuFiles> getMenuFiles() {
        return menuFiles;
    }

    public void setMenuFiles(Set<MenuFiles> menuFiles) {
        this.menuFiles = menuFiles;
    }

    public Integer getReimbursableOption() {
        return reimbursableOption;
    }

    public void setReimbursableOption(Integer reimbursableOption) {
        this.reimbursableOption = reimbursableOption;
    }

    public Integer getMinParticipantCount() {
        return minParticipantCount;
    }

    public void setMinParticipantCount(Integer minParticipantCount) {
        this.minParticipantCount = minParticipantCount;
    }

    public Integer getParent() {
        return parent;
    }

    public void setParent(Integer parent) {
        this.parent = parent;
    }

    public Integer getBlockChargeType() {
        return blockChargeType;
    }

    public void setBlockChargeType(Integer blockChargeType) {
        this.blockChargeType = blockChargeType;
    }

    public SpaceAdditionalDetails getSpaceAdditionalDetails() {
        return spaceAdditionalDetails;
    }

    public void setSpaceAdditionalDetails(SpaceAdditionalDetails spaceAdditionalDetails) {
        this.spaceAdditionalDetails = spaceAdditionalDetails;
    }

    public Integer getLinkSpace() {
        return linkSpace;
    }

    public void setLinkSpace(Integer linkSpace) {
        this.linkSpace = linkSpace;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public Set<SpaceType> getSpaceTypes() {
        return spaceTypes;
    }

    public void setSpaceTypes(Set<SpaceType> spaceTypes) {
        this.spaceTypes = spaceTypes;
    }

    public Set<SpacePromotion> getPromotions() {
        return promotions;
    }

    public void setPromotions(Set<SpacePromotion> promotions) {
        this.promotions = promotions;
    }

    public Set<SpaceContactPerson> getSpaceContactPersons() {
        return spaceContactPersons;
    }

    public void setSpaceContactPersons(Set<SpaceContactPerson> spaceContactPersons) {
        this.spaceContactPersons = spaceContactPersons;
    }

    public EventType getPrimaryEventType() {
        return primaryEventType;
    }

    public void setPrimaryEventType(EventType primaryEventType) {
        this.primaryEventType = primaryEventType;
    }

    @Override
    public SpaceDetailDto build() {
        SpaceDetailDto spaceDto = new SpaceDetailDto();

        spaceDto.setId(id);
        spaceDto.setName(name);
        spaceDto.setAddressLine1(addressLine1);
        spaceDto.setAddressLine2(addressLine2);
        spaceDto.setDescription(description);
        spaceDto.setCancellationPolicy(cancellationPolicy);
        spaceDto.setLatitude(latitude);
        spaceDto.setLongitude(longitude);
        spaceDto.setThumbnailImage(thumbnailImage);
        spaceDto.setSecurityDeposit(securityDeposite);
        spaceDto.setParticipantCount(participantCount);
        spaceDto.setMinParticipantCount(minParticipantCount);
        spaceDto.setRatePerHour(ratePerHour);
        spaceDto.setPreStaringFrom(ratePerHour);
        spaceDto.setSize(size);
        spaceDto.setUser(user);
        spaceDto.setSecurityDeposit(securityDeposite);
        spaceDto.setCalendarEventSize(calendarEventSize);
        spaceDto.setBufferTime(bufferTime);
        spaceDto.setNoticePeriod(noticePeriod);
        spaceDto.setMenuFileName(menuFileName);
        spaceDto.setParent(parent);
        spaceDto.setLinkSpace(linkSpace);
        spaceDto.setApproved(approved);
        try {
            DateFormat df = new SimpleDateFormat(Constants.PATTERN_2);
            if(calendarStart!=null)
                spaceDto.setCalendarStart(df.format(calendarStart));
            if (calendarEnd!=null)
                spaceDto.setCalendarEnd(df.format(calendarEnd));
        } catch (Exception e) {
            LOGGER.error("build exception----->{}",e);
        }

        List<Integer> eventTypeList = new ArrayList<>();
        for (EventType eventType : eventTypes) {
            eventTypeList.add(eventType.getId());
        }
        spaceDto.setEventType(eventTypeList.toArray(new Integer[0]));

        List<Integer> amenityIdList = new ArrayList<>();
        for (Amenity amenity : amenities) {
            amenityIdList.add(amenity.getId());
        }
        spaceDto.setAmenity(amenityIdList.toArray(new Integer[0]));

        List<Integer> rulesIdList = new ArrayList<>();
        for (Rules rule : rules) {
            rulesIdList.add(rule.getId());
        }
        spaceDto.setRules(rulesIdList.toArray(new Integer[0]));

        Set<ExtraAmenityDto> extraAmenityDtoSet = new HashSet<>(0);
        for (SpaceExtraAmenity spaceExtraAmenity : extraAmenities) {
            ExtraAmenityDto extraAmenityDto = spaceExtraAmenity.build();
            extraAmenityDtoSet.add(extraAmenityDto);
        }
        spaceDto.setExtraAmenity(extraAmenityDtoSet);

        Set<SeatingArrangementDto> seatingArrangementDtoSet = new HashSet<>(0);
        for (SpaceSeatingArrangement spaceSeatingArrangement : spaceSeatingArrangements) {
            SeatingArrangementDto seatingArrangementDto = spaceSeatingArrangement.build();
            seatingArrangementDtoSet.add(seatingArrangementDto);
        }
        spaceDto.setSeatingArrangements(seatingArrangementDtoSet);

        //Set<Image> images = getImages();
        Set<String> imageDto = new HashSet<>(0);
        for (Image image : images) {
            String imgurl = image.getUrl();
            imageDto.add(imgurl);
        }
        spaceDto.setImages(imageDto);

        Set<MenuFileDto> menuFileDtos=new HashSet<>();
        for(MenuFiles menuFile:menuFiles){
            MenuFileDto menuFileDto=menuFile.build();
            menuFileDtos.add(menuFileDto);
        }
        spaceDto.setMenuFiles(menuFileDtos);

        Set<CommonDto> spaceTypesDto=new HashSet<>();
        for (SpaceType spaceType:spaceTypes){
            spaceTypesDto.add(spaceType.build());
        }
        spaceDto.setSpaceType(spaceTypesDto);

        spaceDto.setReimbursableOption(reimbursableOption);

        setAvailabilityDtoDetails(spaceDto);
        spaceDto.setAvailabilityMethod(AvailabilityMethods.getInstanceFromValue(chargeType).toString());

        CommonDto measurementType=new CommonDto();
        measurementType.setId(measurementUnitDto.getId());
        measurementType.setName(measurementUnitDto.getUnitType());
        spaceDto.setMeasurementUnit(measurementType);

        spaceDto.setCreatedAt(createdAt);
        spaceDto.setUpdatedAt(updatedAt);

        if (spaceAdditionalDetails!=null) {
            spaceDto.setContactPersonName(spaceAdditionalDetails.getContactPerson());
            spaceDto.setMobileNumber(spaceAdditionalDetails.getMobilePhone());
            spaceDto.setCompanyPhone(spaceAdditionalDetails.getCompanyPhone());
            spaceDto.setCommissionPercentage(spaceAdditionalDetails.getCommissionPercentage());
            spaceDto.setCompanyName(spaceAdditionalDetails.getCompanyName());
            spaceDto.setAccountHolderName(spaceAdditionalDetails.getAccountHolderName());
            spaceDto.setAccountNumber(spaceAdditionalDetails.getAccountNumber());
            spaceDto.setBank(spaceAdditionalDetails.getBank());
            spaceDto.setBankBranch(spaceAdditionalDetails.getBankBranch());
            spaceDto.setHostLogo(spaceAdditionalDetails.getHostLogo());
            spaceDto.setAdvanceOnlyEnable(spaceAdditionalDetails.getAdvanceOnlyEnable());
        }

        if(spaceContactPersons!=null && !spaceContactPersons.isEmpty()){
            SpaceContactPerson spaceContactPerson=spaceContactPersons.iterator().next();
            spaceDto.setEmail2(spaceContactPerson.getContactPersonEmail());
            spaceDto.setMobileNumber2(spaceContactPerson.getContactPersonMobile());
            spaceDto.setContactPersonName2(spaceContactPerson.getContactPersonName());
        }
        return spaceDto;
    }

    public BookingSpaceDto buildBookingSpaceDetails(){
        BookingSpaceDto spaceDto=new BookingSpaceDto();
        spaceDto.setId(id);
        spaceDto.setName(name);
        spaceDto.setAddressLine1(addressLine1);
        spaceDto.setOrganizationName(addressLine2);
        spaceDto.setMinParticipantCount(minParticipantCount);
        spaceDto.setParticipantCount(participantCount);
        spaceDto.setAdvanceOnlyEnable(Optional.ofNullable(spaceAdditionalDetails.getAdvanceOnlyEnable()).orElse(0));
        spaceDto.setChargeType(chargeType);
        spaceDto.setBlockChargeType(blockChargeType);
        spaceDto.setAvailabilityMethod(AvailabilityMethods.getInstanceFromValue(chargeType).toString());
        spaceDto.setAmenity(amenities.stream().map(Amenity::build).collect(Collectors.toList()));
        spaceDto.setEventType(eventTypes.stream().map(EventType::build).collect(Collectors.toList()));
        spaceDto.setSeatingArrangements(spaceSeatingArrangements.stream().map(SpaceSeatingArrangement::build).collect(Collectors.toList()));
        spaceDto.setExtraAmenity(extraAmenities.stream().map(SpaceExtraAmenity::build).collect(Collectors.toList()));

        SpaceDetailDto spaceDetailDto=new SpaceDetailDto();
        setAvailabilityDtoDetails(spaceDetailDto);

        spaceDto.setAvailability(spaceDetailDto.getAvailability());

        return spaceDto;
    }

    public AdminSpaceDetailsDto buildAdminSpaceDto(){
        AdminSpaceDetailsDto spaceDto = new AdminSpaceDetailsDto();
        spaceDto.setId(id);
        spaceDto.setName(name);
        spaceDto.setAddress(addressLine1);
        spaceDto.setOrganizationName(addressLine2);
        spaceDto.setApproved(approved);
        return spaceDto;
    }

    public AdminSpaceDetailsDto buildSpaceSearchData(){
        AdminSpaceDetailsDto spaceDto = new AdminSpaceDetailsDto();
        spaceDto.setId(id);
        spaceDto.setName(name);
        spaceDto.setOrganizationName(addressLine2);
        return spaceDto;
    }

    public AdminSpaceDetailsDto buildChildSpaceDto(){
        AdminSpaceDetailsDto spaceDto = new AdminSpaceDetailsDto();
        spaceDto.setId(id);
        spaceDto.setName(name);
        return spaceDto;
    }

    public SpaceDetailDto buildSmilarSpaceDto() {
        SpaceDetailDto spaceDto = new SpaceDetailDto();
        spaceDto.setId(id);
        spaceDto.setName(name);
        spaceDto.setAddressLine1(addressLine1);
        spaceDto.setAddressLine2(addressLine2);
        spaceDto.setThumbnailImage(thumbnailImage);
        spaceDto.setRatePerHour(ratePerHour);
        spaceDto.setParticipantCount(participantCount);

        return spaceDto;
    }

    public SpaceDetailDto buildCalendarDetailsDto(){
        SpaceDetailDto spaceDto=new SpaceDetailDto();

        setAvailabilityDtoDetails(spaceDto);

        spaceDto.setAvailabilityMethod(AvailabilityMethods.getInstanceFromValue(chargeType).toString());
        spaceDto.setName(name);
        spaceDto.setAddressLine1(addressLine1);
        spaceDto.setAddressLine2(addressLine2);
        spaceDto.setBufferTime(bufferTime);
        spaceDto.setNoticePeriod(noticePeriod);
        spaceDto.setMinParticipantCount(minParticipantCount);
        spaceDto.setReimbursableOption(reimbursableOption);

        Set<ExtraAmenityDto> extraAmenityDtoSet = new HashSet<>(0);
        for (SpaceExtraAmenity spaceExtraAmenity : extraAmenities) {
            ExtraAmenityDto extraAmenityDto = spaceExtraAmenity.build();
            extraAmenityDtoSet.add(extraAmenityDto);
        }
        spaceDto.setExtraAmenity(extraAmenityDtoSet);

        try {
            DateFormat df = new SimpleDateFormat(Constants.PATTERN_2);
            if(calendarStart!=null)
                spaceDto.setCalendarStart(df.format(createdAt));
            if (calendarEnd!=null)
                spaceDto.setCalendarEnd(df.format(calendarEnd));
        } catch (Exception e) {
            LOGGER.error("build exception----->{}",e);
        }

        return spaceDto;
    }


    private void setAvailabilityDtoDetails(SpaceDetailDto spaceDto){
        Set<AvailablityDetailsDto> availablityDetailsDtos = new HashSet<>();

        if (chargeType.equals(ChargeTypeEnum.BLOCK_BASE.value()) && !blockAvailablityBlocks.isEmpty()) {
            spaceDto.setBlockChargeType(blockChargeType);
            if (blockChargeType!=null && blockChargeType.equals(BlockChargeTypeEnum.PER_QUEST_BASED_CHARGE.value()))
                spaceDto.setMinParticipantCount(minParticipantCount);
            for (BlockAvailablity availablityBlock: blockAvailablityBlocks){
                AvailablityDetailsDto availablityDetailsDto =availablityBlock.build();
                if (availablityBlock.getActive().equals(BooleanEnum.TRUE.value()))
                    availablityDetailsDtos.add(availablityDetailsDto);
            }
        }


        if (chargeType.equals(ChargeTypeEnum.HOUR_BASE.value()) && !perHourAvailablity.isEmpty()){
            for(PerHourAvailablity availablity:perHourAvailablity){
                AvailablityDetailsDto availablityDetailsDto =availablity.build();
                if (availablity.getActive().equals(BooleanEnum.TRUE.value()))
                    availablityDetailsDtos.add(availablityDetailsDto);
            }
        }

        spaceDto.setAvailability(availablityDetailsDtos);

    }
    /**
     * Builds the.
     *
     * @param spaceDetailDto the space detail dto
     * @param space          the space
     * @return the space
     */
    public static Space build(SpaceDetailDto spaceDetailDto, Space space, Integer userId) {

        space.setName(spaceDetailDto.getName());
        space.setAddressLine1(spaceDetailDto.getAddressLine1());
        space.setAddressLine2(spaceDetailDto.getAddressLine2());
        space.setCancellationPolicy(spaceDetailDto.getCancellationPolicy());
        space.setDescription(spaceDetailDto.getDescription());
        space.setSize(spaceDetailDto.getSize());
        space.setParticipantCount(spaceDetailDto.getParticipantCount());
        space.setUser(userId);
        space.setLongitude(spaceDetailDto.getLongitude());
        space.setLatitude(spaceDetailDto.getLatitude());
        space.setThumbnailImage(spaceDetailDto.getThumbnailImage());
        space.setSecurityDeposite(spaceDetailDto.getSecurityDeposit());
        space.setNoticePeriod(spaceDetailDto.getNoticePeriod());
        space.setBufferTime(spaceDetailDto.getBufferTime());
        space.setCalendarEventSize(spaceDetailDto.getCalendarEventSize());
        space.setMeasurementUnit(spaceDetailDto.getMeasurementUnit().getId());
        space.setMenuFileName(spaceDetailDto.getMenuFileName());
        space.setChargeType(AvailabilityMethods.valueOf(spaceDetailDto.getAvailabilityMethod()).value());
        space.setBlockChargeType(spaceDetailDto.getBlockChargeType());
        space.setMinParticipantCount(spaceDetailDto.getMinParticipantCount());
        space.setParent(spaceDetailDto.getParent());
        space.setLinkSpace(spaceDetailDto.getLinkSpace());

        space.setUpdatedAt(new Date());
        if (spaceDetailDto.getReimbursableOption()!=null)
            space.setReimbursableOption(spaceDetailDto.getReimbursableOption());

        try {
            DateFormat df = new SimpleDateFormat(Constants.PATTERN_2);
            space.setCalendarStart(df.parse(spaceDetailDto.getCalendarStart()));
            space.setCalendarEnd(df.parse(spaceDetailDto.getCalendarEnd()));
        } catch (Exception e) {
            LOGGER.error("build exception----->{}",e);
        }
        return space;
    }
}
