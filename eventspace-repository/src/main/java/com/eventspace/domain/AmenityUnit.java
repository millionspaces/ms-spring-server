/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.domain;

import com.eventspace.dto.AmenityUnitsDto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;


/**
 * The Class AmenitiesUnit.
 */
@Entity
@Table(name = "amenity_unit")
public class AmenityUnit implements Serializable, BaseDomain {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;


	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	/** The name. */
	@Column(name = "name", nullable = false)
	private String name;

	@OneToMany(mappedBy = "amenityUnit")
	private Set<Amenity> amenitySet;


	public Set<Amenity> getAmenitySet() {
		return amenitySet;
	}

	public void setAmenitySet(Set<Amenity> amenitySet) {
		this.amenitySet = amenitySet;
	}

	/**
	 * The Constructor.
	 *
	 * @param id
	 *            the id
	 */
	public AmenityUnit(int id) {
		this.id = id;
	}

	/**
	 * The Constructor.
	 */
	public AmenityUnit() {
	}


	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	
	/* (non-Javadoc)
	 * @see com.eventspace.domain.BaseDomain#build(java.lang.Integer)
	 */
	@Override
	public AmenityUnitsDto build() {
		AmenityUnitsDto amenityUnitsDto = new AmenityUnitsDto();
		amenityUnitsDto.setName(name);
		amenityUnitsDto.setId(id);
		return amenityUnitsDto;
	}

}
