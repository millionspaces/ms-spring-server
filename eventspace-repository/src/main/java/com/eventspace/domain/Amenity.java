package com.eventspace.domain;

/**
 * Created by Aux-052 on 12/14/2016.
 */

import com.eventspace.dto.AmenityDto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * The Class Space.
 */
@Entity
@Table(name = "amenity")
public class Amenity  implements Serializable, BaseDomain {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /** The name. */
    @Column(name = "name", nullable = false)
    private String name;

    /** The amenity unit. */
    @Column(name="amenity_unit", nullable = false)
    private Integer amenity_unit;

    /** The amenity unit. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="amenity_unit", insertable =false,updatable = false)
    private AmenityUnit amenityUnit;
    
    /** The icon. */
    @Column(name="icon")
    private String icon;

    @Column(name="mobile_icon")
    private String mobileIcon;
    /** The date updated.
     * */
    @Column(name="date_updated", nullable= false)
    private Date updatedDate;

    /** The extra amenities. */
    @OneToMany(mappedBy = "pk.amenity",fetch = FetchType.LAZY)
    private Set<SpaceExtraAmenity> extraAmenities;

    /** The extra amenity set. */
    @OneToMany(mappedBy = "pk1.amenity",fetch = FetchType.LAZY)
    private Set<BookingSpaceExtraAmenity> extraAmenitySet;



    /**
     * Gets the amenity unit.
     *
     * @return the amenity unit
     */
    public AmenityUnit getAmenityUnit() {
        return amenityUnit;
    }

    /**
     * Sets the amenity unit.
     *
     * @param amenityUnit the new amenity unit
     */
    public void setAmenityUnit(AmenityUnit amenityUnit) {
        this.amenityUnit = amenityUnit;
    }

    /**
     * The Constructor.
     */
    public Amenity() {
    }

    /**
     * The Constructor.
     *
     * @param id
     *            the id
     */
    public Amenity(int id) {
        this.id=id;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the amenity_unit.
     *
     * @return the getAmenity_unit
     */
    public Integer getAmenity_unit() {
        return amenity_unit;
    }

    /**
     * Sets the amenity_unit.
     *
     * @param amenity_unit the new amenity_unit
     */
    public void setAmenity_unit(Integer amenity_unit) {
        this.amenity_unit = amenity_unit;
    }

    /**
     * Gets the icon.
     *
     * @return the icon
     */
    public String getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the new icon
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getMobileIcon() {
        return mobileIcon;
    }

    public void setMobileIcon(String mobileIcon) {
        this.mobileIcon = mobileIcon;
    }

    /* (non-Javadoc)
                     * @see com.eventspace.domain.BaseDomain#build(java.lang.Integer)
                     */
    @Override
    public AmenityDto build() {
        AmenityDto amenityDto = new AmenityDto();
        amenityDto.setId(id);
        amenityDto.setName(name);
        amenityDto.setAmenityUnit(amenity_unit);
        amenityDto.setIcon(icon);
        amenityDto.setMobileIcon(mobileIcon);
       amenityDto.setAmenityUnitsDto(amenityUnit.build());
       amenityDto.setTimeStamp(String.valueOf(updatedDate.getTime()));
        return amenityDto;
    }

    /**
     * Builds the.
     *
     * @param amenityDto the amenity dto
     * @return the amenity
     */
    public static Amenity build(AmenityDto amenityDto) {
        Amenity amenity=new Amenity();
        amenity.setId(amenityDto.getId());
        amenity.setName(amenityDto.getName());
        amenity.setAmenity_unit(amenityDto.getAmenityUnit());
        amenity.setIcon(amenityDto.getIcon());
        amenity.setMobileIcon(amenityDto.getMobileIcon());
        return amenity;
    }

    public AmenityDto buildneeded() {
        AmenityDto amenityDto = new AmenityDto();
        amenityDto.setId(id);
        amenityDto.setName(name);
        amenityDto.setIcon(icon);
        amenityDto.setMobileIcon(mobileIcon);
        return amenityDto;
    }

}
