package com.eventspace.domain;

import com.eventspace.dto.CommonDto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "block_charge_type")
public class BlockChargeType implements Serializable, BaseDomain  {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /** The name. */
    @Column(name = "name", nullable = false)
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public CommonDto build() {
        CommonDto chargeType=new CommonDto();
        chargeType.setId(id);
        chargeType.setName(name);

        return  chargeType;
    }
}
