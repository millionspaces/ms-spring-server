package com.eventspace.domain;

import com.eventspace.dto.CommonDto;
import com.eventspace.dto.SpaceUnavailabilityDto;
import com.eventspace.enumeration.BooleanEnum;
import com.eventspace.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Auxenta on 5/22/2017.
 */
/**
 * The Class SpaceUnavailability.
 */
@Entity
@Table(name = "space_unavailablity")
public class SpaceUnavailability implements Serializable,BaseDomain  {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SpaceUnavailability.class);

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /** The space. */
    @Column(name = "space", nullable = false)
    private Integer space;

    /**
     * The from date.
     */
    @Column(name = "from_date", nullable = false)
    private Date fromDate;

    /**
     * The to date.
     */
    @Column(name = "to_date", nullable = false)
    private Date toDate;

    /**
     * The to date.
     */
    @Column(name = "is_blocked", nullable = false)
    private Integer isBlocked;
        
    @Column(name = "title")
    private String title;
    
    @Column(name = "guest_contact_number")
    private String guestContactNumber;
        
    @Column(name = "guest_name")
    private String guestName;
        
    @Column(name = "date_booking_made")
    private String dateBookingMade;
        
    @Column(name = "guest_email")
    private String guestEmail;
        
    @Column(name = "no_of_guests")
    private String noOfGuests;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_type_id")
    private EventType eventType;
        
    @Column(name = "extras_requested")
    private String extrasRequested;
            
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "seating_arrangement_id")
    private SeatingArrangement seatingArrangement;
         
    @Column(name = "cost")
    private String cost;
    
    @Column(name = "note")
    private String note;

    @Column(name = "is_manual")
    private Integer isManual;

    @Column(name="remarks")
    private String remarks;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reservation_status")
    private ReservationStatus reservationStatus;

    @Column(name="booking_id")
    private Integer bookingId;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSpace() {
        return space;
    }

    public void setSpace(Integer space) {
        this.space = space;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Integer getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(Integer isBlocked) {
        this.isBlocked = isBlocked;
    }
    
    public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	


	public String getGuestContactNumber() {
		return guestContactNumber;
	}

	public void setGuestContactNumber(String guestContactNumber) {
		this.guestContactNumber = guestContactNumber;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public String getDateBookingMade() {
		return dateBookingMade;
	}

	public void setDateBookingMade(String dateBookingMade) {
		this.dateBookingMade = dateBookingMade;
	}

	public String getGuestEmail() {
		return guestEmail;
	}

	public void setGuestEmail(String guestEmail) {
		this.guestEmail = guestEmail;
	}

	public String getNoOfGuests() {
		return noOfGuests;
	}

	public void setNoOfGuests(String noOfGuests) {
		this.noOfGuests = noOfGuests;
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public String getExtrasRequested() {
		return extrasRequested;
	}

	public void setExtrasRequested(String extrasRequested) {
		this.extrasRequested = extrasRequested;
	}

	public SeatingArrangement getSeatingArrangement() {
		return seatingArrangement;
	}

	public void setSeatingArrangement(SeatingArrangement seatingArrangement) {
		this.seatingArrangement = seatingArrangement;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

    public Integer getIsManual() {
        return isManual;
    }

    public void setIsManual(Integer isManual) {
        this.isManual = isManual;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public ReservationStatus getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(ReservationStatus reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    @Override
    public SpaceUnavailabilityDto build() {
        SpaceUnavailabilityDto spaceUnavailabilityDto =new SpaceUnavailabilityDto();
        spaceUnavailabilityDto.setId(id);
        spaceUnavailabilityDto.setSpace(space);
        spaceUnavailabilityDto.setFromDate(fromDate.toString());
        spaceUnavailabilityDto.setToDate(toDate.toString());
        spaceUnavailabilityDto.setIsBlocked(isBlocked);
        spaceUnavailabilityDto.setCost(cost);
        spaceUnavailabilityDto.setDateBookingMade(dateBookingMade);
        spaceUnavailabilityDto.setEventTitle(title);
        spaceUnavailabilityDto.setRemark(remarks);
        if (eventType!=null) {
            spaceUnavailabilityDto.setEventTypeId(eventType.getId());
            spaceUnavailabilityDto.setEventTypeName(eventType.getName());
        }
        spaceUnavailabilityDto.setExtrasRequested(extrasRequested);
        spaceUnavailabilityDto.setGuestContactNumber(guestContactNumber);
        spaceUnavailabilityDto.setGuestEmail(guestEmail);
        spaceUnavailabilityDto.setGuestName(guestName);
        spaceUnavailabilityDto.setNoOfGuests(noOfGuests);
        spaceUnavailabilityDto.setNote(note);
        if (seatingArrangement!=null) {
            spaceUnavailabilityDto.setSeatingArrangementIcon(seatingArrangement.getIcon());
            spaceUnavailabilityDto.setSeatingArrangementId(seatingArrangement.getId() + "");
            spaceUnavailabilityDto.setSeatingArrangementName(seatingArrangement.getName());
        }
        if (reservationStatus!=null){
            CommonDto commonDto=new CommonDto();
            commonDto.setId(reservationStatus.getId());
            commonDto.setName(reservationStatus.getName());
            commonDto.setIcon(reservationStatus.getLabel());
            spaceUnavailabilityDto.setReservationStatus(commonDto);
        }
        spaceUnavailabilityDto.setIsManual(isManual);
        return spaceUnavailabilityDto;
    }

    public static SpaceUnavailability build(SpaceUnavailabilityDto spaceUnavailabilityDto) {

        SpaceUnavailability spaceUnavailablity =new SpaceUnavailability();
        spaceUnavailablity.setSpace(spaceUnavailabilityDto.getSpace());
        spaceUnavailablity.setIsBlocked(BooleanEnum.TRUE.value());
        spaceUnavailablity.setCost(spaceUnavailabilityDto.getCost());
        spaceUnavailablity.setDateBookingMade(spaceUnavailabilityDto.getDateBookingMade());
        spaceUnavailablity.setExtrasRequested(spaceUnavailabilityDto.getExtrasRequested());
        spaceUnavailablity.setGuestContactNumber(spaceUnavailabilityDto.getGuestContactNumber());
        spaceUnavailablity.setGuestEmail(spaceUnavailabilityDto.getGuestEmail());
        spaceUnavailablity.setGuestName(spaceUnavailabilityDto.getGuestName());
        spaceUnavailablity.setNoOfGuests(spaceUnavailabilityDto.getNoOfGuests());
        spaceUnavailablity.setNote(spaceUnavailabilityDto.getNote());
        spaceUnavailablity.setTitle(spaceUnavailabilityDto.getEventTitle());
        spaceUnavailablity.setIsManual(spaceUnavailabilityDto.getIsManual());

        if (spaceUnavailabilityDto.getRemark()!=null)
            spaceUnavailablity.setRemarks(spaceUnavailabilityDto.getRemark());
        
        if(spaceUnavailabilityDto.getEventTypeId() != null){
        	EventType eventType = new EventType();
            eventType.setId(spaceUnavailabilityDto.getEventTypeId());
            spaceUnavailablity.setEventType(eventType);
        }
        
        if(spaceUnavailabilityDto.getSeatingArrangementId()!= null){
        	SeatingArrangement seatingArrangement = new SeatingArrangement();
        	seatingArrangement.setId(Integer.parseInt(spaceUnavailabilityDto.getSeatingArrangementId()));
        	 spaceUnavailablity.setSeatingArrangement(seatingArrangement);
        }

        if (spaceUnavailabilityDto.getReservationStatus()!=null){
            ReservationStatus reservationStatus=new ReservationStatus();
            reservationStatus.setId(spaceUnavailabilityDto.getReservationStatus().getId());
            spaceUnavailablity.setReservationStatus(reservationStatus);
        }

        try {
            DateFormat df = new SimpleDateFormat(Constants.PATTERN_1);
           spaceUnavailablity.setFromDate(df.parse(spaceUnavailabilityDto.getFromDate()));
           spaceUnavailablity.setToDate(df.parse(spaceUnavailabilityDto.getToDate()));
        } catch (ParseException e) {
            LOGGER.error("build exception----->{}",e);
        }

        return spaceUnavailablity;
    }
}
