package com.eventspace.domain;

import com.eventspace.dto.AvailablityDetailsDto;
import com.eventspace.dto.MenuFileDto;
import com.eventspace.enumeration.BooleanEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Auxenta on 6/28/2017.
 */
@Entity
@Table(name = "block_availability")
public class BlockAvailablity implements Serializable, BaseDomain{

    /**
     * The id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "block_availability_has_menu_files", joinColumns = {
            @JoinColumn(name = "block_availability", nullable = false, updatable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "menu_files", nullable = false, updatable = false)})
    private Set<MenuFiles> menuFiles;

    /**
     * The space.
     */
    @ManyToOne(fetch=FetchType.LAZY) @JoinColumn(name="space", nullable = false)
    private Space space;

    /**
     * The from date.
     */
    @Column(name = "from_time")
    private  Time from;

    /**
     * The to date.
     */
    @Column(name = "to_time")
    private  Time to;

    /** The refundAmount. */
    @Column(name = "charge")
    private Double charge;

    /** The refundAmount. */
    @Column(name = "menu_items")
    private String menuItems;

    /** The refundAmount. */
    @Column(name = "day")
    private Integer day;

    @Column(name = "is_reimbursable")
    private Integer isReimbursable;

    /** The notificationSeen. */
    @Column(name = "active", nullable = false)
    private Integer active;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Space getSpace() {
        return space;
    }

    public void setSpace(Space space) {
        this.space = space;
    }

    public Time getFrom() {
        return from;
    }

    public void setFrom(Time from) {
        this.from = from;
    }

    public Time getTo() {
        return to;
    }

    public void setTo(Time to) {
        this.to = to;
    }

    public Double getCharge() {
        return charge;
    }

    public void setCharge(Double charge) {
        this.charge = charge;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public String getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(String menuItems) {
        this.menuItems = menuItems;
    }

    public Set<MenuFiles> getMenuFiles() {
        return menuFiles;
    }

    public void setMenuFiles(Set<MenuFiles> menuFiles) {
        this.menuFiles = menuFiles;
    }

    public Integer getIsReimbursable() {
        return isReimbursable;
    }

    public void setIsReimbursable(Integer isReimbursable) {
        this.isReimbursable = isReimbursable;
    }

    @Override
    public AvailablityDetailsDto build() {
        AvailablityDetailsDto availablityDetailsDto =new AvailablityDetailsDto();
        availablityDetailsDto.setId(id);
        availablityDetailsDto.setSpace(space.getId());
        availablityDetailsDto.setMenuItems(menuItems);
        availablityDetailsDto.setFrom(from.toString());
        availablityDetailsDto.setTo(to.toString());
        availablityDetailsDto.setCharge(charge);
        availablityDetailsDto.setDay(day);
        availablityDetailsDto.setIsReimbursable(isReimbursable);

        Set<MenuFileDto> menuFileDtos=new HashSet<>();
        for(MenuFiles menuFile:menuFiles){
            MenuFileDto menuFileDto=menuFile.build();
            menuFileDtos.add(menuFileDto);
        }
        availablityDetailsDto.setMenuFiles(menuFileDtos);
        availablityDetailsDto.setIsAvailable(active);

        return availablityDetailsDto;
    }

    public static BlockAvailablity build(Space space , AvailablityDetailsDto availablityDetailsDto){
        BlockAvailablity blockAvailablity =new BlockAvailablity();
        blockAvailablity.setSpace(space);
        blockAvailablity.setMenuItems(availablityDetailsDto.getMenuItems());
        blockAvailablity.setFrom(Time.valueOf(availablityDetailsDto.getFrom()));
        blockAvailablity.setTo(Time.valueOf(availablityDetailsDto.getTo()));
        blockAvailablity.setCharge(availablityDetailsDto.getCharge());
        blockAvailablity.setDay(availablityDetailsDto.getDay());
        blockAvailablity.setIsReimbursable(availablityDetailsDto.getIsReimbursable());
        if (availablityDetailsDto.getIsAvailable()!=null)
            blockAvailablity.setActive(availablityDetailsDto.getIsAvailable());
        else
            blockAvailablity.setActive(BooleanEnum.TRUE.value());
        return blockAvailablity;
    }
}
