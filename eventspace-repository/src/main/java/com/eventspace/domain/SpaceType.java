package com.eventspace.domain;

import com.eventspace.dto.CommonDto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "space_type")
public class SpaceType implements Serializable, BaseDomain {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /** The name. */
    @Column(name = "name", nullable = false)
    private String name;

    /** The icon. */
    @Column(name="icon")
    private String icon;

    @Column(name="mobile_icon")
    private String mobileIcon;
    /** The date updated.
     * */
    @Column(name="date_updated", nullable= false)
    private Date updatedDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMobileIcon() {
        return mobileIcon;
    }

    public void setMobileIcon(String mobileIcon) {
        this.mobileIcon = mobileIcon;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public CommonDto build() {
        CommonDto spaceType=new CommonDto();
        spaceType.setId(id);
        spaceType.setName(name);
        spaceType.setIcon(icon);
        spaceType.setMobileIcon(mobileIcon);
        return spaceType;
    }
}
