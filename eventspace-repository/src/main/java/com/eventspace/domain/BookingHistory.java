package com.eventspace.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Auxenta on 3/15/2017.
 */
@Entity
@Table(name = "booking_history")
public class BookingHistory implements Serializable, BaseDomain{

    /** The booking. */
    @Id
    @Column(name = "booking", nullable = false)
    private Integer bookingId;

    /** The reservationStatus. */
    @Id
    @Column(name = "reservation_status", nullable = false)
    private Integer reservationStatus;

    /** The refundAmount. */
    @Column(name = "refund_amount")
    private Double refundAmount;

    /** The actionTaker. */
    @Column(name = "action_taker", nullable = false)
    private Integer actionTaker;

    /** The createdAt. */
    @Column(name = "created_at", nullable = false)
    private Date createdAt;

    /** The notificationSeen. */
    @Column(name = "seen", nullable = false)
    private Integer notificationSeen;

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(Integer reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public Double getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(Double refundAmount) {
        this.refundAmount = refundAmount;
    }

    public Integer getActionTaker() {
        return actionTaker;
    }

    public void setActionTaker(Integer actionTaker) {
        this.actionTaker = actionTaker;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getNotificationSeen() {
        return notificationSeen;
    }

    public void setNotificationSeen(Integer notificationSeen) {
        this.notificationSeen = notificationSeen;
    }

    @Override
    public Object build() {
        return null;
    }
}
