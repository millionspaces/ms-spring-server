package com.eventspace.domain;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Aux-052 on 12/14/2016.
 */
@Embeddable
public class SpaceAmenityPK implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    //@JoinColumn(name = "space", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Space space;

    //@JoinColumn(name = "amenity", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Amenity amenity;

    public SpaceAmenityPK(Space space, Amenity amenity) {
        this.space = space;
        this.amenity = amenity;
    }

    public SpaceAmenityPK(){}

    public Space getSpace() {
        return space;
    }

    public Amenity getAmenity() {
        return amenity;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((space == null) ? 0 : space.hashCode());
        result = prime * result + ((amenity == null) ? 0 : amenity.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SpaceAmenityPK other = (SpaceAmenityPK) obj;
        if (space == null) {
            if (other.space != null)
                return false;
        } else if (!space.equals(other.space))
            return false;
        if (amenity == null) {
            if (other.amenity != null)
                return false;
        } else if (!amenity.equals(other.amenity))
            return false;
        return true;
    }


}
