package com.eventspace.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "space_additional_details")
public class SpaceAdditionalDetails implements Serializable, BaseDomain{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @Column(name = "space", nullable = false, unique = true)
    private Integer space;

    /** The name. */
    @Column(name = "contact_person_name")
    private String contactPerson;

    @Column(name = "mobile_phone",nullable = false)
    private String mobilePhone;

    /** The name. */
    @Column(name = "company_name")
    private String companyName;

    @Column(name = "company_phone")
    private String companyPhone;

    @Column(name = "host_logo")
    private String hostLogo;

    /** The name. */
    @Column(name = "account_holder_name")
    private String accountHolderName;

    /** The name. */
    @Column(name = "account_number")
    private String accountNumber;

    /** The name. */
    @Column(name = "bank")
    private String bank;

    /** The name. */
    @Column(name = "bank_branch")
    private String bankBranch;

    /** The name. */
    @Column(name = "commission_percentage")
    private Integer commissionPercentage;

    @Column(name="advance_only_enable")
    private Integer advanceOnlyEnable;

    @Column(name="discount")
    private Double discount;

    @Column(name="allowed_bookings")
    private Integer allowedBookings;

    public Integer getSpace() {
        return space;
    }

    public void setSpace(Integer space) {
        this.space = space;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getHostLogo() {
        return hostLogo;
    }

    public void setHostLogo(String hostLogo) {
        this.hostLogo = hostLogo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public Integer getCommissionPercentage() {
        return commissionPercentage;
    }

    public void setCommissionPercentage(Integer commissionPercentage) {
        this.commissionPercentage = commissionPercentage;
    }

    public Integer getAdvanceOnlyEnable() {
        return advanceOnlyEnable;
    }

    public void setAdvanceOnlyEnable(Integer advanceOnlyEnable) {
        this.advanceOnlyEnable = advanceOnlyEnable;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Integer getAllowedBookings() {
        return allowedBookings;
    }

    public void setAllowedBookings(Integer allowedBookings) {
        this.allowedBookings = allowedBookings;
    }

    @Override
    public Object build() {
        return null;
    }
}
