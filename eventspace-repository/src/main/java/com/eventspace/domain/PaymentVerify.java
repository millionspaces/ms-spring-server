package com.eventspace.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "payment_verify")
public class PaymentVerify implements Serializable, BaseDomain{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "booking_id", nullable = false, unique = true)
    private Integer bookingId;

    /**
     * The name.
     */
    @Column(name = "verified")
    private Integer verified;

    @Column(name = "pdf_url", nullable = true)
    private String pdf;

    @Column(name = "guest_sent_proof", nullable = true)
    private String guestSentProof;

    @Column(name = "host_received_proof", nullable = true)
    private String hostReceivedProof;

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getVerified() {
        return verified;
    }

    public void setVerified(Integer verified) {
        this.verified = verified;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getGuestSentProof() {
        return guestSentProof;
    }

    public void setGuestSentProof(String guestSentProof) {
        this.guestSentProof = guestSentProof;
    }

    public String getHostReceivedProof() {
        return hostReceivedProof;
    }

    public void setHostReceivedProof(String hostReceivedProof) {
        this.hostReceivedProof = hostReceivedProof;
    }

    @Override
    public Object build() {
        return null;
    }
}
