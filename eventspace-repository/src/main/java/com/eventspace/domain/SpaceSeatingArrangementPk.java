package com.eventspace.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Auxenta on 6/16/2017.
 */
@Embeddable
public class SpaceSeatingArrangementPk implements Serializable {

    private static final long serialVersionUID = 1L;

    @JoinColumn(name = "space", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Space space;

    @JoinColumn(name = "seating_arrangement", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY,cascade =CascadeType.ALL)
    private SeatingArrangement seatingArrangement;



    public SpaceSeatingArrangementPk() {}

    public SpaceSeatingArrangementPk(Space space, SeatingArrangement seatingArrangement) {
        this.space = space;
        this.seatingArrangement = seatingArrangement;
    }

    public Space getSpace() {
        return space;
    }

    public SeatingArrangement getSeatingArrangement() {
        return seatingArrangement;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((space == null) ? 0 : space.hashCode());
        result = prime * result + ((seatingArrangement == null) ? 0 : seatingArrangement.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SpaceSeatingArrangementPk other = (SpaceSeatingArrangementPk) obj;
        if (space == null) {
            if (other.getSpace() != null)
                return false;
        } else if (!space.equals(other.space))
            return false;
        if (seatingArrangement == null) {
            if (other.seatingArrangement != null)
                return false;
        } else if (!seatingArrangement.equals(other.seatingArrangement))
            return false;
        return true;
    }

}
