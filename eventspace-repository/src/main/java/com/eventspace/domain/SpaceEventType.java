/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Aux-052 on 12/14/2016.
 */
/**
 * The Class SpaceAmenity.
 */
@Entity
@Table(name = "space_has_event_type")
public class SpaceEventType implements Serializable, BaseDomain {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;


    @EmbeddedId
    private SpaceEventTypePK pk;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "space",insertable=false,updatable=false)
    private Space space;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_type",insertable=false,updatable=false)
    private EventType eventType;


    public SpaceEventTypePK getPk() {
        if(pk==null)
            pk=new SpaceEventTypePK(space,eventType);
        return pk;
    }

    public void setPk(SpaceEventTypePK pk) {
        if(pk!=null){
            this.space=pk.getSpace();
            this.eventType=pk.getEventType();
        }
        this.pk = pk;
    }

    public Space getSpace() {
        return space;
    }

    public void setSpace(Space space) {
        this.space = space;
    }

    public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}


    @Override
    public Object build() {
        return null;
    }
}


