package com.eventspace.domain;

import com.eventspace.dto.CancellationPolicyDto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Auxenta on 1/27/2017.
 */
@Entity
@Table(name = "cancellation_policy")
public class CancellationPolicy implements Serializable, BaseDomain {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "refund_rate")
    private Double refundRate;

    /** The date updated. */
    @Column(name="date_updated", nullable= false)
    private Date updatedDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getRefundRate() {
        return refundRate;
    }

    public void setRefundRate(Double refundRate) {
        this.refundRate = refundRate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public CancellationPolicyDto build() {
        CancellationPolicyDto cancellationPolicyDto = new CancellationPolicyDto();
        cancellationPolicyDto.setId(id);
        cancellationPolicyDto.setName(name);
        cancellationPolicyDto.setDescription(description);
        cancellationPolicyDto.setRefundRate(refundRate);
        cancellationPolicyDto.setTimeStamp(String.valueOf(updatedDate.getTime()));
        return cancellationPolicyDto;
    }
}
