package com.eventspace.domain;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "booking_pay_later")
public class BookingPayLater implements Serializable, BaseDomain{

    @Id
    @Column(name = "booking", nullable = false, unique = true)
    private Integer booking;

    @Column(name = "expire_date")
    private Date expireDate;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = "expire")
    private Boolean expire;

    public Integer getBooking() {
        return booking;
    }

    public void setBooking(Integer booking) {
        this.booking = booking;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Boolean getExpire() {
        return expire;
    }

    public void setExpire(Boolean expire) {
        this.expire = expire;
    }

    @Override
    public Object build() {
        return null;
    }
}
