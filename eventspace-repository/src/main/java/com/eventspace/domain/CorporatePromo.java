package com.eventspace.domain;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "corporate_promo")
public class CorporatePromo implements Serializable, BaseDomain  {

    @Id
    @Column(name = "promo_details_id", nullable = false, unique = true)
    private Integer promoDetailsId;

    @Column(name = "corporate")
    private String corporate;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = "active")
    private Boolean active = true;

    public String getCorporate() {
        return corporate;
    }

    public void setCorporate(String corporate) {
        this.corporate = corporate;
    }

    public Integer getPromoDetailsId() {
        return promoDetailsId;
    }

    public void setPromoDetailsId(Integer promoDetailsId) {
        this.promoDetailsId = promoDetailsId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public Object build() {
        return null;
    }
}
