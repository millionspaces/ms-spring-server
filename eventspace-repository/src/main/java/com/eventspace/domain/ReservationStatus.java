package com.eventspace.domain;

import com.eventspace.dto.ReservationStatusDto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Auxenta on 2/14/2017.
 */
@Entity
@Table(name = "reservation_status")
public class ReservationStatus implements Serializable,BaseDomain{

    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /** The name. */
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "label", nullable = false)
    private String label;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public ReservationStatusDto build() {
        ReservationStatusDto reservationStatusDto = new ReservationStatusDto();
        reservationStatusDto.setId(id);
        reservationStatusDto.setName(name);
        reservationStatusDto.setLabel(label);
        return reservationStatusDto;
    }
}
