package com.eventspace.domain;

import com.eventspace.dto.PrioritySpaceDto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "priority_spaces")
public class PrioritySpaces implements Serializable, BaseDomain{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /** The name. */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn (name="space")
    private Space space;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Space getSpace() {
        return space;
    }

    public void setSpace(Space space) {
        this.space = space;
    }

    @Override
    public PrioritySpaceDto build() {
        PrioritySpaceDto prioritySpaceDto=new PrioritySpaceDto();
        prioritySpaceDto.setId(id);
        prioritySpaceDto.setSpace(space.getId());
        return prioritySpaceDto;
    }

}
