package com.eventspace.domain;

import com.eventspace.dto.SeatingArrangementDto;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Auxenta on 6/16/2017.
 */
@Entity
@Table(name = "space_has_seating_arrangement")
public class SpaceSeatingArrangement implements Serializable, BaseDomain {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private SpaceSeatingArrangementPk spaceSeatingArrangementPk;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "space",insertable=false,updatable=false)
    private Space space;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "seating_arrangement",insertable=false,updatable=false)
    private SeatingArrangement seatingArrangement;

    /** The participant count. */
    @Column(name="participant_count")
    private Integer participantCount;

    public SpaceSeatingArrangementPk getSpaceSeatingArrangementPk() {
        if(spaceSeatingArrangementPk==null)
            spaceSeatingArrangementPk=new SpaceSeatingArrangementPk(space,seatingArrangement);
        return spaceSeatingArrangementPk;
    }

    public void setSpaceSeatingArrangementPk(SpaceSeatingArrangementPk spaceSeatingArrangementPk) {
        if(spaceSeatingArrangementPk!=null){
            this.space=spaceSeatingArrangementPk.getSpace();
            this.seatingArrangement=spaceSeatingArrangementPk.getSeatingArrangement();
        }
        this.spaceSeatingArrangementPk = spaceSeatingArrangementPk;
    }

    public Space getSpace() {
        return space;
    }

    public void setSpace(Space space) {
        this.space = space;
    }

    public SeatingArrangement getSeatingArrangement() {
        return seatingArrangement;
    }

    public void setSeatingArrangement(SeatingArrangement seatingArrangement) {
        this.seatingArrangement = seatingArrangement;
    }

    public Integer getParticipantCount() {
        return participantCount;
    }

    public void setParticipantCount(Integer participantCount) {
        this.participantCount = participantCount;
    }

    @Override
    public SeatingArrangementDto build() {
        SeatingArrangementDto seatingArrangementDto=new SeatingArrangementDto();
        seatingArrangementDto.setId(spaceSeatingArrangementPk.getSeatingArrangement().getId());
        seatingArrangementDto.setName(spaceSeatingArrangementPk.getSeatingArrangement().getName());
        seatingArrangementDto.setIcon(spaceSeatingArrangementPk.getSeatingArrangement().getIcon());
        seatingArrangementDto.setParticipantCount(participantCount);
        seatingArrangementDto.setMobileIcon(spaceSeatingArrangementPk.getSeatingArrangement().getMobileIcon());
        return seatingArrangementDto;
    }

    public static SpaceSeatingArrangement build(Space space,SeatingArrangement seatingArrangement,SeatingArrangementDto seatingArrangementDto) {
        SpaceSeatingArrangement spaceSeatingArrangement=new SpaceSeatingArrangement();
        spaceSeatingArrangement.setSpace(space);
        spaceSeatingArrangement.setSeatingArrangement(seatingArrangement);
        spaceSeatingArrangement.setSpaceSeatingArrangementPk(new SpaceSeatingArrangementPk(space,seatingArrangement));
        spaceSeatingArrangement.setParticipantCount(seatingArrangementDto.getParticipantCount());
        return spaceSeatingArrangement;
    }
}
