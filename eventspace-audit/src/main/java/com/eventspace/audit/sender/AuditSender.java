/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.audit.sender;

import com.eventspace.audit.dto.AuditMetaData;
import com.eventspace.audit.publisher.AuditEventPublisher;
import com.eventspace.dao.SpaceDao;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
@Component
public class AuditSender {

	/** The Constant logger. */
	private static final Log LOGGER = LogFactory.getLog(AuditSender.class);

	/** The Constant ENCODE_TYPE. */
	protected static final String ENCODE_TYPE = "UTF-8";


	/** The sender email. */
	@Value("${sms.api}")
	private String apiCode;


	@Autowired
	AuditEventPublisher eventPublisher;

	@Autowired
	private SpaceDao spaceDao;


	/**
	 * Send email.
	 *
	 * @param metaData
	 *            the meta data
	 */
	@Transactional
	public void sendSms(final AuditMetaData metaData) {
		try {

			LOGGER.info(String.format("Audit sendSms -> get called with [%s]", metaData));

		} catch (Exception ex) {
			LOGGER.error("Exception occured." + ex);
		}
	}
	

}
