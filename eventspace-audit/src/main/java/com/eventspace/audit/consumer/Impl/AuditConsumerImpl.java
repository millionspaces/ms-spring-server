package com.eventspace.audit.consumer.Impl;

import com.eventspace.audit.dto.AuditMetaData;
import com.eventspace.audit.sender.AuditSender;
import com.eventspace.exception.EventspaceException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

@Component
public class AuditConsumerImpl implements MessageListener {

	/** The Constant logger. */
	private static final Log logger = LogFactory.getLog(AuditConsumerImpl.class);

	@Autowired
	private AuditSender auditSender;

	@Override
	public void onMessage(final Message message) {

		logger.info("onMessage -> Get Called");

		try {
			if (message instanceof ObjectMessage) {

				AuditMetaData auditMetaData = (AuditMetaData) ((ObjectMessage) message).getObject();

				//logger.info(
				//		String.format("onMessage -> Casted the message Successfully... [%s]", smsMetaData));
				auditSender.sendSms(auditMetaData);
				
			} else {
				logger.error("onMessage -> Received message of unknown type");
				throw new EventspaceException("Message type isn't compatible");
			}
		} catch (Exception ex) {
			logger.error("onMessage -> Exception : ", ex);
		}
	}

}
