package com.eventspace.audit.publisher.Impl;

import com.eventspace.audit.dto.AuditMetaData;
import com.eventspace.audit.publisher.AuditEventPublisher;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

@Component
public class AuditEventPublisherImpl implements AuditEventPublisher {

	/** The Constant logger. */
	private static final Log logger = LogFactory.getLog(AuditEventPublisherImpl.class);

	@Autowired
	@Qualifier("auditjmsProducerTemplate")
	private  final JmsTemplate jmsTemplate = null;

	@Override
	public void proceedAuditEvent(final AuditMetaData auditMetaData) {
		logger.info(String.format(
				"proceedAuditEvent -> get called with auditMetaData : message[%s]",
				auditMetaData.getMessage()));


			jmsTemplate.send(new MessageCreator() {
				@Override
				public Message createMessage(Session session) throws JMSException {

					ObjectMessage objMessage = session.createObjectMessage();
					objMessage.setObject(auditMetaData);

					return objMessage;
				}
			});
	}
}
